//
//  BidsVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/15/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class BidsVC: UIViewController,UIScrollViewDelegate,BidSentCellDelegate,BidRecieveCellDelegate {
  
    //MARK: - Outlets
    var isSentSelected = true
    
    //Constarints
    @IBOutlet var viewSlideLeading: NSLayoutConstraint!
    //Table
    @IBOutlet var tblRecieveList: UITableView!
    @IBOutlet var tblSentList: UITableView!

    //Label
    @IBOutlet var lblNoItem: UILabel!

    //MARK: - Variables
    var countSent = 0
    var countRecieve = 0
    var selectedIndex = 0

    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isReceiveSelected:Bool?
    //Array
    private lazy var arrRecived = [ItemListData]()
    private lazy var arrSent = [ItemListData]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = false
        dataLoaded = false
        isScrolling = false
        if !isSentSelected {
            self.lblNoItem.isHidden = true
            
            //self.tblSentList.bringSubview(toFront: tblRecieveList)
            tblRecieveList.isHidden = false
            tblSentList.isHidden = true
            
            isReceiveSelected = true
            self.viewSlideLeading.constant = self.view.frame.size.width/2
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            if arrRecived.count == 0 {
                GetRecieveBidList()
            }
        } else {
            isReceiveSelected = false
            GetSentBidList()
        }
        
        self.tblRecieveList.tableFooterView = UIView()
        self.tblSentList.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBActions
    @IBAction func RejectBid(_ sender: UIButton) {
        var items : ItemListData?
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if sender.currentTitle == "Cancel Request"{
            items = self.arrSent[sender.tag]
            self.arrSent[sender.tag].bid_status_id = 3
            
        }else{
            items = self.arrRecived[sender.tag]
            self.arrRecived[sender.tag].bid_status_id = 3
        }
        
        if let item = items , let id = item.id , let bidId = item.bid_Id {
            self.actionOnBid(itemId: String(id), bidId:  String(bidId), actionStatus:  String(3)) {(response) in
                //self.tblSentList.reloadRows(at: [IndexPath(row: sender.tag, section: 0)] , with: .automatic)\
                
                DispatchQueue.main.async {
                    if sender.currentTitle == "Cancel Request"{
                        self.tblSentList.reloadRows(at: [indexPath], with: .automatic)
                    }else{
                        self.tblRecieveList.reloadRows(at: [indexPath], with: .automatic)
                    }
                    sender.isUserInteractionEnabled = false
                    sender.setTitle("Cancelled", for: .normal)
                    
                    if let msg = response["message"] as? String{
                        self.showAlertMessage(titleStr: KMessage, messageStr:msg)
                    }
                    
                }
            }
        }
        
    }
    @IBAction func ActionAcceptBid(_ sender: UIButton) {
        let item = self.arrRecived[sender.tag]
        if let id = item.id , let bidId = item.bid_Id {
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
            self.actionOnBid(itemId: String(id), bidId:  String(bidId), actionStatus:  String(2)) {(response) in
                
                self.arrRecived[sender.tag].bid_status_id = 2
                
                DispatchQueue.main.async {
                    if sender.currentTitle == "Accept"{
                        self.tblRecieveList.reloadRows(at: [indexPath], with: .automatic)
                    }
                    
                    sender.setTitle("Accepted", for: .normal)
                    sender.isUserInteractionEnabled = false
                    
                    if let msg = response["message"] as? String{
                        self.showAlertMessage(titleStr: KMessage, messageStr:msg)
                    }
                }
            }
        }
    }
    @IBAction func BackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SentRequestsAction(_ sender: Any)
    {
        self.lblNoItem.isHidden = true
        //self.tblRecieveList.bringSubview(toFront: tblSentList)
        tblRecieveList.isHidden = true
        tblSentList.isHidden = false

        isReceiveSelected = false
        self.viewSlideLeading.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrSent.count == 0
        {
            GetSentBidList()
        }
    }
    @IBAction func RecieveRequestsAction(_ sender: Any)
    {
        self.lblNoItem.isHidden = true
        
        //self.tblSentList.bringSubview(toFront: tblRecieveList)
        tblRecieveList.isHidden = false
        tblSentList.isHidden = true
        
        isReceiveSelected = true
        self.viewSlideLeading.constant = self.view.frame.size.width/2
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrRecived.count == 0
        {
            GetRecieveBidList()
        }
    }
    
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            if isReceiveSelected == true{
            if (arrRecived.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.countRecieve = self.countRecieve+KPaginationcount
                GetRecieveBidList()
            }
            }
            else
            {
                if (arrSent.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countSent = self.countSent+KPaginationcount
                    GetSentBidList()
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    
    
    //MARK: - Other Actions
    func DeleteItem()
    {
        print(selectedIndex)
        arrSent.remove(at: selectedIndex)
        tblSentList.reloadData()
        
    }
    func PaymentRequestAction(_ sender: BidRecieveCell) {
        KCommonFunctions.PushToContrller(from: self, ToController: .PaymentRequestSeller, Data: arrRecived[sender.tag])
    }
    func CheckOutAction(_ sender: BidSentCell) {
        print(sender.tag)
        selectedIndex = sender.tag
        let obj =  arrSent[sender.tag]
        print(obj)
        if obj.categoryID==4
        {
            KCommonFunctions.PushToContrller(from: self, ToController: .PaymentRequestBuyer, Data: arrSent[sender.tag])
        }
        else
        {
            KCommonFunctions.PushToContrller(from: self, ToController: .CheckOut, Data: obj)
        }
    }
    func GetRecieveBidList()  {
        if let userID = UserDefaults.standard.getUserID()
        {
            
            let receivedCount = 0
            let urlStr = KsellerbidRecords + "\(userID)" +  "/" + "\(KPaginationcount)" +  "/" + "\(countRecieve)/"  + "received" + "/" + "\(receivedCount)"
            print(urlStr)
            dataLoaded = false
            //parm[Kcategory_id] = KServicesType
            Bids.sharedManager.GetRecieveBidsApi(url: urlStr,Target: self, completionResponse: { (Response) in
                
                print(Response.count)
                if Response.count>0
                {
                    if(self.countRecieve == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrRecived.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrRecived.insert(item, at: self.arrRecived.count)
                            }
                        }
                        self.tblRecieveList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrRecived.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrRecived.insert(item, at: self.arrRecived.count)
                            }
                        }
                        self.tblRecieveList.reloadData()
                        print(self.arrRecived)
                    }
                    print(self.countRecieve)
                    self.lblNoItem.isHidden = true
                    self.tblRecieveList.isHidden = false

                }
                else
                {
                    if(self.countRecieve != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countRecieve == 0 || self.arrRecived.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblRecieveList.isHidden = true
                    }
                }
                self.isLoading = false
                //            if self.isDeleted!
                //            {
                //                self.isDeleted = false
                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                //            }
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetRecieveBidList()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.countRecieve == 0 || self.arrRecived.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblRecieveList.isHidden = true

                        }
                    }
                    else
                    {
                        if (self.countRecieve == 0 || self.arrRecived.count == 0)
                        {
                            self.arrRecived.removeAll()
                            self.tblRecieveList.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblRecieveList.isHidden = true

                        }
                        
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        }
    }
    
    func GetSentBidList()  {
        if let userID = UserDefaults.standard.getUserID()
        {
            let sentCount = 0
            
            let urlStr = KsellerbidRecords + "\(userID)" +  "/" + "\(KPaginationcount)" +  "/" + "\(countSent)/" + "sent" + "/" + "\(sentCount)"
            print(urlStr)
            dataLoaded = false
            //parm[Kcategory_id] = KServicesType
            Bids.sharedManager.GetSentBidsApi(url: urlStr,Target: self, completionResponse: { (Response) in
                
                print(Response.count)
                if Response.count>0
                {
                    if(self.countSent == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrSent.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSent.insert(item, at: self.arrSent.count)
                            }
                        }
                        self.tblSentList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrSent.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSent.insert(item, at: self.arrSent.count)
                            }
                        }
                        self.tblSentList.reloadData()
                        print(self.arrSent)
                    }
                    print(self.countSent)
                    self.lblNoItem.isHidden = true
                    self.tblSentList.isHidden =  false
                }
                else
                {
                    if(self.countSent != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countSent == 0 || self.arrSent.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblSentList.isHidden = true
                        
                    }
                }
                self.isLoading = false
            }
            , completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.GetSentBidList()
                    })
                }
                else if statusCode == 201
                {
                    self.dataLoaded = true
                    if (self.countSent == 0 || self.arrSent.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblSentList.isHidden = true
                        
                    }
                }
                else
                {
                    if (self.countSent == 0 || self.arrSent.count == 0)
                    {
                        self.arrSent.removeAll()
                        self.tblSentList.reloadData()
                        self.lblNoItem.isHidden = false
                        self.tblSentList.isHidden = true
                    }
                    
                }
                self.isLoading = false
                
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        }
    }
    func actionOnBid(itemId: String , bidId: String , actionStatus: String ,completion: @escaping ([String:Any]) -> Void){
        
     let urlStr = "/seller/actionOnBid/" + itemId + "/" + bidId + "/" + actionStatus
      
        AddProduct.sharedManager.PostApi(url: urlStr, parameter: [:], Target: self, completionResponse: { (response) in
          completion(response)
      //   completion(response)
        }, completionnilResponse: { (Response) in
            print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
        
    }

    
}

extension BidsVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        if tableView == tblRecieveList
        {
            return arrRecived.count
        }
        else
        {
            return arrSent.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblRecieveList
        {
            return 120
        }
        else
        {
            return UITableViewAutomaticDimension
        }
       // return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        if tableView == tblRecieveList
        {
            let cell : BidRecieveCell = tableView.dequeueReusableCell(withIdentifier: "bidRecieveCell") as! BidRecieveCell
           
            cell.LoadData(dic:arrRecived[indexPath.row], type: .Book)
            cell.btnAccept.tag = indexPath.row
            cell.btnReject.tag = indexPath.row
            cell.tag = indexPath.row
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell : BidSentCell = tableView.dequeueReusableCell(withIdentifier: "bidSentCell") as! BidSentCell
            print(arrSent)
            cell.LoadData(dic:arrSent[indexPath.row], type: .Book)
            cell.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
}
extension BidsVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        UserDefaults.standard.setShowEditButton(value: false)
        UserDefaults.standard.setShowSellerInfo(value: true)
       

        if tableView == tblRecieveList
        {
            let data = arrRecived[indexPath.row]
        
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }
        else
        {
            let data = arrSent[indexPath.row]
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }
    }
}

