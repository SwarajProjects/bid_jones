//
//  AlertTableCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 7/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class AlertTableCell: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func LoadData(Title:String,ImageStr:String)
    {
        lblTitle.text = Title
        imgView.image = (UIImage(named: ImageStr)!)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
