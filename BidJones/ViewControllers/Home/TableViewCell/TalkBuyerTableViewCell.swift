//
//  TalkBuyerTableViewCell.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import SDWebImage

class TalkBuyerTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    //@IBOutlet var lblBidType: UILabel!
   
    @IBOutlet weak var img_name: UIImageView!
    
    @IBOutlet weak var lbl_userName: UILabel!
    
    @IBOutlet weak var lbl_itemName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func LoadData(dic: TalkData)
    {
       // lblTitle.text = dic.username!
       // lblPrice.text = dic.
        lbl_userName.text = dic.username!
        lbl_itemName.text = dic.itemName!
        
        let imageString = dic.userImage!
        
        let url = URL(string:imageString)
        
        img_name.sd_setImage(with: url!)
        
    }
    

}
