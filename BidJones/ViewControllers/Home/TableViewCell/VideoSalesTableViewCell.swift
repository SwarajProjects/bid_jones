//
//  VideoSalesTableViewCell.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import SDWebImage

class VideoSalesTableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var img_Sale: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func cellData(arrayMusic : [MySaleData], indexPath : IndexPath) {
        
        lbl_name.text = arrayMusic[indexPath.row].titleName!
        
        let imageString = arrayMusic[indexPath.row].image!
        
        let url = URL(string:imageString)
        
        img_Sale.sd_setImage(with: url!)
        
        //   btn_play.addTarget(self, action: #selector(songPlay(songName :)), for: .touchUpInside)
        
    }
}
