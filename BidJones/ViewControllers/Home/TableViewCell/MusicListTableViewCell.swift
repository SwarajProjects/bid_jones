//
//  MusicListTableViewCell.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/27/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import AVKit

protocol MusicListTableViewCellDelegate: class{
    func play_Action(_ sender: MusicListTableViewCell)
}


class MusicListTableViewCell: UITableViewCell {

    
   
    
    
    @IBOutlet weak var btn_play: UIButton!
    @IBOutlet weak var lbl_songTitle: UILabel!
    @IBOutlet weak var img_song: UIImageView!
    
    weak var delegate: MusicListTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
    
    func cellData(arrayMusic : [MusicListData], indexPath : IndexPath) {
        print("array count: \(arrayMusic.count)")
        lbl_songTitle.text = arrayMusic[indexPath.row].songTilte!
        
       if let imageString = arrayMusic[indexPath.row].imageSong as? String
       {
        let url = URL(string:imageString)
        img_song.image = UIImage.init(named:"logo")

        img_song.sd_setImage(with: url!)
      }
        //sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "back_arrow"))
        
        //btn_play.addTarget(self, action: #selector(songPlay(songName :)), for: .touchUpInside)
        
    }
    
//    @objc func songPlay(songName : String){
//
//
//
//    }
    
    @IBAction func play_Action(_ sender: Any)
    {
        delegate?.play_Action(self)
    }
    
    
}
