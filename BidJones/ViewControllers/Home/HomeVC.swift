//
//  HomeVC.swift
//  BidJones
//
//  Created by Harpreet Singh on 20/03/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

//var sideBarOpen = false

class HomeVC: BaseViewController {
    
    //MARK:- Variables
    
    let array  = ["Search Item", "Search Trade", "Search Free Item"]
    
    let arrayImages = [UIImage(named:"search"), UIImage(named:"Trade") ,UIImage(named:"FreeImage")]
    //MARK:- Outlets
    @IBOutlet weak var tblView: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
      
       drawerType = DrawerType.Home
        
        SDPhotosHelper.createAlbum(withTitle: KAlbumName) { (success, error) in
            if success
            {
                print("Created album : \(KAlbumName)")
            }
            else
            {
                if let error = error
                {
                    print("Error in creating album : \(error.localizedDescription)")
                }
            }
        }
//        DispatchQueue.global(qos: .background).async {
//            if let url = URL(string: "https://bidjonesbucketprivate.s3.us-east-2.amazonaws.com/bidjones/anvjot/1533897585.mp4"),
//                let urlData = NSData(contentsOf: url)
//            {
//                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
//                let filePath="\(documentsPath)/tempFile.mp4"
//                let url = URL(string: filePath)
//                print(url as Any)
//                DispatchQueue.main.async {
//                    urlData.write(toFile: filePath, atomically: true)
//                    SDPhotosHelper.addNewVideo(withFileUrl: url!, inAlbum: KAlbumName, onSuccess: { (str) in
//                        print(str)
//                    }) { (error) in
//                        print(error.debugDescription)
//                    }
//                }
//            }
//            else
//            {
//                print("Fail")
//            }
//        }

//        let url = URL(string: "https://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_20mb.mp4")
//
//        DispatchQueue.global(qos: .background).async {
//            if let url = URL(string: urlString),
//                let urlData = NSData(contentsOf: url) {
//                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
//                let filePath="\(documentsPath)/tempFile.mp4"
//
//        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//        let fileName = "\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
//        let fileURL = documentsDirectory.appendingPathComponent(fileName)
//        do {
//            let weatherData = try NSData(contentsOf: url!, options: NSData.ReadingOptions())
//            print(weatherData)
//            do {
//                // writes the image data to disk
//                try weatherData.write(to: fileURL)
//                SDPhotosHelper.addNewVideo(withFileUrl: fileURL, inAlbum: KAlbumName, onSuccess: { (str) in
//                    print(str)
//                }) { (error) in
//                    print(error.debugDescription)
//                }
//                //print("file saved")
//            } catch
//            {
//                //print("error saving file:", error)
//            }
//        } catch {
//            print(error)
//        }
//        if let data = UIImageJPEGRepresentation(pickedImage, 1.0),
//            !FileManager.default.fileExists(atPath: fileURL.path)
//        {
//            do {
//                // writes the image data to disk
//                try weatherData.write(to: fileURL)
//                if let Data = ImagesData(ImgUrl: fileURL, Image: pickedImage)
//                {
//                    arrImages.insert(Data, at: 0)
//                }
//                //print(arrImages)
//                let indexPath = IndexPath(row: 0, section: 0)
//                self.CollectionView_Images.scrollToItem(at: indexPath, at: .left, animated: true)
//                CollectionView_Images.reloadData()
//                //print("file saved")
//            } catch
//            {
//                //print("error saving file:", error)
//            }
//        }

//        SDPhotosHelper.addNewVideo(withFileUrl: url!, inAlbum: KAlbumName, onSuccess: { (str) in
//            print(str)
//        }) { (error) in
//            print(error.debugDescription)
//        }
        addSlideMenuButton()
        self.tblView.tableFooterView = UIView()
        
        let notificationButton = SSBadgeButton()
        let notifiImg:UIImage = UIImage(named: "barAlerts")!

        notificationButton.frame = CGRect(x: 0, y: 0, width: notifiImg.size.width, height: notifiImg.size.height)
        notificationButton.setImage(notifiImg.withRenderingMode(.alwaysTemplate), for: .normal)
       // notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 18, left: 0, bottom: 0, right: 12)
        notificationButton.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        notificationButton.tintColor = .black
       // notificationButton.badge = ""
     
          let radioiImg:UIImage = #imageLiteral(resourceName: "radio_icon")
        
        let radioView:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: radioiImg.size.width, height: radioiImg.size.height))
        radioView.addTarget(self, action:#selector(self.didTapRadioButton), for: .touchUpInside)
        radioView.setImage(radioiImg, for: .normal)
         let radioButton:UIBarButtonItem = UIBarButtonItem(customView: radioView)
        let NotifyButton = UIBarButtonItem(customView: notificationButton)
        
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0


        navigationItem.rightBarButtonItems = [NotifyButton,fixedSpace,radioButton]

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        //RefreshTokken()

        
    
    }
    @objc func didTapRadioButton(sender: AnyObject){
        print("Radio button pressed")
       // showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
        
        let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "MusicListVC") as! MusicListVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tblView.isHidden = true
        //MusicListVC.isMusicList == true ||
        
//        if   AlertsVC.isAlertVC == true {
//
//        self.onSlideMenuButtonPressed(btnShowMenu)
//
//        }
        self.title = KBIDJONES
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                self.onSlideMenuButtonPressed(btnShowMenu)
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down"   )
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
  //@IBAction func ActionTrade(_ sender: UIButton) {
    //KCommonFunctions.PushToContrller(from: self, ToController: .Trade, Data: nil)

 // }
    @IBAction func ActionSearch(_ sender: Any) {
        //      KCommonFunctions.PushToContrller(from: self, ToController: .Search, Data: nil)
        
        //  self.showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
        self.tblView.isHidden = false
    }
    
  //  @IBAction func Free_Action(_ sender: Any) {
        
       // KCommonFunctions.PushToContrller(from: self, ToController: .Free, Data: nil)
        
   // }
    //MARK: - Other Actions
    
    @objc func buttonClicked()  {
              //  self.onSlideMenuButtonPressed(btnShowMenu)

    KCommonFunctions.PushToContrller(from: self, ToController: .Alerts, Data: nil)
        //self.showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
    }
}
extension  HomeVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? HomeTableViewCell
        cell?.selectionStyle = .none
        cell?.lblName.text = array[indexPath.row]
        cell?.imgView.image = arrayImages[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{

        KCommonFunctions.PushToContrller(from: self, ToController: .Search, Data: nil)
            
        }else if indexPath.row == 1{
            KCommonFunctions.PushToContrller(from: self, ToController: .Trade, Data: nil)
        }else if indexPath.row == 2{
            KCommonFunctions.PushToContrller(from: self, ToController: .Free, Data: nil)
        }
        
    }
}
