//
//  ChangePasswordVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 9/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController, UITextFieldDelegate {
 
    //MARK: - Outlets
    @IBOutlet var txtFld_confirmPassword: UITextField!
    @IBOutlet var txtFld_oldPassword: UITextField!
    @IBOutlet var txtFld_newPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
         self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- IBOutlets
    
    @IBAction func UpdateAction(_ sender: Any)
    {
        self.view.endEditing(true)
        
        setAPI()
        
    }
    
    func setAPI() {
        
        do {
            try Validations()
            let urlStr = KuserchangePassword
            var parm = [String : Any]()
            parm["current_password"] = txtFld_oldPassword.text!
            parm["new_password"] = txtFld_newPassword.text!
            parm["new_password_confirm"]  = txtFld_confirmPassword.text!
            Auth.sharedmanagerAuth.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print(Response)
                if let msg =  Response[Kmessage] as? String
                    
                {
                    //UserDefaults.setPassword(txtFld_confirmPassword.text!)
                   // UserDefaults.setValue(self.txtFld_confirmPassword.text!, forKey: "password")
                    UserDefaults.standard.setPassword(value:self.txtFld_newPassword.text!)
                    
                    self.AlertWithNavigatonPurpose(message: msg, navigationType: .pop, ViewController: .none, rootViewController: .none,Data:nil)
                }
                // UserDefaults.standard.setLoggedIn(value: true)
                
            }, completionnilResponse: { (Response) in
                print(Response)
                let statusCode = Response[Kstatus] as! Int
                print(statusCode)
                if statusCode == 400
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr: "New Password cannot be same as your current password")
                    //KInvalidemailaddressorpassword
                    //New Password cannot be same as your current password
                }
                else if statusCode == 500 {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.setAPI()
                    })
                }
                    
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
                
                
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        } catch let error {
            switch  error {
            case ValidationError.emptyOldPassword:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenteroldpassword)
            case ValidationError.wrongOldPassword:
                self.showAlertMessage(titleStr: KMessage, messageStr: "Incorrect old password")
            case ValidationError.passwordMinChar:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPasswordshouldbeof)
            case ValidationError.passwordSufficientComplexity:
                self.showAlertMessage(titleStr: KMessage, messageStr:"Please enter at least one numeric , one capital letter and one special character" )
            case ValidationError.emptyNewPassword:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenternewpassword)
            case ValidationError.emptyConfirmPassword:
                self.showAlertMessage(titleStr: KMessage, messageStr: KPleaseenterconfirmpassword)
            case ValidationError.emptyNewPasswordDoesntMatch:
                self.showAlertMessage(titleStr: KMessage, messageStr: "New password and confirm password do not match")
                
            default: self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
            }
        }
        
    }
    
    //MARK:- BACK BUTTON ACTION
    
    @IBAction func Back_ButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    //MARK: - Textfield delegate

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
       
        if textField == txtFld_newPassword
        {
            return textField.RestrictMaxCharacter(maxCount: 20, range: range, string: string)
        }
        
        if textField == txtFld_confirmPassword
        {
            return textField.RestrictMaxCharacter(maxCount: 20, range: range, string: string)
        }
        return true
    }

    
    //MARK: - Other functions
    
    func Validations() throws
    {
    
        guard let oldPasswword = txtFld_oldPassword.text,  !oldPasswword.isEmpty, !oldPasswword.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyOldPassword
        }
        let passwordOld = UserDefaults.standard.getPassword()
        print("your old password is :\(passwordOld)  and written yet \(oldPasswword)")
        if oldPasswword != passwordOld
         {
        throw ValidationError.wrongOldPassword
         }
        guard let NewPassword  = txtFld_newPassword.text, !NewPassword.isEmpty, !NewPassword.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyNewPassword
        }
        if(!NewPassword.passwordMinLength)
        {
            throw ValidationError.passwordMinChar
        }
        if(!NewPassword.checkTextSufficientComplexity())
        {
            throw ValidationError.passwordSufficientComplexity
        }
        guard let ConfirmPassword  = txtFld_confirmPassword.text, !ConfirmPassword.isEmpty, !ConfirmPassword.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyConfirmPassword
                //.emptyNewPassword
        }
        if NewPassword != ConfirmPassword
        {
            throw ValidationError.emptyNewPasswordDoesntMatch
        }
    }
   
}
