//
//  ServiceTableViewCell.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/25/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_description: UILabel!
    
    @IBOutlet weak var img_Service: UIImageView!
    
    @IBOutlet weak var lbl_ServiceName: UILabel!
    
    @IBOutlet weak var btn_PaymentRequest: CustomButton!
    
    @IBOutlet weak var lbl_Date: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
