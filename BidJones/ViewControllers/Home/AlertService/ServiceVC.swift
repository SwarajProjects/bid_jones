//
//  ServiceVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/25/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import SDWebImage

class ServiceVC: UIViewController  , UIScrollViewDelegate{

    @IBOutlet weak var lbl_no_data_found: UILabel!
    var count = 0
    var KPaginationcount = 10
    
    
    var indexDelete:Int?
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isDeleted:Bool?
    
    var arrayService =  [ServiceModel]()
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        tableView.separatorColor = nil
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.separatorColor = UIColor.clear
        
        
       GetList()

        self.navigationItem.setHidesBackButton(true, animated: false)
        
        
    }
    
    @IBAction func Back_buttonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        
    }
    func GetList() {
        
        
        var parm = [String : Any]()
        
        parm["offset"] = count
        parm["limit"]  = KPaginationcount
        
        
        
        let url = "/servicesRequestReceived"
        
        Service.sharedManager.PostApi(url: url, parameter: parm, Target: self, completionResponse: { response in
            
            
            print("enter and check here : \(response)")
            
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            
//            if response != nil
//                //let msg =  response[Kmessage]
//            {
                //         m       self.arrBooks.remove(at: self.indexDelete!)
                //                self.tblBooksList.reloadData()
                //                self.isDeleted = true
                //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
           //     UserDefaults.standard.setUserType(value: true)
                //self.showToast(message: msg as! String)
                //  self.AddMoreAction(AnyObject.self)
                 DispatchQueue.main.async {
                if response.count>0
                {
                   
                     
                   // MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    if(self.count == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in response
                        {
                            let oldIds = self.arrayService.map { $0.id } as? [Int]
                            print("This is :\(oldIds as Any)")
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrayService.insert(item, at: self.arrayService.count)
                            }
                        }
                        
                        self.tableView.reloadData()
                    }
                    else
                    {
                       // MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        for item in response
                        {
                            let oldIds = self.arrayService.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrayService.insert(item, at: self.arrayService.count)
                            }
                        }
                        self.tableView.reloadData()
                        print(self.arrayService)
                    }
                    print(self.count)
                  //  self.lblNoItem.isHidden = true
                    self.tableView.isHidden = false
                    
                    }
                    else
                    {
                        if(self.count != 0)
                        {
                            print("abcdefgh")
                            self.dataLoaded = true
                        }
                        if (self.count == 0 || self.arrayService.count == 0)
                        {
                            self.lbl_no_data_found.isHidden = false
                           //self.lblNoItem.isHidden = false
                            self.tableView.isHidden = true
                        }
                    }
                    self.isLoading = false
                    //            if self.isDeleted!
                    //            {
                    //                self.isDeleted = false
                    //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                    //            }
            }
            }
           
            , completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        
                        self.GetList()
                    })
                }
                else if statusCode == 201
                {
                    self.dataLoaded = true
                    if (self.count == 0 || self.arrayService.count == 0)
                    {
                       // self.lblNoItem.isHidden = false
                        self.tableView.isHidden = true
                        
                    }
                }
                else
                {
                    if (self.count == 0 || self.arrayService.count == 0)
                    {
                        self.arrayService.removeAll()
                        self.tableView.reloadData()
                     //   self.lblNoItem.isHidden = false
                        self.tableView.isHidden = true
                        
                    }
                    
                }
                self.isLoading = false
                
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        }
        
        
        
   
    
    
    
    
    //MARK: - UISCrollview delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            if (arrayService.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.count = self.count+KPaginationcount
                GetList()
            }
//            else if isDeleted!
//            {
//                isDeleted = false
//                print("222222222222")
//                isScrolling = true
//                print("reach bottom")
//                isLoading = true
//                GetList()
//            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    


}

extension ServiceVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
}

extension ServiceVC : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayService.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ServiceTableViewCell
        
        
        let imgUrl = arrayService[indexPath.row].ImageUrl as! String
        
        let url = URL(string: imgUrl)
       
            
            cell.img_Service.sd_setImage(with: url!, placeholderImage: #imageLiteral(resourceName: "back_arrow"))
       
       
        
        
        cell.lbl_Date.text = arrayService[indexPath.row].bidDate!
        
        cell.lbl_ServiceName.text = arrayService[indexPath.row].title!
        
        cell.lbl_description.text = arrayService[indexPath.row].descrption!
    
        
      //  func tableView(<#T##tableView: UITableView##UITableView#>, heightForRowAt: <#T##IndexPath#>)
        
        
          cell.selectionStyle = .none
        
        
        
        
    return cell
        
    }
    
    
}
