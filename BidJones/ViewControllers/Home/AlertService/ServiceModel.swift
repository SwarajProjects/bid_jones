//
//  ServiceModel.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/25/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct ServiceModel :Codable
{
    var id:Int?
    
    var title:String?
    var ImageUrl:String?
    var date:String?
    var bidDate:String?
    
    var descrption:String?
    
    
    
    init?(dict:[String:Any]) {
        print(dict)
        guard let ID = dict[Kid],let Title = dict[Ktitle] else
        {
            return nil
        }
        
        
        
        
        if let Date = dict["created_at"] as? String
        {
            date = "\(Date)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let Date = dateFormatter.date(from: self.date!)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let strDate = dateFormatter.string(from: Date!)
            date = strDate
            print(date as Any)
        }
        if let Date = dict["updated_at"] as? String
        {
            bidDate = "\(Date)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let Date = dateFormatter.date(from: self.bidDate!)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let strDate = dateFormatter.string(from: Date!)
            bidDate = strDate
            print(date as Any)
        }
        if let Descrption = dict["description"]
        {
            descrption = "\(Descrption)"
            print(descrption as Any)
        }
        
        id = ID as? Int
        title = Title as? String
        
        if let meta = dict["meta"] as? [String:Any]
        {
            if let arrImages = meta[Kimages] as? [String]
            {
                ImageUrl = arrImages[0]
            }
        }
        else
        {
            if let image  = dict["imgName"] as? String
            {
                ImageUrl = (image as! String)
            }
        }
    }
    
}
