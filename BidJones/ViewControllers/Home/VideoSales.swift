//
//  VideoSales.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class VideoSales: UIViewController {

    
    //MARK:- VARIABLES
    var saleArray = [MySaleData]()
    
   
    //MARK:- ALL OUTLETS
    @IBOutlet weak var lbl_no_data_found: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- CLASS OVERRIDE FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        getDataSaleList(genreId : 0)
        // Do any additional setup after loading the view.
        
        
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.separatorColor = nil
        tableView.separatorColor = UIColor.clear
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- OTHER FUNCTIONS
    func getDataSaleList(genreId : Int) {
        
        let id = UserDefaults.standard
        
        let loginUserID = id.getUserID()
        
        
        print("Login ID : \(loginUserID!)")
        
        let url = "/itemHistory"
        
        let param = ["user_id" : loginUserID!,"user_type": "seller", "category_id" : 6, "limit" : 10 , "offset" : 0] as [String : Any]
        
        
        MySale.sharedManager.PostApi(url: url, parameter: param, Target: self, completionResponse: { (response) in
            
            
            print("here you reached at your destination ")
            print("you get here : \(response)")
            
            self.saleArray = response
            
            
            if self.saleArray.count == 0 {
                
                
                self.lbl_no_data_found.isHidden = false
                
                
            }
            //            for each in self.saleArray {
            //
            //                self.array.append(each.titleName!)
            //
            //
            //            }
            print("your array data  : \(self.saleArray)")
            
            self.tableView.reloadData()
            print("your count : \(self.saleArray.count)")
            
        } , completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    //   self.GetRecieveBidList()
                    self.getDataSaleList(genreId : 0)
                })
            }
            else if statusCode == 201
            {
                
            }
            
            
            
            
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        
        
        
        
    }
    
    
    // MARK:- ACTION BUTTON
    
    @IBAction func Back_ButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    

    
}

//MARK:- TABLE VIEW DELEGATE FUNCTIONS
extension VideoSales : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
}

extension VideoSales : UITableViewDataSource  {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return saleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! VideoSalesTableViewCell
        
        cell.cellData(arrayMusic : saleArray, indexPath : indexPath)
        
        cell.backgroundColor = UIColor.clear
        cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
          cell.selectionStyle = .none
        
        return cell
    }
    
    
    
    
    
}



