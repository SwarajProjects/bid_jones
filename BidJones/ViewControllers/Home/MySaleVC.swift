//
//  MySaleVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/1/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class MySaleVC: UIViewController {

    
    // MARK:- ALL OUTLETS
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbl_Genre: UILabel!
    @IBOutlet weak var txtfield_search: UITextField!
    @IBOutlet weak var lbl_no_data_found: UILabel!
    @IBOutlet weak var view_forGenre: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- ALL CLASS VARIABLES
    var genreid                  :                  Int?
    var array                    =                  [String]()
    var saleArray                =                  [MySaleData]()
    var searchActive             =                  false
    private var arrPicker        =                  [MySaleGenreData]()
    var arrGenre                 =                  [MySaleGenreData]()
    var GenreType                :                  String?
    var isGenrePicker            =                  true
    enum PickerType: Int {
    case GenrePicker = 1

        init() {
            self = .GenrePicker
        }
    }
    
    private var pickerType: PickerType?
    @IBOutlet weak var pickerView: UIPickerView!
    var filtered:[String] = []
   
    //MARK:- CLASS OVERRIDE FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtfield_search.delegate = self
        getDataMusicList(genreId: 0)
        filtered.removeAll()
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.hideKeyboardWhenTappedAround()
        txtfield_search.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.separatorColor = nil
        tableView.separatorColor = UIColor.clear
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDataMusicList(genreId : Int) {
        
        let id = UserDefaults.standard
        let loginUserID = id.getUserID()
        let url = "/itemHistory"
        let param = ["user_id" : loginUserID!,"user_type": "seller", "category_id" : genreId, "limit" : 10 , "offset" : 0] as [String : Any]
        
        print("gereid",genreId)
        MySale.sharedManager.PostApi(url: url, parameter: param, Target: self, completionResponse: { (response) in
            
            self.GetGener()

            print("here you reached at your destination ")
            print("you get here : \(response)")
        
            self.saleArray = response
            
            if self.saleArray.count == 0 {
                
                
                self.lbl_no_data_found.isHidden = false
                
                self.tableView.reloadData()
            }
            
            
            if self.saleArray.count > 0 {
            
                self.lbl_no_data_found.isHidden = true
                
                
            for each in self.saleArray {
                
                self.array.append(each.titleName!)
                
                
            }
            print("your array data  : \(self.saleArray)")
            self.tableView.reloadData()
            print("your count : \(self.saleArray.count)")
                
            }
            else {
                
                
                
            }
            
        } , completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    //   self.GetRecieveBidList()
                    self.getDataMusicList(genreId: 0)
                })
            }
            else if statusCode == 201
            {
                
            }
            
            if self.saleArray.count == 0 {
                
                
                self.lbl_no_data_found.isHidden = false
                
                
            }
            
            
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        
        
        
        
    }
    
    //MARK:- IB ACTION
    
    
    @IBAction func Back_ButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func getGenreData(_ sender: Any) {
        
        view_forGenre.isHidden = false
        
        if(arrGenre.count>0)
        {
            isGenrePicker = true
            pickerType = PickerType(rawValue: 1)
            //self.arrPicker = self.arrGenre
            
//            let newAllValue = ["id"  : 0 , "name" : "Genre"] as [String : Any]
//
//
//            let  dict = MySaleGenreData(dict: newAllValue)
//
//
//
//
//            self.arrPicker.insert(dict, at: 0)
//
//            print(arrPicker)
//            self.pickerView.reloadAllComponents()
//            pickerView.selectRow(0, inComponent: 0, animated: false)
          
        }
        
        
    }
    
    
    @IBAction func btn_DoneAction(_ sender: Any) {
        
        view_forGenre.isHidden = true

        let row = pickerView.selectedRow(inComponent: 0)
        
        saleArray.removeAll()
             array.removeAll()
       
        let dic = arrPicker[row] 
            let title = dic.name
            let id = dic.id
            //let ID:String = String(id)
//            strGenreID = ID
//            txtFld_Genre.text = title
       
        getDataMusicList(genreId : id!)
        lbl_Genre.text = title
    }
    
    @IBAction func btn_CancelAction(_ sender: Any) {
        
        view_forGenre.isHidden = true
       
        saleArray.removeAll()
        array.removeAll()
        
        getDataMusicList(genreId : 0)
         lbl_Genre.text = "Genre"
        
        txtfield_search.text = ""
    }
    
    
    
    //MARK:- GET GENER DATA
    func GetGener()  {
        let urlStr = "/seller/listcategories"
            //KGenerApi + String(KMusicType)
       MySale.sharedManager.GetApi(url: urlStr,Target: self, completionResponse: { (response) in
        self.arrGenre.removeAll()
        self.pickerView.reloadAllComponents()
            self.arrGenre = response
        self.arrPicker = self.arrGenre
        
        let newAllValue = ["id"  : 0 , "name" : "All"] as [String : Any]
        
        
        let  dict = MySaleGenreData(dict: newAllValue)
        
        
        
        
        self.arrPicker.insert(dict, at: 0)
        
        print(self.arrPicker)
        self.pickerView.reloadAllComponents()
        self.pickerView.selectRow(0, inComponent: 0, animated: false)
        
        self.pickerView.reloadAllComponents()
            print(response)
            
            print("here your arrGener Data : \(self.arrGenre)")
            //print(BidType)
            //print(maxImages)
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    
                    self.GetGener()
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    
    
    
   
    
    
}

extension MySaleVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchActive = true;
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchActive = false;
    }
    
    
    
    
    
    
    @objc func textFieldDidChange(textField: UITextField) {
        if textField ==  txtfield_search {
            
            
            if (txtfield_search.text?.isEmpty)! {
                searchActive = false;
                filtered.removeAll()
                array.removeAll()
                print("your data is :\(filtered)")
                
                getDataMusicList(genreId: 0)
                
                
            }
            else {
                searchActive = true;
            filtered = array.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: txtfield_search.text!, options: NSString.CompareOptions.caseInsensitive)
                //rangeOfCharacter(from: searchText, options: NSString.CompareOptions.caseInsensitive)
                //    let range = tmp.rangeOfString(searchText, options: NSString.CompareOptions.CaseInsensitiveSearch)
                return range.location != NSNotFound
            })
                
            }
            
            if(filtered.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.tableView.reloadData()
            
            
        }
        
    }
    
    
    
    
    
    
    
    
    
}



extension MySaleVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    
}


extension MySaleVC : UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if(searchActive) {
            print("here is :\(filtered.count)")
            print("here is array filtered : \(filtered)")
            return filtered.count
        }
        
        
        return saleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MySaleTableViewCell
        
        
        if(searchActive){
            
            //            var titleArray = [String]()
            //
            //            for each in musicArray {
            //
            //
            //
            //                titleArray.append(each.songTilte!)
            //
            //              //  if filtered[indexPath.row] ==
            //
            //            }
            
            print("here is my array data : \(array)")
            print("here is my saleArray : \(saleArray)")
             if saleArray.count > 0 {
            if array.contains(filtered[indexPath.row]) {
                //filtered[indexPath.row].contains(titleArray) {
                
                print("your selected Data : \(filtered[indexPath.row])")
                
                let index =   array.index(of: filtered[indexPath.row])
                
                print("here your no. is : \(index)")
                let count = index as! Int
                let myCount = IndexPath(row: count, section: 0)
                
                
                cell.cellData(arrayMusic: saleArray, indexPath: myCount)
                
            }
            
            
            
        }
        }
        else {
            
            cell.cellData(arrayMusic: saleArray, indexPath : indexPath)
        }
        
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.0
    }
    
    
    
}

extension MySaleVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //print(arrPicker.count)
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("you picker whole array data : \(arrPicker)")
        
        let dic = arrPicker[row]
       let title = (dic ).name
        self.genreid = 0
        self.GenreType = ""
       // lbl_Genre.text = dic.name
        self.genreid = dic.id!
        
        print("picker array count : \(arrPicker)")
        print("your row is : \(arrPicker[row])")
        print("Special array id : \(arrPicker[row].id!) and nameType : \(arrPicker[row].name!)")
        print("here you get : \(dic.id!)")
        print("met with id : \(self.genreid)")
            //Int((dic ).id!)
        
        self.GenreType = dic.name!
        
        
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        print("you clicked here \(row)")
        
       // let dic = arrPicker[row]
       // genreid = (dic as! GenreData).id
        
        
        
    }
    
}

