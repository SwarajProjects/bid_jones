//
//  HelpVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 9/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class HelpVC: UIViewController,UITextViewDelegate {
    //MARK: - Outlets
    @IBOutlet var textView: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = "Message"
        textView.textColor = UIColor.lightGray
        self.textView.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)

        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UITextView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message"
            textView.textColor = UIColor.lightGray
        }
    }
    //MARK: - Other functions
    func Validations() throws
    {
        if let textMsg = textView.text,  !textMsg.isEmpty, !textMsg.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        {
            if textMsg == "Message"
            {
                if textView.textColor == UIColor.lightGray
                {
                    throw ValidationError.emptyMessage
                }
            }
        }
        else
        {
            throw ValidationError.emptyMessage
        }
    }
    
    //MARK: - IBOutlets
    @IBAction func SendMessageAction(_ sender: Any) {
        self.view.endEditing(true)
        do {
            try Validations()
            let urlStr = Kuserhelp
            var parm = [String : Any]()
            parm[Kmessage] = textView.text!
            print(parm)
            Help.sharedmanager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print(Response)
                 if let msg =  Response[Kmessage] as? String
                    
                {
                    self.AlertWithNavigatonPurpose(message: msg, navigationType: .pop, ViewController: .none, rootViewController: .none,Data:nil)
                 }
                
            }, completionnilResponse: { (Response) in
                print(Response )
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 400
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:KEmailaddressdoesnotexist)
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
            
        } catch let error {
            switch  error {
            case ValidationError.emptyMessage:
                self.showAlertMessage(titleStr: KMessage, messageStr:"Please enter message")
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
            }
        }
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
