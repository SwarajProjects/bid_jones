//
//  VerifyOTPVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/12/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

protocol CustomStripeDelegate {
    func StripeAccountCreated()
    func StripeAccountNotCreated()

}

class VerifyOTPVC: UIViewController,UITextFieldDelegate,CustomStripeDelegate {
    func StripeAccountCreated() {
      stripeAccountCreated = true
    }
    func StripeAccountNotCreated() {
        stripeAccountCreated = false
    }
    
    //MARK: - UIButton Outlets
    @IBOutlet var btnResend: UIButton!
    //MARK: - Label Outlets
    //MARK: - TextField Outlets
    @IBOutlet var txtFld_OTP: UITextField!
    
    @IBOutlet weak var lblEmail: UILabel!
    //MARK: - Variable
    var user_ID:Int?
    var timer: Timer?
    var value = 900
    var redirect_to_Stripe:Int!
    var registerData:RegisterData?
    var is_Account_Check:Int?
    var stripeAccountCreated:Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.schedule()
         NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
//        NotificationCenter.default.addObserver(self, selector: Selector("willEnterForeground:"), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        if let email = RegisterData.sharedInstance?.email{
        self.lblEmail.text = "Please check your email \(email) for OTP."
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(stripeAccountCreated == true)
        {
//              self.AlertWithNavigatonPurpose(message: KUserregisteredsuccessfully, navigationType: .root, ViewController: .none, rootViewController: .HomeNavigation,Data:nil)
            
              self.AlertWithNavigatonPurpose(message: KUserregisteredsuccessfully, navigationType: .root, ViewController: .none, rootViewController: .LoginNavigation,Data:nil)
            
        }
        else if(stripeAccountCreated == false)
        {
            KCommonFunctions.popTocontroller(from: self)
        }
        if(Global.sharedInstance.isFromSafari){
            //KCommonFunctions.SetRootViewController(rootVC:.LoginNavigation)
           // KCommonFunctions.PresentTocontroller(from: self, ToController: .Login)
           

            
           
           // KCommonFunctions.PushToContrller(from: self, ToController: .Login, Data: nil)
        }
    }
    @objc func willEnterForeground() {
        // do stuff
        print("yes enter")
        if(Global.sharedInstance.isFromSafari){
            Global.sharedInstance.isFromSafari = false
            KCommonFunctions.SetRootViewController(rootVC:.LoginNavigation)
            // KCommonFunctions.PresentTocontroller(from: self, ToController: .Login)
            
            
            
            
          
        }
    }
    deinit {

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
//
//    func willEnterForeground(notification: NSNotification!) {
//
//        print("yes enter")
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- IBActions
    @IBAction func ActionBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ActionResend(_ sender: Any)
    {
        if !(btnResend.currentTitle?.contains("Remaining"))!
        {
        self.view.endEditing(true)
        guard let email = RegisterData.sharedInstance?.email, let username = RegisterData.sharedInstance?.username   else {
            return
        }
        let urlStr = KverifyUser + "/" + "\(email)" + "/" + "\(username)" + "resendotp"
        Auth.sharedmanagerAuth.GetApi(url: urlStr,Target: self, completionResponse: { (Response) in
            print(Response)
                if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                    self.schedule()
                }
        }, completionnilResponse: { (Response) in
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 400
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:KEmailaddressalreadyexist)
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
        }
    }
    @IBAction func ActionVerify(_ sender: Any)
    {
      
        self.view.endEditing(true)
        do {
            try Validations()
            self.timer?.invalidate()
            value = 900
            btnResend.setTitle("Resend OTP", for: .normal)
            let urlStr = KverifyUser
            var parm = [String : Any]()
            if let email = RegisterData.sharedInstance?.email
            {
            parm[Kemail] = email
            }
            parm[Kotp]  = txtFld_OTP.text!
            Auth.sharedmanagerAuth.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print("gagan",Response)
                    self.Register()
            }, completionnilResponse: { (Response) in
                    if let msg =  Response[Kmessage]
                    {
                        self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                    }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        } catch let error {
            switch  error {
            case ValidationError.emptyOTPAddress:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterOTP)
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterOTP)
            }
        }
    }
    //MARK: - TextField Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
        if textField == txtFld_OTP
        {
            if textField.RestrictMaxCharacter(maxCount: 6, range: range, string: string)
            {
                let regex = try! NSRegularExpression(pattern: "[A-Za-z0-9^]*", options: [])
                let range = regex.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.count))
                return range.length == string.count
            }
            return false
        }
        return true
    }
    
    //MARK: - Other functions
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func Register()
    {
    self.view.endEditing(true)
    let urlStr = KRegisterApi
    var parm = [String : Any]()
    parm[Kfirst_name]    = RegisterData.sharedInstance?.firstName
    parm[Klast_name]     = RegisterData.sharedInstance?.lastName
    parm[Kemail]         = RegisterData.sharedInstance?.email
    parm[Kpassword]      = RegisterData.sharedInstance?.password
    parm[Kusername]      = RegisterData.sharedInstance?.username
    parm[Kimage]         = RegisterData.sharedInstance?.imageStrBase64
    parm[Kaddress]       = RegisterData.sharedInstance?.address
    parm[Kcountry_id]    = RegisterData.sharedInstance?.countryID
    parm[Kstate_id]      = RegisterData.sharedInstance?.stateID
    parm[Kcity_id]       = RegisterData.sharedInstance?.cityID
    parm[Kzip_code]      = RegisterData.sharedInstance?.zipCode
    parm[Kdevice_token]  = RegisterData.sharedInstance?.deviceTokken
    parm[Kdevice_type]   = RegisterData.sharedInstance?.deviceType
    parm[Kis_seller]     = RegisterData.sharedInstance?.isSeller
    parm[Klat]           = RegisterData.sharedInstance?.lat
    parm[Klng]          = RegisterData.sharedInstance?.lng


   // print(parm)
        
    Auth.sharedmanagerAuth.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
    print(Response)
        if Response[Kredirect] != nil{
             self.redirect_to_Stripe = Response[Kredirect] as! Int
        }else{
            self.redirect_to_Stripe = 1
        }
       
        let statusCode = Response[Kstatus] as! Int
    
    if statusCode == 200
    {
        if self.redirect_to_Stripe == 1 {
            
           
            if let result = Response[Kresult]
            {
                ProfieData.sharedInstance.StoreData(json: result as! [String:Any])
                if let userType  = (result as! [String:Any])[Kis_seller]
                {
                    print("here i am get user type : \(userType)")
                    UserDefaults.standard.setUserType(value: userType as! Bool)
                    print(UserDefaults.standard.getUserType())
                }
                
                if let userID  = (result as! [String:Any])[Kid]
                {
                    UserDefaults.standard.setUserID(value: userID as! Int)
                }
                if let is_Account  = Response[Kis_account]
                {
                    self.is_Account_Check = is_Account as! Int
                }

                if let stateID  = (result as! [String:Any])[Kstate_id]
                {
                    UserDefaults.standard.setStateID(value: stateID as! Int)
                }
                
                if let accessTokken  = Response[Kaccess_token]
                {
                    UserDefaults.standard.setAccessTokken(value: accessTokken as! String)
                }
                if let refresh_token = Response[Krefresh_token]
                {
                    UserDefaults.standard.setRefreshTokken(value: refresh_token as! String)
                }
                
                
                if(UserDefaults.standard.getUserType() == true){
                    //open webpage
                    
                    if(self.is_Account_Check == 1){
                         UserDefaults.standard.setStripeExist(value: false)
                    // UserDefaults.standard.set(false, forKey: "ISStripeAccountExist")
                          self.AlertWithNavigatonPurpose(message: KOTPSentToMail, navigationType: .root, ViewController: .none, rootViewController: .LoginNavigation,Data:nil)
                        
                        
                        
                        print("show message opened")
//                         self.showAlertMessage(titleStr: KMessage, messageStr:  "We sent a link to your registered email to connect your stripe account with BidJones to receive payments.")
                    }
                    else if(self.is_Account_Check == 0){
                        if  let userID = UserDefaults.standard.getUserID()
                        {
                            self.user_ID =  userID
                            self.MakeSellerShowAlert(msg: Response["message"] as! String)
                            //                        print(state_ID)
                            //                        var url = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(state_ID)"
                            //                        //append state_id to url and send to another vc
                            //                        KCommonFunctions.PushToContrller(from: self, ToController: .Terms, Data:url)
                            
                        }
                    }else{
                        print("Something wrong happend")
                    }
                    
                  
                }
                else{
                    
               //  UserDefaults.standard.setLoggedIn(value: true)
                    self.AlertWithNavigatonPurpose(message: KUserregisteredsuccessfully, navigationType: .root, ViewController: .none, rootViewController: .LoginNavigation,Data:nil)
//                self.AlertWithNavigatonPurpose(message: KUserregisteredsuccessfully, navigationType: .root, ViewController: .none, rootViewController: .HomeNavigation,Data:nil)
                }
            
            }
        }
    //RegisterData.sharedInstance = nil

    }
    else if let msg =  Response[Kmessage]
    {
    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)

    }
    }, completionnilResponse: { (Response) in
    let statusCode = Response[Kstatus] as! Int
    if statusCode == 400
    {
    self.showAlertMessage(titleStr: KMessage, messageStr:KEmailaddressalreadyexist)

    }
    else if let msg =  Response[Kmessage]
    {
    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
    }
    }, completionError: { (error) in
    self.showAlertMessage(titleStr: KMessage, messageStr:KError)
    },networkError: {(error) in
    self.showAlertMessage(titleStr: KMessage, messageStr: error)
    })
    }
    
    func Validations() throws
    {
        guard let otp = txtFld_OTP.text,  !otp.isEmpty, !otp.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyOTPAddress
        }
    }
    func MakeSellerShowAlert(msg : String)
    {
        let alertController = UIAlertController(title: KMessage, message: msg, preferredStyle: .alert)
        
        // Create the actions
        let NoAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
            UIAlertAction in
//            var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.user_ID!)"
//            if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
//                Global.sharedInstance.isFromSafari = true
//                UIApplication.shared.openURL(url)
//            }
//            NSLog("OK Pressed")
            
            fromRegister = true
           // KCommonFunctions.PushToContrller(from: self, ToController: .WebView, Data: self.user_ID!)
            fromRegister = false
            self.AlertWithNavigatonPurpose(message: KUserregisteredsuccessfully, navigationType: .root, ViewController: .none, rootViewController: .LoginNavigation,Data:nil)
            
            
        }
        let YesAction = UIAlertAction(title: "Abort", style: UIAlertActionStyle.default) {
            UIAlertAction in
             //KCommonFunctions.PushToContrller(from: self, ToController: .Login, Data: nil)
            KCommonFunctions.popTocontroller(from: self)
            NSLog("OK Pressed")
        }
        self.dismiss(animated: true, completion: nil)
        alertController.addAction(NoAction)
        alertController.addAction(YesAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    func schedule() {
        self.timerDidFire()
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,
                                              selector: #selector(self.timerDidFire), userInfo: nil, repeats: true)
        }
    }
    
    @objc private func timerDidFire() {
        if(value>0)
        {
        value = value-1
//            if value >= 10
//            {
//            btnResend.setTitle("Remaining Time  00:\(value) sec", for: .normal)
//            }
//            else
//            {
//                btnResend.setTitle("Remaining Time  00:0\(value) sec", for: .normal)
//            }
            
            btnResend.setTitle("\(secondsToHoursMinutesSeconds(seconds: value))", for: .normal)
        }
        else
        {
            timer?.invalidate()
            value = 900
            txtFld_OTP.text = ""
            btnResend.setTitle("Resend OTP", for: .normal)
        }
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        let hrs = seconds / 3600
        let mins =  (seconds % 3600) / 60
        let sec = (seconds % 3600) % 60
     //   let nextStr =  "\(mins)" + ":" + "\(sec)"
      return  String(format:"%02i:%02i", mins,sec)
    }
}
