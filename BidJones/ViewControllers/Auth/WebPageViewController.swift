//
//  File.swift
//  BidJones
//
//  Created by Gagan on 4/19/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class WebPageViewController: UIViewController,UINavigationControllerDelegate,UINavigationBarDelegate,WKNavigationDelegate,WKUIDelegate {
    
    @IBOutlet var processIndicator: UIActivityIndicatorView!
    
    @IBOutlet var progressView: UIProgressView!
    //var progressView: UIProgressView?
    var webView: WKWebView?
    var webUrl="https://web-application-8ca8d.firebaseapp.com/terms-conditions.html"
    //http://www.hotfuse.com/dashboard/view_mob/Terms
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        navigationController!.navigationBar.isHidden = false
        
    }
    override func viewDidLoad()
    {
     
        print("Webpage Url:- \(webUrl)")
        /* Create our preferences on how the web page should be loaded */
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        /* Create a configuration for our preferences */
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences

        /* Now instantiate the web view */
        webView = WKWebView(frame: view.bounds, configuration: configuration)
        webView?.contentMode = .scaleAspectFit
        if let theWebView = webView{
            /* Load a web page into our web view */
            let url = NSURL(string: self.webUrl)
            let urlRequest = NSURLRequest(url: url! as URL)
            theWebView.load(urlRequest as URLRequest)
            theWebView.navigationDelegate = self
           // theWebView.uiDelegate = self
            theWebView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
            view.addSubview(theWebView)
            view.addSubview(progressView!)
           
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            self.processIndicator.stopAnimating()
            self.processIndicator.isHidden=true
        })
       
    }
    func showProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.progressView!.alpha = 1
        }, completion: nil)
    }
    
    func hideProgressView() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.progressView!.alpha = 0
        }, completion: nil)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView?.progress = Float(webView!.estimatedProgress)
            
        }
    }
    
    @IBAction func btn_Back(_ sender: Any) {
      
        webView?.stopLoading()
        self.navigationController?.popViewController(animated: true)
    }
    
    /* Start the network activity indicator when the web view is loading */
    func webView(_ webView: WKWebView,didStartProvisionalNavigation navigation: WKNavigation){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
      
        processIndicator.startAnimating()
    }
    
    /* Stop the network activity indicator when the loading finishes */
    func webView(webView: WKWebView,didFinishNavigation navigation: WKNavigation){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        processIndicator.stopAnimating()
    }
    
    func webView(webView: WKWebView,
                 decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse,decisionHandler: ((WKNavigationResponsePolicy) -> Void)){
        //print(navigationResponse.response.MIMEType)
        decisionHandler(.allow)
        
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!)
    {
        let url = webView.url?.absoluteString
        print("---Hitted URL--->\(url!)") // here you are getting URL
        // processIndicator.stopAnimating()
    }
//    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//         processIndicator.stopAnimating()
//    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideProgressView()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showProgressView()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        hideProgressView()
    }
}

