 
 
  //
//  RegisterVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/8/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import DropDown


class RegisterVC:  UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate {
   
    //MARK: - Label Outlets
    @IBOutlet var lbl_AreYouRegister: UILabel!
    //MARK: - TextField Outlets
    
   fileprivate var ImageData:Data?
    
    @IBOutlet weak var txtFieldVerifyPasswor: UITextField!
    @IBOutlet var txtFld_LastName: UITextField!
    @IBOutlet var txtFld_FirstName: UITextField!
    @IBOutlet var txtFld_UserName: UITextField!
    @IBOutlet var txtFld_Address: UITextField!
    @IBOutlet var txtFld_Country: UITextField!
    @IBOutlet var txtFld_State: UITextField!
    @IBOutlet var txtFld_City: UITextField!
    @IBOutlet var txtFld_EmailAddress: UITextField!
    @IBOutlet var txtFld_Password: UITextField!
    @IBOutlet var txtFld_forOpeningPickers: UITextField!
    @IBOutlet var txtFld_PostalCode: UITextField!
    //MARK: - TextView Outlets
    //MARK: - UItableView Outlets
    //MARK: - UISCrollView Outlets

    @IBOutlet var scrollView_main: UIScrollView!
    //MARK: - UIButton Outlets
    
    @IBOutlet weak var isChecked: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var btnYes: UIButton!
    //MARK: - UIImageView Outlets
    @IBOutlet var ImgView_User: UIImageView!

    //MARK: - UIView Outlets
   /// @IBOutlet var viewPicker: UIView!
    //MARK: - UIPicker Outlets
   // @IBOutlet var pickerView: UIPickerView!


    
    
    //MARK: - Int Variable
    //MARK: - Bool Variable
    var dropDown = DropDown()
    var is_seller = Bool()
    var user_Id:Int?
    static var is_checked = Bool()
    //MARK: - String Variable
    private var strCountryID = String()
    private var strStateID = String()
    private var strCityID = String()

    private var strImgBase64 = ""


    //MARK: - array Variable
    private var arrCity = [AddressData]()
    private var arrState = [AddressData]()
   // private var arrCountry = [[String : Any]]()
    private var arrPicker = [AddressData]()
    private var arrCountry = [AddressData]()
    //MARK: - UIPickerView Variable
    private var pickerView = UIPickerView()


    //MARK: - dictionary Variable

    //MARK: - UIImagePickerController
    private var imagePicker =  UIImagePickerController()
    //MARK: - Enum Variable

    
//    enum value : [String : Any] {
//        
//        case driver = {"1" : "Driver"}
//        case car = {"2" : "car"}
//        
//        init(){
//           self = .driver
//            
//        }
//        
//    }
    
    
    enum PickerTypes: Int  {
        case countryPicker = 1
        case statePicker = 2
        case cityPicker = 3
        init() {
            self = .countryPicker
        }
    }
    private var pickerType: PickerTypes?

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        is_seller = true
        RegisterVC.is_checked = false
        self.automaticallyAdjustsScrollViewInsets = false
       // self.view.insertSubview(scrollView_main, at: 0)
        let screenWidth = self.ViewWidth()
        if(screenWidth == 320)
        {
            lbl_AreYouRegister.font = UIFont.systemFont(ofSize: 12.0)
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFld_forOpeningPickers.inputView = pickerView
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFld_forOpeningPickers.inputAccessoryView = toolBar
        self.fetchCountries()
     
        self.txtFld_Country.isUserInteractionEnabled = true
        self.txtFld_Country.addTarget(self, action: #selector(showDropDownCountry(_:)), for: .editingChanged)
        self.txtFld_State.addTarget(self, action: #selector(showDropDownState(_:)), for: .editingChanged)
        self.txtFld_City.addTarget(self, action: #selector(showDropDownCity(_:)), for: .editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ImgView_User.setRounded()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    
    @IBAction func ActionCheck_mark(_ sender: Any) {
        if(RegisterVC.is_checked){
            //uncheckimage asssign
            RegisterVC.is_checked = false

            isChecked.setImage(#imageLiteral(resourceName: "img_check"), for: UIControlState.normal)
        }else{
            //checkimage
            RegisterVC.is_checked = true
            isChecked.setImage(#imageLiteral(resourceName: "img_check_fill"), for: UIControlState.normal)
        }
        
    }
    @IBAction func ActionTerms(_ sender: Any) {
    // let url =  "https://web-application-8ca8d.firebaseapp.com/terms-conditions"
        
   // KCommonFunctions.PushToContrller(from: self, ToController: .Terms, Data: url)
        KCommonFunctions.PushToContrller(from: self, ToController: .TermsNew, Data: nil)
    }
    @IBAction func ActionNo(_ sender: Any)
    {
        is_seller = false
        if let image  = UIImage(named: Kselected)
        {
            btnNo.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnYes.setImage(image, for: .normal)
        }
    }
    @IBAction func ActionYes(_ sender: Any)
    {
        is_seller = true
        if let image  = UIImage(named: Kselected)
        {
            btnYes.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnNo.setImage(image, for: .normal)
        }
    }


    @objc func showDropDownCountry(_ sender: UITextField) {
        
        //gurleen
        var arrCountrySearched = [String]()
        dropDown.anchorView = sender
        for element in self.arrCountry{
            let option = element.name
            let range = option.lowercased().range(of: sender.text!, options: NSString.CompareOptions.caseInsensitive, range: nil,   locale: Locale(identifier:sender.text!))
            
            if range != nil {
                if !arrCountrySearched.contains(where: {$0 == option}){
                    arrCountrySearched.append(option)
                }
            }
        }
        dropDown.dataSource = arrCountrySearched
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            sender.text = item
            if(self.txtFld_Country.text != ""){
                //                if let value =  self.GetIndexFromArrayForString(arr: array, str: (txtFld_Country.text)!, key: Kname,type:.AddressData)
                //                {
                //                    pickerView.selectRow(value, inComponent: 0, animated: false)
                //                }
              
                 let data = self.arrCountry.first{ $0.name == item }
                if let adressData = data{
                     let id = adressData.id
                self.strCountryID = String(id)
                 self.fetchState(strCountryID: self.strCountryID)
                }
                
                
            }
            
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
    }
    
    @objc func showDropDownState(_ sender: UITextField) {
        
        guard let country  = txtFld_Country.text, !country.isEmpty, !country.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillCountryname)
            return
        }
        //gurleen
        var arrCountrySearched = [String]()
        dropDown.anchorView = sender
        for element in self.arrState{
            let option = element.name
            let range = option.lowercased().range(of: sender.text!, options: NSString.CompareOptions.caseInsensitive, range: nil,   locale: Locale(identifier:sender.text!))
            
            if range != nil {
                if !arrCountrySearched.contains(where: {$0 == option}){
                    arrCountrySearched.append(option)
                }
            }
        }
        dropDown.dataSource = arrCountrySearched
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            sender.text = item
            if(self.txtFld_State.text != ""){
                //                if let value =  self.GetIndexFromArrayForString(arr: array, str: (txtFld_Country.text)!, key: Kname,type:.AddressData)
                //                {
                //                    pickerView.selectRow(value, inComponent: 0, animated: false)
                //                }
          
                let data = self.arrState.first{ $0.name == item }
                if let adressData = data{
                    let id = adressData.id
                    self.strStateID = String(id)
                    self.fetchCity(strStateID: self.strStateID)
                }
                
                
            }
            
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
    }
    
    @objc func showDropDownCity(_ sender: UITextField) {
        guard let state  = txtFld_State.text, !state.isEmpty, !state.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillStatename)
            return
        }
        //gurleen
        var arrCountrySearched = [String]()
        dropDown.anchorView = sender
        for element in self.arrCity{
            let option = element.name
            let range = option.lowercased().range(of: sender.text!, options: NSString.CompareOptions.caseInsensitive, range: nil,   locale: Locale(identifier:sender.text!))
            
            if range != nil {
                if !arrCountrySearched.contains(where: {$0 == option}){
                    arrCountrySearched.append(option)
                }
            }
        }
        dropDown.dataSource = arrCountrySearched
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            sender.text = item
            if(self.txtFld_City.text != ""){
                let id  = self.arrCity[index].id
                self.strCityID = String(id)
             }
            
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
    }
    
    
    func checkCountries(array: [AddressData]){
        if(array.count>0)
        {
            pickerType = PickerTypes(rawValue: 1)
            self.arrPicker = array
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_Country.text != ""){
                if let value =  self.GetIndexFromArrayForString(arr: array, str: (txtFld_Country.text)!, key: Kname,type:.AddressData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
        else
        {
            fetchCountries()
        }
    }
    @IBAction func ActionCountry(_ sender: Any)
    {
//        if(arrCountry.count>0)
//        {
//            pickerType = PickerTypes(rawValue: 1)
//            self.arrPicker = self.arrCountry
//            self.pickerView.reloadAllComponents()
//            pickerView.selectRow(0, inComponent: 0, animated: false)
//            txtFld_forOpeningPickers.becomeFirstResponder()
//            if(txtFld_Country.text != "")
//            {
//                if let value =  self.GetIndexFromArrayForString(arr: arrCountry, str: (txtFld_Country.text)!, key: Kname,type:.AddressData)
//                {
//                    pickerView.selectRow(value, inComponent: 0, animated: false)
//                }
//            }
//        }
//        else
//        {
//            fetchCountries()
//        }
    }
    @IBAction func ActionState(_ sender: Any) {
//        guard let country  = txtFld_Country.text, !country.isEmpty, !country.trimmingCharacters(in: .whitespaces).isEmpty else
//        {
//            self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillCountryname)
//            return
//        }
//
//        if(
//            arrState.count>0)
//        {
//            pickerType = PickerTypes(rawValue: 2)
//            self.arrPicker = self.arrState
//            self.pickerView.reloadAllComponents()
//            pickerView.selectRow(0, inComponent: 0, animated: false)
//            txtFld_forOpeningPickers.becomeFirstResponder()
//            if(txtFld_State.text != "")
//            {
//                if let value =  self.GetIndexFromArrayForString(arr: arrState, str: (txtFld_State.text)!, key: Kname,type:.AddressData)
//                {
//                    pickerView.selectRow(value, inComponent: 0, animated: false)
//                }
//            }
//        }
//        else
//        {
//            fetchState(strCountryID: self.strCountryID)
//        }
    }
    @IBAction func ActionCity(_ sender: Any) {
//        guard let state  = txtFld_State.text, !state.isEmpty, !state.trimmingCharacters(in: .whitespaces).isEmpty else
//        {
//            self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillStatename)
//            return
//        }
//        if(arrCity.count>0)
//        {
//            pickerType = PickerTypes(rawValue: 3)
//            self.arrPicker = self.arrCity
//            self.pickerView.reloadAllComponents()
//            pickerView.selectRow(0, inComponent: 0, animated: false)
//            txtFld_forOpeningPickers.becomeFirstResponder()
//            if(txtFld_City.text != "")
//            {
//                if let value =  self.GetIndexFromArrayForString(arr: arrCity, str: (txtFld_City.text)!, key: Kname,type:.AddressData)
//               {
//                pickerView.selectRow(value, inComponent: 0, animated: false)
//                }
//            }
//        }
//        else
//        {
//          fetchCity()
//        }
    }
    
    @IBAction func ActionSubmit(_ sender: Any) {
      //  if(RegisterVC.is_checked){
        self.view.endEditing(true)
                do {
                    try Validations()
                    if(RegisterVC.is_checked){

                    if let _  = txtFld_FirstName
                    {
                     RegisterData.sharedInstance?.firstName = txtFld_FirstName.text
                    }
                     RegisterData.sharedInstance?.lastName  = txtFld_LastName.text
                     RegisterData.sharedInstance?.email     =  txtFld_EmailAddress.text
                     RegisterData.sharedInstance?.password  = txtFld_Password.text
                     RegisterData.sharedInstance?.username  = txtFld_UserName.text
                     RegisterData.sharedInstance?.imageStrBase64     = strImgBase64
                     RegisterData.sharedInstance?.address      = txtFld_Address.text
                     RegisterData.sharedInstance?.countryID    = Int(strCountryID)
                     RegisterData.sharedInstance?.stateID      = Int(strStateID)
                     RegisterData.sharedInstance?.cityID       = Int(strCityID)
                     RegisterData.sharedInstance?.zipCode      = txtFld_PostalCode.text
                     RegisterData.sharedInstance?.deviceTokken  = KDummyDeviceTokken
                    RegisterData.sharedInstance?.lat  = "\(Location.sharedInstance.GetCurrentLocation().latitude)"
                    RegisterData.sharedInstance?.lng  = "\(Location.sharedInstance.GetCurrentLocation().longitude)"


                    if let deviceTokken = UserDefaults.standard.getDeivceTokken()
                    {
                        RegisterData.sharedInstance?.deviceTokken   = deviceTokken
                    }
                     RegisterData.sharedInstance?.deviceType    = KDeviceType
                     RegisterData.sharedInstance?.isSeller      = is_seller
                    
                    let urlStr = KverifyUser + "/" + "\(String(describing: txtFld_EmailAddress.text!))" + "/" + "\(String(describing: txtFld_UserName.text!))" + "sendotp"
                    
                    let urlwithPercentEscapes = urlStr.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)

                    Auth.sharedmanagerAuth.GetApi(url: urlwithPercentEscapes!,Target: self, completionResponse: { (Response) in
                            print("signup",Response)
                                if let msg =  Response[Kmessage]
                                {
                                    self.AlertWithNavigatonPurpose(message : msg as! String, navigationType: .push, ViewController: .VerifyOTP, rootViewController: .none,Data:nil)
                                }
                                
                        }, completionnilResponse: { (Response) in
                            if let msg =  Response[Kmessage]
                            {
                                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                            }
                        }, completionError: { (error) in
                            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                        },networkError: {(error) in
                            self.showAlertMessage(titleStr: KMessage, messageStr: error)
                        })
            }
                   else{
                                          self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectTerms)
                                     }                } catch let error {
                    switch  error {
                    case ValidationError.emptyFirstName:
                            self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillFirstname)
                    case ValidationError.firstNameMinChar:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KFirstnameshouldbeof)
                    case ValidationError.emptyLastName:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillLastname)
                    case ValidationError.lastNameMinChar:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KLastNameShouldbeof)
                    case ValidationError.emptyUsername:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillUsername)
                    case ValidationError.userNameMinChar:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KUsernameshouldbeof)
                    case ValidationError.emptyAddress:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefilladdreass)
                    case ValidationError.addressMinChar:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KAddressShouldbeof)
                    case ValidationError.emptyCountry:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillCountryname)
                    case ValidationError.emptyState:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillStatename)
                    case ValidationError.emptyCity:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillCityname)
                    case ValidationError.emptyPostalCode:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillPostalCode)
                    case ValidationError.postalCodeMinChar:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPostalCodeshouldbeof)
                    case ValidationError.emptyEmailAddress:
                        self.showAlertMessage(titleStr: KMessage, messageStr: KPleasefillEmailAddress)
                    case ValidationError.invalidEmail:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
                    case ValidationError.emptyPassword:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillpassword)
                    case ValidationError.passwordMinChar:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPasswordshouldbeof)
                    case ValidationError.passwordSufficientComplexity:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenteratleastonecaptialletter)
                    case ValidationError.emptyImg:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseAddImg)
                    case ValidationError.enterVerifyPassword:
                       self.showAlertMessage(titleStr: KMessage, messageStr:KPasswordVerification)
                    case ValidationError.MismatchPassword:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KMismatchPassword)
                    default:
                        self.showAlertMessage(titleStr: KMessage, messageStr:KUnknownerror)
                    }
                }
//        }else{
//             self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectTerms)
//        }
    }
    @IBAction func BackAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ActionCancel(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func AddImageAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.OpenGallaryCamera(pickerController: self.imagePicker)
    }
    
    //MARK: - UImagePickerDelegate
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
       ImageData = self.compressImage(image: info[UIImagePickerControllerEditedImage] as! UIImage, compressionQuality: 1.0)
        ImgView_User.image = info[UIImagePickerControllerEditedImage] as? UIImage
        if (ImageData != nil)
        {
            strImgBase64 = (ImageData?.base64EncodedString(options: .lineLength64Characters))!
            strImgBase64 = KBase64Prefix + strImgBase64
        }
        dismiss(animated:true, completion: nil)
    }
     func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //print("Cancel")
        dismiss(animated:true, completion: nil)
    }
    
    //MARK: - Other Methods
 
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        let dic = arrPicker[row]
            //print(dic)
             let title = dic.name
             let id = dic.id
             let ID:String = String(id)
            switch pickerType
            {
            case .countryPicker?:
                txtFld_Country.text = title
                txtFld_State.text = ""
                txtFld_City.text = ""
                strCountryID = ID
                fetchState(strCountryID: ID)
            case .statePicker?:
                txtFld_State.text = title
                txtFld_City.text = ""
                strStateID = ID
                fetchCity(strStateID: ID)
            case .cityPicker?:
                txtFld_City.text = title
                strCityID = ID
            case .none:
                print("none")
             }
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    @objc func CancelPicker()
    {
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func Validations() throws
    {
        //gurleen
//        if(strImgBase64.isEmpty && strImgBase64.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
//        {
//            throw ValidationError.emptyImg
//        }
        guard let firstName = txtFld_FirstName.text,  !firstName.isEmpty, !firstName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyFirstName
        }
        if(!firstName.nameMinLength)
        {
            throw ValidationError.firstNameMinChar
        }
        guard let lastName  = txtFld_LastName.text, !lastName.isEmpty, !lastName.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyLastName
        }
        if(!lastName.nameMinLength)
        {
            throw ValidationError.lastNameMinChar
        }
        guard let userName  = txtFld_UserName.text, !userName.isEmpty, !userName.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyUsername
        }
        if(!userName.nameMinLength)
        {
            throw ValidationError.userNameMinChar
        }
        guard let address  = txtFld_Address.text, !address.isEmpty, !address.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyAddress
        }
        guard let country  = txtFld_Country.text, !country.isEmpty, !country.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyCountry
        }
        guard let state  = txtFld_State.text, !state.isEmpty, !state.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyState
        }
        guard let city  = txtFld_City.text, !city.isEmpty, !city.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyCity
        }
        guard let postalCode  = txtFld_PostalCode.text, !postalCode.isEmpty, !postalCode.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyPostalCode
        }
        if(!postalCode.postalCodeMinLength)
        {
            throw ValidationError.postalCodeMinChar
        }
        guard let emailAddress  = txtFld_EmailAddress.text, !emailAddress.isEmpty, !emailAddress.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyEmailAddress
        }
        if(!emailAddress.isEmail)
        {
          throw ValidationError.invalidEmail
        }
        guard let password  = txtFld_Password.text, !password.isEmpty, !password.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyPassword
        }
       if(!password.passwordMinLength)
        {
            throw ValidationError.passwordMinChar
        }
        if(!password.checkTextSufficientComplexity())
        {
            throw ValidationError.passwordSufficientComplexity
        }
        guard let verifypassword  = txtFieldVerifyPasswor.text, !verifypassword.isEmpty, !verifypassword.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.enterVerifyPassword
        }
        if password != verifypassword{
            throw ValidationError.MismatchPassword
        }
    }
    
    //MARK: - Fetch Countries Name
    
    func  fetchCountries()
    {
        let urlStr = KCountryApi
        FetchAddress.sharedFetchAddress.GetAddressListApi(url: urlStr, Target: self, completionResponse: { (countries) in
            self.arrCountry = countries
            self.arrPicker = self.arrCountry
            self.setDefaultCountry()
            self.pickerView.reloadAllComponents()
        }, completionnilResponse: { (Response) in
            if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        },completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: (error))
        })
    }
    func setDefaultCountry(){
        self.txtFld_Country.text = "United States"
        let arr = self.arrCountry.map { elem -> Void in
           
            if elem.name == "United States" {
                self.strCountryID = String(elem.id)
                self.fetchState(strCountryID: self.strCountryID)
                
            }
        }
         print(arr)
    }
    //MARK: - Fetch City Names
    
    func fetchState(strCountryID: String) {
            let urlStr = KStateApi + strCountryID
        FetchAddress.sharedFetchAddress.GetAddressListApi(url: urlStr, Target: self, completionResponse: { (states) in
            self.arrState = states
            self.arrPicker = self.arrState
//            if(self.arrState.count>0)
//            {
//                self.pickerView.reloadAllComponents()
//            }
        }, completionnilResponse: { (Response) in
            if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
            self.txtFld_State.text = self.txtFld_Country.text
            self.strStateID = KZero
            self.arrState.removeAll()
        },completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: (error))
        })

 }
    //MARK: - Fetch State Names

    func fetchCity(strStateID:String)
    {
           let urlStr = KCityApi + strStateID
        FetchAddress.sharedFetchAddress.GetAddressListApi(url: urlStr, Target: self, completionResponse: { (cities) in
            self.arrCity = cities
            self.arrPicker = self.arrCity
            if(self.arrCity.count>0)
            {
            self.pickerView.reloadAllComponents()
            }
        }, completionnilResponse: { (Response) in
            if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
            self.arrCity.removeAll()
            self.txtFld_City.text = self.txtFld_State.text
            self.strCityID = KZero
        },completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: (error))
        })
 }
    
    //MARK: - TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 150
                }, completion: nil)
            }
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 0
                }, completion: nil)
            }
            
            
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
        if (textField == txtFld_UserName || textField == txtFld_FirstName || textField == txtFld_LastName)
        {
            if textField.RestrictMaxCharacter(maxCount: 30, range: range, string: string)
            {
                if (string == " ")
                {
                    return false
                }
                }
            else
            {
                return false
            }
            }
        if (textField == txtFld_FirstName || textField == txtFld_LastName )
        {
            let regex = try! NSRegularExpression(pattern: "[a-zA-Z\\s]+", options: [])
            let range = regex.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.count))
            return range.length == string.count
        }
        if textField == txtFld_PostalCode
        {
            if textField.RestrictMaxCharacter(maxCount: 8, range: range, string: string)
            {
            let regex = try! NSRegularExpression(pattern: "[A-Za-z0-9^]*", options: [])
            let range = regex.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.count))
            return range.length == string.count
            }
            else
            {
                return false
            }
        }
        if (textField == txtFld_EmailAddress || textField == txtFld_Address )
        {
            return textField.RestrictMaxCharacter(maxCount: 100, range: range, string: string)
        }
         if textField == txtFld_Password
         {
            return textField.RestrictMaxCharacter(maxCount: 20, range: range, string: string)
         }
        return true
    }
}
extension RegisterVC: UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPicker.count;
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let dic = arrPicker[row]
        let title = dic.name
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }

 }

