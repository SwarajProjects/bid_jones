//
//  ForgotVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/21/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class ForgotVC: UIViewController,UITextFieldDelegate {
    var userId:Int?
    //MARK: - TextField Outlets
    @IBOutlet var txtFld_UserName: UITextField!
    //MARK: - UIButton Outlets

    override func viewDidLoad() {
        super.viewDidLoad()
      //  print(userId!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func ActionCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func ActionSubmit(_ sender: Any)
    {
        self.view.endEditing(true)
        do {
            try Validations()
            let urlStr = KForgotApi
            var parm = [String : Any]()
            parm[Kemail] = txtFld_UserName.text!
            print(parm)
            Auth.sharedmanagerAuth.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                    print(Response)
                       self.AlertWithNavigatonPurpose(message: KPasswordhasbeensenttoyouremailaddresssuccessfully, navigationType: .pop, ViewController: .none, rootViewController: .none,Data:nil)
                }, completionnilResponse: { (Response) in
                    print(Response )
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 400
                    {
                        self.showAlertMessage(titleStr: KMessage, messageStr:KEmailaddressdoesnotexist)
                    }
                    else if let msg =  Response[Kmessage]
                    {
                        self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                    }
                }, completionError: { (error) in
                    self.showAlertMessage(titleStr: KMessage, messageStr: KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
           
        } catch let error {
            switch  error {
            case ValidationError.emptyEmailAddress:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillEmailAddress)
            case ValidationError.invalidEmail:
                self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
            }
        }
    }
    @IBAction func ActionBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Other Actions
    
    func Validations() throws
    {
        guard let username = txtFld_UserName.text,  !username.isEmpty, !username.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyEmailAddress
        }
        if(!username.isEmail)
        {
            throw ValidationError.invalidEmail
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
   
    //MARK: - TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true;
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " " || string == "  ") &&  range.location == 0
        {
            return false
        }
        if textField == txtFld_UserName
        {
            return textField.RestrictMaxCharacter(maxCount: 100, range: range, string: string)
        }
        return true
    }
}
