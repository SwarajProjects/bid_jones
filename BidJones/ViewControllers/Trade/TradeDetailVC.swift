//
//  TradeDetailVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 1/30/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit

class TradeDetailVC: UIViewController {

  // MARK : - IBOutlets
  @IBOutlet var lblDate: UILabel!
  @IBOutlet var lblTitle1: UILabel!
  @IBOutlet var lblDescrption1: UILabel!
  @IBOutlet var lblTitle2: UILabel!
  @IBOutlet var lblDescrption2: UILabel!
  @IBOutlet var btnAccept: CustomButton!
  @IBOutlet var btnReject: CustomButton!
  
  var tradeDetail:TradeRequestListData?
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
      print(tradeDetail ?? "Null" as Any)
      
      lblDate.text = tradeDetail?.createdDate
      
      if let id = UserDefaults.standard.getUserID()
      {
        if(id == tradeDetail?.sender_id)
        {
          lblTitle1.text = tradeDetail?.trade_item_detail?.title
          lblDescrption1.text = tradeDetail?.trade_item_detail?.description
          
          lblTitle2.text = tradeDetail?.item_detail?.title
          lblDescrption2.text = tradeDetail?.item_detail?.description
      
        }
        else
        {
          lblTitle1.text = tradeDetail?.item_detail?.title
          lblDescrption1.text = tradeDetail?.item_detail?.description
          
          lblTitle2.text = tradeDetail?.trade_item_detail?.title
          lblDescrption2.text = tradeDetail?.trade_item_detail?.description

        }
      }
      if(tradeDetail?.isHistory == 0)
      {
        if let id = UserDefaults.standard.getUserID()
        {
          if(id == tradeDetail?.sender_id)
          {
            btnReject.isHidden = true
            btnAccept.alpha = 0.5
            btnAccept.backgroundColor = .gray
            btnAccept.setTitle("REQUEST SENT",for: .normal)
            btnAccept.isUserInteractionEnabled = false
          }
        }
      }
      else
      {
       // if(tradeDetail?.paymentComplete == true)
       // {
          btnReject.alpha = 0.5
          btnReject.backgroundColor = .gray
          btnAccept.setTitle(KRATINGSELLER,for: .normal)
          btnReject.setTitle("TRADE COMPLETED",for: .normal)
          btnReject.isUserInteractionEnabled = false
//        }
//        else
//        {
//          btnReject.isHidden = true
//          btnAccept.alpha = 0.5
//          btnAccept.setTitle("REQUEST ACCEPTED",for: .normal)
//          btnAccept.isUserInteractionEnabled = false
//        }
      }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  //MARK: - Actions
  @IBAction func BackAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
  }
  @IBAction func OtherItemViewAction(_ sender: CustomButton) {
    UserDefaults.standard.setShowEditButton(value: false)
    UserDefaults.standard.setShowSellerInfo(value: true)
    print(tradeDetail?.sender_detail?.first_name!)

       tradeDetail?.myProduct = 0
        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: tradeDetail)
  }
  
  @IBAction func MyItemViewAction(_ sender: CustomButton) {
    UserDefaults.standard.setShowEditButton(value: false)
    UserDefaults.standard.setShowSellerInfo(value: false)
    tradeDetail?.myProduct = 1
    CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: tradeDetail)
  }
  @IBAction func RejectAction(_ sender: CustomButton)
  {
  SendRequestResponse(value: 3)
  
  }
  @IBAction func AcceptAction(_ sender: Any)
  {
    if(btnAccept.currentTitle == KRATINGSELLER)
    {
      KCommonFunctions.PushToContrller(from: self, ToController: .Rating, Data: self.tradeDetail)
    }
    else
    {
      SendRequestResponse(value: 2)
    }
  }
  
  func SendRequestResponse(value:Int)
  {
    let urlStr = Ktradeacceptrejectrequest
    var parm = [String : Any]()
    parm[Krequest_id] = tradeDetail?.id
    parm[Kstatus] = value
 
    //KDeviceType
    Trade.sharedManager.PostApiForAR(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
      print(Response)
      NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)

      if let msg =  Response[Kmessage]
      {
        if let result = (Response[Kresult] as? [String:Any])
        {
          //self.clearTempFolder()
          if let imagesArr = result[Kimages] as? [String]
          {
            var arrImages = [ImagesData]()
            var index = 0
            for item in imagesArr
            {
              let url = URL(string: item)
              if let data = ImagesData(ImgUrl: url , Image:AddProuductData.sharedInstance?.arrImages![index].image){
                arrImages.append(data)
              }
              index = index+1
            }
            AddProuductData.sharedInstance?.arrImages = arrImages
          }
        }
        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
     //   self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .pop, ViewController: .none, rootViewController: .none,Data:nil)
        self.showAlertMessage(titleStr: KBIDJONES, messageStr: msg as! String)
        
        KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
        //                let alert = UIAlertController(title: KMessage, message: msg as! String, preferredStyle: UIAlertControllerStyle.alert)
        //                let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
        //                    UIAlertAction in
        //                    NSLog("OK Pressed")
        //                        self.popBack(3)
        //                }
        //                // Add the actions
        //                alert.addAction(okAction)
        //                self.present(alert, animated: true, completion: nil)
      }
      
    }, completionnilResponse: { (Response) in
      //print(Response)
      let statusCode = Response[Kstatus] as! Int
      if statusCode == 500
      {
        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
          //print(resonse)
          self.SendRequestResponse(value: value)
        })
      }
      else if let msg =  Response[Kmessage]
      {
        self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
      }
    }, completionError: { (error) in
      self.showAlertMessage(titleStr: KMessage, messageStr:KError)
    },networkError: {(error) in
      self.showAlertMessage(titleStr: KMessage, messageStr: error)
    })
    
  }
  
}
