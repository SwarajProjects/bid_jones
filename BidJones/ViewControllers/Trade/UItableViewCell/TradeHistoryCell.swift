//
//  TradeHistoryCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 2/5/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit

class TradeHistoryCell: UITableViewCell {

  @IBOutlet var lblTitle1: UILabel!
  @IBOutlet var lblTitle2: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
  
   func LoadData(dic:Any)
   {
    if let data = dic as? TradeRequestListData
    {
      if let id = UserDefaults.standard.getUserID()
      {
        if(id == data.sender_id)
        {
          lblTitle1.text = data.trade_item_detail?.title
          lblTitle2.text = data.item_detail?.title
        }
        else
        {
          lblTitle1.text = data.item_detail?.title
          lblTitle2.text = data.trade_item_detail?.title
        }
      }
      
    }
  }
}
