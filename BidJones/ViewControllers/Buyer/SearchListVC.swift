//
//  SearchListVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/21/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import MapKit



protocol SearchBidItemDelegate: class{
    func DeleteBidItemAction()
}

class SearchListVC: UIViewController,CLLocationManagerDelegate,UIScrollViewDelegate,SearchBidItemDelegate,ProductTableCellDelegate {
   
    
   
    var dataSearch:SearchData?
    //MARK: - Label Outlets
    @IBOutlet var viewNotify: UIView!
    @IBOutlet var lblNoItem: UILabel!
    //MARK: - UItableView Outlets
    @IBOutlet var tblList: UITableView!
    //MARK: - array Variable
    private lazy var arrList = [ItemListData]()
   // var lat:Double?
   // var lng:Double?
    var count = 0
    var timer:Timer?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isLoading:Bool?
    var isDeleted:Bool?
    var Index = 0
    var IsEscrowNull = Bool()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
         self.automaticallyAdjustsScrollViewInsets = false
        
        UserDefaults.standard.setShowEditButton(value: false)
        UserDefaults.standard.setShowSellerInfo(value: true)
        isDeleted = false
        isLoading = false
        dataLoaded = false
        isScrolling = false
        if #available(iOS 11.0, *) {
            tblList.contentInsetAdjustmentBehavior = .automatic
        } else {
            // Fallback on earlier versions
        }
     //   MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading

        print(dataSearch ?? "Blank")
        print(dataSearch?.category ?? "Blank")
        print(dataSearch?.categoryID ?? "Blank")
        print(dataSearch?.is_escrow ?? "Blank")
        print(dataSearch?.keyword ?? "Blank")
        print(dataSearch?.radius ?? "Blank")
        print(dataSearch?.unit ?? "Blank")
        IsEscrowNull = true

       if let value = dataSearch?.categoryID
       {
        if value == 1
        {
            self.title = "BOOKS"
        }
        else if value == 2
        {
            self.title = "GRAPHICS"
        }
        else if value == 3
        {
            self.title = "MUSIC"
        }
        else if value == 4
        {
            //gurleen
           // self.title = "SELL SERVICES"
            self.title = "SEARCH RESULTS"
             IsEscrowNull = false

        }
        else if value == 5
        {
            self.title = "STUFF"
        }
        else
        {
            //gurleen
             self.title = "SEARCH RESULTS"
                //self.title = "SELL SERVICES"
        }
       }
        
        lblNoItem.isHidden = true
        // GetList()
        tblList.estimatedRowHeight = 1000
        tblList.rowHeight = UITableViewAutomaticDimension
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(delayedAction), userInfo: nil, repeats: true)
        viewNotify.isHidden = true
        
        
        
        
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Mark CustomCell Delegate
    func DeleteBidItemAction() {
        print(Index)
        //gurleen
       // self.arrList.remove(at: self.Index)
        self.tblList.reloadData()
     //   self.isDeleted = true
//        if arrList.count == 0
//        {
//            tblList.isHidden = true
//            lblNoItem.isHidden = false
//        }
    }
    //Mark ProductTableCellDelegate

    func DeleteButtonAction(_ sender: ProductTableCell) {
        
    }
    
    func YesIWantYourService(_ sender: ProductTableCell) {
        print(sender.tag)
        print(arrList[(sender as AnyObject).tag])
        if let itemID = arrList[sender.tag].id
        {
            if let sellerID = arrList[sender.tag].sellerID
            {
                print(sellerID)
                print(itemID)
                
                
               guard let userID = UserDefaults.standard.getUserID() else
               {
                return
                }
               
                let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChatRoomVC") as! ChatRoomVC
                vc.anotherUserID = "\(sellerID)"
                vc.login_userID = "\(userID)"
                vc.itemID = "\(itemID)"
                
                navigationController?.pushViewController(vc, animated: true)
                
                
            }
        }
        
       // KCommonFunctions.PushToContrller(from: self, ToController: .Talk, Data: nil)
        
    }
    
    //MARK: - Other Actions
    @objc func delayedAction() {
        if let lattitude = Location.sharedInstance.lat
        {
            print(lattitude)
            timer?.invalidate()
            GetSearchData()
        }

    }
    func GetSearchData()  {
        let urlStr = Ksearchitems
        var parm = [String : Any]()
        if let userID = UserDefaults.standard.getUserID()
        {
            parm["seller_id"] = userID
        }
        if let lattitude = Location.sharedInstance.lat
        {
            parm["current_lat"] = lattitude
        }
        if let longitude = Location.sharedInstance.lng
        {
            parm["current_lng"] = longitude
        }
        if let Category = dataSearch?.categoryID
        {
            parm["category"] = Category
        }
        if let subCategory = dataSearch?.subCategoryID
        {
            parm["category_meta_id"] = subCategory
        }
        if let Keywords = dataSearch?.keyword
        {
            parm["keywords"] = Keywords
        }
        if let Distance = dataSearch?.radius
        {
            parm["distance"] = Distance
        }
        if let Unit = dataSearch?.unit
        {
            if Unit == "Miles"
            {
            parm["distance_type"] = "mi"
            }
            else
            {
                parm["distance_type"] = "km"
            }
        }
        if let escrow_service = dataSearch?.is_escrow
        {
            if IsEscrowNull == true
            {
                if escrow_service == true {
                parm["escrow_service"] = "1"
                }
                else {
                    parm["escrow_service"] = "0"
                    
                }
            }
            else
            {
                parm["escrow_service"] = "null"

            }
        }
        
        
        
        parm["offset"] = count
        parm["limit"]  = KPaginationcount

        print(parm)
        Search.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            print(self.arrList)

            if Response.count>0
            {
                if(self.count == 0)
                {
                    //  self.arrStuff = Response
                    for item in Response
                    {
                        let oldIds = self.arrList.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrList.insert(item, at: self.arrList.count)
                        }
                    }
                    self.tblList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrList.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrList.insert(item, at: self.arrList.count)
                        }
                    }
                    self.tblList.reloadData()
                    print(self.arrList)
                }
                print(self.count)
                self.lblNoItem.isHidden = true
            }
            else
            {
                if(self.count != 0)
                {
                    print("abcdefgh")
                    self.dataLoaded = true
                }
                if (self.count == 0 || self.arrList.count == 0)
                {
                self.lblNoItem.isHidden = false
                    self.viewNotify.isHidden = false
                }
            }
            self.isLoading = false
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.GetSearchData()
                })
            }
//          else if statusCode == 201
//            {
//                self.dataLoaded = true
//            }
            else
            {
                if (self.count == 0 || self.arrList.count == 0)
                {
                    self.arrList.removeAll()
                    self.tblList.reloadData()
                    self.lblNoItem.isHidden = false
                    self.viewNotify.isHidden = false

                 }
               
            }
            self.isLoading = false

        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            self.isLoading = false
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            self.isLoading = false
        })
    }
    //MARK: - IBAction
    @IBAction func YesAction(_ sender: Any) {
        self.viewNotify.isHidden = true
        let urlStr = Kbuyersavesearchpreference
        var parm = [String : Any]()
        if let userID = UserDefaults.standard.getUserID()
        {
            parm["seller_id"] = userID
        }
        if let Keywords = dataSearch?.keyword
        {
            parm["keywords"] = Keywords
        }
        if let Distance = dataSearch?.radius
        {
            parm["radius"] = Distance
        }
        if let Unit = dataSearch?.unit
        {
            if Unit == "Miles"
            {
                parm["distance_type"] = "mi"
            }
            else
            {
                parm["distance_type"] = "km"
            }
        }

        
        print(parm)
        Search.sharedManager.PostApiForSearchSave(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            print(self.arrList)
            if let msg =  Response[Kmessage] as? String
            {
                 // self.showAlertMessage(titleStr: KMessage, messageStr: "Saved succesfully!")
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.YesAction(AnyObject.self)
                })
            }
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    @IBAction func NoAction(_ sender: CustomButton) {
        self.viewNotify.isHidden = true
    }
    @IBAction func YesIwantAction(_ sender: Any) {
       // print(sender.tag)
       // print(arrList[(sender as AnyObject).tag])
       // if sellerID = arrList[sender]
        
        KCommonFunctions.PushToContrller(from: self, ToController: .Talk, Data: nil)

        //self.showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {

    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
         print(arrList.count)
            print(arrList.count)
           // print(isScrolling)
          //  print(isLoading)
         //print(dataLoaded)


            if (arrList.count>0 && arrList.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.count = self.count+KPaginationcount
                    GetSearchData()
            }
            else if isDeleted!
            {
                isDeleted = false
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                GetSearchData()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
}
    extension SearchListVC : UITableViewDelegate {
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let data = arrList[indexPath.row]
            Index = indexPath.row
           // let itemId = data.id
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }
    }
    
    extension SearchListVC : UITableViewDataSource {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            //print(arrBooks.count)
            return arrList.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : ProductTableCell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell) as! ProductTableCell
            cell.delegate = self
            cell.LoadData(dic:arrList[indexPath.row], type: .Book)
            cell.tag = indexPath.row
            cell.selectionStyle = .none
            
            return cell
        }
    }



