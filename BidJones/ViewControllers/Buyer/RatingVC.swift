//
//  RatingVC.swift
//  stripeIntegration
//
//  Created by Kuldeep Singh on 6/21/18.
//  Copyright © 2018 Kuldeep Singh. All rights reserved.
//

import UIKit
import Cosmos

class RatingVC :  UIViewController,UITextViewDelegate  {
    
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var imgViewFace: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var view_for_star_Rating: CosmosView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var view_for_textfield: UIView!
    
    //MARK: - Variable
    var productDetail : ItemListData?
    var tradeProductDetail : TradeRequestListData?

    var Ratings = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.navigationItem.setHidesBackButton(true, animated: false)
        print(productDetail as Any)
        
        if let decoded = UserDefaults.standard.getRatingData(),
            let decodedTeams = try? JSONDecoder().decode(ItemListData.self, from: decoded) {
                productDetail = decodedTeams
            }
        
        if let object = productDetail {
            print(object)
            if let encoded = try? JSONEncoder().encode(object) {
                UserDefaults.standard.setRatingData(value:encoded)
            }
        }
        textView.delegate = self
        textView.text = "Comment Here"
        textView.textColor = UIColor.lightGray
        textView.layer.borderColor = UIColor.black.cgColor
        view_for_star_Rating.didTouchCosmos = didTouchCosmos
        view_for_star_Rating.didFinishTouchingCosmos = didFinishTouchingCosmos
        //  view_for_textfield.didTouchCosmos =
        
        submitButton.CreateShadowRadius(shadowOpacity: 0.5, shadowRadius: 10, shadowColor: UIColor.blue.cgColor)
        
        print("this :\(view_for_star_Rating.rating)")
        
        //  shadow_for_view(view: view_for_textfield)
        // Do any additional setup after loading the view.
        
        lblRating.text = "Great"
        imgViewFace.image = UIImage.init(named:"5_num_")
        Ratings = 5
        view_for_star_Rating.rating = 5
        self.hideKeyboardWhenTappedAround()
      
      if(tradeProductDetail == nil)
      {
        self.navigationItem.leftBarButtonItem = nil
      }
    }
    
    //MARK: - Cosmos Delegate
    private func didTouchCosmos(_ rating: Double) {
        print("This is rating1 : \(rating)")
       
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        let com = Float(rating)
        print("This is rating2 : \(com)")
        let storeValue = UserDefaults.standard
        storeValue.set(com, forKey: "LastRating")
        Ratings = Int(rating)
        switch rating {
        case 1:
            lblRating.text = "Poor"
            imgViewFace.image = UIImage.init(named:"1_num_")
        case 2:
            lblRating.text = "Fair"
            imgViewFace.image = UIImage.init(named:"2_num_")
        case 3:
            lblRating.text = "Ok"
            imgViewFace.image = UIImage.init(named:"3_num_")
        case 4:
            lblRating.text = "Good"
            imgViewFace.image = UIImage.init(named:"4_num_")
        default:
            lblRating.text = "Great"
            imgViewFace.image = UIImage.init(named:"5_num_")
        }
    }
    
    //MARK: - TextView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    //MARK: - Other functions
    func SubmitRating()
    {
        if let userID = UserDefaults.standard.getUserID()
        {
            let urlStr = KrateSeller
            var parm = [String : Any]()
            print(productDetail?.sellerID as Any)
            print(productDetail?.sellerID as Any)
          
          if(tradeProductDetail != nil)
          {
            parm["seller_id"] = tradeProductDetail?.sender_id
              if(userID == tradeProductDetail?.sender_id)
              {
                parm["seller_id"] = tradeProductDetail?.reciver_id
              }
            
            parm["item_id"] = tradeProductDetail?.item_detail?.itemId
            parm["buyer_id"] = userID
            parm["rating"]  = Ratings
          }
          else
          {
            parm["seller_id"] = productDetail?.sellerID
            parm["buyer_id"] = userID
            parm["rating"]  = Ratings
            parm["item_id"] = productDetail?.id
          }
            parm["comment"] = ""
          if textView.textColor != UIColor.lightGray {
            parm["comment"] = textView.text
            }
            print(parm)
            print(urlStr)
            //dataLoaded = false
            Rating.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
                if let msg =  Response[Kmessage]
                {
                    print(msg)
                    UserDefaults.standard.setRatingData(value:nil)
                    self.AlertWithNavigatonPurpose(message: "Rating added successfully", navigationType: .root, ViewController: .none, rootViewController: .HomeNavigation,Data: nil)

                }
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.SubmitRating()
                        })
                    }
                  if let msg =  Response[Kmessage]
                  {
                    print(msg)
                    UserDefaults.standard.setRatingData(value:nil)
                    self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .pop, ViewController: .none, rootViewController: .none,Data: nil)
                    
                  }
                    else if statusCode == 201
                    {
                    }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
  @IBAction func BackAction(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)

  }
  @IBAction func SubmitAction(_ sender: Any) {
        SubmitRating()
    }
    
    //MARK: - TextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"
        {
            self.view.endEditing(true)
        }
        return true
    }
}

