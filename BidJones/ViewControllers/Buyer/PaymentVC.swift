
//
//  PaymentVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/22/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

//
//  ViewController.swift
//  stripeIntegration
//
//  Created by Kuldeep Singh on 9/4/17.
//  Copyright © 2017 Kuldeep Singh. All rights reserved.
//

import UIKit
import Stripe
class PaymentVC: UIViewController
{
    @IBOutlet weak var view_for_CVV: UIView!
    @IBOutlet weak var view_for_MMYY: UIView!
    @IBOutlet weak var view_for_CardNo: UIView!
    @IBOutlet var email: UITextField!
    @IBOutlet var cardNum: UITextField!
    @IBOutlet var exp: UITextField!
    @IBOutlet var cvc: UITextField!
    //    @IBOutlet var payButton: UIButton!
    
    @IBOutlet weak var PayButton: UIButton!
    
    //MARK: - Variable
    var productDetail:ItemListData?
    var TradeProductDetail:TradeRequestListData?
    var account_already:Int?
    var userType : String?
    var user_Id:Int?
    let url = "/user/detail"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        shadow_for_view(view: view_for_CardNo)
        shadow_for_view(view: view_for_CVV)
        shadow_for_view(view: view_for_MMYY)
        PayButton.CreateShadowRadius(shadowOpacity: 0.5, shadowRadius: 10, shadowColor: UIColor.blue.cgColor)
        if let bidAmount  = productDetail?.bidAmount
        {
            let taxAmount = (Double(bidAmount)!*3)/100
            let taxAmountRounded = taxAmount.rounded(toPlaces: 2).cleanValue

            let totalAmount = Double(taxAmountRounded)! + Double(bidAmount)!
            PayButton.setTitle("PAY $" + "\(totalAmount)", for: .normal)
        }
        
        if let bidAmount  = TradeProductDetail?.tradeAmount
        {
            let taxAmount = (Double(bidAmount)*3)/100
            let taxAmountRounded = taxAmount.rounded(toPlaces: 2).cleanValue
            
            let totalAmount = Double(taxAmountRounded)! + Double(bidAmount)
            PayButton.setTitle("PAY $" + "\(totalAmount)", for: .normal)
        }
        
        cardNum.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.cardNumNext_Action), Title: KNext)
        exp.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.expNext_Action), Title: KNext)
        cvc.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.cvcDone_Action), Title: KDone)
        self.hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Other functions
    func ShowAlert(msg:String)
    {
        //{
        let alertController = UIAlertController(title: KMessage, message: msg, preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.popBackToBidsView()
        }
        self.dismiss(animated: true, completion: nil)
        // Add the actions
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        // }
    }
    func popBackToBidsView()
    {
        if let viewControllers = navigationController?.viewControllers {
            for item in viewControllers {
                // some process
                if item.isKind(of: BidsVC.self)
                {
                    print("yes it is")
//                    if let obj =  item as? BidsVC
//                    {
//                       obj.DeleteItem()
//                    }
                    self.navigationController?.popToViewController(item, animated: true)
                }
            }
        }
    }
    func Validations() throws
    {
        guard let cardNumber = cardNum.text,  !cardNumber.isEmpty, !cardNumber.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyCardNum
        }
        if !cardNumber.MinimumRangeofTextFieldValue(minCharCount: 16, value: cardNumber)
        {
            throw ValidationError.incorrectCardNum
        }
        guard let expireDate = exp.text,  !expireDate.isEmpty, !expireDate.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyExpireDate
        }
      
        if !expireDate.MinimumRangeofTextFieldValue(minCharCount: 7, value: expireDate)
        {
            throw ValidationError.incorrectExpireDate
        }
        
            let com = expireDate
            let split = com.components(separatedBy: "/")
            
            let firstNumber: String = split[0]
            let lastNumber: String = split[1]
        
        let a = Int(firstNumber)!
        print(a)
        print(lastNumber)
        let year = Int (lastNumber)!
        print(year)
        let date = Date()
        let calendar = Calendar.current
        let year1 = calendar.component(.year, from: date)
         if a < 0 || a > 12 &&  year < year1
         {
            throw ValidationError.incorrectExpireMonthAndYear
        }
        
        if a < 0 || a > 12
        {
           throw ValidationError.incorrectExpireMonth
        }
      
        print("This is current year:\(year1)")
        if year < year1
        {
          throw  ValidationError.incorrectExpireYear
        }
        guard let cvc = cvc.text,  !cvc.isEmpty, !cvc.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyCvv
        }
        if !cvc.MinimumRangeofTextFieldValue(minCharCount: 3, value: cvc)
        {
            throw ValidationError.incorrectCvv
        }
    }
    @objc func cardNumNext_Action()
    {
        self.exp.becomeFirstResponder()
    }
    @objc func expNext_Action()
    {
        self.cvc.becomeFirstResponder()
    }
    @objc func cvcDone_Action()
    {
        self.cvc.resignFirstResponder()
    }
    func shadow_for_view(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 4.2
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize.init(width: 0.2, height: 0.2)
        view.clipsToBounds = false
    }
  
  
  func UpdatePayment()
  {
    
      var parm = [String : Any]()
      var urlStr = Ktradeupdatepayment
      
    
        
        parm["trade_id"] = TradeProductDetail?.id
        
        if let id = UserDefaults.standard.getUserID()
        {
          if(TradeProductDetail?.sender_id == id)
          {
               parm["payment_sender"] = id
               parm["payment_receiver"] = TradeProductDetail?.reciver_id

            
          }
          else
          {
            parm["payment_sender"] = id
            parm["payment_receiver"] = TradeProductDetail?.sender_id

          }
          
          parm["buyer_id"] = id
        }
    
    if let bidAmount  = TradeProductDetail?.tradeAmount
        {
          let taxAmount = (Double(bidAmount)*3)/100
          let taxAmountRounded = taxAmount.rounded(toPlaces: 2).cleanValue
          let totalAmount = Double(taxAmountRounded)! + Double(bidAmount)
          parm["amount"]  = "\(totalAmount)"
        }
    
  
      print(parm)
      Payment.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
        print(Response)
        if let msg =  Response[Kmessage]
        {
//          if self.productDetail?.categoryID == 4 &&  self.productDetail?.transcationNum != 3
//          {
//            self.ShowAlert(msg: msg as! String)
//          }
//          else
//          {
            if self.TradeProductDetail != nil
            {
              self.AlertWithNavigatonPurpose(message: "Payment done successfully", navigationType: .root, ViewController: .none, rootViewController: .HomeNavigation,Data: nil)
            }
            else
            {
              self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .push, ViewController: .Rating, rootViewController: .none,Data: self.productDetail)
            }
          //}
          //                    {
          //                        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
          //                            guard viewControllers.count < nb else {
          //                                print(viewControllers[viewControllers.count - nb]);
          //                                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
          //                                return
          //                            }
          //                        }
          //                    }
          
        }
        
      }, completionnilResponse: { (Response) in
        //print(Response)
        let statusCode = Response[Kstatus] as! Int
        if statusCode == 500
        {
          KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
            //print(resonse)
            self.UpdatePayment()
          })
        }
        if statusCode == 205
        {
          self.showAlertMessage(titleStr: KMessage, messageStr:"Payment Disabled for this item. Contact Seller")
          //                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
          //                        //print(resonse)
          //                        self.PayMethod(tokken: tokken)
          //                    })
        }
        else if let msg =  Response[Kmessage]
        {
          self.showAlertMessage(titleStr: KMessage, messageStr: msg as! String)
          //self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
          // self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .push, ViewController: .Rating, rootViewController: .none,Data: self.productDetail)
        }
      }, completionError: { (error) in
        self.showAlertMessage(titleStr: KMessage, messageStr:KError)
      },networkError: {(error) in
        self.showAlertMessage(titleStr: KMessage, messageStr: error)
      })
    
    }
    
    
  
    
    func PayMethod(tokken:String)
    {
        do {
            try Validations()
            var parm = [String : Any]()
            var urlStr = KsellermakePayment
            
            if(productDetail != nil)
            {

            if productDetail?.categoryID == 4
            {
                urlStr = KsellermakePartPayment
                parm["request_id"] = productDetail?.transactionID
            }
            else
            {
                parm["item_id"] = productDetail?.id
            }
            if let id = UserDefaults.standard.getUserID()
            {
                parm["buyer_id"] = id
            }
            parm["amount"]  = productDetail?.bidAmount
            
            
            if let bidAmount  = productDetail?.bidAmount
            {
                let taxAmount = (Double(bidAmount)!*3)/100
                let taxAmountRounded = taxAmount.rounded(toPlaces: 2).cleanValue
                let totalAmount = Double(taxAmountRounded)! + Double(bidAmount)!
                parm["amount"]  = "\(totalAmount)"
            }
            }
            else if TradeProductDetail != nil
            {
                parm["item_id"] = TradeProductDetail?.item_id
                if let id = UserDefaults.standard.getUserID()
                {
                    parm["buyer_id"] = id
                }
                parm["amount"]  = TradeProductDetail?.tradeAmount
                
                
                if let bidAmount  = TradeProductDetail?.tradeAmount
                {
                    let taxAmount = (Double(bidAmount)*3)/100
                    let taxAmountRounded = taxAmount.rounded(toPlaces: 2).cleanValue
                    let totalAmount = Double(taxAmountRounded)! + Double(bidAmount)
                    parm["amount"]  = "\(totalAmount)"
                }
            }
            
            parm["payment_token"] = tokken
            print(parm)
            Payment.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print(Response)
                if let msg =  Response[Kmessage]
                {
                    if self.productDetail?.categoryID == 4 &&  self.productDetail?.transcationNum != 3
                    {
                        self.ShowAlert(msg: msg as! String)
                    }
                    else
                    {
                      if self.TradeProductDetail != nil
                      {
                        self.UpdatePayment()
                      }
                      else
                      {
                        self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .push, ViewController: .Rating, rootViewController: .none,Data: self.productDetail)
                      }
                    }
//                    {
//                        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
//                            guard viewControllers.count < nb else {
//                                print(viewControllers[viewControllers.count - nb]);
//                                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
//                                return
//                            }
//                        }
//                    }
                   
                }
                
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.PayMethod(tokken: tokken)
                    })
                }
                if statusCode == 205
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:"Payment Disabled for this item. Contact Seller")
//                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                        //print(resonse)
//                        self.PayMethod(tokken: tokken)
//                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr: msg as! String)
                      //self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                   // self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .push, ViewController: .Rating, rootViewController: .none,Data: self.productDetail)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        } catch let error {
            switch  error {
            case ValidationError.emptyCardNum:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentercardnumber)
            case ValidationError.emptyCvv:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentercvv)
            case ValidationError.emptyExpireDate:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterexpiredate)
            case ValidationError.incorrectCardNum:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentervalidcardnumber)
            case ValidationError.incorrectExpireDate:
                    self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentervalidexpirationdate)
            case ValidationError.incorrectCvv:
                        self.showAlertMessage(titleStr: KMessage, messageStr:Pleaseentervalidcvv)
            case ValidationError.incorrectExpireMonth:
                self.showAlertMessage(titleStr: KMessage, messageStr:PleaseentervalidMonth)
            case ValidationError.incorrectExpireYear:
                self.showAlertMessage(titleStr: KMessage, messageStr:PleaseentervalidYear)
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KUnknownerror)
            }
    }
        
       
    }
    
    //MARK: - IBActions
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func getStripeToken(sender: AnyObject){
        self.view.endEditing(true)
        print("yes")
        do {
            try Validations()
            if let userType = UserDefaults.standard.getUserType(), userType == true {
                    MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
                               
                               let cardParams = STPCardParams()
                               cardParams.number = cardNum.text!
                               
                               if (!(exp.text!.isEmpty)){
                                   let expArr = exp.text?.components(separatedBy: "/")
                                   if (expArr!.count > 1)
                                   {
                                       let expMonth: NSNumber = Int(expArr![0])! as NSNumber
                                       let expYear: NSNumber = Int(expArr![1])! as NSNumber
                                       print("Month:\(expMonth.uintValue)")
                                       print("Year:\(expYear.uintValue)")
                                       cardParams.expMonth = expMonth.uintValue
                                       cardParams.expYear  = expYear.uintValue
                                   }
                               }
                               cardParams.cvc = cvc.text!
                               
                STPAPIClient.shared.createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
                                   
                                   if (error != nil){
                                       MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                   //                     self.AlertWithNavigatonPurpose(message: "fail", navigationType: .push, ViewController: .Rating, rootViewController: .none,Data: self.productDetail)
                                       //print(error?.localizedDescription as Any)
                                       print(error?.localizedDescription as Any)
                                       self.showAlertMessage(titleStr: KMessage, messageStr: (error?.localizedDescription)!)

                                       
                                   }
                                   else{
                                       print(token?.tokenId)
                                       self.PayMethod(tokken: (token?.tokenId)!)
                                   }
                //   KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: nil)
            }
            }else{
                 
            let checkvalue = self.isKeyPresentInUserDefaults(key:"ISStripeAccountExist")
            if(checkvalue){
            let check = UserDefaults.standard.getStripeExist()
            if(check!){
            
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            
            let cardParams = STPCardParams()
            cardParams.number = cardNum.text!
            
            if (!(exp.text!.isEmpty)){
                let expArr = exp.text?.components(separatedBy: "/")
                if (expArr!.count > 1)
                {
                    let expMonth: NSNumber = Int(expArr![0])! as NSNumber
                    let expYear: NSNumber = Int(expArr![1])! as NSNumber
                    print("Month:\(expMonth.uintValue)")
                    print("Year:\(expYear.uintValue)")
                    cardParams.expMonth = expMonth.uintValue
                    cardParams.expYear  = expYear.uintValue
                }
            }
            cardParams.cvc = cvc.text!
            
                STPAPIClient.shared.createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
                
                if (error != nil){
                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
//                     self.AlertWithNavigatonPurpose(message: "fail", navigationType: .push, ViewController: .Rating, rootViewController: .none,Data: self.productDetail)
                    //print(error?.localizedDescription as Any)
                    print(error?.localizedDescription as Any)
                    self.showAlertMessage(titleStr: KMessage, messageStr: (error?.localizedDescription)!)

                    
                }
                else{
                    print(token?.tokenId)
                    self.PayMethod(tokken: (token?.tokenId)!)
                }
            }
            }else{
                   
                self.MakeSellerShowAlert()
                                 
            }
            }
        }
        
        } catch let error {
            switch  error {
            case ValidationError.emptyCardNum:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentercardnumber)
            case ValidationError.emptyCvv:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentercvv)
            case ValidationError.emptyExpireDate:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterexpiredate)
            case ValidationError.incorrectCardNum:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentervalidcardnumber)
            case ValidationError.incorrectExpireDate:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentervalidexpirationdate)
            case ValidationError.incorrectCvv:
                self.showAlertMessage(titleStr: KMessage, messageStr:Pleaseentervalidcvv)
            case ValidationError.incorrectExpireMonth:
                self.showAlertMessage(titleStr: KMessage, messageStr:PleaseentervalidMonth)
            case ValidationError.incorrectExpireYear:
                self.showAlertMessage(titleStr: KMessage, messageStr:PleaseentervalidYear)
            case ValidationError.incorrectExpireMonthAndYear:
                self.showAlertMessage(titleStr: KMessage, messageStr:PleaseentervalidMonthandYear)
                
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KUnknownerror)
            }
       
      }
    }
    
    
    func apiCall(url:String,vc:UIViewController){
        
        //        MyProfile.sharedManager.GetApi(url: url, Target: vc, completionResponse: { (response) in
        //
        //            // self.profileInfo = response
        //            //response
        //
        //            print("now you get data profile : \(response)")
        //            if let StripeExist = response[0].stripe_account_id {
        //               let chkStripe = StripeExist
        //                print("chkStripe",chkStripe)
        //                if(StripeExist.contains("<null>") || StripeExist.isEmpty){
        //                    //self.isStripeExist = false
        //
        //                    UserDefaults.standard.setStripeExist(value: false)
        //
        //                }else if(!(StripeExist.contains("<null>"))){
        //                     UserDefaults.standard.setStripeExist(value: true)
        //                }else{
        //                    print("Something wrong happend")
        //                }
        //
        //                self.checkPermissonToSell()
        //
        //
        //            }else{
        //                print("error ")
        //            }
        //
        //
        //
        //
        //
        //        }, completionnilResponse: { (Response) in
        //            //print(Response)
        //            let statusCode = Response[Kstatus] as! Int
        //            if statusCode == 500
        //            {
        //                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
        //                    //print(resonse)
        //
        //                })
        //            }
        //            else if statusCode == 201
        //            {
        //                let message =  Response["message"]
        //
        //
        //
        //            }
        //
        //
        //
        //
        //
        //        }, completionError: { (error) in
        //            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        //
        //        }, networkError: {(error) in
        //            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        //
        //        })
        
        
        
        
        
        
        
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        var chckRtoS:Int?
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            print("Make me seller",Response)
            let status = Response["status"] as! Int
            print("status",status)
            //            let message = Response["message"] as? String
            //            print("message",message)
            if let accountalready = Response["account_already"] as? Int{
                self.account_already = accountalready
            }
            print("account_already",self.account_already)
            let data  = Response["result"] as! [String:Any]
            self.user_Id =  data["id"] as? Int
            //            print("data",data)
            if let reToStripe = data["redirect_to_stripe"] as? Int{
                print("reToStripe",reToStripe)
                chckRtoS  =  reToStripe
            }
            let stripe_account_id = data["stripe_account_id"] as? Any
            print("stripe_account_id",stripe_account_id)
            let stpID = "\(stripe_account_id!)"
            print(stpID)
            
            if(status == 200){
                // id not null and account already // 3rd check+
                if(!(stpID.contains("<null>"))){
                    UserDefaults.standard.setUserType(value: true)
                    self.account_already = 1
                    UserDefaults.standard.setStripeExist(value: true)
                    KCommonFunctions.PushToContrller(from: self, ToController: .Invoice, Data: nil)
                }
                
                //account already = 0
                else if(chckRtoS == 1 || self.account_already == 0){
                    print("Web Open")
                     self.MakeSellerShowAlert()
                   
                    
                    
                    //web Open
                    // id null but account already 1
                    //send message of popup link
                }else if(self.account_already == 1){
                    print("Message")
                    self.showAlertMessage(titleStr: KMessage, messageStr:  "We sent a link to your registered email to connect your stripe account with BidJones to receive payments.")
                    
                    
                }
            }
            
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
 }
        func MakeMeSeller()  {
            guard let id = UserDefaults.standard.getUserID()else {
                return
            }
            let urlStr = KmakeMeSeller + String(id)
            Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
                //print(Response)
                if let msg =  Response[Kmessage]
                {
                    //                self.arrBooks.remove(at: self.indexDelete!)
                    //                self.tblBooksList.reloadData()
                    //                self.isDeleted = true
                    //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
    //                UserDefaults.standard.setUserType(value: true)
    //               // self.showToast(message: msg as! String)
    //                self.AddMoreAction(AnyObject.self)
                    
                    if let showMessage = Response[KshowMessage] as? Int , showMessage == 1{
                      //  UserDefaults.standard.setUserType(value: true)
                        
                        UserDefaults.standard.setShowMessageType(value: "1")
                        
                        self.userType = UserDefaults.standard.getMessageType()
                        
                        print("Here your userType is \(self.userType)")
                        //self.showToast(message: msg as! String)
                        //self.AddMoreAction(AnyObject.self)
                        // self.goToAddScreen()
                        
                    }
                    else {
                        
                        
                        
                        //  print("Here your userType is \(self.userType)")
                        
                        UserDefaults.standard.setShowMessageType(value: "0")
                        
                        self.userType = UserDefaults.standard.getMessageType()
                        
                        print("Here your userType is \(self.userType)")
                        //  self.goToAddScreen()
                        
                       // self.AddMoreAction(AnyObject.self)
                    }
                    
                    
                    
                }
                
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.MakeMeSeller()
                    })
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
    //MARK: - Other function
    func MakeSellerShowAlert(){
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }

            let alertController = UIAlertController(title: KMessage, message: "You must have a stripe account linked with bidjones.Tap Continue to Create.", preferredStyle: .alert)
            
            // Create the actions
            let NoAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
                UIAlertAction in
    //            var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.user_Id!)"
                
             //   self.apiCall(url: self.url, vc: self)
                
    //            if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
    //                UIApplication.shared.openURL(url)
    //            }
               KCommonFunctions.PushToContrller(from: self, ToController: .WebView, Data: id)
                NSLog("OK Pressed")
            }
            let YesAction = UIAlertAction(title: "Abort", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            self.dismiss(animated: true, completion: nil)
            alertController.addAction(NoAction)
            alertController.addAction(YesAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
    }
   
    //MARK: - Other actions

    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

  
}
extension PaymentVC : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // CardNumber
        if textField == cardNum {
            
            if  (cardNum.text?.count)! == 16 && string.count != 0 {
                return false
            }
            
        }
        
        // Expiry Date and Month
        
        if textField == exp {
            
            print("This is : \((exp.text?.count)!) ")
            if  (exp.text?.count)! == 2 && string.count != 0 {
                let z = "/"
                textField.text! = textField.text!  + z
                print("this is :\(textField)")
            }
            if  (exp.text?.count)! == 7 && string.count != 0 {
               return false
            }
            if string == ""
            {
                textField.text = ""
                return false
            }
           
        }
          //   CVC of Card
        if textField == cvc {
            
            if  (cvc.text?.count)! == 3 && string.count != 0 {
                return false
            }
            
        }
       
        return true
    }
    
    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
}


extension UIButton {
    
    func CreateShadowRadius(shadowOpacity:Float,shadowRadius:CGFloat,shadowColor:CGColor)  {
        
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = false
        
    }
    
    
    
    
}

