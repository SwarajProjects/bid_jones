//
//  RatingListVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 7/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class RatingListVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var lblNoItem: UILabel!
    var productDetail:ProductData?
    var arrRatingList = [RatingData]()
    var count = 0
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var userId = Int()

    @IBOutlet var tblRatingList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoItem.isHidden = true
        tblRatingList.estimatedRowHeight = 1000
        tblRatingList.rowHeight = UITableViewAutomaticDimension
        tblRatingList.tableFooterView = UIView()
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.automaticallyAdjustsScrollViewInsets = false
        GetRatingList()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UISCrollview delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            if (arrRatingList.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.count = self.count+KPaginationcount
                GetRatingList()
            }
//            else if isDeleted!
//            {
//                isDeleted = false
//                print("222222222222")
//                isScrolling = true
//                print("reach bottom")
//                isLoading = true
//                GetRatingList()
//            }
            
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("11111111111")
        isScrolling = false
    }
    
    //MARK: - Rating List
    func GetRatingList() {
        var parm = [String : Any]()
        if let id = productDetail?.sellerId {
            parm["seller_id"] = id
        } else {
            parm["seller_id"] = userId
        }
        
        parm["offset"] = count
        parm["limit"]  = KPaginationcount
        
        let urlStr = Ksellerratings
        Rating.sharedManager.PostAPI(url: urlStr,parameter: parm, Target: self, completionResponse: { (Response) in
            print(Response)
            if Response.count>0
            {
                if(self.count == 0)
                {
                    // self.arrSongs = Response
                    for item in Response
                    {
                        let oldIds = self.arrRatingList.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrRatingList.insert(item, at: self.arrRatingList.count)
                        }
                    }
                    self.tblRatingList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrRatingList.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrRatingList.insert(item, at: self.arrRatingList.count)
                        }
                    }
                    
                    self.tblRatingList.reloadData()
                    
                }
                print(self.count)
                self.lblNoItem.isHidden = true
            }
                
            else
            {
                if(self.count != 0)
                {
                    print("abcdefgh")
                    self.dataLoaded = true
                }
                if (self.count == 0 || self.arrRatingList.count == 0)
                {
                    self.lblNoItem.isHidden = false
                }
            }
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.GetRatingList()
                })
            }
            else if statusCode == 400
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:"No Record found.")
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }

    @IBAction func Back_Button(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
}
extension RatingListVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
}

extension RatingListVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRatingList.count
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 100
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell : RatingListTableCell = tableView.dequeueReusableCell(withIdentifier: "ratingListTableCell") as! RatingListTableCell

         //   cell.delegate = self
            cell.LoadData(Data:arrRatingList[indexPath.row])
            cell.tag = indexPath.row
            cell.selectionStyle = .none
        
            return cell
        
    }
}
