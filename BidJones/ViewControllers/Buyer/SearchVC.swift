//
//  SearchVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/21/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import MapKit


class SearchVC: UIViewController {
    //MARK: - Outlets
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var lblCategory: CustomUILabel!
    @IBOutlet var lblUnit: CustomUILabel!
    @IBOutlet var lblRadius: CustomUILabel!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var txtFld_forOpeningPickers: UITextField!
    @IBOutlet var lblChooseSub: UILabel!
    @IBOutlet var lblSelect_Outer: CustomUILabel!
    @IBOutlet var lblSelect_inner: CustomUILabel!
    @IBOutlet var btnSubCategory: CustomButton!
    @IBOutlet var lblRadius_Width: NSLayoutConstraint!
    //MARK: - Variables
    var arrCategory = [CategoryData]()
    var arrRadius   = [String]()
    var arrUnit     = [String]()
    var arrGenre =   [GenreData]()
    var arrPicker = [Any]()
    
    
    private var categoryID:String?
    private var subCategoryID:String?

    var is_escrow = Bool()


    @IBOutlet weak var lbl_descriptionForEscrow: UILabel!
    
    @IBOutlet weak var lbl_No: UILabel!
    @IBOutlet weak var lbl_Yes: UILabel!
    private var pickerView = UIPickerView()

    enum PickerType: Int {
      
        case RadiusPicker = 1
        case UnitPicker = 2
        case CategoryPicker = 3
        case SubCategoryPicker = 4

        init() {
            self = .RadiusPicker
        }
    }
  
    private var pickerType: PickerType?
    
    override func viewDidLoad() {
        is_escrow = true
        categoryID = "0"
        subCategoryID = "0"
        super.viewDidLoad()
        pickerView.dataSource = self
        searchBar.delegate = self
        pickerView.delegate = self
        lblRadius.text = "World Wide"
        lblRadius_Width.constant = 100
        lblUnit.text = "Miles"
        lblCategory.text = "Select"
        categoryID = "0"
        
        ///categoryID = "4"
        lblCategory.text = "All"
        let obj0 = CategoryData(ID: 0, Name: "All")
        arrCategory.append(obj0)
        let obj3 = CategoryData(ID: 4, Name: "Services")
        arrCategory.append(obj3)
        let obj4 = CategoryData(ID: 5, Name: "Stuff")
        arrCategory.append(obj4)
        let obj2 = CategoryData(ID: 3, Name: "Music")
        arrCategory.append(obj2)
        let obj = CategoryData(ID: 1, Name: "Books")
        arrCategory.append(obj)
        let obj5 = CategoryData(ID: 6, Name: "Videos")
        arrCategory.append(obj5)
        let obj1 = CategoryData(ID: 2, Name: "Graphics")
        arrCategory.append(obj1)
        print(arrCategory)
        
        
        arrRadius.append("World Wide")
        
        for i in 1...20 {
            arrRadius.append(String(i*10))
        }
        for i in 3...15 {
            arrRadius.append(String(i*100))
        }
        arrRadius.append(String(2000
                                
        ))
        
        
        print(arrRadius)
        
        arrUnit.append("Miles")
        arrUnit.append("Kms")
        print(arrUnit)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFld_forOpeningPickers.inputAccessoryView = toolBar
        self.hideKeyboardWhenTappedAround()
        txtFld_forOpeningPickers.inputView = pickerView
        
        btnSubCategory.isUserInteractionEnabled = false
        lblChooseSub.alpha = 0.5
        lblSelect_Outer.alpha = 0.5
        lblSelect_inner.alpha = 0.5
        btnSubCategory.alpha = 0.5
    }

//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - IBAction
    @IBAction func SubCategoryAction(_ sender: Any) {
        if(
            arrGenre.count>0)
        {
            //isGenrePicker = false
            pickerType = PickerType(rawValue: 4)
            self.arrPicker = self.arrGenre
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(lblCategory.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrGenre, str: (lblSelect_inner.text)!, key: Kname,type:.GenreData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
    }
    @IBAction func KeyboardDisappear(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    @IBAction func UnitAction(_ sender: Any) {
        if(arrUnit.count>0)
        {
            pickerType = PickerType(rawValue: 2)
            self.arrPicker = self.arrUnit
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(lblUnit.text != "")
            {
                print(arrUnit)
                print(lblUnit.text ?? "")
                let index = arrUnit.index(of: lblUnit.text!)
                print(index ?? "Blank")
                pickerView.selectRow(index!, inComponent: 0, animated: false)
            }
        }
    }
    @IBAction func NumAction(_ sender: UIButton) {
        if(
            arrRadius.count>0)
        {
            pickerType = PickerType(rawValue: 1)
            self.arrPicker = self.arrRadius
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(lblRadius.text != "")
            {
                print(arrRadius)
                print(lblRadius.text ?? "")
                let index = arrRadius.index(of: lblRadius.text!)
                print(index ?? "Blank")
                pickerView.selectRow(index!, inComponent: 0, animated: false)
            }
        }
    }
    @IBAction func CategoryAction(_ sender: Any) {
        if(
            arrCategory.count>0)
        {
            //isGenrePicker = false
            pickerType = PickerType(rawValue: 3)
            self.arrPicker = self.arrCategory
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(lblCategory.text != "")
            {
                
                    
                    
                if let value =  self.GetIndexFromArrayForString(arr: arrCategory, str: (lblCategory.text)!, key: Kname,type:.CategoryData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
                    
               
                
                
            }
        }
    }
    @IBAction func YesAction(_ sender: Any) {
        is_escrow = true
        if let image  = UIImage(named: Kselected)
        {
            btnYes.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnNo.setImage(image, for: .normal)
        }
    }
    @IBAction func NoAction(_ sender: Any) {
        is_escrow = false
        if let image  = UIImage(named: Kselected)
        {
            btnNo.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnYes.setImage(image, for: .normal)
        }
    }
    @IBAction func SearchAction(_ sender: Any) {
       // guard let searchStr  = searchBar.text, !searchStr.isEmpty, !searchStr.trimmingCharacters(in: .whitespaces).isEmpty else
//        {
//            showAlertMessage(titleStr: KMessage, messageStr: KPleaseentersearchkeyword)
//            return
//        }
        guard let category  = lblCategory.text, !category.isEmpty, category != "Select", !category.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            showAlertMessage(titleStr: KMessage, messageStr: KPleaseselectcategory)
            return
        }
        print(searchBar.text ?? "Blank")
        print(lblRadius.text ?? "Blank")
        print(lblUnit.text ?? "Blank")
        print(Int(categoryID!) ?? "Blank")
        print(categoryID ?? "Blank")
        print(is_escrow)

        if lblRadius.text == "World Wide"
        {
           
            
            
            if is_escrow == true {
            
        let obj = SearchData(KeyWord: searchBar.text, Radius: "0", Unit: lblUnit.text, Category: lblCategory.text, CategoryID: Int(categoryID!), SubCategoryID: Int(subCategoryID!),IS_Escrow: is_escrow)
            KCommonFunctions.PushToContrller(from: self, ToController: .SearchList, Data: obj)

            }
            else {
                let obj = SearchData(KeyWord: searchBar.text, Radius: "0", Unit: lblUnit.text, Category: lblCategory.text, CategoryID: Int(categoryID!), SubCategoryID: Int(subCategoryID!),IS_Escrow: is_escrow)
                KCommonFunctions.PushToContrller(from: self, ToController: .SearchList, Data: obj)
                
            }
                
                
        }
        else
        {
            if is_escrow == true {
            
            let obj = SearchData(KeyWord: searchBar.text, Radius: lblRadius.text, Unit: lblUnit.text, Category: lblCategory.text, CategoryID: Int(categoryID!), SubCategoryID: Int(subCategoryID!),IS_Escrow: is_escrow)
            KCommonFunctions.PushToContrller(from: self, ToController: .SearchList, Data: obj)
                
            }
            else {
                
                let obj = SearchData(KeyWord: searchBar.text, Radius: lblRadius.text, Unit: lblUnit.text, Category: lblCategory.text, CategoryID: Int(categoryID!), SubCategoryID: Int(subCategoryID!),IS_Escrow: is_escrow)
                KCommonFunctions.PushToContrller(from: self, ToController: .SearchList, Data: obj)
                
                
            }
            
            
        }
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - Other function
    
    
    func GetGener(type:Int)  {
        let urlStr = KGenerApi + String(type)
        
       
        
        
        
        AddProduct.sharedManager.GetApi(url: urlStr,Target: self, completionResponse: { (Genre,BidType,maxImages) in
            self.arrGenre.removeAll()
            self.arrGenre = Genre
            
            print("array genre is : \(self.arrGenre)")
           
            if self.arrGenre.count > 0
            {
                var dic = [String:Any]()
                dic[Kid] = 0
                dic[Kname] = "All"
                let obj0 = GenreData(dict: dic)
                self.arrGenre.insert(obj0!, at: 0)
                self.lblSelect_inner.text = "All"
                self.subCategoryID = "0"
                
                self.btnSubCategory.isUserInteractionEnabled = true
                self.lblChooseSub.alpha = 1.0
                self.lblSelect_Outer.alpha = 1.0
                self.lblSelect_inner.alpha = 1.0
                self.btnSubCategory.alpha = 1.0
                
                self.btnNo.isHidden = false
                self.btnYes.isHidden = false
                
                self.lbl_Yes.isHidden = false
                
                self.lbl_No.isHidden = false
                
                
                self.lbl_descriptionForEscrow.isHidden = false
                
                if self.lblCategory.text == "Services" {
                  
                    self.btnSubCategory.isUserInteractionEnabled = false
                    self.lblChooseSub.alpha = 0.5
                    self.lblSelect_Outer.alpha = 0.5
                    self.lblSelect_inner.alpha = 0.5
                    self.btnSubCategory.alpha = 0.5
                    self.btnNo.isHidden = true
                    self.btnYes.isHidden = true
                    self.lbl_Yes.isHidden = true
                    self.lbl_No.isHidden = true
                    self.lbl_descriptionForEscrow.isHidden = true
                  
                }
            }
            else
            {
                self.subCategoryID = "0"
                self.lblSelect_inner.text = "Select"
                self.btnSubCategory.isUserInteractionEnabled = false
                self.lblChooseSub.alpha     =    0.5
                self.lblSelect_Outer.alpha  =    0.5
                self.lblSelect_inner.alpha  =    0.5
                self.btnSubCategory.alpha   =    0.5
            }
          
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    
                    self.GetGener(type: type)
                })
            }
            else if let msg =  Response[Kmessage] as? String
            {
                if msg == "Category does not exists."
                {
                self.subCategoryID = "0"
                self.btnSubCategory.isUserInteractionEnabled = false
                self.lblChooseSub.alpha = 0.5
                self.lblSelect_Outer.alpha = 0.5
                self.lblSelect_inner.alpha = 0.5
                self.btnSubCategory.alpha = 0.5
                }
               // self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
  
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        switch pickerType
        {
        case .RadiusPicker?:
            let radius = arrPicker[row] as! String
            lblRadius.text = radius
         if radius == "World Wide"
         {
            lblRadius_Width.constant = 100
        }
            else
         {
            lblRadius_Width.constant = 45
        }
          
        case .UnitPicker?:
            let unit = arrPicker[row] as! String
            lblUnit.text = unit
        case .CategoryPicker?:
            let dic = arrPicker[row] as! CategoryData
            let category = dic.name
            let id = dic.id
            let ID:String = String(id)
            categoryID = ID
            lblCategory.text = category
            if categoryID != "0"
            {
            self.GetGener(type: Int(categoryID!)!)
            }
            else
            {
                self.subCategoryID = "0"
                lblSelect_inner.text = "Select"
                self.btnSubCategory.isUserInteractionEnabled = false
                self.lblChooseSub.alpha = 0.5
                self.lblSelect_Outer.alpha = 0.5
                self.lblSelect_inner.alpha = 0.5
                self.btnSubCategory.alpha = 0.5            }
            
            if self.lblCategory.text == "Services" {
                
                
                self.btnNo.isHidden = true
                self.btnYes.isHidden = true
                
                self.lbl_Yes.isHidden = true
                
                self.lbl_No.isHidden = true
                
                
                self.lbl_descriptionForEscrow.isHidden = true
                
            }
            else
            {
                self.btnNo.isHidden = false
                self.btnYes.isHidden = false
                
                self.lbl_Yes.isHidden = false
                
                self.lbl_No.isHidden = false
                
                
                self.lbl_descriptionForEscrow.isHidden = false
                
                
            }
        case .none:
            print("none")
        case .SubCategoryPicker?:
            let dic = arrPicker[row] as! GenreData
            let category = dic.name
            let id = dic.id
            let ID:String = String(id)
            subCategoryID = ID
            lblSelect_inner.text = category
        }
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    @objc func CancelPicker()
    {
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    
}
extension SearchVC: UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.searchBar.endEditing(true)
    }
}
extension SearchVC: UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        print(arrPicker.count)
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dic = arrPicker[row]
        print(dic)
        var title = ""
        switch pickerType
        {
        case .RadiusPicker?:
            title = dic as! String
        case .UnitPicker?:
            title = dic as! String
        case .CategoryPicker?:
            title = (dic as! CategoryData).name
        case .none:
            print("none")
        case .SubCategoryPicker?:
            title = (dic as! GenreData).name
        }
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }
}
