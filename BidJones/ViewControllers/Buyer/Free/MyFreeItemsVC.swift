//
//  MyFreeItemsVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 5/14/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit

class MyFreeItemsVC: UIViewController,ProductTableCellDelegate {
    
    //MARK: - Label Outlets
    @IBOutlet var lblNoItem: UILabel!
    //MARK: - TextField Outlets
    //MARK: - TextView Outlets
    //MARK: - UItableView Outlets
    @IBOutlet var tblTradeList: UITableView!
    //MARK: - UISCrollView Outlets
    //MARK: - UIButton Outlets
    //MARK: - UIImageView Outlets
    //MARK: - UIView Outlets
    @IBOutlet var viewDelete: UIView!
    //MARK: - UIPicker Outlets
    //MARK: - Int Variable
    var count = 0
    var indexDelete:Int?
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isDeleted:Bool?
    var userType : String?
    
    
    //MARK: - String Variable
    //MARK: - array Variable
    private lazy var arrTrade = [ItemListData]()
    //MARK: - UIPickerView Variable
    //MARK: - dictionary Variable
    //MARK: - UIImagePickerController
    //MARK: - Enum Variable
    var productDetail:ProductData?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = false
        dataLoaded = false
        isScrolling = false
        lblNoItem.isHidden = true
        isDeleted = false
        
        viewDelete.isHidden = true
        self.title = "MY PRODUCTS"
        //GetList()
        //tblTradeList.estimatedRowHeight = 1000
        //  tblTradeList.rowHeight = UITableViewAutomaticDimension
        GetList()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - Other function
    func MakeSellerShowAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "You are not Registered as seller? If you want to post ad, Upgrade to seller", preferredStyle: .alert)
        
        // Create the actions
        let NoAction = UIAlertAction(title: "Upgrade", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.MakeMeSeller()
            NSLog("OK Pressed")
        }
        let YesAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        self.dismiss(animated: true, completion: nil)
        alertController.addAction(NoAction)
        alertController.addAction(YesAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: - Other actions
    func MakeMeSeller()  {
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            //print(Response)
            if let msg =  Response[Kmessage]
            {
                //                self.arrBooks.remove(at: self.indexDelete!)
                //                self.tblBooksList.reloadData()
                //                self.isDeleted = true
                //                //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                //                UserDefaults.standard.setUserType(value: true)
                //                //self.showToast(message: msg as! String)
                //                self.AddMoreAction(AnyObject.self)
                if let showMessage = Response[KshowMessage] as? Int , showMessage == 1{
                    UserDefaults.standard.setUserType(value: true)
                    
                    UserDefaults.standard.setShowMessageType(value: "1")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //self.showToast(message: msg as! String)
                    self.AddMoreAction(AnyObject.self)
                    // self.goToAddScreen()
                    
                }
                else {
                    //  print("Here your userType is \(self.userType)")
                    
                    UserDefaults.standard.setShowMessageType(value: "0")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //  self.goToAddScreen()
                    
                    self.AddMoreAction(AnyObject.self)
                }
                
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    func GetList(){
        let urlStr = Kfreemyfreeitems
        var parm = [String : Any]()
        if let id = UserDefaults.standard.getUserID()
        {
            parm[Kuser_id] = id
        }
        parm["offset"] = count
        parm["limit"]  = KPaginationcount
        
        parm[Kcategory_id] = KStuffType
        FetchItemList.sharedmanagerItemList.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            if Response.count>0
            {
                if(self.count == 0)
                {
                    //  self.arrTrade = Response
                    for item in Response
                    {
                        let oldIds = self.arrTrade.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrTrade.insert(item, at: self.arrTrade.count)
                        }
                    }
                    self.tblTradeList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrTrade.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrTrade.insert(item, at: self.arrTrade.count)
                        }
                    }
                    self.tblTradeList.reloadData()
                    print(self.arrTrade)
                }
                print(self.count)
                self.lblNoItem.isHidden = true
            }
            else
            {
                if(self.count != 0)
                {
                    print("abcdefgh")
                    self.dataLoaded = true
                }
                if (self.count == 0 || self.arrTrade.count == 0)
                {
                    self.lblNoItem.isHidden = false
                }
            }
        }
            , completionnilResponse: { (Response) in
                print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.GetList()
                    })
                }
                else if statusCode == 201
                {
                    self.dataLoaded = true
                    if (self.count == 0 || self.arrTrade.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                    }
                }
                else
                {
                    self.arrTrade.removeAll()
                    self.tblTradeList.reloadData()
                    self.lblNoItem.isHidden = false
                }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    //Mark CustomCell Delegate
    func DeleteButtonAction(_ sender: ProductTableCell) {
        viewDelete.isHidden = false
        //print(sender.tag)
        indexDelete = sender.tag
    }
    //MARK: - UISCrollview delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            if (arrTrade.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.count = self.count+KPaginationcount
                GetList()
            }
            else if isDeleted!
            {
                isDeleted = false
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                GetList()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    //MARK: - IBAction
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func YesAction(_ sender: Any){
        //print(indexDelete ?? "")
        self.viewDelete.isHidden = true
        let ItemID = arrTrade[indexDelete!].id
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        let urlStr = Ktradetrashitem + "/" + String(id) + "/" + String(describing: ItemID!)
        DeleteItem.sharedmanagerDeleteItem.GetApi(url: urlStr, Target: self, completionResponse: { (Response) in
            //print(Response)
            if let msg =  Response[Kmessage]
            {
                self.arrTrade.remove(at: self.indexDelete!)
                self.tblTradeList.reloadData()
                self.isDeleted = true
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.YesAction(AnyObject.self)
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    @IBAction func NoAction(_ sender: Any){
        viewDelete.isHidden = true
    }
    @IBAction func CancelAction(_ sender: Any) {
        viewDelete.isHidden = true
    }
    @IBAction func AddMoreAction(_ sender: Any) {
        //        guard let userType = UserDefaults.standard.getUserType(), userType == true else
        //        {
        //            MakeSellerShowAlert()
        //            return
        //        }
        //        KCommonFunctions.PushToContrller(from: self, ToController: .AddStuff, Data: nil)
        if let userType = UserDefaults.standard.getUserType(), userType == true {
            
            KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: nil)
            
            // KCommonFunctions.PushToContrller(from: self, ToController: .AddStuff, Data: nil)
        }
            
        else{
            
            
            if  let type  = UserDefaults.standard.getMessageType() , type == "1" || type == "0" {
                print("here yor type : \(type)")
                print("Here your userType is \(self.userType)")
                self.userType = type
                
                if  userType! == "1"  || userType! == "0"  {
                    
                    if userType! == "1" {
                        
                        userType = ""
                        UserDefaults.standard.setShowMessageType(value: "")
                        
                        showAlertMessage(titleStr: KMessage, messageStr:  "Upgraded as seller with Bidjones and Stripe is successfully. Please check your inbox and claim your stripe account to make sure that your payments arrive in your bank account without delay.")
                        
                        
                        
                    }
                    else {
                        
                        UserDefaults.standard.setUserType(value: true)
                        userType = ""
                        KCommonFunctions.PushToContrller(from: self, ToController: .AddStuff, Data: nil)
                        
                        
                    }
                    
                    
                    
                    
                }
            }
                
            else {
                
                MakeSellerShowAlert()
                
            }
            
        }
        
    }
}

extension MyFreeItemsVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        UserDefaults.standard.setShowEditButton(value: true)
        UserDefaults.standard.setShowSellerInfo(value: false)
        ///tableView.deselectRow(at: indexPath, animated: true)
        
        var data = arrTrade[indexPath.row]
        data.myFreeItem = 1
        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
    }
}

extension MyFreeItemsVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrTrade.count)
        return arrTrade.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell : ProductTableCell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell) as! ProductTableCell
        cell.delegate = self
        cell.LoadData(dic:arrTrade[indexPath.row], type: .Stuff)
        cell.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
}
