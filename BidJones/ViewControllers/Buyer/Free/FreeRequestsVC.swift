//
//  FreeRequestsVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 5/14/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit

class FreeRequestsVC: UIViewController,UIScrollViewDelegate,BidSentCellDelegate,BidRecieveCellDelegate,RemoveFreeItemDelegate {
    
    //MARK: - Outlets
    
    //Constarints
    @IBOutlet var viewSlideLeading: NSLayoutConstraint!
    //Table
    @IBOutlet var tblRecieveList: UITableView!
    @IBOutlet var tblSentList: UITableView!
    
    //Label
    @IBOutlet var lblNoItem: UILabel!
    
    //MARK: - Variables
    var countSent = 0
    var countRecieve = 0
    var selectedIndex = 0
    
    
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isReceiveSelected:Bool?
    var isDeleted:Bool?

    //Array
    private lazy var arrRecived = [TradeRequestListData]()
    private lazy var arrSent = [TradeRequestListData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MY REQUESTS"
        isLoading = false
        dataLoaded = false
        isScrolling = false
        isDeleted = false
        isReceiveSelected = false
        GetSentBidList()
        self.tblRecieveList.tableFooterView = UIView()
        self.tblSentList.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        
       // NotificationCenter.default.addObserver(self, selector: #selector(AcceptRejectRequestCallback(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name("NotificationIdentifier"), object: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBActions
    @IBAction func BackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SentRequestsAction(_ sender: Any)
    {
        self.lblNoItem.isHidden = true
        //self.tblRecieveList.bringSubview(toFront: tblSentList)
        tblRecieveList.isHidden = true
        tblSentList.isHidden = false
        
        isReceiveSelected = false
        self.viewSlideLeading.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrSent.count == 0
        {
            GetSentBidList()
        }
    }
    @IBAction func RecieveRequestsAction(_ sender: Any)
    {
        self.lblNoItem.isHidden = true
        
        //self.tblSentList.bringSubview(toFront: tblRecieveList)
        tblRecieveList.isHidden = false
        tblSentList.isHidden = true
        
        isReceiveSelected = true
        self.viewSlideLeading.constant = self.view.frame.size.width/2
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrRecived.count == 0
        {
            GetRecieveBidList()
        }
    }
    
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            if isReceiveSelected == true{
                if (arrRecived.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countRecieve = self.countRecieve+KPaginationcount
                    GetRecieveBidList()
                }
                else if isDeleted!
                {
                    isDeleted = false
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    GetRecieveBidList()
                }
            }
            else
            {
                if (arrSent.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countSent = self.countSent+KPaginationcount
                    GetSentBidList()
                }
                else if isDeleted!
                {
                    isDeleted = false
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    GetSentBidList()
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    
    
    //MARK: - Other Actions
    
   func RemoveFreeItemAction()
   {
    isDeleted = true
    if isReceiveSelected == true
    {
        arrRecived.remove(at: selectedIndex)
        tblRecieveList.reloadData()
        if(arrRecived.count == 0)
        {
            lblNoItem.isHidden = false
        }
    }
    else
    {
        arrSent.remove(at: selectedIndex)
        tblSentList.reloadData()
        if(arrSent.count == 0)
        {
            lblNoItem.isHidden = false
        }
    }
    }
    
    @objc func AcceptRejectRequestCallback(notification: Notification) {
        if isReceiveSelected == true
        {
            arrRecived.remove(at: selectedIndex)
            tblRecieveList.reloadData()
            if(arrRecived.count == 0)
            {
                lblNoItem.isHidden = false
            }
        }
        else
        {
            arrSent.remove(at: selectedIndex)
            tblSentList.reloadData()
            if(arrSent.count == 0)
            {
                lblNoItem.isHidden = false
            }
        }
        // Take Action on Notification
    }
    func DeleteItem()
    {
        print(selectedIndex)
        arrSent.remove(at: selectedIndex)
        tblSentList.reloadData()
        
    }
    func PaymentRequestAction(_ sender: BidRecieveCell) {
        KCommonFunctions.PushToContrller(from: self, ToController: .PaymentRequestSeller, Data: arrRecived[sender.tag])
    }
    func CheckOutAction(_ sender: BidSentCell) {
   
    }
    func GetRecieveBidList()
    {
        print(countRecieve)
        let urlStr = Kfreerecieverequests
        var parm = [String : Any]()
        if let id = UserDefaults.standard.getUserID()
        {
            parm[Kreceiver_id] = id
        }
        parm["offset"] = countRecieve
 parm["limit"]  = KPaginationcount
        
        //parm[Kcategory_id] = KStuffType
        Trade.sharedManager.PostApiForTradeRequestList(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            if Response.count>0
            {
                if(self.countRecieve == 0)
                {
                    //  self.arrTrade = Response
                    for item in Response
                    {
                        let oldIds = self.arrRecived.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrRecived.insert(item, at: self.arrRecived.count)
                        }
                    }
                    self.tblRecieveList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrRecived.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrRecived.insert(item, at: self.arrRecived.count)
                        }
                    }
                    self.tblRecieveList.reloadData()
                    print(self.arrRecived)
                }
                print(self.countRecieve)
                self.lblNoItem.isHidden = true
            }
            else
            {
                if(self.countRecieve != 0)
                {
                    print("abcdefgh")
                    self.dataLoaded = true
                }
                if (self.countRecieve == 0 || self.arrRecived.count == 0)
                {
                    self.lblNoItem.isHidden = false
                }
            }
        }
            , completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.GetRecieveBidList()
                    })
                }
                else if statusCode == 201
                {
                    self.dataLoaded = true
                    if (self.countRecieve == 0 || self.arrRecived.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                    }
                }
                else
                {
                    self.arrRecived.removeAll()
                    self.tblRecieveList.reloadData()
                    self.lblNoItem.isHidden = false
                }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
        
        
        
    }
    
    func GetSentBidList()
    {
        let urlStr = Kfreesendrequests
        var parm = [String : Any]()
        if let id = UserDefaults.standard.getUserID()
        {
            parm["sender_id"] = id
        }
        parm["offset"] = countSent
        parm["limit"]  = KPaginationcount
        
        //parm[Kcategory_id] = KStuffType
        Trade.sharedManager.PostApiForTradeRequestList(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            if Response.count>0
            {
                if(self.countSent == 0)
                {
                    //  self.arrTrade = Response
                    for item in Response
                    {
                        let oldIds = self.arrSent.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrSent.insert(item, at: self.arrSent.count)
                        }
                    }
                    self.tblSentList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrSent.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrSent.insert(item, at: self.arrSent.count)
                        }
                    }
                    self.tblSentList.reloadData()
                    print(self.arrSent)
                }
                print(self.countSent)
                self.lblNoItem.isHidden = true
            }
            else
            {
                if(self.countSent != 0)
                {
                    print("abcdefgh")
                    self.dataLoaded = true
                }
                if (self.countSent == 0 || self.arrSent.count == 0)
                {
                    self.lblNoItem.isHidden = false
                }
            }
        }
            , completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.GetSentBidList()
                    })
                }
                else if statusCode == 201
                {
                    self.dataLoaded = true
                    if (self.countSent == 0 || self.arrSent.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                    }
                }
                else
                {
                    self.arrSent.removeAll()
                    self.tblSentList.reloadData()
                    self.lblNoItem.isHidden = false
                }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    
    }
}

extension FreeRequestsVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        if tableView == tblRecieveList
        {
            return arrRecived.count
        }
        else
        {
            return arrSent.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblRecieveList
        {
            return UITableViewAutomaticDimension
        }
        else
        {
            return UITableViewAutomaticDimension
        }
        // return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblRecieveList
        {
            let cell : TradeRequestCell = tableView.dequeueReusableCell(withIdentifier: "TradeRequestCell") as! TradeRequestCell
            cell.LoadData(dic:arrRecived[indexPath.row], type: .Recieve)
            cell.tag = indexPath.row
            // cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell : TradeRequestCell = tableView.dequeueReusableCell(withIdentifier: "TradeRequestCell") as! TradeRequestCell
            cell.LoadData(dic:arrSent[indexPath.row], type: .Send)
            cell.tag = indexPath.row
            //cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
}
extension FreeRequestsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedIndex = indexPath.row
        if tableView == tblRecieveList
        {
            var data = arrRecived[indexPath.row]
            data.freeRecieve = 1;
            data.myProduct = 1;
            UserDefaults.standard.setShowEditButton(value: false)
            UserDefaults.standard.setShowSellerInfo(value: false)
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }
        else
        {
            var data = arrSent[indexPath.row]
            data.freeSent = 1;
            data.myProduct = 0;

            UserDefaults.standard.setShowEditButton(value: false)
            UserDefaults.standard.setShowSellerInfo(value: true)
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }
    }
}

