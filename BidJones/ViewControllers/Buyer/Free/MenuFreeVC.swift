//
//  MenuFreeVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 5/14/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit

class MenuFreeVC: UIViewController , UITableViewDataSource, UITableViewDelegate{
 
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
     *  Transparent button to hide menu
     */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        arrayMenuOptions.removeAll()
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        arrayMenuOptions.append([Ktitle:KHome, Kicon:"home"])
        arrayMenuOptions.append([Ktitle:KAlertsTrade, Kicon:Kalert_menu])
     //   arrayMenuOptions.append([Ktitle:KMyProducts, Kicon:"product"])
        arrayMenuOptions.append([Ktitle:KMyRequests, Kicon:"request"])
        //arrayMenuOptions.append([Ktitle:"Bids", Kicon:"bid_hover"])
        arrayMenuOptions.append([Ktitle:"Free History", Kicon:"trade"])
        //arrayMenuOptions.append([Ktitle:KHelp, Kicon:Khelp_menu])
        // arrayMenuOptions.append([Ktitle:KAboutUs, Kicon:Kabt_us_menu])
        //arrayMenuOptions.append([Ktitle:"Change Password", Kicon:"passwod_ic"])
        // arrayMenuOptions.append([Ktitle:KLogout, Kicon:Klogout])
        tblMenuOptions.reloadData()
    }
    
    func logout() {
        
        let id = UserDefaults.standard
        let loginUserID = id.getUserID()
        let isDevicetype = 1 as! NSNumber
        //NSNumber(1)
        
        let url = "/user/logout"
        let parm = ["is_mobile" : isDevicetype] as! [String : Any]
        
        Logout.sharedManager.PostApi(url: url, parameter: parm, Target: self , completionResponse: {
            (response) in
            
            print("my Logout result : \(response)")
            
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    //   self.GetRecieveBidList()
                    // self.getAlertData()
                    self.logout()
                })
            }
            else if statusCode == 201
            {
                
            }
            
            
            
            
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        
        
        
    }
    
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: KcellMenu)!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row][Kicon]!)
        lblTitle.text = arrayMenuOptions[indexPath.row][Ktitle]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
}
