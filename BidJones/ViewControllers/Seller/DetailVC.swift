 //
//  PreviewVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

 import UIKit
 import AVFoundation
 import AVKit
 import QuickLook
 import Cosmos
 import Photos

 
 extension Double {
   
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
 }
 extension Float {
   
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
//    func rounded(toPlaces places:Int) -> Float {
//        let divisor = pow(10.0, Double(places))
//        return (self * divisor).rounded() / divisor
//    }
 }
 
 
 class DetailVC: UIViewController,UIScrollViewDelegate,UITextFieldDelegate,CustomStripeDelegate ,LocationProtocol {
    
    //MARK:-  Outlets object
    @IBOutlet var lblSellerInfo: UILabel!
    @IBOutlet var btnSellerInfo: CustomButton!
    @IBOutlet var btnFreeBook: CustomButton!
    @IBOutlet var btnFreeSellerInfo: CustomButton!
    @IBOutlet var viewFree: UIView!
    @IBOutlet weak var btnRatingAction: UIButton!
    @IBOutlet var btnEdit: UIBarButtonItem!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var collectionViewImages: UICollectionView!
    @IBOutlet var tblViewDetail: UITableView!
    @IBOutlet var tblViewHeight: NSLayoutConstraint!
    @IBOutlet var leadingBtnPay: NSLayoutConstraint!
    
    @IBOutlet var lblDescrptionHeight: NSLayoutConstraint!
    @IBOutlet var viewSellerInfoHeight: NSLayoutConstraint!
    
    @IBOutlet var videoDiscripterConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet var lblvideoDescription: UILabel!
    
    
    @IBOutlet var txtFldAmount: UITextField!
    @IBOutlet var viewPayment: UIView!
    //MARK: -  UIView
    @IBOutlet var viewSellerInfo: UIView!
    @IBOutlet var viewRating: CosmosView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnAudio: UIButton!
    @IBOutlet var btnBid: CustomButton!
    @IBOutlet var btnVideo: UIButton!
    @IBOutlet var btnPdf: UIButton!
    @IBOutlet var btnTrade: CustomButton!
    @IBOutlet var btnChat: CustomButton!
    @IBOutlet var lblSales: UILabel!
    @IBOutlet var viewTrade: UIView!
    @IBOutlet var btnPay: CustomButton!
    @IBOutlet var btnTradeSellerInfo: CustomButton!
    
    @IBOutlet var btnVideoDescription: UIButton!
    @IBOutlet var btnDownload: UIButton!
    
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var lblBidLoc: UILabel!
    
    //MARK: - Variable
    var arrDetail = [[String:Any]]()
    private var arrImages = [ImagesData]()
    private var arrPreviewImages = [ImagesData]()
    
    private var arrVideos = [VideoData]()
    private var arrVideosDescription = [VideoData]()
    
    private var arrAudios = [AudioData]()
    private var arrPdf =    [PdfData]()
    var userType : String?
    var account_already:Int?
    let url = "/user/detail"
    var user_Id:Int?
    var acceptedBtnShown = 0
    var urlSong : URL?
    var latitude: String?
    var longitude: String?
    //MARK: - TextField
    @IBOutlet var txtFldAmountTrade: UITextField!
    
    @IBOutlet weak var viewBid: UIView!
    //MARK: - Arrays
    var arrImagesUrl  = [String]()
    var arrVideosUrl  = [String]()
    var arrAudiosUrl  = [String]()
    var arrPdfUrl     = [String]()
    var downloadUrl   :  URL?
    var soldStatus : String?
    
    var idProduct : String?
    var idSeller : String?
    var videoDownloadURL : URL?
    @objc var videoButton = UIButton()
    
    var show = false
    var selectedVideoType : String?
    var radioiImg:UIImage = #imageLiteral(resourceName: "back_arrow")
    
    var radioView  = UIButton()
    
    
    var productDetail:ProductData?
    var tradeProduct:TradeRequestListData?
    var FreeProduct : FreeRequestListData?
    
    var product:ItemListData?
    var timerObserver:Any?
    var isItemList = false
    //PDF
    let preview = QLPreviewController()
    let tempURL = FileManager.default.temporaryDirectory.appendingPathComponent("BIDJONES.pdf")
    private var count = 0
    private var totalCount = 0
    weak var delegate: SearchBidItemDelegate?
    weak var removedelegate: RemoveFreeItemDelegate?
    
    
    var isType = false
    
    var tableViewHeight: CGFloat {
        tblViewDetail.layoutIfNeeded()
        return tblViewDetail.contentSize.height
    }
    //for Vidoe Player
    var player              =  AVPlayer()
    var stopped             =  false
    var playerController    = AVPlayerViewController()
    var timer               : Timer? = nil
    var timeToStop          : Int    = 10
    var playerObserver      : AnyObject!
    var audioPlayer         : AVAudioPlayer?
    var playerItem          : AVPlayerItem!
    var alertShow           = false
    
    var window :UIWindow = UIApplication.shared.keyWindow!
    
    
    @IBOutlet weak var txtfieldLocation: UITextField!
    
    @IBOutlet weak var btn_accept: UIButton!
    
    
    @IBOutlet weak var btn_reject: UIButton!
    
    override func viewDidLoad(){
        self.automaticallyAdjustsScrollViewInsets = false
        super.viewDidLoad()
        if let value = UserDefaults.standard.getShowEditButton(),value == false {
            self.navigationItem.rightBarButtonItem = nil
        }
        self.automaticallyAdjustsScrollViewInsets = false
        lblSales.isHidden = true
        print(product ?? "Blank")
        tblViewDetail.estimatedRowHeight = 1000
        tblViewDetail.rowHeight = UITableViewAutomaticDimension
        //gurleen
        setTitles()
        GetDetail()
        txtFldAmount.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.DoneAction), Title: KDone)
        txtFldAmountTrade.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.DoneAction), Title: KDone)
        
        self.hideKeyboardWhenTappedAround()
        
        
        radioView = UIButton(frame: CGRect(x: self.view.frame.width - 90, y: 150, width: radioiImg.size.width + 12, height: radioiImg.size.height + 6))
        //     radioView = UIButton(frame: CGRect(x: self.view.frame.width - 90, y: 150, width: 60, height: 25))
        radioView.addTarget(self, action:#selector(self.downloadImage), for: .touchUpInside)
        self.radioView.layer.cornerRadius = 6; // this value vary as per your desire
        self.radioView.clipsToBounds = true
        self.radioView.isHidden = true
        
        
        
        //   self.videoButton = UIButton(frame: CGRect(x: self.view.frame.width - 80, y: 80, width: 25, height: 25))
        self.videoButton = UIButton(frame: CGRect(x: self.view.frame.width - 120, y: 90, width: 100, height: 25))
        
        self.videoButton.layer.cornerRadius = 6; // this value vary as per your desire
        self.videoButton.clipsToBounds = true;
        
        
    }
    func setTitles(){
        if let category = product , let catId = category.categoryID{
            self.lblBidLoc.text = category.address
            switch catId{
            case 1:
                self.title = "BOOK DETAIL"
                break
            case 2:
                self.title = "GRAPHIC DETAIL"
                break
            case 3:
                self.title = "MUSIC DETAIL"
                break
            case 4:
                self.title = "SERVICE DETAIL"
            case 5:
                self.title = "STUFF DETAIL"
            case 6:
                self.title = "VIDEO DETAIL"
            default:
                self.title = "PRODUCT DETAIL"
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.videoButton.addTarget(self, action: #selector(self.downloadImage), for: .touchUpInside)
        if alertShow == true
        {
            self.showAlertMessage(titleStr: KMessage, messageStr: "video Successfully saved")
            alertShow = false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        // self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        print("Table Height : \(self.tblViewDetail.contentSize.height)")
        self.tblViewHeight?.constant = self.tblViewDetail.contentSize.height
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if show == true
        {
            self.videoButton.isHidden = true
            show = false
        }
    }
    
    //MARK: CustomStripeDelegate
    
    func StripeAccountCreated() {
        
        UserDefaults.standard.setUserType(value: true)
    }
    
    func StripeAccountNotCreated() {
        
    }
    
    //MARK: - Other functions
    
    func MakeSellerShowAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "You must have a stripe account linked with bidjones.Tap Continue to Create.", preferredStyle: .alert)
        
        // Create the actions
        let NoAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
            UIAlertAction in
            // self.MakeMeSeller()
            //        var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.user_Id!)"
            //        if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
            //            UIApplication.shared.openURL(url)
            //        }
            KCommonFunctions.PushToContrller(from: self, ToController: .WebView, Data: self.user_Id!)
            
            NSLog("OK Pressed")
        }
        let YesAction = UIAlertAction(title: "Abort", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        self.dismiss(animated: true, completion: nil)
        alertController.addAction(NoAction)
        alertController.addAction(YesAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func MakeMeSeller()  {
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            //print(Response)
            if let msg =  Response[Kmessage]
            {
                //                self.arrBooks.remove(at: self.indexDelete!)
                //                self.tblBooksList.reloadData()
                //                self.isDeleted = true
                //                //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                //                UserDefaults.standard.setUserType(value: true)
                //                //self.showToast(message: msg as! String)
                //                self.AddMoreAction(AnyObject.self)
                if let showMessage = Response[KshowMessage] as? Int , showMessage == 1{
                    UserDefaults.standard.setUserType(value: true)
                    
                    UserDefaults.standard.setShowMessageType(value: "1")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //self.showToast(message: msg as! String)
                    self.TradeAction(nil)
                    // self.goToAddScreen()
                    
                }
                else {
                    //  print("Here your userType is \(self.userType)")
                    
                    UserDefaults.standard.setShowMessageType(value: "0")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //  self.goToAddScreen()
                    
                    self.TradeAction(nil)
                }
                
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    //MARK:- Key Observer
    @objc func DoneAction()
    {
        self.txtFldAmount.resignFirstResponder()
        self.txtFldAmountTrade.resignFirstResponder()
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 0
        {
            print("stop")
            stopped = true
        }
        else if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 1
        {
            print("play")
            if (self.playerController.player?.currentTime().seconds)! >= Double(10.0)
            {
                playerController.player?.seek(to: CMTimeMake(Int64(0), Int32(10)), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (result) in
                    print("result", result)
                })
            }
            stopped = false
        }
    }
    
    //MARK:- Functions
    @objc func stopPlaying()
    {
        player.pause()
    }
    
    func showAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "Purchase this item, to play full video!", preferredStyle: .alert)
        
        
        let OKAction = UIAlertAction(title: "Back", style: .default) { (action:UIAlertAction!) in
            self.playerController.dismiss(animated: true, completion: nil)
            
            
            
        }
        alertController.addAction(OKAction)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
        }
        alertController.addAction(cancelAction)
        
        
        self.playerController.present(alertController, animated: true, completion:nil)
    }
    
    func PlayVideo()
    {
        //        SDPhotosHelper.addNewVideo(withFileUrl: arrVideos[0].videoUrl!, inAlbum: KAlbumName, onSuccess: { (str) in
        //            print(str)
        //        }) { (error) in
        //            print(error.debugDescription)
        //        }
        if timerObserver != nil
        {
            self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
        }
        //  let item = AVPlayerItem(url: URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")!)
        
        if let type = selectedVideoType {
            
            if type == "videoDescription" {
                
                
                let item = AVPlayerItem(url: arrVideosDescription[0].videoUrl!)
                print(item)
                player =  AVPlayer(playerItem: item)
                
            }
            else {
                
                let item = AVPlayerItem(url: arrVideos[0].videoUrl!)
                print(item)
                player =  AVPlayer(playerItem: item)
            }
            
        }
        playerController.player = player
        if #available(iOS 11.0, *) {
            // use the feature only available in iOS 9
            // for ex. UIStackView
            self.playerController.setValue(true, forKey: "requiresLinearPlayback")
            
        } else {
            // or use some work around
        }
        //self.playerController.setValue(true, forKey: "requiresLinearPlayback")
        playerController.player?.play()
        if (self.playerController.player?.currentTime().seconds)! >= Double(10.0)
        {
            playerController.player?.seek(to: CMTimeMake(Int64(0), Int32(10)), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (result) in
                print("result", result)
            })
        }
        let times = [NSValue(time:CMTimeMake(Int64(timeToStop),1))]
        
        timerObserver = self.playerController.player?.addBoundaryTimeObserver(forTimes: times, queue: DispatchQueue.main, using:
                                                                                {
                                                                                    [weak self] in
                                                                                    
                                                                                    if let status = self?.soldStatus {
                                                                                        
                                                                                        //   print("your typed status : \(String(describing: status))")
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        if status ==  "Sold" {
                                                                                            print("your sold item here")
                                                                                        }
                                                                                        else {
                                                                                            print("not sold item is here")
                                                                                            if let type = self?.selectedVideoType {
                                                                                                print("your video type : \(type)")
                                                                                                if  type == "videoDescription" {
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                }
                                                                                                else {
                                                                                                    
                                                                                                    self?.stopPlaying()
                                                                                                    self?.showAlert()
                                                                                                }
                                                                                                
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                                                    }
                                                                                    
                                                                                })
        
        self.playerController.player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
        let playerView = playerController.view
        //   btn_Download.bringSubview(toFront: playerView!)
        
        self.videoButton.setTitle("Download", for: .normal)
        self.videoButton.setTitleColor(UIColor.blue, for: .normal)
        self.videoButton.titleLabel?.font.withSize(16)
        //        videoButton.backgroundColor = .white
        //        videoButton.setImage(#imageLiteral(resourceName: "download"), for: UIControlState.normal)
        //.setImage(#imageLiteral(resourceName: "download"), for: UIControlState.normal)
        //setBackgroundImage(UIImage(named: “testImage.png”), forState: UIControlState.Normal)
        
        
        
        playerView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        //        self.videoButton.isUserInteractionEnabled = true
        //
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9, execute: {
        //            // your code hear
        //        if HistoryVC.isMyPurchase == true {
        //
        //        self.videoButton.isHidden = false
        //
        //
        //            self.window.addSubview(self.videoButton)
        //
        //            }
        //
        //            })
        
        self.present(playerController,animated:true,completion:nil)
        self.show = true
    }
    func GetDetail() {
        print(product ?? "Blank")
        if let productID = product?.id, let sellerID = product?.sellerID
        {
            //  let url = KItemDetail + "\(sellerID)" + "/" + "\(productID)"
            var url = KItemDetail + "\(productID)"
            
            if(product?.FreeSearch == 1 || product?.myFreeItem == 1 || tradeProduct?.freeSent == 1 || tradeProduct?.freeRecieve == 1 || tradeProduct?.FreeHistory == 1)
            {
                url = "/free/freedetail/" + "\(productID)"
                
            }
            
            ProductDetail.sharedManager.GetApi(url: url, Target: self, completionResponse: { (Response) in
                print(Response)
                //                if(self.product?.FreeSearch == 1 || self.product?.myFreeItem == 1 || self.tradeProduct?.freeSent == 1 || self.tradeProduct?.freeRecieve == 1 || self.tradeProduct?.FreeHistory == 1)
                //                {
                //                }
                //                else
                //                {
                DispatchQueue.main.async{
                    self.getItemDetail(productID : productID)
                    
                    // }
                    //  print(productDetail?.lat)
                    //  print(productDetail?.lng)
                    self.LoadData(Data: Response)
                }
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        self.GetDetail()
                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else if  tradeProduct != nil
        {
            var productID:Int?
            var sellerID:Int?
            if(tradeProduct?.free_item_detail != nil)
            {
                if let id  = tradeProduct?.free_item_detail?.itemId
                {
                    productID = id
                    print(id)
                    print(productID)
                }
                if let id  = tradeProduct?.reciever_detail?.id
                {
                    sellerID = id
                }
            }
            else
            {
                if tradeProduct?.myProduct == 1
                {
                    if let id = UserDefaults.standard.getUserID()
                    {
                        if(id == tradeProduct?.sender_id)
                        {
                            if let id = tradeProduct?.trade_item_detail?.itemId
                            {
                                productID = id
                            }
                            if let id = tradeProduct?.sender_id
                            {
                                sellerID = id
                            }
                        }
                        else
                        {
                            if let id = tradeProduct?.item_detail?.itemId
                            {
                                productID = id
                                
                                
                            }
                            if let id = tradeProduct?.reciver_id
                            {
                                sellerID = id
                            }
                        }
                    }
                }
                else
                {
                    if let id = UserDefaults.standard.getUserID()
                    {
                        if(id == tradeProduct?.sender_id)
                        {
                            if let id  = tradeProduct?.item_detail?.itemId
                            {
                                productID = id
                            }
                            if let id = tradeProduct?.reciver_id
                            {
                                sellerID = id
                            }
                        }
                        else
                        {
                            if let id = tradeProduct?.trade_item_detail?.itemId
                            {
                                productID = id
                            }
                            if let id = tradeProduct?.sender_id
                            {
                                sellerID = id
                            }
                        }
                    }
                    
                }
            }
            print(productID)
            var url = KItemDetail + "\(productID!)"
            if(product?.FreeSearch == 1 || product?.myFreeItem == 1 || tradeProduct?.freeSent == 1 || tradeProduct?.freeRecieve == 1 || tradeProduct?.FreeHistory == 1)
            {
                url = "/free/freedetail/" + "\(productID!)"
                
            }
            print(url)
            ProductDetail.sharedManager.GetApi(url: url, Target: self, completionResponse: { (Response) in
                print(Response)
                if(self.product?.FreeSearch == 1 || self.product?.myFreeItem == 1 || self.tradeProduct?.freeSent == 1 || self.tradeProduct?.freeRecieve == 1 || self.tradeProduct?.FreeHistory == 1)
                {
                }
                else
                {
                    self.getItemDetail(productID : productID!)
                }
                //self.getItemDetail(productID : productID!)
                self.LoadData(Data: Response)
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        self.GetDetail()
                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else {
            // From MySearchAlertVC Screen to this screen
            
            
            if  let productID = idProduct, let sellerID = idSeller
            //  if let sellerID  = UserDefaults.standard.getUserID()
            {
                
                var url = KItemDetail + "\(productID)"
                if(product?.FreeSearch == 1 || product?.myFreeItem == 1 || tradeProduct?.freeSent == 1 || tradeProduct?.freeRecieve == 1 || tradeProduct?.FreeHistory == 1)
                {
                    url = "/free/freedetail/" + "\(productID)"
                    
                }
                ProductDetail.sharedManager.GetApi(url: url, Target: self, completionResponse: { (Response) in
                    print(Response)
                    
                    
                    self.LoadData(Data: Response)
                }, completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            self.GetDetail()
                        })
                    }
                    else if let msg =  Response[Kmessage]
                    {
                        self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                    }
                }, completionError: { (error) in
                    self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                },networkError: {(error) in
                    self.showAlertMessage(titleStr: KMessage, messageStr: error)
                })
                
            }
            
        }
    }
    func Location(lat: String, lng: String, address: String) {
        
        print("protcol address")
        print(address)
        print(lat)
        print(lng)
        
        txtfieldLocation.text = address
        self.latitude = lat
        self.longitude = lng
        
        //         AddProuductData.sharedInstance?.lat = Float(lat)
        //         //Float(place.coordinate.latitude)
        //         AddProuductData.sharedInstance?.lng = Float(lng)
        //
    }
    func getItemDetail(productID : Int) {
        
        print("check product ID : \(productID)")
        let url = "/itemdetail/" + "\(productID)"
        
        ItemDetail.sharedManager.GetApi(url: url, Target: self, completionResponse: { (response) in
            
            let btnShow =   response[0].btnshow!
            // self.profileInfo = response
            //response
            
            print("now you get data item Detail : \(response)")
            
            if  let soldStatus1 = response[0].status {
                
                
                self.soldStatus = soldStatus1
                
                if self.soldStatus! == "Publish" {
                    if self.arrVideos.count != 0 {
                        self.btnDownload.isHidden = false
                        self.btnDownload.setTitle("Sample", for: .normal)
                        self.btnDownload.setTitleColor(.black, for: .normal)
                        self.btnDownload.isUserInteractionEnabled = false
                    }
                    
                    if self.arrAudios.count != 0 {
                        self.btnDownload.isHidden = false
                        self.btnDownload.setTitle("Sample", for: .normal)
                        self.btnDownload.setTitleColor(.black, for: .normal)
                        self.btnDownload.isUserInteractionEnabled = false
                        
                    }
                    
                }
                else {
                    
                    
                }
            }
            
            
            if HistoryVC.isMyPurchase == true {
                if let showbutton = btnShow as? Int {
                    
                    print("you entered here this area")
                    
                    if showbutton == 1 {
                        self.acceptedBtnShown = 1
                        self.btn_accept.isHidden = false
                        self.btn_reject.isHidden = false
                        
                        
                        self.btnDownload.isHidden = false
                        self.btnDownload.setTitle("Sample", for: .normal)
                        self.btnDownload.setTitleColor(.black, for: .normal)
                        self.btnDownload.isUserInteractionEnabled = false
                        
                        //      self.btnDownload.isHidden = false
                    }
                    else {
                        self.acceptedBtnShown = 0
                        self.btn_accept.isHidden = true
                        self.btn_reject.isHidden = true
                        
                        if showbutton == 0 && self.soldStatus! == "Sold" {
                            self.btnDownload.isHidden = false
                            self.btnDownload.setTitle("Download", for: .normal)
                            self.btnDownload.setTitleColor(.blue, for: .normal)
                            self.btnDownload.isUserInteractionEnabled = true
                            
                        }
                        //                        if let type = self.soldStatus {
                        //                            if type == "Sold" {
                        //                                self.btnDownload.isHidden = false
                        //                                self.btnDownload.setTitle("Download", for: .normal)
                        //                                self.btnDownload.setTitleColor(.blue, for: .normal)
                        //                                self.btnDownload.isUserInteractionEnabled = true
                        //                            }
                        //
                        //                        }
                        
                    }
                    
                }
            }
            
            
            
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.getItemDetail(productID: (self.product?.id!)!)
                })
            }
            else if statusCode == 201
            {
                let message =  Response["message"]
                
                
                
            }
            
            
            
            
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        
        
    }
    
    @objc func downloadAudioSong() {
        
        if let song = arrAudios[0].audioURL as? URL {
            
            self.urlSong = song
        }
        
        if let audioUrl =  self.urlSong {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KDownloading
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            //            if FileManager.default.fileExists(atPath: destinationUrl.path) {
            //                print("The file already exists at path")
            //                DispatchQueue.main.async {
            //                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            //                }
            // if the file doesn't exist
            //    } else {
            
            // you can use NSURLSession.sharedSession to download the data asynchronously
            URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                guard let location = location, error == nil else { return }
                do {
                    // after downloading your file you need to move it to your destination url
                    
                    //  let saveFile = URL.init(string: destinationUrl as! String)
                    let name =    destinationUrl.lastPathComponent
                    
                    // print("name is : \(name)")
                    
                    //                       let splitArray = name.components(separatedBy: ".")
                    //                        var firstValue = splitArray[0]
                    //                        print("my value : \(firstValue)")
                    //UISaveVideoAtPathToSavedPhotosAlbum(Bundle.main.path(forResource:  "\(firstValue)", ofType: "mp3")!, nil, nil, nil)
                    
                    //                        let filePath = Bundle.main.path(forResource: "\(firstValue)", ofType: "mp3")
                    //                        print("path: \(filePath)")
                    //    UISaveVideoAtPathToSavedPhotosAlbum("\(location)", nil, nil, nil)
                    
                    //     let address = self.createFolder(folderName:"BidAudio")
                    
                    
                    try FileManager.default.moveItem(at: location, to: destinationUrl)
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        //  self.showToast(message : "Saved")
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.showAlertMessage(titleStr: "Bidjones", messageStr: "Download Successfully")
                        print("File moved to documents folder")
                    }
                } catch let error as NSError {
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        
                        print("your error is :\(error.localizedDescription)")
                        self.showAlertMessage(titleStr: "Bidjones", messageStr: "Download Successfully")
                    }
                }
            }).resume()
        }
        
        
    }
    
    
    //didTapBackButton
    @objc func downloadImage() {
        
        if let url = self.downloadUrl {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KDownloading
            var image = UIImage()
            if let data = try? Data(contentsOf: url) {
                image = UIImage(data: data)!
            }
            
            
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            self.showAlertMessage(titleStr: KMessage, messageStr: KsuccessDownloadImage)
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
        }
        
        
        if  let videoImageUrl = self.videoDownloadURL {
            
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KDownloading
            DispatchQueue.global(qos: .background).async {
                if  let urlData = NSData(contentsOf: videoImageUrl) {
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                    let filePath="\(documentsPath)/tempFile.mp4"
                    DispatchQueue.main.async {
                        urlData.write(toFile: filePath, atomically: true)
                        PHPhotoLibrary.shared().performChanges({
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                        }) { completed, error in
                            if completed {
                                
                                print("Video is saved!")
                                self.alertShow = true
                                DispatchQueue.main.async {
                                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                                    
                                    self.alertShow = true
                                    
                                }
                            }
                            else{
                                
                                DispatchQueue.main.async {
                                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                                }
                            }
                        }
                    }
                    
                    
                    
                    
                }
            }
            
            if alertShow == true {
                self.showAlertMessage(titleStr: KMessage, messageStr: "video Successfully saved")
                alertShow = false
            }
            
            
            
        }
    }
    
    
    //
    //    // 1
    //    let optionMenu = UIAlertController(title: "Download", message: "Choose Option", preferredStyle: .actionSheet)
    //
    //    // 2
    //    let photoAction = UIAlertAction(title: "Photo", style: .default, handler: { (action) -> Void in
    //        print("Ok button tapped")
    //
    //
    //
    //
    //
    //    })
    //        //UIAlertAction(title: "Delete", style: .default)
    //    let videoAction = UIAlertAction(title: "Video", style: .default, handler: { (action) -> Void in
    //        print("video button tapped")
    //
    //
    //
    //
    //    })
    //
    //    // 3
    //    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    //
    //    // 4
    //    optionMenu.addAction(photoAction)
    //    optionMenu.addAction(videoAction)
    //    optionMenu.addAction(cancelAction)
    //
    //    // 5
    //    self.present(optionMenu, animated: true, completion: nil)
    //
    //
    //
    
    
    
    
    
    
    // }
    
    //    func saveAudio(){
    //        let videoImageUrl = "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4"
    //
    //        DispatchQueue.global(qos: .background).async {
    //            if let url = URL(string: urlString),
    //                let urlData = NSData(contentsOf: url) {
    //                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
    //                let filePath="\(documentsPath)/tempFile.mp4"
    //                DispatchQueue.main.async {
    //                    urlData.write(toFile: filePath, atomically: true)
    //                    PHPhotoLibrary.shared().performChanges({
    //
    //                            //.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
    //                    }) { completed, error in
    //                        if completed {
    //                            print("Video is saved!")
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //
    //
    //    }
    
    
    
    func LoadData(Data : ProductData) {
        print(Data.lat ?? 0.0)
        print(Data.lng ?? 0.0)
        productDetail = Data
        
        
        if(productDetail?.isTradedProduct == 1)
        {
            self.navigationItem.rightBarButtonItem = nil
        }
        if(productDetail?.status == 1)
        {
            self.navigationItem.rightBarButtonItem = nil
        }
        if let itemType = Data.itemType {
            
            print("Your item : \(itemType)")
            
            if itemType == 1
            {
                isType = true
            }
            
            if itemType == 4 {
                isType = false
            }
            
        }
        
        if let bidType = Data.minBidTypeId
        {
            print("Your bid type :\(bidType)")
            print(Data.minBidType as Any)
            
            
            
            
            //            if bidType == 3
            //            {
            //                print("your amount : \(Data.minBidAmount!)")
            //
            //                if let amount = Data.minBidAmount
            //                {
            //                    = "$" + "\(amount)"
            //                txtFldAmount.isUserInteractionEnabled = false
            //                }
            //            }
        }
        if let id = UserDefaults.standard.getUserID()
        {
            if id == Data.sellerId
            {
                // viewSellerInfo.isHidden = true
                viewSellerInfoHeight.constant = 50
            }
        }
        if let value = UserDefaults.standard.getShowSellerInfo()
        {
            if value == false
            {
                viewSellerInfo.isHidden = true
                //  viewSellerInfoHeight.constant = 0
            }
        }
        
        
        
        
        if let bidAllowed = Data.bidAllowed
        {
            if bidAllowed == 0
            {
                
                btnBid.isHidden = true
                viewBid.isHidden = true
                txtFldAmount.isHidden = true
            }
            else
            {
                if let id = UserDefaults.standard.getUserID()
                {
                    if id == Data.sellerId
                    {
                        btnBid.isHidden = true
                        viewBid.isHidden = true
                        txtFldAmount.isHidden = true
                    }
                }
                
            }
        }
        if let Rating = Data.rating
        {
            viewRating.rating = Rating
        }
        print(productDetail?.musicNames as Any)
        
        if let itemType = Data.itemType, itemType == 2
        {
            if let imagesArr = Data.arrWatermarkImages
            {
                print(imagesArr)
                print("your image count arr: \(arrImages)")
                //self.downloadUrl = imagesArr[0].imgUrl!
                arrPreviewImages = imagesArr
                self.pageControl.numberOfPages = arrImages.count
                self.pageControl.currentPage = 0
                collectionViewImages.reloadData()
                if(imagesArr.count<2)
                {
                    pageControl.isHidden = true
                }
                
                radioView.setImage(#imageLiteral(resourceName: "download"), for: .normal)
                radioView.backgroundColor = .red
                // let radioButton:UIBarButtonItem = UIBarButtonItem(customView: radioView)
                
                //   let profileimage =
                
                //  let NotifyButton = UIBarButtonItem(customView: img_Logo)
                
                
                
                let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
                fixedSpace.width = 20.0
                
                //  navigationItem.rightBarButtonItems = [radioButton]
                
                self.view.addSubview(radioView)
                self.view.bringSubview(toFront: radioView)
                
            }
        }
        else if let imagesArr = Data.arrImages
        {
            print(imagesArr)
            arrImages = imagesArr
            self.pageControl.numberOfPages = arrImages.count
            self.pageControl.currentPage = 0
            collectionViewImages.reloadData()
            if(imagesArr.count<2)
            {
                pageControl.isHidden = true
            }
            
        }
        if let videosArr = Data.arrVideos
        {
            arrVideos = videosArr
            self.videoDownloadURL = videosArr[0].videoUrl!
            btnDownload.isHidden = false
            btnDownload.setTitle("Sample", for: .normal)
            btnDownload.isUserInteractionEnabled = false
            btnDownload.setTitleColor(.black, for: .normal)
            
            if let itemType = Data.itemType, itemType == 6
            {
                //                radioView.setImage(#imageLiteral(resourceName: "download"), for: .normal)
                //                radioView.backgroundColor = .red
                //                let radioButton:UIBarButtonItem = UIBarButtonItem(customView: radioView)
                //
                //                //   let profileimage =
                //
                //                //  let NotifyButton = UIBarButtonItem(customView: img_Logo)
                //
                //                let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
                //                fixedSpace.width = 20.0
                //
                //
                //                navigationItem.rightBarButtonItems = [radioButton]
            }
        }
        if let videoDescriptionArr = Data.arrVideosDiscript {
            arrVideosDescription = videoDescriptionArr
            //       videoDiscripterConstraintHeight.constant = 214
            
            btnPdf.translatesAutoresizingMaskIntoConstraints = false
            btnPdf.leadingAnchor.constraint(equalTo: btnVideoDescription.leadingAnchor).isActive = true
        }
        
        if let audiosArr = Data.arrAudios
        {
            arrAudios = audiosArr
            lblvideoDescription.isHidden = false
            
        }
        if let pdfArr = Data.arrPdf
        {
            arrPdf = pdfArr
        }
        
        if arrAudios.count == 0
        {
            btnVideo.translatesAutoresizingMaskIntoConstraints = false
            btnVideo.leadingAnchor.constraint(equalTo: btnAudio.leadingAnchor).isActive = true
            btnAudio.isHidden = true
        }else{
            btnAudio.isHidden = false
        }
        if arrVideos.count == 0
        {
            btnPdf.translatesAutoresizingMaskIntoConstraints = false
            btnPdf.leadingAnchor.constraint(equalTo: btnVideo.leadingAnchor).isActive = true
            btnVideo.isHidden = true
            btnDownload.isHidden = true
        }else{
            btnVideo.isHidden = false
            btnDownload.isHidden = false
        }
        if arrVideosDescription.count == 0
        {
            btnVideoDescription.isHidden = true
            lblvideoDescription.isHidden = true
            print("your crossing area from description count")
            if arrAudios.count != 0 {
                if arrVideos.count != 0 {
                    self.lblvideoDescription.isHidden = false
                }
            }
        }
        else {
            // videoDiscripterConstraintHeight.constant = 140
            
            btnVideoDescription.isHidden = false
            lblvideoDescription.isHidden = false
            
        }
        
        if arrPdf.count == 0
        {
            btnPdf.isHidden = true
        }else{
            btnPdf.isHidden = false
        }
        print(product?.isTradeItem ?? "")
        if let albumName = Data.title
        {
            var dic = [String:Any]()
            if(Data.itemType == 3)
            {
                dic["Album Name"] = albumName
                arrDetail.append(dic)
            }
        }
        
        if let artistName = Data.artistName
        {
            var dic = [String:Any]()
            if(tradeProduct == nil) && (product?.isTradeItem == nil)
            {
                dic["Artist Name"] = artistName
                arrDetail.append(dic)
            }
        }
        if let title = Data.title
        {
            var dic = [String:Any]()
            if let itemType = Data.itemType, itemType == 3
            {
                //dic["Song Title"] = title
                
            }
            else
            {
                dic["Title"] = title
                arrDetail.append(dic)
                
                
            }
            //
        }
        if let noOfPages = Data.noOfPages
        {
            var dic = [String:Any]()
            dic["No. Of Pages"] = noOfPages
            arrDetail.append(dic)
        }
        if let genre = Data.genre,let type = Data.itemType, type != 1
        {
            var dic = [String:Any]()
            dic["Genre"] = genre
            arrDetail.append(dic)
        }
        //        if let typeOfBook = Data.typeOfBook
        //        {
        //            var dic = [String:Any]()
        //            dic["Book Type"] = typeOfBook
        //            arrDetail.append(dic)
        //        }
        if let serviceType = Data.serviceType
        {
            print("your service type : \(serviceType)")
            
            if isType == false {
                
                var dic = [String:Any]()
                dic["Service Type"] = serviceType
                
                if let itemType = Data.itemType, itemType == 4
                {
                    arrDetail.append(dic)
                }
                
            }
            else
            {
                //                var dic = [String:Any]()
                //                dic["Genre"] = serviceType
                //                arrDetail.append(dic)
                
            }
            
            
            
            
            print(serviceType)
        }
        if let subcategory = Data.subCategoryName
        {
            
            if isType == true {
                
                print("your service type : \(subcategory)")
                
                var dic = [String:Any]()
                dic["Book type"] = subcategory
                arrDetail.append(dic)
                
            }
            //print(serviceType)
        }
        
        
        if let musicName = Data.musicNames
        {
            let musicNames =
                musicName.joined(separator: ",")
            var dic = [String:Any]()
            dic["Music name"] = musicNames
            arrDetail.append(dic)
        }
        if let urlBook = Data.bookUrl,let type = Data.itemType, type == 1
        {
            var dic = [String:Any]()
            dic["Book Url"] = urlBook
            arrDetail.append(dic)
        }
        if let TC = Data.TC
        {
            var dic = [String:Any]()
            if(tradeProduct == nil) && (product?.isTradeItem == nil)
            {
                dic["T&C of Service"] = TC
                arrDetail.append(dic)
            }
        }
        if let duration = Data.duration
        {
            var dic = [String:Any]()
            dic["Duration"] = duration
            arrDetail.append(dic)
        }
        //        if let bidType = Data.minBidType
        //        {
        //            var dic = [String:Any]()
        //            dic["Min Bid Type"] = bidType
        //            arrDetail.append(dic)
        //        }
        
        //        if let minBidAmount = Data.minBidAmount
        //        {
        //             var dic = [String:Any]()
        //
        //            print(minBidAmount.cleanValue)
        //
        //            dic["Minimum Bid Amount"] = "$" + minBidAmount.cleanValue
        //            arrDetail.append(dic)
        //
        //
        //        }
        if let minBidAmount = Data.minBidAmount
        {
            var dic = [String:Any]()
            print(minBidAmount.cleanValue)
            if(tradeProduct == nil) && (product?.isTradeItem == nil)
            {
                if let id = UserDefaults.standard.getUserID()
                {
                    if id == Data.sellerId
                    {
                        dic["Price"] = "$" + minBidAmount.cleanValue
                        arrDetail.append(dic)
                    }
                }
            }
            
            
            
        }
        if let keySearchWord = Data.keySearchWord
        {
            var dic = [String:Any]()
            dic["Key Search Words"] = keySearchWord
            arrDetail.append(dic)
        }
        if let location = Data.location
        {
            if location != ""
            {
                var dic = [String:Any]()
                dic["Location"] = location
                //  arrDetail.append(dic)
            }
        }
        print(arrDetail)
        tblViewDetail.reloadData()
        self.viewWillLayoutSubviews()
        print(tableViewHeight)
        if let description = Data.description
        {
            lblDescription.text = description
        }
        
        if Data.itemType == KTradeItemType
        {
            if let id = UserDefaults.standard.getUserID()
            {
                print(id)
                print(Data.sellerId!)
                
                if id != Data.sellerId
                {
                    viewTrade.isHidden = false
                }
            }
        }
        
        if(tradeProduct?.isHistory == 1)
        {
            btnTrade.alpha = 0.5
            btnTrade.isUserInteractionEnabled = false
            
            if(tradeProduct?.paymentComplete == true)
            {
                btnPay.alpha = 0.5
                btnPay.isUserInteractionEnabled = false
                // btnChat.alpha = 0.5
                
                // btnChat.isUserInteractionEnabled = false
            }
        }
        else if(tradeProduct?.isHistory == 0)
        {
            btnPay.alpha = 0.5
            btnPay.isUserInteractionEnabled = false
            btnTrade.alpha = 0.5
            btnTrade.isUserInteractionEnabled = false
            btnChat.alpha = 0.5
            btnChat.isUserInteractionEnabled = false
            //  btnPay.alpha = 0.5
        }
        else
        {
            btnChat.alpha = 0.5
            btnChat.isUserInteractionEnabled = false
            btnPay.alpha = 0.5
            btnPay.isUserInteractionEnabled = false
            
        }
        
        //        if(FreeProduct?.isHistory == 1)
        //        {
        //            btnTrade.isUserInteractionEnabled = false
        //
        //        }
        //        else if(tradeProduct?.isHistory == 0)
        //        {
        //            btnPay.alpha = 0.5
        //            btnPay.isUserInteractionEnabled = false
        //            btnTrade.alpha = 0.5
        //            btnTrade.isUserInteractionEnabled = false
        //            btnChat.alpha = 0.5
        //            btnChat.isUserInteractionEnabled = false
        //            //  btnPay.alpha = 0.5
        //        }
        //        else
        //        {
        //            btnChat.alpha = 0.5
        //            btnChat.isUserInteractionEnabled = false
        //            btnPay.alpha = 0.5
        //            btnPay.isUserInteractionEnabled = false
        //
        //        }
        
        if(product?.FreeSearch == 1)
        {
            viewTrade.isHidden = true;
            txtFldAmount.isHidden = true
            btnSellerInfo.isHidden = true
            lblSales.isHidden = true
            viewFree.isHidden = false
        }
        if(product?.myFreeItem == 1)
        {
            viewSellerInfo.isHidden = true;
            viewTrade.isHidden = true;
            viewFree.isHidden = true;
        }
        if(tradeProduct?.freeSent == 1)
        {
            viewSellerInfo.isHidden = false;
            txtFldAmount.isHidden = true;
            btnBid.isHidden = true;
            viewBid.isHidden = true
            lblSales.isHidden = true
            viewTrade.isHidden = true;
            viewFree.isHidden = true;
        }
        if(tradeProduct?.freeRecieve == 1)
        {
            viewSellerInfo.isHidden = true;
            viewTrade.isHidden = true;
            viewFree.isHidden = true;
            self.btn_accept.isHidden = false
            self.btn_reject.isHidden = false
        }
        if(tradeProduct?.FreeHistory == 1)
        {
            viewTrade.isHidden = true;
            txtFldAmount.isHidden = true
            btnSellerInfo.isHidden = true
            btnBid.isHidden = true
            viewBid.isHidden = true
            lblSales.isHidden = true
            viewFree.isHidden = false
            btnFreeBook.setTitle("Chat",for: .normal)
            btnFreeSellerInfo.setTitle("Rating",for: .normal)
            
        }
        
        if(tradeProduct?.FreeMyItemHistory == 1)
        {
            viewTrade.isHidden = true;
            txtFldAmount.isHidden = true
            btnSellerInfo.isHidden = true
            btnBid.isHidden = true
            viewBid.isHidden = true
            lblSales.isHidden = true
            viewFree.isHidden = false
            btnFreeBook.setTitle("Chat",for: .normal)
            btnFreeSellerInfo.setTitle("Rating",for: .normal)
            
            
        }
        
        
        if isItemList == true{
            viewLocation.isHidden = false
            /// self.lblBidLoc.text = Data.amount.address
            
            
        }
        
        
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                print(viewControllers[viewControllers.count - nb]);
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    //MARK: - IBActions
    @IBAction func FreeSellerInfoAction(_ sender: Any) {
        if(btnFreeSellerInfo.currentTitle == "Rating")
        {
            KCommonFunctions.PushToContrller(from: self, ToController: .Rating, Data: tradeProduct)
        }
        else
        {
            KCommonFunctions.PushToContrller(from: self, ToController: .SellerInfo, Data: productDetail)
        }
    }
    
    
    @IBAction func ActionMap(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Seller", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
        vc.locationDelegate = self;
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    @IBAction func ActionDownload(_ sender: Any) {
        
        if arrAudios.count != 0  {
            self.downloadAudioSong()
        }
        else {
            downloadImage()
        }
        
        
    }
    
    
    
    
    
    
    @IBAction func FreeBookAction(_ sender: Any?) {
        
        if(btnFreeBook.currentTitle == "Book")
        {
            let urlStr = Kfreebooking
            var parm = [String : Any]()
            parm["item_id"]  = productDetail?.itemId
            parm["status"]  = 8
            if let userID = UserDefaults.standard.getUserID()
            {
                parm["seller_id"]  = userID
            }
            
            
            print(parm)
            ProductDetail.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                
                if let msg =  Response[Kmessage]
                {
                    self.removedelegate?.RemoveFreeItemAction()
                    self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .pop, ViewController: .none, rootViewController: .none,Data:nil)
                }
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        self.FreeBookAction(nil)
                    })
                    
                    
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else
        {
            self.ChatAction(nil)
        }
        
        
    }
    @IBAction func TradePayAction(_ sender: UIButton) {
        txtFldAmountTrade.text = ""
        viewPayment.isHidden = false
    }
    @IBAction func ChatAction(_ sender: UIButton?) {
        if let itemID = productDetail?.itemId
        {
            if let sellerID = productDetail?.sellerId
            {
                print(sellerID)
                print(itemID)
                
                
                guard let userID = UserDefaults.standard.getUserID() else
                {
                    return
                }
                
                var dic = [String:Any]()
                
                if let id = UserDefaults.standard.getUserID()
                {
                    if(id == tradeProduct?.sender_id)
                    {
                        dic["username"] = (tradeProduct?.reciever_detail?.first_name)! + (tradeProduct?.reciever_detail?.last_name)!
                        dic["userimage"] = (tradeProduct?.reciever_detail?.image)!
                        print((tradeProduct?.reciever_detail?.image)!)
                        dic["imgName"] = (tradeProduct?.reciever_detail?.image)!
                        
                    }
                    else
                    {
                        // print(tradeProduct?.sender_detail?.first_name)
                        dic["username"] = (tradeProduct?.sender_detail?.first_name)! + (tradeProduct?.sender_detail?.last_name)!
                        dic["userimage"] = (tradeProduct?.sender_detail?.image)!
                        dic["imgName"] = (tradeProduct?.sender_detail?.image)!
                        print((tradeProduct?.sender_detail?.image)!)
                    }
                }
                
                print(dic)
                let Item:TalkData = TalkData.init(dict: dic)
                let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChatRoomVC") as! ChatRoomVC
                if let id = UserDefaults.standard.getUserID()
                {
                    vc.itemID = "\(tradeProduct?.item_detail?.itemId ?? 0)"
                    
                    if(id == tradeProduct?.sender_id)
                    {
                        vc.anotherUserID = "\(tradeProduct?.reciver_id ?? 0)"
                        
                    }
                    else
                    {
                        vc.anotherUserID = "\(tradeProduct?.sender_id ?? 0)"
                    }
                }
                
                vc.login_userID = "\(userID)"
                print(vc.itemID ?? "")
                print(vc.anotherUserID ?? "")
                print(vc.login_userID ?? "" )
                vc.talkData = Item
                // print(TalkData)
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @IBAction func CancelPayAction(_ sender: UIButton) {
        viewPayment.isHidden = true
    }
    @IBAction func PayAction(_ sender: UIButton)
    {
        guard let bidAmount  = txtFldAmountTrade.text, !bidAmount.isEmpty, !bidAmount.trimmingCharacters(in: .whitespaces).isEmpty, !bidAmount.replacingOccurrences(of: ".", with: "").isEmpty else
        {
            showAlertMessage(titleStr: KMessage, messageStr: KPleaseenterTradeAmount)
            return
        }
        if let amount = NumberFormatter().number(from: txtFldAmountTrade.text!)?.doubleValue
        {
            print("My double: \(amount)")
            tradeProduct?.tradeAmount = amount
            // productDetail?.minBidAmount = amount
            
            
        }
        // productDetail?.isTradedProduct = 1
        KCommonFunctions.PushToContrller(from: self, ToController: .Payment, Data: tradeProduct)
    }
    
    //
    //  }
    @IBAction func TradeAction(_ sender: CustomButton? = nil)
    {
        //
        //      if let userType = UserDefaults.standard.getUserType(), userType == true {
        //
        //        KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: productDetail?.itemId)
        //      }
        //
        //      else{
        //
        //        if  let type  = UserDefaults.standard.getMessageType() , type == "1" || type == "0" {
        //          print("here yor type : \(type)")
        //          print("Here your userType is \(self.userType)")
        //          self.userType = type
        //
        //          if  userType! == "1"  || userType! == "0"  {
        //
        //            if userType! == "1" {
        //
        //              userType = ""
        //              UserDefaults.standard.setShowMessageType(value: "")
        //
        //              showAlertMessage(titleStr: KMessage, messageStr:  "Upgraded as seller with Bidjones and Stripe is successfully. Please check your inbox and claim your stripe account to make sure that your payments arrive in your bank account without delay.")
        //
        //
        //
        //            }
        //            else {
        //
        //              UserDefaults.standard.setUserType(value: true)
        //              userType = ""
        //              KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: nil)
        //
        //
        //            }
        //
        //
        //
        //
        //          }
        //        }
        //
        //        else {
        //
        //          MakeSellerShowAlert()
        //
        //        }
        //
        //      }
        
        if let userType = UserDefaults.standard.getUserType(), userType == true {
            
            
            KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: productDetail?.itemId)
        }
        //First save stripe id at login and check here
        //if it is null then open webpage and
        //when it comes with id response
        
        else {
            let checkvalue = self.isKeyPresentInUserDefaults(key:"ISStripeAccountExist")
            if(checkvalue){
                let check = UserDefaults.standard.getStripeExist()
                if(check!){
                    //code RUN
                    KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: productDetail?.itemId)
                }else{
                    
                    self.apiCall(url: url, vc: self)
                }
                // self.checkPermissonToSell()
                //if exist then check value
                //if no again hit api and check
                //again no open web
                //if yes code run
                
                
            }else{
                
                //profile api hit
                
                self.apiCall(url: url, vc: self)
                //store to userdefalut
                //check if no then open web
                //if yes code run
            }
        }
        
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    func apiCall(url:String,vc:UIViewController){
        
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        var chckRtoS:Int?
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            print("Make me seller",Response)
            let status = Response["status"] as! Int
            print("status",status)
            //            let message = Response["message"] as? String
            //            print("message",message)
            if let accountalready = Response["account_already"] as? Int{
                self.account_already = accountalready
            }
            print("account_already",self.account_already)
            let data  = Response["result"] as! [String:Any]
            self.user_Id =  data["id"] as? Int
            //let state_Id =  data["state_id"] as? Int
            //            print("data",data)
            if let reToStripe = data["redirect_to_stripe"] as? Int{
                print("reToStripe",reToStripe)
                chckRtoS  =  reToStripe
            }
            let stripe_account_id = data["stripe_account_id"] as? Any
            print("stripe_account_id",stripe_account_id)
            let stpID = "\(stripe_account_id!)"
            print(stpID)
            
            if(status == 200){
                // id not null and account already // 3rd check+
                if(!(stpID.contains("<null>"))){
                    UserDefaults.standard.setUserType(value: true)
                    self.account_already = 1
                    UserDefaults.standard.setStripeExist(value: true)
                    KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: self.productDetail?.itemId)
                }
                
                //account already = 0
                else if(chckRtoS == 1 || self.account_already == 0){
                    print("Web Open")
                    self.MakeSellerShowAlert()
                    
                    
                    
                    //web Open
                    // id null but account already 1
                    //send message of popup link
                }else if(self.account_already == 1){
                    print("Message")
                    self.showAlertMessage(titleStr: KMessage, messageStr:  "We sent a link to your registered email to connect your stripe account with BidJones to receive payments.")
                    
                    
                }
            }
            
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
        
        
        
    }
    @IBAction func EditAction(_ sender: Any)
    {
        if let ItemType = productDetail?.itemType
        {
            
            switch ItemType {
            case 1:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddBook, Data: productDetail)
            case 2:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddGaraphics, Data: productDetail)
            case 3:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddMusic, Data: productDetail)
            case 4:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddSellService, Data: productDetail)
            case 5:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddStuff, Data: productDetail)
            case 6:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddVideos, Data: productDetail)
            case 7:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: productDetail)
            default:
                KCommonFunctions.PushToContrller(from: self, ToController: .AddFreeProduct, Data: productDetail)
                
            }
            print(ItemType)
            
        }
        
    }
    @IBAction func BidAction(_ sender: Any)
    {
        
        
        guard let bidAmount  = txtFldAmount.text, !bidAmount.isEmpty, !bidAmount.trimmingCharacters(in: .whitespaces).isEmpty, !bidAmount.replacingOccurrences(of: ".", with: "").isEmpty else
        {
            showAlertMessage(titleStr: KMessage, messageStr: KPleaseenterBidAmount)
            return
        }
        
        if let bidType = productDetail?.minBidTypeId
        {
            print(bidType)
            print(productDetail?.minBidType as Any)
            if bidType == 2
            {
                if let minAmount = productDetail?.minBidAmount
                {
                    if let amount = NumberFormatter().number(from: txtFldAmount.text!)?.doubleValue
                    {
                        print("My double: \(amount)")
                        print(minAmount)
                        if amount < minAmount
                        {
                            //showAlertMessage(titleStr: KMessage, messageStr: "Please enter minimum $\(minAmount.cleanValue)")
                            showAlertMessage(titleStr: KMessage, messageStr: "Your bid amount is too low.")
                            return
                        }
                        
                    }
                    
                }
            }
            if bidType == 3
            {
                if let minAmount = productDetail?.minBidAmount
                {
                    if let amount = NumberFormatter().number(from: txtFldAmount.text!)?.doubleValue
                    {
                        print("My double: \(amount)")
                        print(minAmount)
                        //gurleen
                        if amount != minAmount
                        {
                            showAlertMessage(titleStr: KMessage, messageStr: "The bid entered doesn't match with fix amount")
                            return
                        }
                    }
                }
            }
        }
        
        guard let bidloc  = txtfieldLocation.text, !bidloc.isEmpty else{
            showAlertMessage(titleStr: KMessage, messageStr: KPleaseenterLocation)
            return
        }
        
        
        
        
        let urlStr = KsellerpostBid
        var parm = [String : Any]()
        if let id = UserDefaults.standard.getUserID()
        {
            parm["buyer_id"] = id
        }
        let editedText = txtFldAmount.text?.replacingOccurrences(of: "$", with: "")
        parm["amount"] = editedText
        parm["item_id"]  = productDetail?.itemId
        parm["location"]  = bidloc
        parm["latitude"]  = self.latitude
        parm["longitude"]  = self.longitude
        print(parm)
        ProductDetail.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
            print(Response)
            if let msg =  Response[Kmessage]
            {
                //self.txtFldAmount.isHidden = true
                // self.btnBid.isHidden = true
                self.delegate?.DeleteBidItemAction()
                // dele
                // self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .pop, ViewController: .none, rootViewController: .none,Data:nil)
            }
            //self.LoadData(Data: Response)
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                //                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                //                    self.GetDetail()
                //                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    @IBAction func RatingAction(_ sender: Any)
    {
        KCommonFunctions.PushToContrller(from: self, ToController: .RatingList, Data: productDetail)
    }
    @IBAction func SellerInfoAction(_ sender: Any) {
        if(btnSellerInfo.currentTitle == "Chat")
        {
            self.ChatAction(nil)
            
        }
        else
        {
            KCommonFunctions.PushToContrller(from: self, ToController: .SellerInfo, Data: productDetail)
        }
        
        
    }
    @IBAction func AudioAction(_ sender: Any) {
        if self.checkInternetConnection()
        {
            print(productDetail as Any)
            detailType = .Detail
            
            // HistoryVC.isMyPurchase = true
            
            KCommonFunctions.PushToContrller(from: self, ToController: .AudioPlayer, Data: productDetail)
        }
        else
        {       self.showAlertMessage(titleStr: KMessage, messageStr: KNoInternetConnection)
        }
    }
    @IBAction func VidoeAction(_ sender: Any) {
        show = false
        if self.checkInternetConnection()
        {
            print(arrVideos)
            
            selectedVideoType = "video"
            
            print("your selected video type : \(selectedVideoType!)")
            
            //             self.downloadbutton = UIButton(frame: CGRect(x: self.view.frame.width - 125, y: 30, width: 35, height: 35))
            //            button.backgroundColor = .white
            //            button.setImage(#imageLiteral(resourceName: "download"), for: UIControlState.normal)
            
            
            PlayVideo()
        }
        else
        {       self.showAlertMessage(titleStr: KMessage, messageStr: KNoInternetConnection)
        }
        //        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .VideoPlayer, Data: arrVideos[0].videoUrl)
    }
    
    @IBAction func ActionVideoDescription(_ sender: UIButton) {
        
        selectedVideoType = "videoDescription"
        if self.checkInternetConnection()
        {
            print(arrVideos)
            PlayVideo()
        }
        else
        {       self.showAlertMessage(titleStr: KMessage, messageStr: KNoInternetConnection)
        }
    }
    @IBAction func Accept_button(_ sender: Any) {
        
        // params: status ,item_id"
        var urlStr = ""
        var parm = [String : Any]()
        let itemID = productDetail?.itemId!
        
        if((tradeProduct?.freeRecieve) != nil)
        {
            urlStr = "/free/acceptrejectrequest"
            
            parm = ["status": 2 , "request_id" : tradeProduct?.id! ?? ""]
            
        }
        else
        {
            urlStr = "/buyer/approveReject"
            parm = ["status": 1 , "item_id" : itemID!]
        }
        
        ProductDetail.sharedManager.PostApiForPayment(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
            print("Your get Accepted response: \(Response)")
            self.btn_accept.isHidden = true
            self.btn_reject.isHidden = true
            
            if self.arrVideos.count != 0 {
                
                self.btnDownload.isHidden = false
                self.btnDownload.setTitle("Download", for: .normal)
                self.btnDownload.setTitleColor(.blue, for: .normal)
                self.btnDownload.isUserInteractionEnabled = true
            }
            
            else if self.arrAudios.count != 0 {
                
                self.btnDownload.isHidden = false
                self.btnDownload.setTitle("Download", for: .normal)
                self.btnDownload.setTitleColor(.blue, for: .normal)
                self.btnDownload.isUserInteractionEnabled = true
                
                
            }
            
            
            
            let message = Response["message"]
            self.removedelegate?.RemoveFreeItemAction()
            
            //  NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
            self.showAlertMessage(titleStr: KMessage, messageStr: message as! String )
            
            //self.LoadData(Data: Response)
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                //                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                //                    self.GetDetail()
                //                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    @IBAction func Reject_button(_ sender: Any) {
        
        let alertController = UIAlertController(title: KMessage, message: " Are you sure you want to reject?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Reject", style: UIAlertActionStyle.destructive) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            
            var urlStr = ""
            var parm = [String : Any]()
            let itemID = self.productDetail?.itemId!
            
            if((self.tradeProduct?.freeRecieve) != nil)
            {
                urlStr = "/free/acceptrejectrequest"
                
                parm = ["status": 3 , "request_id" : self.tradeProduct?.id! ?? ""]
                
            }
            else
            {
                urlStr = "/buyer/approveReject"
                parm = ["status": 0 , "item_id" : itemID!]
            }
            
            ProductDetail.sharedManager.PostApiForPayment(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print(Response)
                self.removedelegate?.RemoveFreeItemAction()
                
                // NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                self.btn_accept.isHidden = true
                self.btn_reject.isHidden = true
                
                print("Your get Accepted response:\(Response)")
                
                let message = Response["message"]
                
                KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
                
                self.showAlertMessage(titleStr: KMessage, messageStr: message as! String )
                
                
                //self.LoadData(Data: Response)
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    //                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //                    self.GetDetail()
                    //                })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
            
            
            
            
            //userd// Bool
            //  KCommonFunctions.SetRootViewController(rootVC:.LoginNavigation)
        }
        let cancelAction = UIAlertAction(title: KCancel, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    
    @IBAction func PdfAction(_ sender: Any)
    {
        
        if self.checkInternetConnection()
        {
            
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = "Downloading..."
            
            let Url = arrPdf[0].pdfUrl
            
            
            // CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Pdf, Data: Url)
            preview.delegate = self
            preview.dataSource = self
            preview.currentPreviewItemIndex = 0
            URLSession.shared.dataTask(with: Url) { (data, response,error) in
                guard let data = data, error == nil else {
                    //  in case of failure to download your data you need to present alert to the user and update the UI from the main thread
                    DispatchQueue.main.async
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alert = UIAlertController(title: "Alert", message: error?.localizedDescription ?? "Failed to download the pdf!!!", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default))
                        
                        self.present(alert, animated: false)
                    }
                    return
                }
                
                do {
                    try data.write(to: self.tempURL, options: .atomic)
                    
                    DispatchQueue.main.async
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        if self.tempURL.typeIdentifier == "com.adobe.pdf"
                        {
                            
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            
                            
                            self.navigationController?.pushViewController(self.preview, animated: false)
                        }
                        else
                        {
                            print("the data downloaded it is not a valid pdf file")
                        }
                    }
                } catch {
                    print(error)
                    return
                }
            }.resume()
            
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        else
        {       self.showAlertMessage(titleStr: KMessage, messageStr: KNoInternetConnection)
        }
        
    }
    @IBAction func BackAction(_ sender: Any) {
        if timerObserver != nil
        {
            self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
        }
        HistoryVC.isMyPurchase = false
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - TextField delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtFldAmount || textField == txtFldAmountTrade
        {
            var characterSet = "0123456789."
            
            if textField == txtFldAmount {
                
                if string == "" {
                    
                    return true
                }
                if txtFldAmount.text!.count > 10 {
                    
                    return false
                }
                
                
                if (characterSet.contains(string))
                {
                    if txtFldAmount.text!.contains(".") && string.contains(".") {
                        
                        return false
                    }
                    return true;
                }
                else {
                    
                    return false;
                }
                
            }
            
            
            if textField.RestrictMaxCharacter(maxCount: 10, range: range, string: string)
            {
                
                
                if !((textField.text?.contains("."))! && string.contains("."))
                {
                    var separator = textField.text?.components(separatedBy: ".")
                    let count = Double((separator?.count)!)
                    if count > 1
                    {
                        let sepStr1 = "\(separator![1])"
                        if sepStr1.count == 2 && string != ""  {
                            return false
                        }
                    }
                    if let amount = Double(textField.text! + string)
                    {
                        if amount > 970873.77 && textField == txtFldAmount
                        {
                            return false
                        }
                    }
                    return true
                }
                //}
                //}
            }
            return false
        }
        
        
        //        if (string.count != 0) && (textField.text?.count)! == 0
        //        {
        //            txtFldAmount.text = "$"
        //            return true
        //        }
        return true
    }
    
    //MARK: - UIScrollView Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collectionViewImages.contentOffset
        visibleRect.size = collectionViewImages.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = collectionViewImages.indexPathForItem(at: visiblePoint) else { return }
        print(indexPath)
        self.pageControl.currentPage = indexPath.row
    }
    
 }
 extension DetailVC : QLPreviewControllerDelegate, QLPreviewControllerDataSource
 {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return tempURL as QLPreviewItem
    }
    func previewControllerDidDismiss(_ controller: QLPreviewController) {
        //popBack(2)
    }
 }
extension DetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if let itemTypeID = productDetail?.itemType
        {
            if itemTypeID == 2
            {
                return arrPreviewImages.count
            }
            else
            {
                print(arrImages)
                return arrImages.count
            }
    }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KpreviewCollectionCell, for: indexPath as IndexPath) as? PreviewCollectionCell
        {
            if let itemTypeID = productDetail?.itemType
            {
                if itemTypeID == 2
                {
                    if HistoryVC.isMyPurchase == true {
                    if let numberImg =  arrPreviewImages[indexPath.item].imgUrl {
                        self.downloadUrl = numberImg
                        self.radioView.isHidden = false
                        btnDownload.isHidden = true
                        }
                    }
                    cell.LoadData(Data: arrPreviewImages, index: indexPath)
                }
                else
                {
                    cell.LoadData(Data: arrImages, index: indexPath)
                }
                print(itemTypeID)
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        print(collectionView.bounds.width)
        print(collectionView.bounds.height)
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    //MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
}
extension DetailVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension DetailVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        return arrDetail.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PreviewTableCell = tableView.dequeueReusableCell(withIdentifier: KpreviewTableCell) as! PreviewTableCell
        cell.LoadData(Data: arrDetail[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
}
 extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
 }

