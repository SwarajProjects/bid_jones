//
//  SellerInfoVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 7/17/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class SellerInfoVC: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet var imgViewUser: UIImageView!
    @IBOutlet var tblComment: UITableView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblFollowingCount: UILabel!
    @IBOutlet var lblFollowerCount: UILabel!
    @IBOutlet var lblCurrentlySelling: UILabel!
    @IBOutlet var lblCurrentSellingValue: UILabel!
    @IBOutlet var lblTotalSales: UILabel!
    @IBOutlet var lblTotalSalesValue: UILabel!
    @IBOutlet var Segment: UISegmentedControl!
    
    @IBOutlet var tblSoldList: UITableView!
    @IBOutlet var tblAvailableList: UITableView!
    
    //Label
    @IBOutlet var lblNoItem: UILabel!
    @IBOutlet var btnFollow: CustomButton!
    
    //MARK: - Variables
    var countAvailable = 0
    var countSold = 0
    
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    let dispatchGroup = DispatchGroup()
    var sellerInfo:SellerInfoData? = nil
    //Array
    private lazy var arrSold = [ItemListData]()
    private lazy var arrAvailable = [ItemListData]()
    var sellerId : Int?
    
    var  isFirst = false
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    var productDetail:ProductData?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        isLoading = false
        dataLoaded = false
        isScrolling = false
        lblNoItem.isHidden = true
        self.tblAvailableList.tableFooterView = UIView()
        self.tblSoldList.tableFooterView = UIView()
        
        tblAvailableList.estimatedRowHeight = 1000
        tblAvailableList.rowHeight = UITableViewAutomaticDimension
        
        tblSoldList.estimatedRowHeight = 1000
        tblSoldList.rowHeight = UITableViewAutomaticDimension
        
         MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading

        dispatchGroup.enter()
        GetAvailableList()
        
        dispatchGroup.enter()
        GetSellerInfo()
        
        dispatchGroup.notify(queue: .main) {
            print("Both functions complete 👍")
            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
        }
        
        
        if let id = UserDefaults.standard.getUserID()
        {
           if(id == productDetail?.sellerId)
           {
            btnFollow.isHidden = true
            }
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            if Segment.selectedSegmentIndex == 1{
                if (arrSold.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("Available")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countSold = self.countSold+KPaginationcount
                    MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
                    dispatchGroup.enter()
                    GetSoldList()
                    dispatchGroup.notify(queue: .main) {
                        print("Both functions complete 👍")
                        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                    }
                }
            }
            else
            {
                if (arrAvailable.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("sold")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countAvailable = self.countAvailable+KPaginationcount
                    MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
                    dispatchGroup.enter()
                    GetAvailableList()
                    dispatchGroup.notify(queue: .main) {
                        print("Both functions complete 👍")
                        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                    }
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("11111111111")
        isScrolling = false
    }
    
    
    @IBAction func FollowerList_Action(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "Seller", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FollowerVC") as! FollowerFollowingVC
        vc.followerID = Int(sellerId!)
        vc.followtype = "follower"
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func FollowingList_Action(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Seller", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FollowerVC") as! FollowerFollowingVC
        vc.followerID = Int(sellerId!)
        vc.followtype = "following"
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Other functions
    func GetSoldList()  {
            let urlStr = KitemHistory
            var parm = [String : Any]()
            parm["offset"] = countSold
            parm["limit"]  = KPaginationcount
            parm["user_type"] = "seller"
            parm["user_id"] = productDetail?.sellerId
        if let userID = productDetail?.sellerId
        {
            parm["user_id"] = userID

        }
        else
        {
            if let id = UserDefaults.standard.getUserID()
            {
                parm["user_id"] = id

         }
        }
        
        
            print(parm)
            print(urlStr)
            dataLoaded = false
            SellerInfo.sharedManager.PostApiForSoldItems(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
                self.dispatchGroup.leave()
                if Response.count>0
                {
                    if(self.countSold == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrSold.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSold.insert(item, at: self.arrSold.count)
                            }
                        }
                        self.tblSoldList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrSold.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSold.insert(item, at: self.arrSold.count)
                            }
                        }
                        self.tblSoldList.reloadData()
                        print(self.arrSold)
                    }
                    print(self.countSold)
                    self.lblNoItem.isHidden = true
                    self.tblSoldList.isHidden = false
                    
                }
                else
                {
                    if(self.countSold != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countSold == 0 || self.arrSold.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblSoldList.isHidden = true
                    }
                }
                self.isLoading = false
                //            if self.isDeleted!
                //            {
                //                self.isDeleted = false
                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                //            }
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    self.dispatchGroup.leave()
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetSoldList()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.countSold == 0 || self.arrSold.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblSoldList.isHidden = true
                            
                        }
                    }
                    else
                    {
                        if (self.countSold == 0 || self.arrSold.count == 0)
                        {
                            self.arrSold.removeAll()
                            self.tblSoldList.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblSoldList.isHidden = true
                            
                        }
                        
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.dispatchGroup.leave()
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.dispatchGroup.leave()
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
    }
    func GetAvailableList()  {
            let urlStr = Ksellerallitems
            var parm = [String : Any]()
            parm["offset"] = countAvailable
            parm["limit"]  = KPaginationcount
            parm["seller_id"] = productDetail?.sellerId
        if let userID = productDetail?.sellerId
        {
            parm["seller_id"] = userID

        }
        else
        {
            if let id = UserDefaults.standard.getUserID()
            {
                parm["seller_id"] = id

            }
 }
            print(parm)
            print(urlStr)
            dataLoaded = false
            SellerInfo.sharedManager.PostSellForAvailableItems(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
                self.dispatchGroup.leave()
                if Response.count>0
                {
                    if(self.countAvailable == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrAvailable.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrAvailable.insert(item, at: self.arrAvailable.count)
                            }
                        }
                        self.tblAvailableList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrAvailable.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrAvailable.insert(item, at: self.arrAvailable.count)
                            }
                        }
                        self.tblAvailableList.reloadData()
                        print(self.arrAvailable)
                    }
                    print(self.countAvailable)
                    self.lblNoItem.isHidden = true
                    self.tblAvailableList.isHidden = false
                    
                }
                else
                {
                    if(self.countAvailable != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countAvailable == 0 || self.arrAvailable.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblAvailableList.isHidden = true
                    }
                }
                self.isLoading = false
                //            if self.isDeleted!
                //            {
                //                self.isDeleted = false
                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                //            }
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    self.dispatchGroup.leave()

                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetSoldList()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.countAvailable == 0 || self.arrAvailable.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblAvailableList.isHidden = true
                            
                        }
                    }
                    else
                    {
                        if (self.countAvailable == 0 || self.arrAvailable.count == 0)
                        {
                            self.arrAvailable.removeAll()
                            self.tblAvailableList.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblAvailableList.isHidden = true
                            
                        }
                        
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.dispatchGroup.leave()
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.dispatchGroup.leave()
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
    }
    func GetSellerInfo()
    {
        let urlStr = Ksellerinfo
        var parm = [String : Any]()
//        if let buyerID = productDetail?.sellerId
//        {
//            parm["seller_id"] = buyerID
//        }
        if let userID = productDetail?.sellerId
        {
            parm["seller_id"] = userID
            
            self.sellerId = userID

        }
        else
        {
            if let id = UserDefaults.standard.getUserID()
            {
                parm["seller_id"] = id
                
                self.sellerId = id

            }
        }

        
        print(parm)
        SellerInfo.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
        
            print(Response)
            self.sellerInfo = Response
            print(self.sellerInfo as Any)
            
            if self.isFirst == false {
            
          self.dispatchGroup.leave()
            
            }
            if let ButtonTitle = self.sellerInfo?.buttonText
            {
            self.btnFollow.setTitle(ButtonTitle, for: .normal)
            
//                if ButtonTitle == "Unfollow" {
//
//                    self.btnFollow.backgroundColor =  UIColor.re
//
//
//                }
//                else
//                {
//
//                   self.btnFollow.backgroundColor = UIColor.red
//                }
                
            }
            if let Followers = self.sellerInfo?.followers
            {
                self.lblFollowerCount.text = "\(Followers)"
            }
            if let Following = self.sellerInfo?.following
            {
                self.lblFollowingCount.text = "\(Following)"
            }
            if let Name = self.sellerInfo?.sellerName
            {
                self.lblUserName.text = Name
            }
            if let TotalSales = self.sellerInfo?.totalSales
            {
                self.lblTotalSalesValue.text = "\(TotalSales)"
            }
            if let CurrentlySelling = self.sellerInfo?.currentlySelling
            {
                self.lblCurrentSellingValue.text = "\(CurrentlySelling)"
                //self.lblCurrentSellingValue.text = "dsfjfsd fsdbjkfsdkf sdjkf dsjkfnsd fsd fjkdsb f dsf"
            }
            self.imgViewUser.layer.cornerRadius = self.imgViewUser.frame.size.width / 2;
            self.imgViewUser.contentMode = .scaleToFill
            self.imgViewUser.clipsToBounds = true
            let url = URL(string: (self.sellerInfo?.sellerImage)!)
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = false
            self.imgViewUser.kf.setImage(with: url, placeholder:  UIImage.init(named:"profile"), options: .none, progressBlock: { (receivedSize, totalSize) -> () in
                print("Download Progress: \(receivedSize)/\(totalSize)")
            }, completionHandler: { (image, error,type, imageURL) in
                print("Downloaded and set!")
                self.imgViewUser.contentMode = .scaleAspectFill
                self.imgViewUser.clipsToBounds = true
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            })
            
            
        }, completionnilResponse: { (Response) in
            self.dispatchGroup.leave()
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.GetSellerInfo()
                })
            }
            else
            {
                
            }
        }, completionError: { (error) in
            self.dispatchGroup.leave()
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.dispatchGroup.leave()
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    //MARK: - IBActions
    @IBAction func FollowAction(_ sender: Any) {
        
        isFirst = true
        
        let url = kurlFollowMe
        
     //   print("seller id :\(sellerId)")
        
        
        let id = UserDefaults.standard
        
    
        if let sellerid = sellerId  {
            print("your seller. id :\(sellerid)")
            if let loginUserID = id.getUserID() {
                print("login user id : \(loginUserID)")
                
        let parm = ["user_id" : sellerid , "follower_id" : loginUserID] as! [String : Any]
        
        
        Follow.sharedManager.PostApi(url: url , parameter: parm, Target: self
            , completionResponse: { (response) in
                
                self.dispatchGroup.enter()
                
                self.GetSellerInfo()
                
                 self.dispatchGroup.notify(queue: .main) {
                    print("Both functions complete 👍")
                    MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                }
                
                print(response)
                
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    //   self.GetRecieveBidList()
                })
            }
            else if statusCode == 201
            {
                
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        }
    }
        
    }
        
        
        
    
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func SegmentAction(_ sender: Any) {
        
        if Segment.selectedSegmentIndex == 0
        {
            tblAvailableList.isHidden = false
            tblSoldList.isHidden = true
            tblAvailableList.reloadData()

            self.lblNoItem.isHidden = true
            if arrAvailable.count == 0
            {
                MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
                dispatchGroup.enter()
                GetAvailableList()
                dispatchGroup.notify(queue: .main) {
                    print("Both functions complete 👍")
                    MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                }
            }
        }
        else
        {
            tblAvailableList.isHidden = true
            tblSoldList.isHidden = false
            tblSoldList.reloadData()
            self.lblNoItem.isHidden = true
            if arrSold.count == 0
            {
                MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
                dispatchGroup.enter()
                GetSoldList()
                dispatchGroup.notify(queue: .main) {
                    print("Both functions complete 👍")
                    MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                }
            }
        }
}
}
extension SellerInfoVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
}

extension SellerInfoVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrServiceSell.count)
        if Segment.selectedSegmentIndex == 1{
            return arrSold.count
        }
        else
        {
            return arrAvailable.count
         }
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if Segment.selectedSegmentIndex == 1
        {
            let cell : SellerInfoCell = tableView.dequeueReusableCell(withIdentifier: "sellerInfoCell") as! SellerInfoCell
            // cell.delegate = self
            cell.LoadData(dic:arrSold[indexPath.row])
            cell.tag = indexPath.row
            cell.selectionStyle = .none
            return cell
        }
        else{
            let cell : SellerInfoCell = tableView.dequeueReusableCell(withIdentifier: "sellerInfoCell") as! SellerInfoCell
            // cell.delegate = self
            cell.LoadData(dic:arrAvailable[indexPath.row])
            cell.tag = indexPath.row
            cell.selectionStyle = .none
            return cell
        }
    }
}
