//
//  ChangePriceVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 8/6/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class ChangePriceVC: UIViewController,UITextFieldDelegate {
    @IBOutlet var txtFldPNewPrice: UITextField!
    @IBOutlet var lblCurrentPrice: UILabel!
    var productDetail:MerchPickupData?

    override func viewDidLoad() {
        super.viewDidLoad()
        //bidAmount
        print("here your data mo : \(productDetail!.amount!)")
        lblCurrentPrice.text = "$"+"\(productDetail!.amount!)"
        txtFldPNewPrice.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonAction), Title: KDone)

         self.automaticallyAdjustsScrollViewInsets = false
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - Other Actions
    func ShowAlert(msg:String)
    {
        //{
        let alertController = UIAlertController(title: KMessage, message: msg, preferredStyle: .alert)
        // Create the actions
        let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.popBackToBidsView()
        }
        self.dismiss(animated: true, completion: nil)
        // Add the actions
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        // }
    }
    func popBackToBidsView()
    {
        if let viewControllers = navigationController?.viewControllers {
            for item in viewControllers {
                // some process
                if item.isKind(of: AlertsVC.self){
                    print("yes it is")
                    self.navigationController?.popToViewController(item, animated: true)
                }
            }
        }
    }
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    //MARK: - TextField delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtFldPNewPrice
        {
            if textField.RestrictMaxCharacter(maxCount: 10, range: range, string: string)
            {
                if !((textField.text?.contains("."))! && string.contains("."))
                {
                    var separator = textField.text?.components(separatedBy: ".")
                    let count = Double((separator?.count)!)
                    if count > 1
                    {
                        let sepStr1 = "\(separator![1])"
                        if sepStr1.count == 2 && string != ""  {
                            return false
                        }
                    }
//                    if let amount = Double(textField.text! + string)
//                    {
//                        if amount > 970873.77
//                        {
//                            return false
//                        }
//                    }
                    return true
                }
            }
            return false
        }
        
        return true
    }
    
    //MARK:- IBActions
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func UpdateAction(_ sender: Any) {
        guard let bidAmount  = txtFldPNewPrice.text, !bidAmount.isEmpty, !bidAmount.trimmingCharacters(in: .whitespaces).isEmpty, !bidAmount.replacingOccurrences(of: ".", with: "").isEmpty else
        {
            showAlertMessage(titleStr: KMessage, messageStr: KPleaseenterBidAmount)
            return
        }
        
        let urlStr = KsellerupdateBid
        var parm = [String : Any]()
        if let buyerID = productDetail!.buyerID
        {
        
            print("this is buyerID : \(buyerID)")
            parm["buyer_id"] = buyerID
        }
        
        print("your data update: \(productDetail!.id)  and \(txtFldPNewPrice.text) and \(productDetail!.bid_id!)")
        
        parm["item_id"] = productDetail!.bid_id!
        parm["amount"]  = txtFldPNewPrice.text!
        parm["bid_id"] = productDetail!.id!
        
            //productDetail?.bid_Id
        
        print(parm)
        MerchPick.sharedManager.PostApiChangePrice(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            if let msg =  Response[Kmessage]
            {
                self.ShowAlert(msg: msg as! String)
            }
          
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.UpdateAction(AnyObject.self)
                })
            }
            else
            {
                
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    @IBAction func CancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
}
