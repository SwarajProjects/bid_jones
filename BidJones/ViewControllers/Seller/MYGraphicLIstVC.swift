//
//  MYGraphicLIstVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/1/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import MobileCoreServices


class MYGraphicLIstVC : UIViewController,ProductTableCellDelegate,CustomStripeDelegate {
    
    //MARK: - Label Outlets
    @IBOutlet var lblNoItem: UILabel!
    //MARK: - TextField Outlets
    //MARK: - TextView Outlets
    //MARK: - UItableView Outlets
    @IBOutlet var tblGraphicsList: UITableView!
    //MARK: - UISCrollView Outlets
    //MARK: - UIButton Outlets
    //MARK: - UIImageView Outlets
    //MARK: - UIView Outlets
    @IBOutlet var viewDelete: UIView!
    //MARK: - UIPicker Outlets
    //MARK: - Int Variable
    var count = 0
    var indexDelete:Int?
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isDeleted:Bool?
    var userType : String?
    let url = "/user/detail"
    var user_Id: Int?
    var account_already:Int?
    //MARK: - String Variable
    //MARK: - array Variable
    private lazy var arrGraphics = [ItemListData]()
    //MARK: - UIPickerView Variable
    //MARK: - dictionary Variable
    //MARK: - UIImagePickerController
    //MARK: - Enum Variable
    var productDetail:ProductData?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = false
        dataLoaded = false
        isScrolling = false
        isDeleted = false
        
        lblNoItem.isHidden = true
        viewDelete.isHidden = true
        
        //GetList()
        //tblGraphicsList.estimatedRowHeight = 1000
       // tblGraphicsList.rowHeight = UITableViewAutomaticDimension
        
        GetList()

    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: CustomStripeDelegate
    
    func StripeAccountCreated() {
        
        UserDefaults.standard.setUserType(value: true)
    }
    
    func StripeAccountNotCreated() {
        
    }
    //MARK: - Other function
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func MakeSellerShowAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "You must have a stripe account linked with bidjones.Tap Continue to Create.", preferredStyle: .alert)
        
        // Create the actions
        let NoAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
            UIAlertAction in
//            var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.user_Id!)"
//            if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.openURL(url)
//            }
            
            self.apiCall(url: self.url, vc: self)
            
      //       KCommonFunctions.PushToContrller(from: self, ToController: .WebView, Data: self.user_Id!)
            NSLog("OK Pressed")
        }
        let YesAction = UIAlertAction(title: "Abort", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        self.dismiss(animated: true, completion: nil)
        alertController.addAction(NoAction)
        alertController.addAction(YesAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK: - Other actions
    
    
    func apiCall(url:String,vc:UIViewController){
        
        //        MyProfile.sharedManager.GetApi(url: url, Target: vc, completionResponse: { (response) in
        //
        //            // self.profileInfo = response
        //            //response
        //
        //            print("now you get data profile : \(response)")
        //            if let StripeExist = response[0].stripe_account_id {
        //               let chkStripe = StripeExist
        //                print("chkStripe",chkStripe)
        //                if(StripeExist.contains("<null>") || StripeExist.isEmpty){
        //                    //self.isStripeExist = false
        //
        //                    UserDefaults.standard.setStripeExist(value: false)
        //
        //                }else if(!(StripeExist.contains("<null>"))){
        //                     UserDefaults.standard.setStripeExist(value: true)
        //                }else{
        //                    print("Something wrong happend")
        //                }
        //
        //                self.checkPermissonToSell()
        //
        //
        //            }else{
        //                print("error ")
        //            }
        //
        //
        //
        //
        //
        //        }, completionnilResponse: { (Response) in
        //            //print(Response)
        //            let statusCode = Response[Kstatus] as! Int
        //            if statusCode == 500
        //            {
        //                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
        //                    //print(resonse)
        //
        //                })
        //            }
        //            else if statusCode == 201
        //            {
        //                let message =  Response["message"]
        //
        //
        //
        //            }
        //
        //
        //
        //
        //
        //        }, completionError: { (error) in
        //            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        //
        //        }, networkError: {(error) in
        //            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        //
        //        })
        
        
        
        
        
        
        
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        var chckRtoS:Int?
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            print("Make me seller",Response)
            let status = Response["status"] as! Int
            print("status",status)
            //            let message = Response["message"] as? String
            //            print("message",message)
            if let accountalready = Response["account_already"] as? Int{
                self.account_already = accountalready
            }
            print("account_already",self.account_already)
            let data  = Response["result"] as! [String:Any]
            self.user_Id =  data["id"] as? Int
            //            print("data",data)
            if let reToStripe = data["redirect_to_stripe"] as? Int{
                print("reToStripe",reToStripe)
                chckRtoS  =  reToStripe
            }
            let stripe_account_id = data["stripe_account_id"] as? Any
            print("stripe_account_id",stripe_account_id)
            let stpID = "\(stripe_account_id!)"
            print(stpID)
            
            if(status == 200){
                // id not null and account already // 3rd check+
                if(!(stpID.contains("<null>"))){
                    UserDefaults.standard.setUserType(value: true)
                    self.account_already = 1
                    UserDefaults.standard.setStripeExist(value: true)
                    KCommonFunctions.PushToContrller(from: self, ToController: .AddGaraphics, Data: nil)
                }
                
                //account already = 0
                else if(chckRtoS == 1 || self.account_already == 0){
                    print("Web Open")
                    self.MakeSellerShowAlert()
                    
                    
                    
                    //web Open
                    // id null but account already 1
                    //send message of popup link
                }else if(self.account_already == 1){
                    print("Message")
                    self.showAlertMessage(titleStr: KMessage, messageStr:  "We sent a link to your registered email to connect your stripe account with BidJones to receive payments.")
                    
                    
                }
            }
            
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
        
        
        
    }
    
    func MakeMeSeller()  {
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            //print(Response)
            if let msg =  Response[Kmessage]
            {
                //                self.arrBooks.remove(at: self.indexDelete!)
                //                self.tblBooksList.reloadData()
                //                self.isDeleted = true
                //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
//                UserDefaults.standard.setUserType(value: true)
//                //self.showToast(message: msg as! String)
//                self.AddMoreAction(AnyObject.self)
                if let showMessage = Response[KshowMessage] as? Int , showMessage == 1{
                    UserDefaults.standard.setUserType(value: true)
                    
                    UserDefaults.standard.setShowMessageType(value: "1")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //self.showToast(message: msg as! String)
                    self.AddMoreAction(AnyObject.self)
                    // self.goToAddScreen()
                    
                }
                else {
                    
                    
                    
                    //  print("Here your userType is \(self.userType)")
                    
                    UserDefaults.standard.setShowMessageType(value: "0")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //  self.goToAddScreen()
                    
                    self.AddMoreAction(AnyObject.self)
                }
                
                
                
                
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    func GetList()  {
        let urlStr = KsellerlistitemsApi
        var parm = [String : Any]()
        if let id = UserDefaults.standard.getUserID()
        {
            parm[Kseller_id] = id
        }
        parm["offset"] = count
        parm["limit"]  = KPaginationcount

        parm[Kcategory_id] = KGraphicsType
        FetchItemList.sharedmanagerItemList.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            if Response.count>0
            {
                if(self.count == 0)
                {
                   // self.arrGraphics = Response
                    for item in Response
                    {
                        let oldIds = self.arrGraphics.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrGraphics.insert(item, at: self.arrGraphics.count)
                        }
                    }
                    self.tblGraphicsList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrGraphics.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrGraphics.insert(item, at: self.arrGraphics.count)
                        }
                    }
                    self.tblGraphicsList.reloadData()
                    print(self.arrGraphics)
                }
                print(self.count)
                self.lblNoItem.isHidden = true
            }
            else
            {
                if(self.count != 0)
                {
                    print("abcdefgh")
                    self.dataLoaded = true
                }
               
                if (self.count == 0 || self.arrGraphics.count == 0)
                {
                    self.lblNoItem.isHidden = false
                }
            }
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.GetList()
                })
            }
            else if statusCode == 201
            {
                self.dataLoaded = true
                if (self.count == 0 || self.arrGraphics.count == 0)
                {
                    self.lblNoItem.isHidden = false
                }
            }
            else
            {
                self.arrGraphics.removeAll()
                self.tblGraphicsList.reloadData()
                self.lblNoItem.isHidden = false
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    //Mark CustomCell Delegate
    func DeleteButtonAction(_ sender: ProductTableCell) {
        viewDelete.isHidden = false
        //print(sender.tag)
        indexDelete = sender.tag
    }
    //MARK: - UISCrollview delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            if (arrGraphics.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.count = self.count+KPaginationcount
                GetList()
            }
            else if isDeleted!
            {
                isDeleted = false
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                GetList()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    //MARK: - IBAction
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func YesAction(_ sender: Any){
        //print(indexDelete ?? "")
        self.viewDelete.isHidden = true
        let ItemID = arrGraphics[indexDelete!].id
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        let urlStr = KsellertrashitemApi + "/" + String(id) + "/" + String(describing: ItemID!)
        DeleteItem.sharedmanagerDeleteItem.GetApi(url: urlStr, Target: self, completionResponse: { (Response) in
            //print(Response)
            if let msg =  Response[Kmessage]
            {
                self.arrGraphics.remove(at: self.indexDelete!)
                self.tblGraphicsList.reloadData()
                self.isDeleted = true
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.YesAction(AnyObject.self)
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    @IBAction func NoAction(_ sender: Any){
        viewDelete.isHidden = true
    }
    @IBAction func CancelAction(_ sender: Any) {
        viewDelete.isHidden = true
    }
    @IBAction func AddMoreAction(_ sender: Any) {
        if let userType = UserDefaults.standard.getUserType(), userType == true {
            
            
            KCommonFunctions.PushToContrller(from: self, ToController: .AddGaraphics, Data: nil)
        }
            //First save stripe id at login and check here
            //if it is null then open webpage and
            //when it comes with id response
            
        else {
            let checkvalue = self.isKeyPresentInUserDefaults(key:"ISStripeAccountExist")
            if(checkvalue){
                let check = UserDefaults.standard.getStripeExist()
                if(check!){
                    //code RUN
                    KCommonFunctions.PushToContrller(from: self, ToController: .AddGaraphics, Data: nil)
                }else{
                    self.MakeSellerShowAlert()
                 //   self.apiCall(url: url, vc: self)
                }
                // self.checkPermissonToSell()
                //if exist then check value
                //if no again hit api and check
                //again no open web
                //if yes code run
                
                
            }else{
                
                //profile api hit
                self.MakeSellerShowAlert()
                
                //store to userdefalut
                //check if no then open web
                //if yes code run
                
            }
            // IMPORTANT UNCOMMENTED
            
            
            //        if  let type  = UserDefaults.standard.getMessageType() , type == "1" || type == "0" {
            //            print("here yor type : \(type)")
            //            print("Here your userType is \(self.userType)")
            //            self.userType = type
            //
            //
            //        if  userType! == "1"  || userType! == "0"  {
            //
            //            if userType! == "1" {
            //
            //
            //                userType = ""
            //                UserDefaults.standard.setShowMessageType(value: "")
            //
            //                showAlertMessage(titleStr: KMessage, messageStr:  "Upgraded as seller with Bidjones and Stripe is successfully. Please check your inbox and claim your stripe account to make sure that your payments arrive in your bank account without delay.")
            //
            //
            //
            //            }
            //            else {
            //                UserDefaults.standard.setUserType(value: true)
            //
            //                userType = ""
            //                  KCommonFunctions.PushToContrller(from: self, ToController: .AddSellService, Data: nil)
            //
            //            }
            //
            //
            //
            //
            //        }
            //        }
            
            //        else {
            //
            //            MakeSellerShowAlert()
            //
            //        }
        }
        
        
    }
}

extension MYGraphicLIstVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        let data = arrGraphics[indexPath.row]
       // let itemId = data.id
        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
    }
}

extension MYGraphicLIstVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrGraphics.count)
        return arrGraphics.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        guard let cell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell, for: indexPath) as? ProductTableCell else {
        //            let cell = ProductTableCell(style: .default, reuseIdentifier: KproductTableCell)
        //            cell.LoadData(dic:arrBooks[indexPath.row])
        //            return cell
        //        }
        let cell : ProductTableCell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell) as! ProductTableCell
        cell.delegate = self
        cell.LoadData(dic:arrGraphics[indexPath.row], type: .graphic)
        cell.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
}

