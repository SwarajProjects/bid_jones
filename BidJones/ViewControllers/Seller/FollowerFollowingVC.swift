//
//  FollowerFollowingVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/15/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class FollowerFollowingVC: UIViewController {
    
    @IBOutlet weak var lbl_no_data_found: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var arrayData = [FollowerData]()
    var followerID : Int?
    var followtype : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.separatorColor = nil
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.separatorColor = UIColor.clear
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        if followtype == "follower" {
            self.title = "Followers"
        } else {
            self.title = "Following"
        }
        getData(followerID: followerID ?? 0, followType: followtype ?? "follower")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func Back_ButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getData(followerID : Int , followType : String){
        
        let url = "/followerList"
        
        let parm = ["type" : followType , "user_id" :followerID , "limit" : 10 , "offset": 0] as [String : Any]
        
        Follower.sharedManager.PostApi(url: url, parameter: parm, Target: self , completionResponse: {
            (response) in
            
            print("my search alert : \(response)")
            
            self.arrayData = response
            //  if self.saleArray.count == 0 {
            
            if self.arrayData.count == 0 {
                self.lbl_no_data_found.isHidden = false
            }
            else {
                self.lbl_no_data_found.isHidden = true
            }
            self.tableView.reloadData()
            //    self.lbl_no_data_found.isHidden = false
            
            //     print("my alert data : \(self.AlertDataArray)")
            // }
            //   self.tableView.reloadData()
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    //   self.GetRecieveBidList()
                    // self.getAlertData()
                })
            }
            else if statusCode == 201
            {
                
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
}
extension FollowerFollowingVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension FollowerFollowingVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FollowerTableViewCell
        
        cell.cellData(array : arrayData, indexPath : indexPath)
        
        cell.backgroundColor = UIColor.clear
        cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        
        return cell
    }
}
