//
//  VideosCollectionCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/9/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import AVFoundation

protocol VideosCollectionCellDelegate: class{
    func DeleteVideo(_ sender: Any)
}
class VideosCollectionCell: UICollectionViewCell {
    @IBOutlet var ImgView: UIImageView!
    @IBOutlet var btnDelete: UIButton!
    weak var delegate: VideosCollectionCellDelegate?
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    func createThumbnailOfVideoFromRemoteUrl(forUrl url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(1.0, 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        
        let asset : AVAsset = AVAsset(url: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time        : CMTime = CMTimeMake(1, 30)
                do {
                    let imageRef = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                    return UIImage(cgImage: imageRef)
                }
                catch let error as NSError {
                  if let image =  self.getThumbNailImage(forUrl: url)
                  {
                    return image
                    }
                    else
                  {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                  }
                    print(error.description)
                    print(error.debugDescription)
                    }
        return nil
    }
    
    func getThumbNailImage(forUrl url: URL) -> UIImage?
    {
            let asset = AVAsset(url: url)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMake(1, 2)
        do {
            let imageRef = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        }
        catch let error as NSError {
            if let image =  self.createThumbnailOfVideoFromRemoteUrl(forUrl: url)
            {
                return image
            }
            else
            {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            }
            print(error.description)
            print(error.debugDescription)
        }
        
        return nil

    }

    func LoadData(Data : [VideoData], index:IndexPath)
    {
        print(Data)
        btnDelete.tag = index.row
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        if let image  = Data[index.row].image
        {
            btnDelete.isHidden = false
            ImgView.contentMode = .scaleAspectFill
            ImgView.clipsToBounds = true
            ImgView.image = image
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
        else if let ImgThumbUrl = Data[index.row].videoUrl 
        {
            print(ImgThumbUrl)
             ImgView.contentMode = .scaleToFill
           DispatchQueue.global(qos: .background).async(execute: {
                if let thumbNailImage =  self.getThumbnailImage(forUrl: ImgThumbUrl)
                {
                    switch thumbNailImage.imageOrientation
                    {
                    case .up:
                        print("up")
                    case .down:
                        print("down")
                    case .left:
                        print("left")
                    case .right:
                        print("right")
                    case .upMirrored:
                        print("upMirrored")
                    case .downMirrored:
                        print("downMirrored")
                    case .leftMirrored:
                        print("leftMirrored")
                    case .rightMirrored:
                        print("rightMirrored")
                    }
                    DispatchQueue.main.async {
                        self.ImgView.image = thumbNailImage
                        self.ImgView.contentMode = .scaleAspectFill
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.isHidden = true
                    }
                }
            })
            btnDelete.isHidden = false
            ImgView.contentMode = .scaleToFill
            ImgView.clipsToBounds = true
        }
        else
        {
            btnDelete.isHidden = true
            ImgView.contentMode = .scaleToFill
            ImgView.image = UIImage.init(named:"image")
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }
    func LoadImageAgainIfFail(forUrl url: URL)
    {
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        DispatchQueue.global(qos: .background).async(execute: {
            if let thumbNailImage =  self.getThumbnailImage(forUrl: url)
            {
                switch thumbNailImage.imageOrientation
                {
                case .up:
                    print("up")
                case .down:
                    print("down")
                case .left:
                    print("left")
                case .right:
                    print("right")
                case .upMirrored:
                    print("upMirrored")
                case .downMirrored:
                    print("downMirrored")
                case .leftMirrored:
                    print("leftMirrored")
                case .rightMirrored:
                    print("rightMirrored")
                }
                DispatchQueue.main.async {
                    self.ImgView.image = thumbNailImage
                    self.ImgView.contentMode = .scaleAspectFill
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
            }
        })
        
        
    }
    func LoadBlankData(index:IndexPath)
    {
        btnDelete.isHidden = true
        ImgView.contentMode = .scaleToFill
        ImgView.image = UIImage.init(named:"video")
        btnDelete.tag = index.row
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }
    @IBAction func DeleteAction(_ sender: Any)
    {
        delegate?.DeleteVideo(sender)
    }
}
