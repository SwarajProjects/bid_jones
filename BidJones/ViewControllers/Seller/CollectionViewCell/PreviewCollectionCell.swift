//
//  PreviewCollectionCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import Kingfisher

class PreviewCollectionCell: UICollectionViewCell {
    @IBOutlet var imgViewProducts: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    func LoadData(Data : [ImagesData], index:IndexPath)
    {
        print(Data)
        print(Data)

        if let image  = Data[index.row].image
        {
            //imgViewProducts.contentMode = .scaleAspectFill
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
            imgViewProducts.contentMode = .scaleAspectFit
            imgViewProducts.clipsToBounds = true
            imgViewProducts.image = image
        }
        else if let imageUrl  = Data[index.row].imgUrl
        {
            print(imageUrl)
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            imgViewProducts.contentMode = .scaleAspectFit

            imgViewProducts.kf.setImage(with: imageUrl as Resource, placeholder: nil, options: .none, progressBlock: { (receivedSize, totalSize) -> () in
                print("Download Progress: \(receivedSize)/\(totalSize)")
            }, completionHandler: { (image, error,type, imageURL) in
                print("Downloaded and set!")
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true

            })
        }
        else
        {
            //imgViewProducts.contentMode = .scaleToFill
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
            imgViewProducts.contentMode = .scaleAspectFit
            imgViewProducts.image = UIImage.init(named:"image")
        }
    }
    func LoadBlankData(index:IndexPath)
    {
        imgViewProducts.contentMode = .scaleToFill
        imgViewProducts.image = UIImage.init(named:"image")
    }
}
