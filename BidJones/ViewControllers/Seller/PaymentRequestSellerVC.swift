//
//  PaymentRequestSelllerVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 7/30/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class PaymentRequestSellerVC: UIViewController,UITextFieldDelegate {
    //MARK: IBOutlets
    @IBOutlet var lblTotalValue: UILabel!
    @IBOutlet var view1: CustomUIView!
    @IBOutlet var lblEnterAmount1: UILabel!
    @IBOutlet var txtFldAmount1: CustomTextField!
    @IBOutlet var lblMaterials1A: UILabel!
    @IBOutlet var lblMaterials1B: UILabel!
    @IBOutlet var lblPaidAmount1: UILabel!
    @IBOutlet var view2: CustomUIView!
    @IBOutlet var lblEnterAmount2: UILabel!
    @IBOutlet var txtFldAmount2: CustomTextField!
    @IBOutlet var lblMaterials2A: UILabel!
    @IBOutlet var lblMaterials2B: UILabel!
    @IBOutlet var lblPaidAmount2: UILabel!
    @IBOutlet var view3: CustomUIView!
    @IBOutlet var lblEnterAmount3: UILabel!
    @IBOutlet var txtFldAmount3: CustomTextField!
    @IBOutlet var lblMaterials3A: UILabel!
    @IBOutlet var lblMaterials3B: UILabel!
    @IBOutlet var lblPaidAmount3A: UILabel!
    @IBOutlet var imgViewSuccess: UIImageView!
    @IBOutlet var lblJobCompleted: UILabel!
    @IBOutlet var lblPaidAmount3B: UILabel!
    @IBOutlet var btnPayment: CustomButton!
    @IBOutlet var btnCancel: CustomButton!
    
    var transcationArr = [[String:Any]]()
    var productDetail: ItemListData?
    enum PaymentPart
    {
        case first
        case second
        case third
        case done

        init() {
            self = .first
        }
    }
    var paymemtPart:PaymentPart?
    var requestAllowed:Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        
           self.automaticallyAdjustsScrollViewInsets = false
        requestAllowed = false
        
        lblMaterials1B.isHidden = true
        lblPaidAmount1.isHidden = true
        txtFldAmount2.isHidden = false
        lblMaterials2A.isHidden = false
        lblMaterials2B.isHidden = true
        lblPaidAmount2.isHidden = true
        txtFldAmount3.isHidden = false
        lblMaterials3A.isHidden = false
        lblMaterials3B.isHidden = true
        lblPaidAmount3A.isHidden = true
        lblPaidAmount3B.isHidden = true
        imgViewSuccess.isHidden = true
        lblJobCompleted.isHidden = true
        lblTotalValue.text = "Total Amount: $\(productDetail?.bidAmount ?? "")"
        lblMaterials1A.text = productDetail?.title
        lblMaterials1B.text = productDetail?.title
        lblMaterials2A.text = productDetail?.title
        lblMaterials3A.text = productDetail?.title
        lblMaterials3B.text = productDetail?.title
        lblMaterials2B.text = productDetail?.title
        txtFldAmount1.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonAction), Title: KDone)
        txtFldAmount2.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonAction), Title: KDone)
        txtFldAmount3.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonAction), Title: KDone)
        GetPaymentStatus()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func CancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func PaymentRequestAction(_ sender: Any) {
        do {
            try Validations()
            let urlStr = KpayForService
            var parm = [String : Any]()
            if let id = UserDefaults.standard.getUserID()
            {
                parm["seller_id"] = id
            }
            parm["item_id"] = productDetail?.id
            switch paymemtPart {
            case .first?:
                parm["amount"]  = txtFldAmount1.text
            case .second?:
                parm["amount"]  = txtFldAmount2.text
            case .third?:
                parm["amount"]  = txtFldAmount3.text
            case .none:
                print("none")
            case .done?:
                print("done")
            }
            parm["buyer_id"]  = productDetail?.buyerID
            print(parm)
            Payment.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print(Response)
                if let msg =  Response[Kmessage]
                {
      self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .pop, ViewController: .none, rootViewController: .none,Data: nil)
                }
                
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.PaymentRequestAction(AnyObject.self)
                    })
                }
               
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr: msg as! String)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        } catch let error {
            switch  error {
            case ValidationError.emptyAmount:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenteramount)
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenteramount)
            }
        }
    }
    @IBAction func BackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - TextField delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
        
        if textField == txtFldAmount1 || textField == txtFldAmount2 || textField == txtFldAmount3
        {
            if textField.RestrictMaxCharacter(maxCount: 10, range: range, string: string)
            {
                if !((textField.text?.contains("."))! && string.contains("."))
                {
                    var separator = textField.text?.components(separatedBy: ".")
                    let count = Double((separator?.count)!)
                    if count > 1
                    {
                        let sepStr1 = "\(separator![1])"
                        if sepStr1.count == 2 && string != ""  {
                            return false
                        }
                    }
                    var MaxAmount = "0"
                    switch paymemtPart {
                    case .first?:
                        MaxAmount = (productDetail?.bidAmount)!
                    case .second?:
                        let transcation1 = transcationArr[0]
                        if let amount1 = transcation1["amount"] as? String
                        {
                            if let TotalAmount = productDetail?.bidAmount
                            {
                                let totalAmount = Double(TotalAmount)
                                MaxAmount =  (totalAmount! - Double(amount1)!).rounded(toPlaces: 2).cleanValue
                            }
                        }
                    case .third?:
                        let transcation1 = transcationArr[0]
                        let transcation2 = transcationArr[1]
                        if let amount1 = transcation1["amount"] as? String, let amount2 = transcation2["amount"] as? String
                        {
                            if let TotalAmount = productDetail?.bidAmount
                            {
                                let totalAmount = Double(TotalAmount)
                                MaxAmount =  (totalAmount! - Double(amount1)! - Double(amount2)!).rounded(toPlaces: 2).cleanValue
                            }
                        }
                    case .done?:
                        print("Done")
                    default:
                        print("Done")
                    }

                    if let amount = Double(textField.text! + string)
                    {
                            if let maxAmount = MaxAmount as? String
                            {
                                let maxAmount:Double = (maxAmount as NSString).doubleValue
                                if amount >= maxAmount
                                {
                                    return false
                                }
                            }
                    }
                    return true
                }
            }
            return false
        }
        return true
    }
    //MARK: - Other functions
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    func LoadView()
    {
    switch paymemtPart {
    case .first?:
    print("first")
    txtFldAmount1.isHidden = false
    lblMaterials1A.isHidden = false
    lblMaterials1B.isHidden = true
    lblPaidAmount1.isHidden = true
    view2.alpha = CGFloat(0.3)
    view3.alpha = CGFloat(0.3)
    txtFldAmount2.isUserInteractionEnabled = false
    txtFldAmount3.isUserInteractionEnabled = false
        
    if self.requestAllowed == false && self.transcationArr.count == 1
    {
         let transcation = transcationArr[0]
        
            if let amount = transcation["amount"] as? String
            {
                let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
                print(Amount as Any)
                txtFldAmount1.text = "$\(Amount ?? "")"
                txtFldAmount1.isUserInteractionEnabled = false
            }
        //self.btnPayment.isHidden = true
       // self.btnCancel.isHidden = true
        btnPayment.isUserInteractionEnabled = false
        btnPayment.alpha = 0.5
        btnCancel.isHidden = false
        
        self.lblEnterAmount1.text = "Requesed Amount"
    }
    case .second?:
    print("second")
    self.lblEnterAmount1.text = ""

    txtFldAmount1.isHidden = true
    lblMaterials1A.isHidden = true
    lblMaterials1B.isHidden = false
    lblPaidAmount1.isHidden = false
    
    // lblpa
    txtFldAmount2.isHidden = false
    lblMaterials2A.isHidden = false
    lblMaterials2B.isHidden = true
    lblPaidAmount2.isHidden = true
    
    view3.alpha = CGFloat(0.3)
    txtFldAmount3.isUserInteractionEnabled = false
    txtFldAmount1.isUserInteractionEnabled = false
    
    if self.transcationArr.count == 2
    {
        let transcation1 = transcationArr[0]
        let transcation2 = transcationArr[1]
        if let amount = transcation1["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            lblPaidAmount1.attributedText = AttributedString(value: "$\(Amount ?? "")")
            lblEnterAmount1.text = ""
        }
        if let amount = transcation2["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            txtFldAmount2.text = "$\(Amount ?? "")"
            txtFldAmount2.isUserInteractionEnabled = false
        }
        if self.requestAllowed == false
        {
        //self.btnPayment.isHidden = true
        //self.btnCancel.isHidden = true
            btnPayment.isUserInteractionEnabled = false
            btnPayment.alpha = 0.5
            btnCancel.isHidden = false
            
        self.lblEnterAmount2.text = "Requesed Amount"
        }
        }
    if self.transcationArr.count == 1
    {
        let transcation1 = transcationArr[0]
        if let amount = transcation1["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            lblPaidAmount1.attributedText = AttributedString(value: "$\(Amount ?? "")")
            lblEnterAmount1.text = ""
        }
    }
    case .third?:
    print("second")
    self.lblEnterAmount1.text = ""
    self.lblEnterAmount2.text = ""

    txtFldAmount1.isHidden = true
    lblMaterials1A.isHidden = true
    lblMaterials1B.isHidden = false
    lblPaidAmount1.isHidden = false
    
    txtFldAmount2.isHidden = true
    lblMaterials2A.isHidden = true
    lblMaterials2B.isHidden = false
    lblPaidAmount2.isHidden = false
    
    
    txtFldAmount3.isHidden = false
    lblMaterials3A.isHidden = false
    lblMaterials3B.isHidden = true
    lblPaidAmount3A.isHidden = true
    lblPaidAmount3B.isHidden = true
    imgViewSuccess.isHidden = true
    lblJobCompleted.isHidden = true
    
    
    txtFldAmount2.isUserInteractionEnabled = false
    txtFldAmount1.isUserInteractionEnabled = false
     if self.transcationArr.count == 3
    {
        let transcation1 = transcationArr[0]
        let transcation2 = transcationArr[1]
        let transcation3 = transcationArr[2]
        if let amount = transcation1["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            lblPaidAmount1.attributedText = AttributedString(value: "$\(Amount ?? "")")
            lblEnterAmount1.text = ""
        }
        if let amount = transcation2["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            lblPaidAmount2.attributedText = AttributedString(value: "$\(Amount ?? "")")
            lblEnterAmount2.text = ""
        }
        if let amount = transcation3["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            txtFldAmount3.text = "$\(Amount ?? "")"
            txtFldAmount3.isUserInteractionEnabled = false
            self.lblEnterAmount3.text = ""
        }
        if self.requestAllowed == false
        {
       // self.btnPayment.isHidden = true
        //self.btnCancel.isHidden = true
            btnPayment.isUserInteractionEnabled = false
            btnPayment.alpha = 0.5
            btnCancel.isHidden = false
            
            
        self.lblEnterAmount3.text = "Requesed Amount"
        }
        }
    if self.transcationArr.count == 2
    {
        let transcation1 = transcationArr[0]
        let transcation2 = transcationArr[1]
        if let amount = transcation1["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            lblPaidAmount1.attributedText = AttributedString(value: "$\(Amount ?? "")")
            lblEnterAmount1.text = ""
        }
        if let amount = transcation2["amount"] as? String
        {
            let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
            print(Amount as Any)
            lblPaidAmount2.attributedText = AttributedString(value: "$\(Amount ?? "")")
            lblEnterAmount2.text = ""
        }
        if let amount1 = transcation1["amount"] as? String, let amount2 = transcation2["amount"] as? String
        {
            if let TotalAmount = productDetail?.bidAmount
            {
                let totalAmount = Double(TotalAmount)
               let MaxAmount =  (totalAmount! - Double(amount1)! - Double(amount2)!).rounded(toPlaces: 2).cleanValue
                txtFldAmount3.text = "\(MaxAmount)"
                txtFldAmount3.isUserInteractionEnabled = false
                
            }
        }
        
        }
    case .done?:
    self.lblEnterAmount1.text = ""
    self.lblEnterAmount2.text = ""
    self.lblEnterAmount3.text = ""
    txtFldAmount1.isHidden = true
    lblMaterials1A.isHidden = true
    lblMaterials1B.isHidden = false
    lblPaidAmount1.isHidden = false
    
    txtFldAmount2.isHidden = true
    lblMaterials2A.isHidden = true
    lblMaterials2B.isHidden = false
    lblPaidAmount2.isHidden = false
    
    txtFldAmount3.isHidden = true
    lblMaterials3A.isHidden = true
    lblMaterials3B.isHidden = true
    lblPaidAmount3A.isHidden = true
    lblPaidAmount3B.isHidden = false
    imgViewSuccess.isHidden = false
    lblJobCompleted.isHidden = false
    
    txtFldAmount2.isUserInteractionEnabled = false
    txtFldAmount3.isUserInteractionEnabled = false
    txtFldAmount1.isUserInteractionEnabled = false
    
    let transcation1 = transcationArr[0]
    let transcation2 = transcationArr[1]
    let transcation3 = transcationArr[2]
    if let amount = transcation1["amount"] as? String
    {
        let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
        print(Amount as Any)
        lblPaidAmount1.attributedText = AttributedString(value: "$\(Amount ?? "")")
        lblEnterAmount1.text = ""
    }
    if let amount = transcation2["amount"] as? String
    {
        let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
        print(Amount as Any)
        lblPaidAmount2.attributedText = AttributedString(value: "$\(Amount ?? "")")
        lblEnterAmount2.text = ""
        }
    if let amount = transcation3["amount"] as? String
    {
        let Amount = Double(amount)?.rounded(toPlaces: 2).cleanValue
        print(Amount as Any)
        lblPaidAmount3B.attributedText = AttributedString(value: "$\(Amount ?? "")")
        lblEnterAmount3.text = ""
        }
        //btnPayment.isHidden = true
        //btnCancel.isHidden = true
        
    btnPayment.isUserInteractionEnabled = false
    btnPayment.alpha = 0.5
    btnCancel.isHidden = false

        
    default:
    print("third")
    }
    }
    func GetPaymentStatus()
    {
            let urlStr = KsellerpaymentRequest
            var parm = [String : Any]()
            if let id = UserDefaults.standard.getUserID()
            {
                parm["seller_id"] = id
            }
            parm["item_id"] = productDetail?.id
            print(parm)
            Payment.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print(Response)
                if let msg =  Response[Kmessage] as? String
                {
                    if let RequestAllowed  = Response["requestAllowed"] as? Bool
                    {
                        self.requestAllowed = RequestAllowed
                    }
                    if msg == "No payment request found."
                    {
                            self.paymemtPart = .first
                            print(self.requestAllowed as Any)
                    }
                    if let arr = Response[Kresult] as? [[String:Any]]
                    {
                        self.transcationArr = arr
                    }
                    
                    if self.requestAllowed == false && self.transcationArr.count == 1
                    {
                        self.paymemtPart = .first
                       
                    }
                    else if self.requestAllowed == true && self.transcationArr.count == 1
                    {
                        self.paymemtPart = .second
                    }
                    else if self.requestAllowed == false && self.transcationArr.count == 2
                    {
                        self.paymemtPart = .second
                    }
                    else if self.requestAllowed == true && self.transcationArr.count == 2
                    {
                        self.paymemtPart = .third

                    }
                    else if self.requestAllowed == false && self.transcationArr.count == 3
                    {
                        self.paymemtPart = .third

                    }
                    else if self.requestAllowed == true && self.transcationArr.count == 3
                    {
                        self.paymemtPart = .done
                    }
                    self.LoadView()
                }
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.GetPaymentStatus()
                    })
                }
                    else if statusCode == 400
                {
                    if let msg =  Response[Kmessage] as? String
                    {
                        if msg == "No payment request found."
                        {
                            self.paymemtPart = .first
                            self.loadView()
                        }
               }
                    
                }
                    
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr: msg as! String)
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
       // }
    }
    
    func AttributedString(value: String) -> NSAttributedString
    {
        let attrs1 = [NSAttributedStringKey.font : UIFont(name: "OpenSans", size: 13), NSAttributedStringKey.foregroundColor : UIColor.black]
        let attrs2 = [NSAttributedStringKey.font : UIFont(name: "OpenSans", size: 13), NSAttributedStringKey.foregroundColor : UIColor( red: CGFloat(68.0/255.0), green: CGFloat(180.0/255.0), blue: CGFloat(61.0/255.0), alpha: CGFloat(1.0) )]
        let attributedString1 = NSMutableAttributedString(string:"Paid Amount: ", attributes:attrs1 as Any as? [NSAttributedStringKey : Any])
        let attributedString2 = NSMutableAttributedString(string:"\(value)", attributes:attrs2 as Any as? [NSAttributedStringKey : Any])
        attributedString1.append(attributedString2)
        return attributedString1
    }
    
    
    func Validations() throws
    {
        switch paymemtPart {
        case .first?:
            guard let amount = txtFldAmount1.text,  !amount.isEmpty, !amount.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
            {
                throw ValidationError.emptyAmount
            }
        case .second?:
            guard let amount = txtFldAmount2.text,  !amount.isEmpty, !amount.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
            {
                throw ValidationError.emptyAmount
            }
        default:
            guard let amount = txtFldAmount3.text,  !amount.isEmpty, !amount.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
            {
                throw ValidationError.emptyAmount
            }
        }
       
    }

}
