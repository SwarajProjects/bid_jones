//
//  SuccessVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/7/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import FacebookShare
import TwitterKit
import Photos
import Branch


class SuccessVC: UIViewController, TWTRComposerViewControllerDelegate  {
    @IBOutlet var lblAdPosted: UILabel!
    
    //MARK:- Variables
    
    var img               :        UIImage!
    
    var image             :        UIImage!
    
    
    var appURL            =        URL(string: "twitter://user?screen_name")!
    
    
    
    let isFBInstalled        = UIApplication.shared.canOpenURL( URL(fileURLWithPath: "fb://"))
    let fbLoginmanager       = FBSDKLoginManager()
    var branchUniversalObject: BranchUniversalObject?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let isEdited = AddProuductData.sharedInstance?.IsEdited, isEdited == true
        {
          lblAdPosted.text = "AD UPDATED"
        }
        //clearTempFolder()
        // Do any additional setup after loading the view.
        xxxD()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let arrPdf = AddProuductData.sharedInstance?.arrLocalPdf, arrPdf.count>0
        {
            for item in arrPdf
            {
                removeImage(itemName:(item.pdfUrl.absoluteString))
            }
            
        }
        if let arrAudios = AddProuductData.sharedInstance?.arrLocalAudios, arrAudios.count>0
        {
            for item in arrAudios
            {
                removeImage(itemName:(item.audioURL.absoluteString))
            }
            
        }
        if let arrImages = AddProuductData.sharedInstance?.arrLocalImages, arrImages.count>0
        {
            for item in arrImages
            {
                removeImage(itemName:(item.imgUrl?.absoluteString)!)
            }
            
        }
        
        if let arrVideos = AddProuductData.sharedInstance?.arrLocalVideos, arrVideos.count>0
        {
            for item in arrVideos
            {
                removeImage(itemName:(item.videoUrl?.absoluteString)!)
            }
            
        }
        print("success")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func xxxD(){
        
        self.branchUniversalObject = BranchUniversalObject(canonicalIdentifier: "123")    // just for tracking count and analytics
        self.branchUniversalObject?.title = "some title"    // This will appear as the shared content's title
        self.branchUniversalObject?.contentDescription = "some description"    // This will appear as the shared content's description
        self.branchUniversalObject?.imageUrl = "www.example.com/test.png"
        self.branchUniversalObject?.addMetadataKey("uid", value: "123434")
       // branchUniversalObject.addMetadataKey("otherInfo", value: self.otherInfo)
    }
    //MARK: - IBActions
    @IBAction func BackToHomePageAction(_ sender: Any) {
    CommonFunctions.sharedInstance.SetRootViewController(rootVC: .HomeNavigation)
    }
    @IBAction func faceBookAction(_ sender: Any) {
     
        checkForInstalledFB()
        
    }
    
    @IBAction func InstaAction(_ sender: UIButton)
    {
        UIImageWriteToSavedPhotosAlbum((AddProuductData.sharedInstance?.arrImages![0].image)!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
        
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error != nil {
            print(error)
        }
        
        let fetchOptions = PHFetchOptions()
        print(fetchOptions)
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        print(fetchResult)
        if let lastAsset = fetchResult.firstObject as? PHAsset {
            print(lastAsset)
            let localIdentifier = lastAsset.localIdentifier
            print(localIdentifier)
            let u = "instagram://library?LocalIdentifier=" + localIdentifier
            print(u)
            
            let url = NSURL(string: u)!
            if UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(NSURL(string: u)! as URL)
            } else {
                let alertController = UIAlertController(title: "Error", message: "Instagram is not installed", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }

    
    @IBAction func TwitterAction(_ sender: Any)
    {
        
        if let image = AddProuductData.sharedInstance?.arrImages![0].image!
        {
            print("thx: \(appURL)")
            // Checking App is present or not
            if UIApplication.shared.canOpenURL(appURL as URL)
            {
                if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                    let composer = TWTRComposer()
                   // composer.setText(descrption)
                    composer.setImage(image)
                    // Present composer
                    composer.show(from: self) { result in
                        print(result)
                        if (result == TWTRComposerResult.cancelled) {
                            print("Tweet composition cancelled.")
                        }
                        else if (result == TWTRComposerResult.done) {
                            print("Sending tweet...")
           
                            _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { timer in
                                DispatchQueue.main.async {
                                    self.showAlert()
                                }                            }
                        }
                        print("here is :\(result)")
                    }
                }
                    
                else {
                    TWTRTwitter.sharedInstance().logIn(with: self){session,Error in
                        
                        // who want login first time
                        print( "this is new account for login" )
                        if session != nil {
                            let composer = TWTRComposer()
                           // composer.setText(descrption)
                            composer.setImage(self.image)
                            
                            // Present composer
                            composer.show(from: self) { result in
                                print(result)
                                if (result == TWTRComposerResult.cancelled) {
                                    print("Tweet composition cancelled.")
                                }
                               else if (result == TWTRComposerResult.done) {
                                    print("Sending tweet...")
                                    _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { timer in
                                        DispatchQueue.main.async {
                                            self.showAlert()
                                        }                            }
                                }
                            }
                        }
                        else
                        {
                            print(Error?.localizedDescription as Any)
                        }
                    }
                }
            }
                
            else {
                
                // WEB BROWSER PART
                _ = URL.init(string: "http://twitter.com")
                
                // USER ALREADY LOGIN
                if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                    
                    print("yes")
                    
                    let composer1 = TWTRComposerViewController(initialText: "", image: image, videoURL: nil)
                    
                    composer1.delegate = self  as TWTRComposerViewControllerDelegate
                    present(composer1, animated: false, completion: {
                        print("This code always run")
                    })
                } else {
                    // NEW ACCOUNT OR USER WANT TO LOGIN
                    TWTRTwitter.sharedInstance().logIn(with: self) { session, error in
                      
                      print("session\(session)")
                        if session != nil {
                            // Log in succeeded
                            
                            let composer1 = TWTRComposerViewController(initialText: "", image: image, videoURL: nil)
                            composer1.delegate = self as TWTRComposerViewControllerDelegate
                            self.present(composer1, animated: true, completion: nil)
                        }
                        else {
                            
                            let alert = UIAlertController(title: KMessage, message: "Failed to authenticate by twitter. Please try later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: false, completion: nil)
                        }
                    }
                }
            }
        }
    }
    @IBAction func OtherAction(_ sender: Any)
    {
        let url = AddProuductData.sharedInstance?.arrImages![0].image!
        let textToShare = AddProuductData.sharedInstance?.description
        if let myWebsite = url{
            let objectsToShare = [textToShare as Any, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivityType.init(rawValue: "com.instagram.exclusivegram"), UIActivityType.postToTwitter , UIActivityType.postToFacebook]
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
        
        
    }
    @IBAction func BackAction(_ sender: Any)
    {
        CommonFunctions.sharedInstance.SetRootViewController(rootVC: .HomeNavigation)
    }
    //MARK:- Functions
    func removeImage(itemName:String) {
        print(itemName)
        let fullNameArr = itemName.components(separatedBy: "/")
        print(fullNameArr)
        let fileName = fullNameArr[fullNameArr.count-1]
        print(fileName)
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return
        }
        let filePath = "\(dirPath)/\(fileName)"
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print(error.debugDescription)
        }
        
    }
    func clearTempFolder() {
        let fileurl = AddProuductData.sharedInstance?.arrImages![0].imgUrl
        print(fileurl as Any)
        // let fileURL = NSURL(fileURLWithPath: fileurl)
        let fileManager = FileManager.default
        //  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        // let tempFolderPath = NSTemporaryDirectory()
        do {
          //  let filePaths = try fileManager.contentsOfDirectory(atPath: (fileurl?.absoluteString)!)
           // for filePath in filePaths {
                try fileManager.removeItem(atPath: (fileurl?.absoluteString)!)
           // }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    func checkForInstalledFB()
    {
        
//        let linkProperties = BranchLinkProperties()
//        linkProperties.channel = "Facebook"
//        linkProperties.feature = "Share"
//        var url = ""
//
//        // generate link
//        branchUniversalObject?.getShortUrl(with: linkProperties, andCallback: { (shareURL, error) in
//            url = shareURL ?? ""
//        })
      
        if isFBInstalled
        {
            print(FBSDKAccessToken.current())
//            let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
//            content.contentURL = URL(string: "myapp://profile")


                //AddProuductData.sharedInstance?.arrImages![0].imgUrl
            var images = [FBSDKSharePhoto]()
            if let imagss  = AddProuductData.sharedInstance?.arrImages{
            for (i,img) in imagss.enumerated(){
                print(i)
                if let image = img.image{

                let photoa = FBSDKSharePhoto()

                    photoa.image = image
                    photoa.isUserGenerated = true
                    photoa.caption = "dsfsdf"


                    images.append(photoa)
            }

            }
            }

            let photo  = FBSDKSharePhotoContent()
            photo.photos = images
            photo.ref =  "myapp://"
            
            let dialg = FBSDKShareDialog()
            dialg.mode =   FBSDKShareDialogMode.native
            dialg.delegate = self
    
            dialg.shareContent = photo
            dialg.show()
        }
        else
        {
            //            let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
            //            content.contentURL = NSURL(string: "http://www.hotfuse.com/hotfuseuploads/profileimage/547536113009.124.jpg") as! URL
            //                let dialg = FBSDKShareDialog()
            //                dialg.mode =   FBSDKShareDialogMode.web
            //                dialg.delegate = self
            //                dialg.shareContent = content
            //                dialg.show()
            
          //  print(AddProuductData.sharedInstance?.arrImages![0].imgUrl!)
          //  print(AddProuductData.sharedInstance?.description)
           doFacebookLogin()
            if let url = AddProuductData.sharedInstance?.arrImages![0].imgUrl!, let descrption  = AddProuductData.sharedInstance?.description
            {
            let mycontent = LinkShareContent(url: url, quote: descrption)
           // let mycontent = LinkShareContent(url:   url)

            let shareDialog = ShareDialog(content: mycontent)
            shareDialog.mode = .web
            //shareDialog.failsOnInvalidData = true
            do
            {
                try shareDialog.show()
                shareDialog.completion = { result in
                    switch result {
                    case .success:
                        FBSDKAccessToken.setCurrent(nil)
                        FBSDKProfile.setCurrent(nil)
                        self.fbLoginmanager.logOut()
                        DispatchQueue.main.async {
                            self.showAlert()
                        }
                    case .failed:
                        FBSDKAccessToken.setCurrent(nil)
                        FBSDKProfile.setCurrent(nil)
                        self.fbLoginmanager.logOut()
//                        DispatchQueue.main.async {
//                            self.showAlert()
//                        }
                    case .cancelled:
                        FBSDKAccessToken.setCurrent(nil)
                        FBSDKProfile.setCurrent(nil)
                        self.fbLoginmanager.logOut()
//                        DispatchQueue.main.async {
//                            self.showAlert()
//                        }
                    }
                }
            }
                
            catch(let error)
            {
                print(error)
            }
            }
            
        }
    }
    func doFacebookLogin()
    {
      fbLoginmanager.loginBehavior = .web
        fbLoginmanager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if (error == nil)
            {
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                print("result : ",result as Any)
                print("permissions", fbloginresult.grantedPermissions )
                if fbloginresult.grantedPermissions != nil
                {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        print(FBSDKAccessToken.current())
                        if((FBSDKAccessToken.current()) != nil)
                        {
                            self.getUserData()
                            //self.checkForInstalledFB()
                            
                        }
                    }
                }
                else
                {
                    print("grant permissions")
                    
                }
            }
        }
    }
    func getUserData()
    {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil)
            {
                print(result ?? "Blank")
                
            }
            else
            {
                print(error ?? "Blank")
            }
        })
    }
}
extension SuccessVC: FBSDKSharingDelegate
{
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        print("sharer Cancel",sharer)
    }
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("sharer Results", results)
    }
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("error",error)
    }
    
    
    // MARK:- DELEGATE METHODS FOR TWTR COMPOSER VIEW CONTROLLER
    func composerDidCancel(_ controller: TWTRComposerViewController) {
        dismiss(animated: false, completion: nil)
    }
    
    func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
        dismiss(animated: false, completion: nil)
    }
    
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        dismiss(animated: false, completion: nil)
        showAlert()
    }
    
    
    func showAlert()
    {
        
        let alert = UIAlertController(title: KMessage, message: "Post shared successfully.", preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        
        
        
        
        self.present(alert, animated: false, completion: nil)
        
        
    }
    
    
    
    
    
    
}
