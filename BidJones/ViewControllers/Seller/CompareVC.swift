//
//  CompareVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/13/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class CompareVC: UIViewController,UIScrollViewDelegate {
    
    //MARK: - Label Outlets
    @IBOutlet var lblNoItem: UILabel!
    //MARK: - UItableView Outlets
    @IBOutlet var tblItemList: UITableView!
    //MARK: - array Variable
    private lazy var arrSimilarItems = [ItemListData]()
    //MARK: - Int Variable
    var count = 0
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false

        
        UserDefaults.standard.setShowSellerInfo(value: false)
        UserDefaults.standard.setShowEditButton(value: false)

         GetList()
        // Do any additional setup after loading the view.
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {

            if (arrSimilarItems.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.count = self.count+KPaginationcount
                GetList()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    //MARK: - Other functions
    func GetList()  {
        if let categoryID = AddProuductData.sharedInstance?.itemType, let keyWords = AddProuductData.sharedInstance?.keySearchWord
        {
        //let urlStr = KsellergetCompairRates + "\(categoryID)" + "/" + "\(keyWords)" + "/" + "\(KPaginationcount)" + "/" + "\(count)"
            
            let urlStr = KsellergetCompairRates
            var parm = [String : Any]()
            parm["offset"] = count
            parm["limit"]  = KPaginationcount
            parm["category"] = categoryID
            parm["keywords"] = keyWords
            print(parm)
        print(urlStr)
        dataLoaded = false
        //parm[Kcategory_id] = KServicesType
            Compare.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            
            print(Response.count)
            if Response.count>0
            {
                if(self.count == 0)
                {
                    // self.arrServiceSell = Response
                    for item in Response
                    {
                        let oldIds = self.arrSimilarItems.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrSimilarItems.insert(item, at: self.arrSimilarItems.count)
                        }
                    }
                    self.tblItemList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrSimilarItems.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrSimilarItems.insert(item, at: self.arrSimilarItems.count)
                        }
                    }
                    self.tblItemList.reloadData()
                    print(self.arrSimilarItems)
                }
                print(self.count)
                self.lblNoItem.isHidden = true
            }
            else
            {
                if(self.count != 0)
                {
                    print("abcdefgh")
                    self.dataLoaded = true
                }
                if (self.count == 0 || self.arrSimilarItems.count == 0)
                {
                    self.lblNoItem.isHidden = false
                }
            }
            self.isLoading = false
            //            if self.isDeleted!
            //            {
            //                self.isDeleted = false
            //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
            //            }
        }
            , completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.GetList()
                    })
                }
                else if statusCode == 201
                {
                    self.dataLoaded = true
                    if (self.count == 0 || self.arrSimilarItems.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                    }
                    //                if self.isDeleted!
                    //                {
                    //                    self.isDeleted = false
                    //                    self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                    //                }
                }
                else
                {
                    if (self.count == 0 || self.arrSimilarItems.count == 0)
                    {
                        self.arrSimilarItems.removeAll()
                        self.tblItemList.reloadData()
                        self.lblNoItem.isHidden = false
                    }
                    
                }
                self.isLoading = false
                
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            self.isLoading = false
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            self.isLoading = false
        })
    }
    }
}
extension CompareVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = arrSimilarItems[indexPath.row]
        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
    }
}

extension CompareVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        return arrSimilarItems.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        guard let cell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell, for: indexPath) as? ProductTableCell else {
        //            let cell = ProductTableCell(style: .default, reuseIdentifier: KproductTableCell)
        //            cell.LoadData(dic:arrBooks[indexPath.row])
        //            return cell
        //        }
        let cell : CompareCell = tableView.dequeueReusableCell(withIdentifier: KcompareCell) as! CompareCell
        //cell.delegate = self
        cell.LoadData(dic:arrSimilarItems[indexPath.row], type: .Book)
        cell.tag = indexPath.row
        cell.selectionStyle = .none
        
        return cell
    }
}
