//
//  PreviewVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import QuickLook

class PreviewVC: UIViewController {
    //MARK:-  Outlets object
    @IBOutlet var btnCompare: CustomButton!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var collectionViewImages: UICollectionView!
    @IBOutlet var tblViewDetail: UITableView!
    @IBOutlet var tblViewHeight: NSLayoutConstraint!
    @IBOutlet var lblDescrptionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblMarketPlace: UILabel!
    @IBOutlet weak var tblViuewMarketPlace: UITableView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnAudio: UIButton!
    @IBOutlet var btnVideo: UIButton!
    @IBOutlet var btnPdf: UIButton!
    @IBOutlet var compareBtnHeight: NSLayoutConstraint!
    
    @IBOutlet var submitBtnTop: NSLayoutConstraint!
    //MARK : Variable
    var arrDetail = [[String:Any]]()
    private var arrImages = [ImagesData]()
    private var arrVideos = [VideoData]()
    private var arrVideoDiscript = [VideoData]()
    private var arrAudios = [AudioData]()
    private var arrPdf = [PdfData]()
    
    var arrImagesUrl  = [String]()
    var arrWatermarkImagesUrl  = [String]()
    var arrVideosUrl  = [String]()
    var arrVideoDiscriptUrl = [String]()
    var arrAudiosUrl  = [String]()
    var arrPdfUrl     = [String]()
    var arrRemoveMedia    = [String]()
    var arrRadioUrl    = [[String:Any]]()
    var arrcount = 0
    
    //MARK: - array Variable
    private lazy var arrSimilarItems = [ItemListData]()
    
    var count = 0
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    // private var count = 0
    private var totalCount = 0
    var timerObserver:Any?
    //for Vidoe Player
    var player              =  AVPlayer()
    var stopped             =  false
    var playerController    = AVPlayerViewController()
    var timer               : Timer? = nil
    var timeToStop          : Int    = 10
    var playerObserver      : AnyObject!
    var audioPlayer         : AVAudioPlayer?
    var playerItem          : AVPlayerItem!
    let preview = QLPreviewController()
    let tempURL = FileManager.default.temporaryDirectory.appendingPathComponent("BIDJONES.pdf")
    
    @IBOutlet weak var tblViewMarketHeight: NSLayoutConstraint!
    
    var tableViewHeight: CGFloat {
        tblViewDetail.layoutIfNeeded()
        return tblViewDetail.contentSize.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetList()
        if  AddProuductData.sharedInstance?.itemType == KTradeItemType || AddProuductData.sharedInstance?.itemType == KFreeItemType
        {
            btnCompare.isHidden = true
            submitBtnTop.constant = 40
        }
        
        if let itemTypeID = AddProuductData.sharedInstance?.itemType
        {
            if itemTypeID == 1 || itemTypeID == 2 || itemTypeID == 3 || itemTypeID == 6
            {
                btnCompare.isHidden = true
                submitBtnTop.constant = 40
            }
            print(itemTypeID)
        }
        if let imagesArr = AddProuductData.sharedInstance?.arrImages
        {
            
            arrImages = imagesArr
            self.pageControl.numberOfPages = arrImages.count
            self.pageControl.currentPage = 0
            collectionViewImages.reloadData()
            if(imagesArr.count<2)
            {
                pageControl.isHidden = true
            }
        }
        if let videosArr = AddProuductData.sharedInstance?.arrVideos
        {
            arrVideos = videosArr
        }
        if let videoDescriptArr = AddProuductData.sharedInstance?.arrVideosDiscript {
            arrVideoDiscript = videoDescriptArr
        }
        if let audiosArr = AddProuductData.sharedInstance?.arrAudios
        {
            arrAudios = audiosArr
        }
        if let pdfArr = AddProuductData.sharedInstance?.arrPdf
        {
            arrPdf = pdfArr
        }
        
        if arrAudios.count == 0
        {
            btnVideo.translatesAutoresizingMaskIntoConstraints = false
            btnVideo.leadingAnchor.constraint(equalTo: btnAudio.leadingAnchor).isActive = true
            btnAudio.isHidden = true
        }else{
            btnAudio.isHidden = false
        }
        if arrVideos.count == 0
        {
            btnPdf.translatesAutoresizingMaskIntoConstraints = false
            btnPdf.leadingAnchor.constraint(equalTo: btnVideo.leadingAnchor).isActive = true
            btnVideo.isHidden = true
            
        }else{
            btnVideo.isHidden = false
        }
        if arrVideoDiscript.count == 0 {
            print("your video count of  ddiscript zero")
        }
        
        if arrPdf.count == 0
        {
            btnPdf.isHidden = true
        }else{
            btnPdf.isHidden = false
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
        tblViewDetail.estimatedRowHeight = 1000
        tblViewDetail.rowHeight = UITableViewAutomaticDimension
        if let albumName = AddProuductData.sharedInstance?.albumName
        {
            var dic = [String:Any]()
            dic["Album Name"] = albumName
            arrDetail.append(dic)
        }
        if let title = AddProuductData.sharedInstance?.title
        {
            var dic = [String:Any]()
            
            if let itemType = AddProuductData.sharedInstance?.itemType, itemType == 3
            {
                // dic["Song Title"] = title
            }
            else
            {
                dic["Title"] = title
                arrDetail.append(dic)
                
            }
        }
        if let artistName = AddProuductData.sharedInstance?.artistName
        {
            var dic = [String:Any]()
            dic["Artist Name"] = artistName
            arrDetail.append(dic)
        }
        
        
        if let noOfPages = AddProuductData.sharedInstance?.noOfPages
        {
            var dic = [String:Any]()
            dic["No. Of Pages"] = noOfPages
            arrDetail.append(dic)
        }
        //        if let genre = AddProuductData.sharedInstance?.genre, let type = AddProuductData.sharedInstance?.itemType, type != 1
        if let genre = AddProuductData.sharedInstance?.genre
        {
            var dic = [String:Any]()
            dic["Genre"] = genre
            arrDetail.append(dic)
        }
        if let typeOfBook = AddProuductData.sharedInstance?.typeOfBook
        {
            var dic = [String:Any]()
            dic["Book Type"] = typeOfBook
            arrDetail.append(dic)
        }
        if let serviceType = AddProuductData.sharedInstance?.serviceType ,let type  = AddProuductData.sharedInstance?.itemType, type != 1
        {
            var dic = [String:Any]()
            dic["Service Type"] = serviceType
            arrDetail.append(dic)
        }
        if let musicName = AddProuductData.sharedInstance?.musicNames
        {
            let musicNames =
                musicName.joined(separator: ",")
            var dic = [String:Any]()
            dic["Music name"] = musicNames
            arrDetail.append(dic)
        }
        if let urlBook = AddProuductData.sharedInstance?.bookUrl
        {
            var dic = [String:Any]()
            dic["Book Url"] = urlBook
            arrDetail.append(dic)
        }
        if let TC = AddProuductData.sharedInstance?.TC
        {
            var dic = [String:Any]()
            dic["T&C of Service"] = TC
            arrDetail.append(dic)
        }
        if let duration = AddProuductData.sharedInstance?.duration
        {
            var dic = [String:Any]()
            dic["Duration"] = duration
            arrDetail.append(dic)
        }
        if let bidType = AddProuductData.sharedInstance?.minBidType
        {
            var dic = [String:Any]()
            dic["Min Bid Type"] = bidType
            arrDetail.append(dic)
        }
        if let minBidAmount = AddProuductData.sharedInstance?.minBidAmount
        {
            print(minBidAmount)
            print(minBidAmount.cleanValue)
            
            var dic = [String:Any]()
            //  dic["Minimum Bid Amount"] = "$" +  minBidAmount.cleanValue
            dic["Price"] = "$" +  minBidAmount.cleanValue
            arrDetail.append(dic)
        }
        
        if let keySearchWord = AddProuductData.sharedInstance?.keySearchWord
        {
            var dic = [String:Any]()
            dic["Key Search Words"] = keySearchWord
            arrDetail.append(dic)
        }
        if let location = AddProuductData.sharedInstance?.location
        {
            var dic = [String:Any]()
            dic["Location"] = location
            arrDetail.append(dic)
        }
        print(arrDetail)
        tblViewDetail.reloadData()
        
        if let description = AddProuductData.sharedInstance?.description
        {
            lblDescription.text = description
            // lblDescrptionHeight.constant = lblDescription.optimalHeight
        }
        //  lblDescription
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tblViewHeight?.constant = self.tblViewDetail.contentSize.height
        
    }
    
    //MARK: - Other functions
    //MARK:- Key Observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        
        if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 0
        {
            print("stop")
            stopped = true
        }
        else if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 1
        {
            print("play")
            if (self.playerController.player?.currentTime().seconds)! >= Double(10.0)
            {
                playerController.player?.seek(to: CMTimeMake(Int64(0), Int32(10)), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (result) in
                    print("result", result)
                    
                })
            }
            stopped = false
        }
    }
    
    //MARK:- Functions
    func DeleteMedia()
    {
        if arrRemoveMedia.count>0
        {
            //            if let str = arrRemoveMedia[0] as? String , str.contains("amazonaws")
            //            {
            //                var parm = [String:Any]()
            //                parm["filename"] = arrRemoveMedia[0]
            //                let urlStr = Ksellerdeletemedia
            print(arrRemoveMedia)
            var arrMediaDelete = [String]()
            for item in arrRemoveMedia
            {
                //                SDImageCache.shared().removeImage(forKey: item
                //                    , fromDisk: true, withCompletion: {
                //                        print("Success")
                //                })
                print(item)
                if !item.contains("Containers")
                {
                    arrMediaDelete.append(item)
                }
            }
            print(arrMediaDelete)
            if arrMediaDelete.count>0
            {
                //                        if let str = arrRemoveMedia[0] as? String , str.contains("amazonaws")
                //                        {
                var parm = [String:Any]()
                parm["filename"] = arrMediaDelete
                let urlStr = Ksellerdeletemedia
                MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading
                Preview.sharedManager.PostDeleteMediaApi(url: urlStr,parameter:parm ,Target: self, completionResponse: { (Response) in
                    print(Response)
                    //                    SDImageCache.shared().removeImage(forKey: self.arrRemoveMedia[0]
                    //                        , fromDisk: true, withCompletion: {
                    //                            print("Success")
                    //                    })
                    self.arrRemoveMedia.removeAll()
                    self.DeleteMedia()
                }, completionnilResponse: { (Response) in
                    print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        })
                    }
                    else if let msg =  Response[Kmessage]
                    {
                        self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                    }
                }, completionError: { (error) in
                    self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                },networkError: {(error) in
                    self.showAlertMessage(titleStr: KMessage, messageStr: error)
                })
            }
            else
            {
                Submitstep1()
            }
            //            }
            //            else
            //            {
            //                self.arrRemoveMedia.remove(at: 0)
            //                self.DeleteMedia()
            //            }
            
        }
        else
        {
            Submitstep1()
        }
        
    }
    func MakeSellerShowAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "You are not Registered as seller? If you want to post ad, Upgrade to seller", preferredStyle: .alert)
        
        // Create the actions
        let NoAction = UIAlertAction(title: "Upgrade", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.MakeMeSeller()
            NSLog("OK Pressed")
        }
        let YesAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        self.dismiss(animated: true, completion: nil)
        alertController.addAction(NoAction)
        alertController.addAction(YesAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func MakeMeSeller()  {
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            //print(Response)
            if let msg =  Response[Kmessage]
            {
                //                self.arrBooks.remove(at: self.indexDelete!)
                //                self.tblBooksList.reloadData()
                //                self.isDeleted = true
                //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                UserDefaults.standard.setUserType(value: true)
                self.showToast(message: msg as! String)
                self.SubmitAction(AnyObject.self)
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    func PlayVideo()
    {
        if timerObserver != nil
        {
            self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
        }
        //  let item = AVPlayerItem(url: URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")!)
        let item = AVPlayerItem(url: arrVideos[0].videoUrl!)
        player =  AVPlayer(playerItem: item)
        playerController.player = player
        if #available(iOS 11.0, *) {
            // use the feature only available in iOS 9
            // for ex. UIStackView
            self.playerController.setValue(true, forKey: "requiresLinearPlayback")
            
        } else {
            // or use some work around
        }
        //self.playerController.setValue(true, forKey: "requiresLinearPlayback")
        playerController.player?.play()
        if (self.playerController.player?.currentTime().seconds)! >= Double(10.0)
        {
            playerController.player?.seek(to: CMTimeMake(Int64(0), Int32(10)), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (result) in
                print("result", result)
            })
        }
        self.playerController.player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
        self.present(playerController,animated:true,completion:nil)
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                print(viewControllers[viewControllers.count - nb]);
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func clearTempFolder() {
        let fileurl = AddProuductData.sharedInstance?.arrImages![0].imgUrl
        print(fileurl as Any)
        // let fileURL = NSURL(fileURLWithPath: fileurl)
        let fileManager = FileManager.default
        let tempFolderPath = NSTemporaryDirectory()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: (fileurl?.absoluteString)!)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: tempFolderPath + filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    func Submitstep1()
    {
        var parm = [String : Any]()
        var urlStr = KAddItemApi
        if(AddProuductData.sharedInstance?.itemType == KTradeItemType)
        {
            // isItemForTrade
            urlStr = KAddTradeItemApi
            parm["status"] = 1
            if(AddProuductData.sharedInstance?.isItemForTrade == true)
            {
                parm["status"] = 6
                parm["item_id"] = AddProuductData.sharedInstance?.itemID
            }
        }
        
        if(AddProuductData.sharedInstance?.itemType == KFreeItemType)
        {
            // isItemForTrade
            urlStr = Kfreenewitem
            parm["status"] = 1
            //            if(AddProuductData.sharedInstance?.isItemForTrade == true)
            //            {
            //                parm["status"] = 6
            //                parm["item_id"] = AddProuductData.sharedInstance?.itemID
            //            }
        }
        
        if let isEdited = AddProuductData.sharedInstance?.IsEdited, isEdited == true
        {
            if(AddProuductData.sharedInstance?.itemType == KTradeItemType)
            {
                urlStr = Ktradeupdateitem
            }
            else if(AddProuductData.sharedInstance?.itemType == KFreeItemType)
            {
                urlStr = Kfreeupdateitem
            }
            else
            {
                urlStr = Ksellerupdateitem
                
            }
            if let ItemID = AddProuductData.sharedInstance?.itemID
            {
                parm["item_id"] = ItemID
            }
        }
        
        
        
        
        parm[Kcategory_id]   =  AddProuductData.sharedInstance?.itemType
        if let title =  AddProuductData.sharedInstance?.title
        {
            parm[Ktitle] = title
        }
        if let artistName =  AddProuductData.sharedInstance?.artistName
        {
            parm["singer"] = artistName
        }
        if arrRadioUrl.count>0
        {
            print("all url radio : \(arrRadioUrl)")
            parm["radio_file"] = arrRadioUrl
        }
        if let description = AddProuductData.sharedInstance?.description
        {
            parm[Kdescription] = description
        }
        if let id = AddProuductData.sharedInstance?.id
        {
            parm[Kseller_id] = id
        }
        if let bidTypeID =  AddProuductData.sharedInstance?.minBidTypeId
        {
            parm[Kbid_type_id] = Int(bidTypeID)
        }
        
        if let itemType = AddProuductData.sharedInstance?.itemType, itemType == 4
        {
            if let serviceType = AddProuductData.sharedInstance?.serviceType
            {
                parm["service_type"] = serviceType
            }
            parm[Kcategory_meta_id] = NSNull()
        }
        else
        {
            if let GenreID = AddProuductData.sharedInstance?.genreId
            {
                parm[Kcategory_meta_id] = Int(GenreID)
            }
        }
        
        print(parm)
        
        if let minBid = AddProuductData.sharedInstance?.minBidAmount
        {
            print(minBid)
            let myDoubleString = String(minBid)
            print(myDoubleString)
            print(Float(myDoubleString) as Any)
            parm[Kmin_bid] = myDoubleString
        }
        if let toc =  AddProuductData.sharedInstance?.TC
        {
            parm[Ktoc] = toc
        }
        if let keywords = AddProuductData.sharedInstance?.keySearchWord
        {
            parm[Kkeywords] = keywords
        }
        if let albumName = AddProuductData.sharedInstance?.albumName
        {
            parm[Kalbum_name]  = albumName
        }
        if let duration = AddProuductData.sharedInstance?.duration
        {
            parm[Kduration] = duration
        }
        if let numOfPages = AddProuductData.sharedInstance?.noOfPages
        {
            parm[Ktotal_pages] = numOfPages
        }
        if let url = AddProuductData.sharedInstance?.bookUrl
        {
            parm[Kurl] = url
        }
        if let isEscrow = AddProuductData.sharedInstance?.is_escrow
        {
            parm[Kescrow_service] = isEscrow
            
        }
        if let lat = AddProuductData.sharedInstance?.lat
        {
            parm[Klat] = lat
        }
        if let lng = AddProuductData.sharedInstance?.lng
        {
            parm[Klng] = lng
        }
        if arrImagesUrl.count>0
        {
            parm[Kimages] = arrImagesUrl
        }
        
        if arrWatermarkImagesUrl.count>0
        {
            parm[kpreview_images] = arrWatermarkImagesUrl
        }
        
        if arrVideosUrl.count>0
        {
            parm[Kvideos] = arrVideosUrl[0]
        }
        else
        {
            parm[Kvideos] = ""
        }
        if arrVideoDiscriptUrl.count > 0 {
            
            parm[kvideosDiscript] = arrVideoDiscriptUrl[0]
        }
        else
        {
            parm[kvideosDiscript] = ""
        }
        
        
        if let Location = AddProuductData.sharedInstance?.location
        {
            parm[Kaddress] = Location
        }
        if arrAudiosUrl.count>0
        {
            parm[Kmusic] = arrAudiosUrl
            print(AddProuductData.sharedInstance?.musicNames ?? "Blnk")
            parm[Kmusic_name] = AddProuductData.sharedInstance?.musicNames
        }
        if arrPdfUrl.count>0
        {
            parm[Kpdf] = arrPdfUrl[0]
        }
        
        print(parm)
        Preview.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
            print("your response of video : \(Response)")
            if let msg =  Response[Kmessage]
            {
                if let result = (Response[Kresult] as? [String:Any])
                {
                    //self.clearTempFolder()
                    if let imagesArr = result[Kimages] as? [String]
                    {
                        var arrImages = [ImagesData]()
                        var index = 0
                        for item in imagesArr
                        {
                            let url = URL(string: item)
                            if let data = ImagesData(ImgUrl: url , Image:AddProuductData.sharedInstance?.arrImages![index].image){
                                arrImages.append(data)
                            }
                            index = index+1
                        }
                        AddProuductData.sharedInstance?.arrImages = arrImages
                    }
                }
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                
                if AddProuductData.sharedInstance?.IsEdited == true || (AddProuductData.sharedInstance?.itemType == KTradeItemType && AddProuductData.sharedInstance?.itemID != nil)
                
                {
                    self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .root, ViewController: .none, rootViewController: .HomeNavigation,Data:nil)
                }
                else
                {
                    self.AlertWithNavigatonPurpose(message: msg as! String, navigationType: .push, ViewController: .Success, rootViewController: .none,Data:nil)
                }
                
                //                let alert = UIAlertController(title: KMessage, message: msg as! String, preferredStyle: UIAlertControllerStyle.alert)
                //                let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
                //                    UIAlertAction in
                //                    NSLog("OK Pressed")
                //                        self.popBack(3)
                //                }
                //                // Add the actions
                //                alert.addAction(okAction)
                //                self.present(alert, animated: true, completion: nil)
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.SubmitAction(AnyObject.self)
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    
    //MARK: - IBActions
    @IBAction func AudioAction(_ sender: Any) {
        detailType = .Preview
        KCommonFunctions.PushToContrller(from: self, ToController: .AudioPlayer, Data: nil)
        
    }
    @IBAction func VideoAction(_ sender: Any) {
        PlayVideo()
    }
    @IBAction func PdfAction(_ sender: Any) {
        MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = "Downloading..."
        
        let Url = arrPdf[0].pdfUrl
        
        
        // CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Pdf, Data: Url)
        preview.delegate = self
        preview.dataSource = self
        preview.currentPreviewItemIndex = 0
        URLSession.shared.dataTask(with: Url) { (data, response,error) in
            guard let data = data, error == nil else {
                //  in case of failure to download your data you need to present alert to the user and update the UI from the main thread
                DispatchQueue.main.async
                {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    let alert = UIAlertController(title: "Alert", message: error?.localizedDescription ?? "Failed to download the pdf!!!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: false)
                }
                return
            }
            
            do {
                try data.write(to: self.tempURL, options: .atomic)
                
                DispatchQueue.main.async
                {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if self.tempURL.typeIdentifier == "com.adobe.pdf"
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        self.navigationController?.pushViewController(self.preview, animated: false)
                    }
                    else
                    {
                        print("the data downloaded it is not a valid pdf file")
                    }
                }
            } catch {
                print(error)
                return
            }
        }.resume()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func CompareAction(_ sender: Any)
    {
        KCommonFunctions.PushToContrller(from: self, ToController: .compare, Data: nil)
    }
    @IBAction func SubmitAction(_ sender: Any) {
        
        if let itemTypeID = AddProuductData.sharedInstance?.itemType, itemTypeID != 8
        {
            guard let userType = UserDefaults.standard.getUserType(), userType == true else
            {
                MakeSellerShowAlert()
                return
            }
        }
        
        AddProuductData.sharedInstance?.arrLocalPdf = arrPdf
        AddProuductData.sharedInstance?.arrLocalImages = arrImages
        AddProuductData.sharedInstance?.arrLocalVideos = arrVideos
        AddProuductData.sharedInstance?.arrLocalAudios = arrAudios
        AddProuductData.sharedInstance?.arrlocalVideoDiscript = arrVideoDiscript
        
        print(arrImages)
        
        let imagescount = arrImages.count
        let videoCount = arrVideos.count
        let videoDiscriptCount = arrVideoDiscript.count
        let audioCount = arrAudios.count
        let pdfCount    = arrPdf.count
        
        totalCount = imagescount + videoCount + audioCount + pdfCount + videoDiscriptCount
        let total = audioCount+imagescount+videoCount + pdfCount + videoDiscriptCount
        print("for your total count is: \(total)")
        print(imagescount)
        print(videoCount)
        print(audioCount)
        print(arrcount)
        print("your count for all : \(videoDiscriptCount)")
        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
        if(arrcount < imagescount)
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.arrcount+1) + "/" + String(self.totalCount)
            //print(arrImages)
            
            var Type:AWSDataType?
            Type = .Image
            
            if let itemTypeID = AddProuductData.sharedInstance?.itemType
            {
                if itemTypeID == 2
                {
                    Type = .Graphic
                }
                print(itemTypeID)
            }
            
            UploadMultiPart.sharedInstance.UploadData(awsType: Type!, url: arrImages[arrcount].imgUrl, completionHandler: { (error, urlStr, watermark) -> (Void) in
                //print(error ?? "Blank")
                print(urlStr ?? "Blank")
                if error == nil
                {
                    print(watermark as Any as Any)
                    self.arrImagesUrl.append(urlStr!)
                    print(self.arrImagesUrl as Any)
                    self.arrWatermarkImagesUrl.append(watermark!)
                    print(self.arrWatermarkImagesUrl as Any)
                    
                    self.arrcount = self.arrcount+1
                    DispatchQueue.main.async {
                        self.SubmitAction(AnyObject.self)
                    }
                }
                else
                {
                    self.AlertMessageWithOkCancelAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {(str) in
                        if str == "Yes"
                        {
                            self.arrcount = 0
                            self.arrImagesUrl.removeAll()
                            self.arrVideosUrl.removeAll()
                            self.arrVideoDiscriptUrl.removeAll()
                            self.arrAudiosUrl.removeAll()
                            self.arrPdfUrl.removeAll()
                            //  self.SubmitAction(AnyObject.self)
                        }
                    })
                }
            }, completionnilResponse: { (Response) in
                print("upload",Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.SubmitAction(AnyObject.self)
                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, Target: self, networkError: {(error) in
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
            
        }
        else if (arrcount < audioCount+imagescount)
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.arrcount+1) + "/" + String(self.totalCount)
            //print(arrAudios)
            //print(arrAudios[count-imagescount].audioURL)
            
            UploadMultiPart.sharedInstance.UploadData(awsType: .Song, url: arrAudios[arrcount-imagescount].audioURL, completionHandler: { (error, urlStr, watermark) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrAudiosUrl.append(urlStr!)
                    if self.arrAudios[self.arrcount-imagescount].isRadio == true
                    {
                        var dic = [String:Any]()
                        dic["mediaUrl"] = urlStr
                        dic["mediaName"] = self.arrAudios[self.arrcount-imagescount].title
                        self.arrRadioUrl.append(dic)
                    }
                    self.arrcount = self.arrcount+1
                    DispatchQueue.main.async {
                        self.SubmitAction(AnyObject.self)
                    }
                }else
                {
                    self.AlertMessageWithOkCancelAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {(str) in
                        if str == "Yes"
                        {
                            self.arrcount = 0
                            self.arrImagesUrl.removeAll()
                            self.arrVideosUrl.removeAll()
                            self.arrVideoDiscriptUrl.removeAll()
                            self.arrAudiosUrl.removeAll()
                            self.arrPdfUrl.removeAll()
                            //  self.SubmitAction(AnyObject.self)
                        }
                    })
                }
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.SubmitAction(AnyObject.self)
                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else if(arrcount < audioCount+imagescount+videoCount)
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.arrcount+1) + "/" + String(self.totalCount)
            //print(arrVideos)
            UploadMultiPart.sharedInstance.UploadData(awsType: .Video, url: arrVideos[arrcount-imagescount-audioCount].videoUrl, completionHandler: { (error, urlStr, watermark) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrVideosUrl.append(urlStr!)
                    
                    self.arrcount = self.arrcount+1
                    DispatchQueue.main.async {
                        self.SubmitAction(AnyObject.self)
                    }
                }
                else
                {
                    self.AlertMessageWithOkCancelAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {(str) in
                        if str == "Yes"
                        {
                            self.arrcount = 0
                            self.arrImagesUrl.removeAll()
                            self.arrVideosUrl.removeAll()
                            self.arrVideoDiscriptUrl.removeAll()
                            self.arrAudiosUrl.removeAll()
                            self.arrPdfUrl.removeAll()
                            // self.SubmitAction(AnyObject.self)
                        }
                    })
                }
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.SubmitAction(AnyObject.self)
                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        
        
        else if (arrcount < pdfCount+imagescount+audioCount+videoCount)
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.arrcount+1) + "/" + String(self.totalCount)
            //print(arrPdf)
            // print("Pdf Url : \(arrPdf[count-imagescount-audioCount-videoCount].pdfUrl)")
            
            
            UploadMultiPart.sharedInstance.UploadData(awsType: .Pdf, url: arrPdf[arrcount-imagescount-audioCount-videoCount].pdfUrl as URL, completionHandler: { (error, urlStr, watermark) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrPdfUrl.append(urlStr!)
                    self.arrcount = self.arrcount+1
                    DispatchQueue.main.async {
                        self.SubmitAction(AnyObject.self)
                    }
                }
                else
                {
                    self.AlertMessageWithOkCancelAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {(str) in
                        if str == "Yes"
                        {
                            self.arrcount = 0
                            self.arrImagesUrl.removeAll()
                            self.arrVideosUrl.removeAll()
                            self.arrVideoDiscriptUrl.removeAll()
                            self.arrAudiosUrl.removeAll()
                            self.arrPdfUrl.removeAll()
                            //self.SubmitAction(AnyObject.self)
                        }
                    })
                }
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.SubmitAction(AnyObject.self)
                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
            let total = audioCount+imagescount+videoCount + pdfCount + videoDiscriptCount
            print("for your total count is: \(total)")
            print("your count is for videoDiscriptCount :\(arrcount)")
        }
        
        else if(arrcount < audioCount+imagescount+videoCount + pdfCount + videoDiscriptCount)
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.arrcount+1) + "/" + String(self.totalCount)
            //print(arrVideos)
            UploadMultiPart.sharedInstance.UploadData(awsType: .Video, url: arrVideoDiscript[arrcount-imagescount-audioCount-videoCount-pdfCount].videoUrl, completionHandler: { (error, urlStr, watermark) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrVideoDiscriptUrl.append(urlStr!)
                    print("you reached here description url")
                    self.arrcount = self.arrcount+1
                    DispatchQueue.main.async {
                        self.SubmitAction(AnyObject.self)
                    }
                }
                else
                {
                    self.AlertMessageWithOkCancelAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {(str) in
                        if str == "Yes"
                        {
                            self.arrcount = 0
                            self.arrImagesUrl.removeAll()
                            self.arrVideosUrl.removeAll()
                            self.arrVideoDiscriptUrl.removeAll()
                            self.arrAudiosUrl.removeAll()
                            self.arrPdfUrl.removeAll()
                            // self.SubmitAction(AnyObject.self)
                        }
                    })
                }
            }, completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.SubmitAction(AnyObject.self)
                    })
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        
        else
        {
            //print(arrImagesUrl)
            //print(arrAudiosUrl)
            //  print(AddProuductData.sharedInstance?.arrRemoveMedia! as Any)
            if let ArrRemoveMedia = AddProuductData.sharedInstance?.arrRemoveMedia, ArrRemoveMedia.count>0
            {
                arrRemoveMedia = ArrRemoveMedia
                DeleteMedia()
            }
            else
            {
                Submitstep1()
            }
            
        }
    }
    
    //MARK: - UIScrollView Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collectionViewImages.contentOffset
        visibleRect.size = collectionViewImages.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = collectionViewImages.indexPathForItem(at: visiblePoint) else { return }
        
        print(indexPath)
        self.pageControl.currentPage = indexPath.row
        isScrolling = false
    }
    
}
extension PreviewVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KpreviewCollectionCell, for: indexPath as IndexPath) as? PreviewCollectionCell
        {
            cell.LoadData(Data: arrImages, index: indexPath)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        print(collectionView.bounds.width)
        print(collectionView.bounds.height)
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    //    func configureCell(cell: UICollectionViewCell, forItemAtIndexPath: NSIndexPath)
    //        {
    //        cell.backgroundColor = UIColor.black
    //        }
    
    //MARK: - UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
    }
}
extension PreviewVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tblViuewMarketPlace{
            let data = arrSimilarItems[indexPath.row]
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }else{
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

extension PreviewVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        if tableView == self.tblViuewMarketPlace{
            
            return arrSimilarItems.count
        }else{
            return arrDetail.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var myCell = UITableViewCell ()
        if tableView == self.tblViuewMarketPlace{
            
            let cell : CompareCell = tableView.dequeueReusableCell(withIdentifier: KcompareCell) as! CompareCell
            //cell.delegate = self
            cell.LoadData(dic:arrSimilarItems[indexPath.row], type: .Book)
            cell.tag = indexPath.row
            cell.selectionStyle = .none
            self.tblViewMarketHeight.constant = self.tblViuewMarketPlace.contentSize.height
            myCell = cell
        }else{
            
            let cell : PreviewTableCell = tableView.dequeueReusableCell(withIdentifier: KpreviewTableCell) as! PreviewTableCell
            cell.LoadData(Data: arrDetail[indexPath.row])
            cell.selectionStyle = .none
            myCell = cell
        }
        
        return myCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblViuewMarketPlace{
            return 100
        }else{
            return 44
        }
    }
}
extension PreviewVC : QLPreviewControllerDelegate, QLPreviewControllerDataSource
{
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return tempURL as QLPreviewItem
    }
    func previewControllerDidDismiss(_ controller: QLPreviewController) {
        //popBack(2)
    }
    
    
    
}

extension PreviewVC: UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            
            if (arrSimilarItems.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.count = self.count+KPaginationcount
                GetList()
            }
        }
    }
    
    //MARK: - Other functions
    func GetList()  {
        if let categoryID = AddProuductData.sharedInstance?.itemType, let keyWords = AddProuductData.sharedInstance?.keySearchWord
        {
            //let urlStr = KsellergetCompairRates + "\(categoryID)" + "/" + "\(keyWords)" + "/" + "\(KPaginationcount)" + "/" + "\(count)"
            
            let urlStr = KsellergetCompairRates
            var parm = [String : Any]()
            parm["offset"] = count
            parm["limit"]  = KPaginationcount
            parm["category"] = categoryID
            parm["keywords"] = keyWords
            print(parm)
            print(urlStr)
            dataLoaded = false
            //parm[Kcategory_id] = KServicesType
            Compare.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
                
                print(Response.count)
                if Response.count>0
                {
                    if(self.count == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrSimilarItems.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSimilarItems.insert(item, at: self.arrSimilarItems.count)
                            }
                        }
                        self.tblViuewMarketPlace.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrSimilarItems.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSimilarItems.insert(item, at: self.arrSimilarItems.count)
                            }
                        }
                        self.tblViuewMarketPlace.reloadData()
                        print(self.arrSimilarItems)
                    }
                    print(self.count)
                    self.lblMarketPlace.isHidden = false
                    
                }
                else
                {
                    if(self.count != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.count == 0 || self.arrSimilarItems.count == 0)
                    {
                        self.lblMarketPlace.isHidden = true
                        self.tblViewMarketHeight.constant = 0.0
                    }
                }
                self.isLoading = false
                //            if self.isDeleted!
                //            {
                //                self.isDeleted = false
                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                //            }
            }
            , completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.GetList()
                    })
                }
                else if statusCode == 201
                {
                    self.dataLoaded = true
                    if (self.count == 0 || self.arrSimilarItems.count == 0)
                    {
                        self.lblMarketPlace.isHidden = true
                        self.tblViewMarketHeight.constant = 0.0
                    }
                    //                if self.isDeleted!
                    //                {
                    //                    self.isDeleted = false
                    //                    self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                    //                }
                }
                else
                {
                    if (self.count == 0 || self.arrSimilarItems.count == 0)
                    {
                        self.arrSimilarItems.removeAll()
                        self.tblViuewMarketPlace.reloadData()
                        self.lblMarketPlace.isHidden = true
                        self.tblViewMarketHeight.constant = 0.0
                    }
                    
                }
                self.isLoading = false
                
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        }
    }
}
