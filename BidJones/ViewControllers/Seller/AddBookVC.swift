//
//  AddBookVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/6/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import MobileCoreServices
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import Photos


class  AddBookVC:UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,ImagesCollectionCellDelegate,VideosCollectionCellDelegate , LocationProtocol {
  
    

    @IBOutlet var ViewDelete: UIView!

    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var btnProceed: CustomButton!

    //MARK: - Label Outlets
    @IBOutlet var lblPdfName: UILabel!
    //MARK: - TextField Outlets
    @IBOutlet var txtFld_Location: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_KeySearchWord: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_MinBidAmount: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Title: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Descrption: CustomUITextFieldWithPadding!
    @IBOutlet var txtFldBidTYpe: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Genre: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_BookUrl: UITextField!
    @IBOutlet var txtFld_NumOfPages: UITextField!
    @IBOutlet var txtFld_forOpeningPickers: UITextField!
    //MARK: - TextView Outlets
    //MARK: - UICollectionView Outlets
    @IBOutlet var CollectionView_Images: UICollectionView!
    @IBOutlet var CollectionView_Video: UICollectionView!    //MARK: - UItableView Outlets
    //MARK: - UISCrollView Outlets
    @IBOutlet var scrollView_main: UIScrollView!
    //MARK: - UIButton Outlets
    @IBOutlet var btnPdf: CustomButton!
    @IBOutlet weak var btnOpenLink: UIButton!
    //MARK: - UIImageView Outlets
    //MARK: - UIView Outlets
    //MARK: - UIPicker Outlets
    //MARK: - Int Variable
    private var count = 0
    private var totalCount = 0
    private var strGenreID:String?
    private var strBidTypeID:String?
    private var maxNumImages = 0
    private var DeletedIndex = 0

    //MARK: - NSData Variables
    //MARK: - Bool Variable
    var isGenrePicker = true
    var is_escrow = Bool()

    //MARK: - String Variable
    //MARK: - array Variable
    var arrImages = [ImagesData]()
    var arrVideos = [VideoData]()
    var arrPdf   = [PdfData]()
    var arrImagesUrl = [String]()
    var arrVideosUrl  = [String]()
    var arrPdfUrl  = [String]()
    var arrGenre =   [GenreData]()
    var arrBidType = [BidTypeData]()
    var arrRemoveMedia   = [String]()

    var arrPicker = [Any]()

    //MARK: - UIPickerView Variable
    //MARK: - dictionary Variable
    //MARK: - UIImagePickerController
    private var imagePicker =  UIImagePickerController()
    //MARK: - UIPickerView Variable
    private var pickerView = UIPickerView()
    //MARK: - AudioCollection_heightConstraints
    @IBOutlet var AudioCollection_heightConstraints: NSLayoutConstraint!
    @IBOutlet var ImagesCollection_heightConstarints: NSLayoutConstraint!
    //MARK: - Enum Variable
    
    enum PickerType: Int {
        case GenrePicker = 1
        case BidTypePicker = 2
        init() {
            self = .GenrePicker
        }
    }
    private var pickerType: PickerType?
    var productDetail:ProductData?
    var deleteItemType:AWSDataType?


    override func viewDidLoad() {
        super.viewDidLoad()
        ViewDelete.isHidden = true

        self.hideKeyboardWhenTappedAround()

//        saveImage()
        GetGener()
        is_escrow = true
        lblPdfName.text = ""
        //  AudioCollection_heightConstraints.constant = 0
        self.automaticallyAdjustsScrollViewInsets = false
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFld_forOpeningPickers.inputView = pickerView
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFld_forOpeningPickers.inputAccessoryView = toolBar
        txtFld_MinBidAmount.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonAction), Title: KDone)
         txtFld_NumOfPages.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButton_NumOfPages_Action), Title: KNext)

        AddProuductData.sharedInstance?.Reinitilize()
        LoadDataForEdit()
        //Set Attributed text for btn open link
        KCommonFunctions.setAttributedStringInBtnOpenLink(button: btnOpenLink)

    }
    override func viewWillAppear(_ animated: Bool) {

    }
    override func viewDidAppear(_ animated: Bool) {
          }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Other Functions
    func ShowAlert()
    {
        ViewDelete.isHidden = false
    }
    //Set data for web page view controller
    func setDataForWebPageViewController(){
        var dict = [String: Any]()
        dict["url"] = "https://app.bernstein.io/bidjones"
        dict["title"] = "Secure Trade"
        KCommonFunctions.PushToContrller(from: self, ToController: .TermsNew, Data: dict)
    }
    func LoadDataForEdit()
    {
        if let Data = productDetail
        {
            self.title = "EDIT BOOK"
            btnProceed.setTitle("UPDATE", for: .normal)

            print(Data)
            print(Data)
            if let ItemID = Data.itemId
            {
                AddProuductData.sharedInstance?.itemID = ItemID
            }
            if let title =  Data.title
            {
                txtFld_Title.text = title
                AddProuductData.sharedInstance?.title = title
            }
            if let description = Data.description
            {
                txtFld_Descrption.text = description
                AddProuductData.sharedInstance?.description = description
            }
            if let GenreID = Data.genreId
            {
                strGenreID = "\(GenreID)"
                AddProuductData.sharedInstance?.genreId = Int(GenreID)
            }
            if let typeOfBook = Data.subCategoryName
            {
                txtFld_Genre.text = typeOfBook
                AddProuductData.sharedInstance?.serviceType = typeOfBook
            }
            if let numOfPages = Data.noOfPages
            {
                txtFld_NumOfPages.text = numOfPages
                AddProuductData.sharedInstance?.noOfPages = numOfPages
            }
            if let url = Data.bookUrl
            {
                txtFld_BookUrl.text = url
                AddProuductData.sharedInstance?.bookUrl = url
            }
            if let bidTypeID = Data.minBidTypeId
            {
                strBidTypeID = "\(bidTypeID)"
                AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
            }
            if let minBidType = Data.minBidType
            {
                txtFldBidTYpe.text = minBidType
                AddProuductData.sharedInstance?.minBidType = minBidType
            }
            if let minBid = Data.minBidAmount
            {
                txtFld_MinBidAmount.text = "\(minBid)"
                AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
            }
            if let ArrVideos = productDetail?.arrVideos
            {
                arrVideos = ArrVideos
                CollectionView_Video.reloadData()
                AddProuductData.sharedInstance?.arrVideos = ArrVideos
            }  
            if let keywords = Data.keySearchWord
            {
                txtFld_KeySearchWord.text = keywords
                AddProuductData.sharedInstance?.keySearchWord = keywords
            }
//            if arrImages.count>0
//            {
//                AddProuductData.sharedInstance?.arrImages = arrImages
//            }
            if let ArrImages = Data.arrImages
            {
                arrImages = ArrImages
                CollectionView_Images.reloadData()
                AddProuductData.sharedInstance?.arrImages = ArrImages
            }
            if let ArrPdf = Data.arrPdf
            {
                arrPdf = ArrPdf
                AddProuductData.sharedInstance?.arrPdf = ArrPdf
                var arr = ArrPdf[0].pdfUrl.absoluteString.components(separatedBy: "/")
                lblPdfName.text = arr[arr.count-1]
                
//                if let Data = PdfData(PdfUrl: filename as URL, Name: theFileName)
//                {
//                    arrPdf.removeAll()
//                    lblPdfName.text = =
//                    arrPdf.insert(Data, at: 0)
//                }
                
            }
//            if arrVideos.count>0
//            {
//                AddProuductData.sharedInstance?.arrVideos = arrVideos
//            }
//            if let ArrVideos = Data.arrVideos
//            {
//                arrVideos = ArrVideos
//                AddProuductData.sharedInstance?.arrVideos = ArrVideos
//            }
            if let location = Data.location
            {
                txtFld_Location.text = location
                AddProuductData.sharedInstance?.location = location
            }
            if let minBidType = Data.minBidType
            {
                txtFldBidTYpe.text = minBidType
                AddProuductData.sharedInstance?.minBidType = minBidType
            }
           // AddProuductData.sharedInstance?.is_escrow = is_escrow
            
                        if let isEscrow = Data.is_escrow
                        {
                            AddProuductData.sharedInstance?.is_escrow = isEscrow
                            if isEscrow == true
                            {
                                is_escrow = true
                                if let image  = UIImage(named: Kselected)
                                {
                                    btnYes.setImage(image, for: .normal)
                                }
                                if let image  = UIImage(named: Kunselected)
                                {
                                    btnNo.setImage(image, for: .normal)
                                }
                            }
                            else
                            {
                                is_escrow = false
                                if let image  = UIImage(named: Kselected)
                                {
                                    btnNo.setImage(image, for: .normal)
                                }
                                if let image  = UIImage(named: Kunselected)
                                {
                                    btnYes.setImage(image, for: .normal)
                                }
                            }
            }
            
            AddProuductData.sharedInstance?.itemType = KBookType
            if let lat = Data.lat
            {
                AddProuductData.sharedInstance?.lat = lat
            }
            if let lng = Data.lng
            {
                AddProuductData.sharedInstance?.lng = lng
            }
            AddProuductData.sharedInstance?.IsEdited = true

//            if let title =  Data.title
//            {
//                txtFld_Title.text = title
//                // AddProuductData.sharedInstance?.title = title
//            }
//            if let description = Data.description
//            {
//                txtFld_Descrption.text = description
//                //AddProuductData.sharedInstance?.description = description
//            }
//            //            if let id = UserDefaults.standard.getUserID()
//            //            {
//            //                AddProuductData.sharedInstance?.id = id
//            //            }
//            if let bidTYpe = Data.minBidType
//            {
//                txtFld_BidType.text = bidTYpe
//                //AddProuductData.sharedInstance?.minBidType = bidTYpe
//            }
//            if let bidTypeID = Data.minBidTypeId
//            {
//                AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
//            }
//            if let serviceType = Data.serviceType
//            {
//                txtFld_Genre.text = serviceType
//                //AddProuductData.sharedInstance?.serviceType = serviceType
//            }
//            if let GenreID = Data.genreId
//            {
//                AddProuductData.sharedInstance?.genreId = Int(GenreID)
//            }
//            if let minBid = Data.minBidAmount
//            {
//                txtFld_MinBidAmount.text = "\(minBid)"
//                //                print(minBid)
//                //                print(Float(minBid) as Any)
//                //                print(Double(minBid) as Any)
//                //                AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
//                //                print(Double(minBid))
//                //                print(AddProuductData.sharedInstance?.minBidAmount)
//            }
//            if let toc = Data.TC
//            {
//                txtFld_TCService.text = toc
//                AddProuductData.sharedInstance?.TC = toc
//            }
//            if let ArrImages = Data.arrImages
//            {
//                arrImages = ArrImages
//                CollectionView_Images.reloadData()
//                AddProuductData.sharedInstance?.arrImages = ArrImages
//            }
//            if let keywords = Data.keySearchWord
//            {
//                txtFld_KeySearchWord.text = keywords
//                //AddProuductData.sharedInstance?.keySearchWord = keywords
//            }
//            if let ArrVideos = Data.arrVideos
//            {
//                arrVideos = ArrVideos
//                CollectionView_Video.reloadData()
//                AddProuductData.sharedInstance?.arrVideos = ArrVideos
//            }
//            if let location = Data.location
//            {
//                txtFld_Location.text = location
//                // AddProuductData.sharedInstance?.location = location
//            }
//            //            if let minBidType = txtFld_BidType.text
//            //            {
//            //                txtFld_BidType.text = minBidType
//            //                AddProuductData.sharedInstance?.minBidType = minBidType
//            //            }
//
//            if let isEscrow = Data.is_escrow
//            {
//                AddProuductData.sharedInstance?.is_escrow = isEscrow
//                if isEscrow == true
//                {
//                    is_escrow = true
//                    if let image  = UIImage(named: Kselected)
//                    {
//                        btnYes.setImage(image, for: .normal)
//                    }
//                    if let image  = UIImage(named: Kunselected)
//                    {
//                        btnNo.setImage(image, for: .normal)
//                    }
//                }
//                else
//                {
//                    is_escrow = false
//                    if let image  = UIImage(named: Kselected)
//                    {
//                        btnNo.setImage(image, for: .normal)
//                    }
//                    if let image  = UIImage(named: Kunselected)
//                    {
//                        btnYes.setImage(image, for: .normal)
//                    }
//                }
//                // AddProuductData.sharedInstance?.location = location
//            }
//            //            if let itemType = Data.itemType
//            //            {
//            //                AddProuductData.sharedInstance?.itemType = itemType
//            //                // AddProuductData.sharedInstance?.location = location
//            //            }
//            //            AddProuductData.sharedInstance?.is_escrow = is_escrow
//            AddProuductData.sharedInstance?.itemType = KServicesType
//            if let lat = Data.lat
//            {
//                AddProuductData.sharedInstance?.lat = lat
//            }
//            if let lng = Data.lng
//            {
//                AddProuductData.sharedInstance?.lng = lng
//            }
        }
        
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    @objc func doneButtonAction()
    {
        self.txtFld_KeySearchWord.becomeFirstResponder()
    }
    @objc func doneButton_NumOfPages_Action()
    {
        self.txtFld_Descrption.becomeFirstResponder()
    }
    func saveImage(){
        
        let url1 = Bundle.main.url(forResource: "Jaspreet epic BJ", withExtension: "PDF")
        let one1 = NSData(contentsOf: url1!)
        
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = "\(documentPath)/filename.pdf"
        one1?.write(toFile: path, atomically: true)
        
    }
    func savePdf()
    {
        let url1 = Bundle.main.url(forResource: "logo", withExtension: "png")
        let one1 = NSData(contentsOf: url1!)
        
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let path = "\(documentPath)/Image.jpeg"
        one1?.write(toFile: path, atomically: true)
    }
    
    func loadPDFAndShare()
    {
    }
    func Validations() throws
    {
        guard let title = txtFld_Title.text,  !title.isEmpty, !title.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyTitle
        }
        
        guard let numOfPages  = txtFld_NumOfPages.text, !numOfPages.isEmpty, !numOfPages.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyNumOfpage
        }
        guard let typeOfBook  = txtFld_Genre.text, !typeOfBook.isEmpty, !typeOfBook.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyGenre
        }
        guard let descrption  = txtFld_Descrption.text, !descrption.isEmpty, !descrption.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyDescription
        }
        
        if let bookUrl  = txtFld_BookUrl.text, !bookUrl.isEmpty, !bookUrl.trimmingCharacters(in: .whitespaces).isEmpty
        {
            if !bookUrl.isValidURL {
                throw ValidationError.invalidBookUrl
            }
        }
        guard let bidType  = txtFldBidTYpe.text, !bidType.isEmpty, !bidType.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyBidType
        }
        guard let bidAmount  = txtFld_MinBidAmount.text, !bidAmount.isEmpty, !bidAmount.trimmingCharacters(in: .whitespaces).isEmpty, !bidAmount.replacingOccurrences(of: ".", with: "").isEmpty else
        {
            throw ValidationError.emptyBidAmount
        }
        guard let keysearchWord  = txtFld_KeySearchWord.text, !keysearchWord.isEmpty, !keysearchWord.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyKeySearchWord
        }
        guard let location  = txtFld_Location.text, !location.isEmpty, !location.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyLocation
        }
        if arrPdf.count == 0
        {
            throw ValidationError.emptyPdf
        }
        if arrImages.count == 0
        {
            throw ValidationError.emptyImages
        }
//        if arrVideos.count == 0
//        {
//            throw ValidationError.emptyVideos
//        }
    }
    func GetGener()  {
        let urlStr = KGenerApi + String(KBookType)
        AddProduct.sharedManager.GetApi(url: urlStr,Target: self, completionResponse: { (Genre,BidType,maxImages) in
            self.arrGenre = Genre
            self.arrBidType = BidType
            self.maxNumImages = maxImages
            //print(Genre)
            //print(BidType)
            //print(maxImages)
            self.CollectionView_Images.reloadData()
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.GetGener()
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        
        switch pickerType
        {
        case .GenrePicker?:
            let dic = arrPicker[row] as! GenreData
            let title = dic.name
            let id = dic.id
            let ID:String = String(id)
            strGenreID = ID
            txtFld_Genre.text = title
        case .BidTypePicker?:
            let dic = arrPicker[row] as! BidTypeData
            let title = dic.name
            let id = dic.id
            let ID:String = String(id)
            strBidTypeID = ID
            txtFldBidTYpe.text = title
        case .none:
            print("none")
        }
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    @objc func CancelPicker()
    {
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    
    func Submitstep1() {
        let imagescount = arrImages.count
        let videoCount = arrVideos.count
        let pdfCount = arrPdf.count
        totalCount = imagescount + videoCount + pdfCount
        //print(imagescount)
        //print(videoCount)
        //print(pdfCount)
        //print(count)
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
        if (count < imagescount)
        {
             MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
            //print(arrImages)
            AmazonServices.sharedInstance.UploadData(awsType: .Image, url: arrImages[count].imgUrl, completionHandler: { (error, urlStr) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrImagesUrl.append(urlStr!)
                
                self.count = self.count+1
                DispatchQueue.main.async {
                    DispatchQueue.main.async {
                        self.Submitstep1()
                    }                }
                }
                else
                {
                    self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                        self.count = 0
                        self.arrImagesUrl.removeAll()
                        self.arrVideosUrl.removeAll()
                        self.arrPdfUrl.removeAll()
                       // self.SubmitAction(AnyObject.self)
                    })
                }
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else if (count < pdfCount+imagescount)
        {
             MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
            //print(arrPdf)
            print("Pdf Url : \(arrPdf[count-imagescount].pdfUrl)")
            
            AmazonServices.sharedInstance.UploadDataPdf(awsType: .Pdf, url: arrPdf[count-imagescount].pdfUrl as NSURL, completionHandler: { (error, urlStr) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                self.arrPdfUrl.append(urlStr!)
                self.count = self.count+1
                DispatchQueue.main.async {
                    self.Submitstep1()
                    }
                }
                else
                {
                    self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                        self.count = 0
                        self.arrImagesUrl.removeAll()
                        self.arrVideosUrl.removeAll()
                        self.arrPdfUrl.removeAll()
                        self.arrPdf.removeAll()
                        self.lblPdfName.text = ""
                       // self.SubmitAction(AnyObject.self)
                    })
                }
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else if(count < pdfCount+imagescount+videoCount)
        {
             MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
            //print(arrVideos)
            AmazonServices.sharedInstance.UploadData(awsType: .Video, url: arrVideos[count-imagescount-pdfCount].videoUrl, completionHandler: { (error, urlStr) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrVideosUrl.append(urlStr!)
                
                self.count = self.count+1
                DispatchQueue.main.async {
                    self.Submitstep1()
                }
                }
                else
                {
                    self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                        self.count = 0
                        self.arrImagesUrl.removeAll()
                        self.arrVideosUrl.removeAll()
                        self.arrPdfUrl.removeAll()
                        //self.SubmitAction(AnyObject.self)
                    })
                }
                
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else
        {
            //print(arrImagesUrl)
            //print(arrPdfUrl)
            //print(arrVideosUrl)
            Submitstep2()
        }
    }
    func Submitstep2()
    {
        if let title =  txtFld_Title.text
        {
            AddProuductData.sharedInstance?.title = title
        }
        if let description = txtFld_Descrption.text
        {
            AddProuductData.sharedInstance?.description = description
        }
        if let id = UserDefaults.standard.getUserID()
        {
            AddProuductData.sharedInstance?.id = id
        }
        if let GenreID = strGenreID
        {
            AddProuductData.sharedInstance?.genreId = Int(GenreID)
        }
        if let typeOfBook = txtFld_Genre.text
        {
            AddProuductData.sharedInstance?.typeOfBook = typeOfBook
        }
        if let numOfPages = txtFld_NumOfPages.text
        {
            AddProuductData.sharedInstance?.noOfPages = numOfPages
        }
        if let url = txtFld_BookUrl.text, !url.isEmpty
        {
            AddProuductData.sharedInstance?.bookUrl = url
        }
        if let bidTypeID = strBidTypeID
        {
            AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
        }
        if let minBidType = txtFldBidTYpe.text
        {
            AddProuductData.sharedInstance?.minBidType = minBidType
        }
        if let minBid = txtFld_MinBidAmount.text
        {
            //AddProuductData.sharedInstance?.minBidAmount = Float(minBid)
            AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
        }
        if let keywords = txtFld_KeySearchWord.text, !keywords.isEmpty
        {
            AddProuductData.sharedInstance?.keySearchWord = keywords
        }
        if arrImages.count>0
        {
            AddProuductData.sharedInstance?.arrImages = arrImages
        }
        if arrPdf.count>0
        {
            AddProuductData.sharedInstance?.arrPdf = arrPdf
        }
        if arrVideos.count>0
        {
            AddProuductData.sharedInstance?.arrVideos = arrVideos
        }
        else
        {
            let blank = [VideoData]()
            AddProuductData.sharedInstance?.arrVideos = blank
        }
        if let location = txtFld_Location.text
        {
            AddProuductData.sharedInstance?.location = location
        }
        if let minBidType = txtFldBidTYpe.text
        {
            AddProuductData.sharedInstance?.minBidType = minBidType
        }
        AddProuductData.sharedInstance?.is_escrow = is_escrow
        
        AddProuductData.sharedInstance?.itemType = KBookType
        print(arrRemoveMedia)
        AddProuductData.sharedInstance?.arrRemoveMedia = arrRemoveMedia
        KCommonFunctions.PushToContrller(from: self, ToController: .Preview, Data: nil)
    }
    
    //MARK: - IBActions
    @IBAction func YesDeleteAction(_ sender: Any) {
        
        switch deleteItemType {
        case .Pdf?:
            print("Pdf")
        case .Video?:
            //print((sender as AnyObject).tag)`
            let index = DeletedIndex
            if index < arrVideos.count
            {
                self.arrRemoveMedia.append((arrVideos[index].videoUrl?.absoluteString)!)
                
                arrVideos.remove(at: index)
                CollectionView_Video.reloadData()
            }
        case .Image?:
            //print((sender as AnyObject).tag)
            let index = DeletedIndex
            if index < arrImages.count
            {
                self.arrRemoveMedia.append((arrImages[index].imgUrl?.absoluteString)!)
                arrImages.remove(at: index)
                CollectionView_Images.reloadData()
            }
        default:
            print("Fail")
        }
        ViewDelete.isHidden = true
    }
    @IBAction func NoDeleteAction(_ sender: Any) {
        ViewDelete.isHidden = true
    }
    @IBAction func MapAction(_ sender: Any)
    {
//        //        mapView.isHidden = false
//        //        imgView_Pin.isHidden = false
//        //        btnSave.title = "SAVE"
//        //        btnSave.isEnabled = true
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePickerViewController(config: config)
//        placePicker.delegate = self
//        present(placePicker, animated: true, completion: nil)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
        vc.locationDelegate = self;
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    func Location(lat: String, lng: String, address: String) {
        print("your lat \(lat) and lng \(lng)")
        txtFld_Location.text = address
        AddProuductData.sharedInstance?.lat = Float(lat)
        //Float(place.coordinate.latitude)
        AddProuductData.sharedInstance?.lng = Float(lng)
    }
    
    //For open the link
    @IBAction func onClickOpenLink(_ sender: UIButton) {
        setDataForWebPageViewController()
    }
    
    @IBAction func YesAction(_ sender: Any) {
        is_escrow = true
        if let image  = UIImage(named: Kselected)
        {
            btnYes.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnNo.setImage(image, for: .normal)
        }
    }
    @IBAction func NoAction(_ sender: Any) {
        is_escrow = false
        if let image  = UIImage(named: Kselected)
        {
            btnNo.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnYes.setImage(image, for: .normal)
        }
    }
    

    @IBAction func ScrollCollectionAction(_ sender: Any)
    {
        //print((sender as AnyObject).tag)
        
        if (sender as AnyObject).tag == 0
        {
            let visibleItems: NSArray = self.CollectionView_Images.indexPathsForVisibleItems as NSArray
            let minIndex = visibleItems.map { ($0 as AnyObject).row }.min()
            let nextItem: IndexPath = IndexPath(item: minIndex! - 1, section: 0)
            //print("nextItem : \(nextItem)")
            if(nextItem.row >= 0)
            {
                //print("LeftScroll")
                self.CollectionView_Images.scrollToItem(at: nextItem, at: .right, animated: true)
            }
        }
        else if  (sender as AnyObject).tag == 1
        {
            let visibleItems: NSArray = self.CollectionView_Images.indexPathsForVisibleItems as NSArray
            let maxIndex = visibleItems.map { ($0 as AnyObject).row }.max()
            let nextItem: IndexPath = IndexPath(item: maxIndex! + 1, section: 0)
            //print(nextItem)
            if(maxNumImages > nextItem.row)
            {
                //print("RightScroll")
                self.CollectionView_Images.scrollToItem(at: nextItem, at: .left, animated: true)
            }
        }
    }
    @IBAction func AddVideoAction(_ sender: Any) {
        
        if (Int(arrVideos.count) < 1)
        {
            self.OpenGallaryCameraForVideo(pickerController: imagePicker)
        }
        else
        {
            showAlertMessage(titleStr: KMessage, messageStr: "\(KYoucanuploadmaximum) \(KMaxVideo) \(Kvideo)")
        }
    }
    
    @IBAction func AddImageAction(_ sender: Any) {
        if (Int(arrImages.count) < maxNumImages)
        {
            self.OpenGallaryCamera(pickerController: imagePicker)
        }
        else
        {
            showAlertMessage(titleStr: KMessage, messageStr: "\(KYoucanuploadmaximum) \(maxNumImages) \(Kimages)")
        }
    }
    @IBAction func AddPdfAction(_ sender: Any) {
        OpenDocuments()
    }
    @IBAction func SubmitAction(_ sender: Any) {
        self.view.endEditing(true)
        do {
            try Validations()
           Submitstep2()
        } catch let error {
            switch  error {
            case ValidationError.emptyTitle:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentersongtitle)
            case ValidationError.emptyDescription:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterdescrption)
            case ValidationError.invalidBookUrl:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentervalidbookurl)
            case ValidationError.emptyBidType:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterBidType)
            case ValidationError.emptyBidAmount:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterBidAmount)
            case ValidationError.emptyNumOfpage:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenternumberofPages)
            case ValidationError.emptyGenre:
                    self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectbooktype)
            case ValidationError.emptyPdf:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectPdf)
            case ValidationError.emptyImages:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectatleastoneimage)
            case ValidationError.emptyVideos:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectVideo)
            case ValidationError.emptyLocation:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterlocation)
            case ValidationError.emptyKeySearchWord:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterkeysearchword)
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KUnknownerror)
            }
        }
    }
    @IBAction func BidTypeAction(_ sender: Any)
    {
        if(
            arrBidType.count>0)
        {
            isGenrePicker = false
            pickerType = PickerType(rawValue: 2)
            self.arrPicker = self.arrBidType
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFldBidTYpe.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrBidType, str: (txtFldBidTYpe.text)!, key: Kname,type:.BidTypeData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
    }
    @IBAction func GenerAction(_ sender: Any)
    {
        if(
            arrGenre.count>0)
        {
            isGenrePicker = true
            pickerType = PickerType(rawValue: 1)
            self.arrPicker = self.arrGenre
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_Genre.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrGenre, str: (txtFld_Genre.text)!, key: Kname,type:.GenreData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
    }
    @IBAction func btnMediaPickerAction(_ sender: UIButton)
    {
    }
   
    @IBAction func SaveAction(_ sender: Any) {
        saveImage()
    }
    
    @IBAction func ShareAction(_ sender: Any) {
       // mySpecialFunction()
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - CollectionCell Delegate
    func DeleteImage(_ sender: Any) {
        DeletedIndex = (sender as AnyObject).tag
        deleteItemType = .Image
        ShowAlert()
       
    }
    
    func DeleteVideo(_ sender: Any) {
        DeletedIndex = (sender as AnyObject).tag
        deleteItemType = .Video
        ShowAlert()
    }
    
    //MARK: - TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            if(!isGenrePicker)
            {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                        self.scrollView_main.contentOffset.y = 250
                    }, completion: nil)
                }
            }
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 0
                }, completion: nil)
            }
            
            
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
                if (string == " ") && (textField.text?.count)! == 0
                {
                    return false
                }
                if textField == txtFld_Title
                {
                    return textField.RestrictMaxCharacter(maxCount: 150, range: range, string: string)
                }
                if textField == txtFld_NumOfPages
                {
                    return textField.RestrictMaxCharacter(maxCount: 5, range: range, string: string)
                }
                if textField == txtFld_Descrption
                {
                    return textField.RestrictMaxCharacter(maxCount: 1000, range: range, string: string)
                }
                if textField == txtFld_BookUrl
                {
                    return textField.RestrictMaxCharacter(maxCount: 150, range: range, string: string)
                }
                if textField == txtFld_KeySearchWord
               {
                return textField.RestrictMaxCharacter(maxCount: 30, range: range, string: string)
               }
                if textField == txtFld_MinBidAmount
                {
                    if textField.RestrictMaxCharacter(maxCount: 10, range: range, string: string)
                    {
                    if !((textField.text?.contains("."))! && string.contains("."))
                    {
                        var separator = txtFld_MinBidAmount.text?.components(separatedBy: ".")
                        let count = Double((separator?.count)!)
                        if count > 1
                        {
                            let sepStr1 = "\(separator![1])"
                            if sepStr1.count == 2 && string != ""  {
                                return false
                            }
                        }
                        if let amount = Double(textField.text! + string)
                        {
                            if amount > 970873.77
                            {
                                return false
                            }
                        }
                        
                        return true
                    }
                    }
                    return false
                }
        return true
    }
    
    //MARK: - UImage & Video Picker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        // Handle a movie capture
        if mediaType == kUTTypeMovie {
            guard let videoURL = info[UIImagePickerControllerMediaURL] as? URL else
            {return}
            if let thumbNailImage =  getThumbnailImage(forUrl: videoURL)
            {
                if let Data = VideoData(VideoUrl: videoURL, Image: thumbNailImage)
                {
                    ////print(videoURL)
                    arrVideos.removeAll()
                    arrVideos.insert(Data, at: 0)
                }
                
                CollectionView_Video.reloadData()
            }
            
        }
            // Handle Image Capture
        else{
            
//            if #available(iOS 11.0, *) {
//                if let asset = info[UIImagePickerControllerPHAsset] as? PHAsset{
//
//                    let u = "instagram://library?LocalIdentifier=" + asset.localIdentifier + "&InstagramCaption=aaaa"
//                    let url = NSURL(string: u)!
//                    if UIApplication.shared.canOpenURL(url as URL) {
//                        UIApplication.shared.openURL(NSURL(string: u)! as URL)
//                    } else {
//                        let alertController = UIAlertController(title: "Error", message: "Instagram is not installed", preferredStyle: .alert)
//                        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                        self.present(alertController, animated: true, completion: nil)
//                    }
//                    //                if let fileName = asset.value(forKey: "filename") as? String{
//                    //                    print(fileName)
//                    //                }
//                }
//            } else {
//                // Fallback on earlier versions
//            }
            
            ////print(info)
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                // choose a name for your image
                let fileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
                // create the destination file url to save your image
                let fileURL = documentsDirectory.appendingPathComponent(fileName)
                

                // get your UIImage jpeg data representation and check if the destination file url already exists
                if let data = UIImageJPEGRepresentation(pickedImage, 1.0),
                    !FileManager.default.fileExists(atPath: fileURL.path) {
                    do {
                        // writes the image data to disk
                        try data.write(to: fileURL)
                        if let Data = ImagesData(ImgUrl: fileURL, Image: pickedImage)
                        {
                            arrImages.insert(Data, at: 0)
                        }
                        ////print(arrImages)
                        let indexPath = IndexPath(row: 0, section: 0)
                        self.CollectionView_Images.scrollToItem(at: indexPath, at: .left, animated: true)
                        CollectionView_Images.reloadData()
                        ////print("file saved")
                    } catch {
                        ////print("error saving file:", error)
                    }
                }
            }
            
            
        }
        dismiss(animated:true, completion: nil)
        
    }
}
extension AddBookVC : GMSMapViewDelegate,GMSPlacePickerViewControllerDelegate
{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("lat \(place.coordinate.latitude)")
        print("lng \(place.coordinate.longitude)")
        print("lng \(place.name)")
        print(place.addressComponents ?? "Blank")
        print("lng \(place.placeID)")
        if let address = place.formattedAddress
        {
            txtFld_Location.text = address
            AddProuductData.sharedInstance?.lat = Float(place.coordinate.latitude)
            AddProuductData.sharedInstance?.lng = Float(place.coordinate.longitude)
        }
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    
    //    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    //    {
    //        let lat = mapView.camera.target.latitude
    //        let lng = mapView.camera.target.longitude
    //      // lattitude = Float(lat)
    //        longitude = Float(lng)
    //    }
    //    func fetchCountryAndCity(location: CLLocation, completion: @escaping (String) -> ()) {
    //        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
    ////            print(placemarks?.first ?? "BLANK")
    ////            print(placemarks?.first?.country)
    ////            print(placemarks?.first?.subLocality)
    ////            print(placemarks?.first?.locality)
    ////            print(placemarks?.first?.location)
    ////            print(placemarks?.first?.region)
    ////            print(placemarks?.first?.administrativeArea)
    ////            print(placemarks?.first?.areasOfInterest)
    ////            print(placemarks?.first?.name)
    ////            print(placemarks?.first?.subThoroughfare)
    ////            print(placemarks?.first?.thoroughfare)
    //            if let error = error {
    //                print(error)
    //            }
    //            else if let city = placemarks?.first?.locality {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.subLocality
    //            {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.administrativeArea {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.name {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.thoroughfare {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.subThoroughfare {
    //                completion(city)
    //            }
    //        }
    //    }
    
}


extension AddBookVC : UIDocumentMenuDelegate,UIDocumentPickerDelegate
{
    @available(iOS 8.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL)
    {
        let myURL = url as NSURL
        ////print("import result : \(myURL)")
        let theFileName = myURL.lastPathComponent
        
       if let data = NSData (contentsOf: myURL as URL)
       {
        print(data)
        let filename = getDocumentsDirectory().appendingPathComponent(theFileName!)
        try? data.write(to: filename)
        if let Data = PdfData(PdfUrl: filename as URL, Name: theFileName)
        {
            arrPdf.removeAll()
            lblPdfName.text = theFileName
            arrPdf.insert(Data, at: 0)
        }
            }
      //  }
      //  }
      
        
     //   do{
//            let directoryURL = try FileManager.default.urls(for:.documentDirectory,in:.userDomainMask).last
//
//            let docURL = NSURL(string:theFileName!, relativeTo:directoryURL)
//            print("docURL : \(String(describing: docURL))")
//
//
        
//        if FileManager.default.fileExists(atPath: filePath) {
//            if let fileData = FileManager.default.contents(atPath: filePath) {
//                // process the file data
//            } else {
//                print("Could not parse the file")
//            }
//        } else {
//            print("File not exists")
//        }
//
        ////print(theFileName)
      
        
//        }
//        catch{print("ERROR")
//        }
        ////print(arrPdf)
    }
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController)
    {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController)
    {
        ////print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func OpenDocuments()
    {
        let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.composite-content"], in: UIDocumentPickerMode.import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        self.present(documentPicker, animated: true, completion: nil)
    }
}

extension AddBookVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        switch collectionView {
        case CollectionView_Images:
            return maxNumImages
        case CollectionView_Video:
            return 1
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case CollectionView_Images:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KImagesCollectionCell, for: indexPath as IndexPath) as? ImagesCollectionCell
            {
                cell.delegate = self
                if(arrImages.count>indexPath.row)
                {
                    cell.LoadData(Data: arrImages, index: indexPath)
                }
                else
                {
                    cell.LoadBlankData(index: indexPath)
                }
                return cell
            }
        case CollectionView_Video:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KvideosCollectionCell, for: indexPath as IndexPath) as? VideosCollectionCell
            {
                cell.delegate = self
                if(arrVideos.count>indexPath.row)
                {
                    cell.LoadData(Data: arrVideos, index: indexPath)
                }
                else
                {
                    cell.LoadBlankData(index: indexPath)
                }
                return cell
            }
        default:
        print("nil")
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 0
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
//        if(collectionView == CollectionView_Audio)
//        {
//            //print(collectionView.bounds.height)
//            if(arrAudios.count>0)
//            {
//                AudioCollection_heightConstraints.constant = 50
//                
//                return CGSize(width: dim, height: dim+50)
//            }
//            AudioCollection_heightConstraints.constant = 0
//            return CGSize(width: dim, height: dim)
//        }
        ImagesCollection_heightConstarints.constant = dim
        return CGSize(width: dim, height: dim)
    }
    
    //    func configureCell(cell: UICollectionViewCell, forItemAtIndexPath: NSIndexPath)
    //        {
    //        cell.backgroundColor = UIColor.black
    //        }
    
    //MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
    }
    
    
}
extension AddBookVC: UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dic = arrPicker[row]
        var title = ""
        switch pickerType
        {
        case .GenrePicker?:
            title = (dic as! GenreData).name
        case .BidTypePicker?:
            title = (dic as! BidTypeData).name
        case .none:
            print("none")
        }
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }
}



