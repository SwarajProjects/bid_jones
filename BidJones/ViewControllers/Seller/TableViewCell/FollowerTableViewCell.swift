//
//  FollowerTableViewCell.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/15/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class FollowerTableViewCell: UITableViewCell {
    @IBOutlet weak var img_follow: UIImageView!
    
    
    @IBOutlet weak var lbl_Follwer: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    
    func cellData(array : [FollowerData], indexPath : IndexPath) {
        
       
        
        let imageString = array[indexPath.row].image!
        print("image string : \(imageString)")
        let url = URL(string:imageString)
        print("your following url : \(url)")
       
        
        if let url1 = url as? URL {
            
          LoadImage(urlStr: imageString)
            
        }
       lbl_Follwer.text = array[indexPath.row].name!
        
      
        
    }
    func LoadImage(urlStr:String)
    {
        print(urlStr)
        var imageCache = SDImageCache.shared().imageFromCache(forKey: urlStr)
        
        if let Image = imageCache {
            print(Image)
            print(img_follow)
            img_follow.image = Image
        }
        else if let Image = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
        {
            img_follow.image = Image
        }
        else{
            imageCache = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
            if imageCache != nil {
                img_follow.image = imageCache
            }
            else
            {
                SDWebImageManager.shared().loadImage(with: URL(string:urlStr), options: .highPriority , progress: { (receivedSize :Int, ExpectedSize :Int, url : URL?) in
                    
                    
                }, completed: { (image : UIImage?, data : Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    
                    DispatchQueue.main.async {
                        self.img_follow.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached , completed: nil)
                    }
                })
            }
        }
    }
}
