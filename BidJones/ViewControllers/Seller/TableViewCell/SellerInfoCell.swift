//
//  SellerInfoCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 8/8/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class SellerInfoCell: UITableViewCell {
    
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func LoadData(dic:Any)
    {
        print(dic)
        if let data = dic as? ItemListData
        {
            imgView.contentMode = .scaleAspectFill
            imgView.clipsToBounds = true
            print(data)
            lbl_description.text = data.descrption!
            lblTitle.text = data.title!
            lblDate.text = data.date
            lblPrice.text = "$"+"\(data.bidAmount ?? "")"
            imgView.image = UIImage.init(named:"sound_placeholder")
            if let categoryID = data.categoryID
            {
                switch categoryID
                {
                case 1:
                    imgView.image =  UIImage.init(named:"book_placeholder")
                case 2:
                    imgView.image = UIImage.init(named:"graphic_2_")
                case 3:
                    imgView.image = UIImage.init(named:"sound_placeholder")
                case 4:
                    imgView.image = UIImage.init(named:"services_placeholder")
                case 5:
                    imgView.image = UIImage.init(named:"stuff_placeholder")
                case 6:
                    imgView.image = UIImage.init(named:"video_2_")
                default:
                    imgView.image = UIImage.init(named:"logo")
                }
            }
            
            if let imgUrlStr = data.ImageUrl
            {
                LoadImage(urlStr:imgUrlStr)
            }
            
        }
        print(dic)
    }
    
    func LoadImage(urlStr:String)
    {
        print(urlStr)
        var imageCache = SDImageCache.shared().imageFromCache(forKey: urlStr)
        
        if let Image = imageCache {
            print(Image)
            print(imgView)
            imgView.image = Image
        }
        else if let Image = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
        {
            imgView.image = Image
        }
        else{
            imageCache = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
            if imageCache != nil {
                imgView.image = imageCache
            }
            else
            {
                SDWebImageManager.shared().loadImage(with: URL(string:urlStr), options: .highPriority , progress: { (receivedSize :Int, ExpectedSize :Int, url : URL?) in
                    
                    
                }, completed: { (image : UIImage?, data : Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    
                    DispatchQueue.main.async {
                        self.imgView.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached , completed: nil)
                    }
                })
            }
        }
    }
    
}
