//
//  ProductTableCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/5/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

enum CellType
{
    case Music
    case Book
    case Stuff
    case SellService
    case video
    case graphic
}



 @objc protocol ProductTableCellDelegate: class{
    
 @objc optional  func DeleteButtonAction(_ sender: ProductTableCell)
 @objc optional  func YesIWantYourService(_ sender: ProductTableCell)

}

class ProductTableCell: UITableViewCell {

    @IBOutlet var btn_YesIwant: UIButton!
    @IBOutlet var title: UILabel!
    @IBOutlet var imgView: UIImageView!
    weak var delegate: ProductTableCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func LoadDataPickup(dic: [MerchPickupData] , indexPath : IndexPath) {
        
        title.text = dic[indexPath.row].titleName!
       // imgView.sd_setImage(with: <#T##URL?#>)
        let image = dic[indexPath.row].image!
        let url = URL(string:image)
        imgView.sd_setImage(with: url)
    }
    
    
    func LoadData(dic:Any,type:CellType)
    {
        if let data = dic as? ItemListData
        {
            imgView.contentMode = .scaleAspectFill
            imgView.clipsToBounds = true
            print(data)
            title.text = data.title
            imgView.image = UIImage.init(named:"sound_placeholder")
            switch type
            {
            case .Music:
                imgView.image = UIImage.init(named:"sound_placeholder")
            case .Book:
                imgView.image =  UIImage.init(named:"book_placeholder")
            case .Stuff:
                imgView.image = UIImage.init(named:"stuff_placeholder")
            case .SellService:
                imgView.image = UIImage.init(named:"services_placeholder")
            case .video:
                imgView.image = UIImage.init(named:"video_2_")
            case .graphic:
                imgView.image = UIImage.init(named:"graphic_2_")
            }
            
            if let categoryID = data.categoryID
            {
                switch categoryID
                {
                case 1:
                    imgView.image =  UIImage.init(named:"book_placeholder")
                case 2:
                    imgView.image = UIImage.init(named:"graphic_2_")
                case 3:
                    imgView.image = UIImage.init(named:"sound_placeholder")
                case 4:
                    imgView.image = UIImage.init(named:"services_placeholder")
                case 5:
                    imgView.image = UIImage.init(named:"stuff_placeholder")
                case 6:
                    imgView.image = UIImage.init(named:"video_2_")
                default:
                    imgView.image = UIImage.init(named:"logo")
                }
            }
            if let imgUrlStr = data.ImageUrl
            {
            LoadImage(urlStr:imgUrlStr)
            }
        }
        print(dic)
    }
    
    func LoadImage(urlStr:String)
    {
        print(urlStr)
    var imageCache = SDImageCache.shared().imageFromCache(forKey: urlStr)
    
    if let Image = imageCache {
    print(Image)
    print(imgView)
    imgView.image = Image
    }
    else if let Image = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
        {
            imgView.image = Image
        }
    else{
    imageCache = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
    if imageCache != nil {
     imgView.image = imageCache
    }
    else
    {
        SDWebImageManager.shared().loadImage(with: URL(string:urlStr), options: .highPriority , progress: { (receivedSize :Int, ExpectedSize :Int, url : URL?) in
    
    
    }, completed: { (image : UIImage?, data : Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
    
    DispatchQueue.main.async {
        self.imgView.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached , completed: nil)
    }
    })
    }
    }
    }
    @IBAction func YesIWantYourService(_ sender: Any)
    {
        delegate?.YesIWantYourService!(self)
    }
    
    @IBAction func DeleteAction(_ sender: Any)
    {
        delegate?.DeleteButtonAction!(self)
    }
    
}
