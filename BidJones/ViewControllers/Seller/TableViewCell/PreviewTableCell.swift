//
//  PreviewTableCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class PreviewTableCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet var lblType: UILabel!
  //  @IBOutlet var lblDetail: UILabel!
    @IBOutlet var txtViewDetail: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //lblDetail.sizeToFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func LoadData(Data dic:[String:Any])
    {
        print(dic)
        let keys = dic.flatMap(){ $0.0 as? String}
        let values = dic.flatMap(){ $0.1 as? String }
        print(keys)
        print(values)
        lblType.text = keys[0]
        txtViewDetail.text = values[0]
    }

}
