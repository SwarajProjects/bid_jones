//
//  AudioPlayerVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/23/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Alamofire


enum DetailType
{
    case Detail
    case Preview

}
var detailType: DetailType?


class AudioPlayerVC: UIViewController {

    @IBOutlet weak var btn_Download: UIButton!
    //MARK: - UItableView Outlets
    @IBOutlet var tblSongList: UITableView!
    var productDetail:ProductData?
    var arrMusicName = [String]()

    //for Audio Player
    var player              =  AVPlayer()
    var stopped             =  false
    var playerController    = AVPlayerViewController()
    var timer               : Timer? = nil
    var timeToStop          : Int    = 10
    var playerObserver      : AnyObject!
    var audioPlayer         : AVAudioPlayer?
    var playerItem          : AVPlayerItem!
    var timerObserver:Any?
    var urlSong             : URL?
    var downloadButton     =  UIButton()
    var show               =  false
    var playerActive       =  false
    
     var window :UIWindow = UIApplication.shared.keyWindow!
    
    override func viewDidLoad() {
        print(productDetail ?? "Blank")
        super.viewDidLoad()
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)

        } catch let error {
            print(error.localizedDescription)
        }
        
        
    
        if detailType == .Detail
        {
        arrMusicName = (productDetail?.musicNames)!
        }
        else
        {
            if let musicName = AddProuductData.sharedInstance?.musicNames
            {
               arrMusicName = musicName
            }
        }
        print(arrMusicName)
        tblSongList.estimatedRowHeight = 1000
        tblSongList.rowHeight = UITableViewAutomaticDimension
        
         self.downloadButton = UIButton(frame: CGRect(x: self.view.frame.width - 95, y: 80, width: 35, height: 35))
        
        self.downloadButton.addTarget(self, action: #selector(self.downloadImage), for: .touchUpInside)
       // print("your url here : \(urlSong!)")
        
     //   StarAudio(Url:urlSong!)
         self.automaticallyAdjustsScrollViewInsets = false
        tblSongList.reloadData()

    }
    
//    override func viewWillAppear(_ animated: Bool) {
//
//        if show == true {
//            self.downloadButton.isHidden = true
//            show = false
//        }
//
//    }
//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
        
    }
//    @objc func setData() {
//        if self.progressView.progress >= 1 {
//            self.progressView.progress = 0.0
//        }
//        // schedule timer
//        //   self.myTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
//        // DispatchQueue.sync(
//
//
//
//    }
//
//
//    @objc func updateProgress() {
//
//        self.progressView.progress += 0.01
//        // set label for progress value
//        self.lbl_label.text = "\(Int(self.progressView.progress * 100))"
//        // "\(Int(self.progressView.progress * 100))"
//        // invalidate timer if progress reach to 1
//        if self.progressView.progress >= 1 {
//            // invalidate timer
//            self.myTimer?.invalidate()
//
//        }
//
    
    
    @objc func downloadImage() {
        
    
//         if let audioUrl =  self.urlSong {
//            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
//                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//                let documentsURL = URL(fileURLWithPath: documentsPath, isDirectory: true)
//                let fileURL = documentsURL.appendingPathComponent(audioUrl.lastPathComponent)
//
//                return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
//            }
//
//            if self.progressView.progress >= 1 {
//                self.progressView.progress = 0.0
//            }
//            let audioUrl = "https://bidjonesbucketprivate.s3.us-east-2.amazonaws.com/bidjones/rkumar/72701530270877.jpg"
//            //  let fileUrl = self.getSaveFileUrl(fileName: audioUrl)
//            //   let destination: DownloadRequest.DownloadFileDestination = { _, _ in
//            //   let destination = ""
//            // Alamofire.down
//
//
//
//
//
//
//            let url = URL.init(string: audioUrl)
//            Alamofire.download(url!, to: destination).downloadProgress { (progress) in
//                self.lbl_label.text = (String)(progress.fractionCompleted)
//
//                self.updateProgress()
//
//                }
//                .responseData { (data) in
//
//
//                    print(data)
//                    // at this stage , the downloaded data are already saved in fileUrl
//                    // self.surahNameKana.text = "Completed!"
//            }
//
//        }
//
        
        if let audioUrl =  self.urlSong {
             MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KDownloading
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)

            // to check if it exists before downloading it
//            if FileManager.default.fileExists(atPath: destinationUrl.path) {
//                print("The file already exists at path")
//                DispatchQueue.main.async {
//                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
//                }
                // if the file doesn't exist
        //    } else {

                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url

                      //  let saveFile = URL.init(string: destinationUrl as! String)
                   let name =    destinationUrl.lastPathComponent

                       // print("name is : \(name)")

//                       let splitArray = name.components(separatedBy: ".")
//                        var firstValue = splitArray[0]
//                        print("my value : \(firstValue)")
                        //UISaveVideoAtPathToSavedPhotosAlbum(Bundle.main.path(forResource:  "\(firstValue)", ofType: "mp3")!, nil, nil, nil)

//                        let filePath = Bundle.main.path(forResource: "\(firstValue)", ofType: "mp3")
//                        print("path: \(filePath)")
                    //    UISaveVideoAtPathToSavedPhotosAlbum("\(location)", nil, nil, nil)

                   //     let address = self.createFolder(folderName:"BidAudio")


                       try FileManager.default.moveItem(at: location, to: destinationUrl)
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                         //  self.showToast(message : "Saved")
                        }
        
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        }
                        print("your error is :\(error.localizedDescription)")
                    }
                }).resume()
            }
        
            
        }
        
        
        
      //downloadButton.isHidden = true
  // }
    
    
    func createFolder(folderName:String)->URL
    {
        var paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = paths[0] as? String ?? ""
        let dataPath: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(folderName).absoluteString
        if !FileManager.default.fileExists(atPath: dataPath) {
            try? FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
        }
        let fileURL = URL(string: dataPath)
        return fileURL!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if show == true {
            
            downloadButton.isHidden = true
            show = false
            
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if show == true {
            
            downloadButton.isHidden = true
            show = false
            
            
        }
        
        
        
    }
    
    //MARK: - IBAction
    @IBAction func BackAction(_ sender: Any) {
        
     //   self.playerController.player?.removeObserver(self, forKeyPath: "rate")
       // self.playerController.player?.currentItem?.removeObserver(self, forKeyPath: "timedMetadata")
        
        
        if playerActive == true {
            
//            if detailType == .Detail
//            {
//                self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
//            }
            
               self.playerController.player?.removeObserver(self, forKeyPath: "rate")
           
            
        }
        
        
        
        if playerObserver != nil
        {
            if detailType == .Detail
            {
                self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
            }
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
            self.playerController.player?.currentItem?.removeObserver(self, forKeyPath: "timedMetadata")
        //   self.playerController.player?.removeObserver(self, forKeyPath: "rate")
            
        }
     
           self.navigationController?.popViewController(animated: true)
       
        
    }
    
    //MARK: - Other function
    //MARK:- Key Observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        
        if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 0
        {
            print("stop")
            stopped = true
        }
        else if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 1
        {
            print("play")
            if (self.playerController.player?.currentTime().seconds)! >= Double(10.0)
            {
                if detailType == .Detail
                {
                    playerController.player?.seek(to: CMTimeMake(Int64(0), Int32(10)), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (result) in
                        print("result", result)
               
            
                    
                })
                }
            }
            stopped = false
        }
    }
    
    //MARK:- Functions
    @objc func stopPlaying()
    {
        player.pause()
    }
    
    func showAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "Purchase this item, to play full audio!", preferredStyle: .alert)
        
        
        let OKAction = UIAlertAction(title: "Back", style: .default) { (action:UIAlertAction!) in
            self.playerController.dismiss(animated: true, completion: nil)
            
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
          //  self.playerController.player?.currentItem?.removeObserver(self, forKeyPath: "timedMetadata")
        }
        alertController.addAction(OKAction)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
            
        }
        alertController.addAction(cancelAction)
        
        
        self.playerController.present(alertController, animated: true, completion:nil)
    }
    
    func StarAudio(Url:URL)  {
    //let path = Bundle.main.path(forResource: "Qismat", ofType: "mp3")
   // let soundUrl = NSURL(fileURLWithPath: path!)
        let audiourl = Url
            //URL(string: "http://stgsd.appsndevs.com:9048/uploads/1538141075242_MyAudio.mp3")
        if timerObserver != nil
        {
            if detailType == .Detail
            {
                self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
            }
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
            self.playerController.player?.currentItem?.removeObserver(self, forKeyPath: "timedMetadata")
        }
        
        let item = AVPlayerItem(url: audiourl)
    player = AVPlayer(playerItem: item)
    
    
    playerController.player = player
    playerController.allowsPictureInPicturePlayback = true
    
    // item.forwardPlaybackEndTime = CMTime(seconds: 10.0, preferredTimescale: 1)
    
    
    playerController.player?.play()
        
        self.playerActive = true
        
        let playerView = playerController.view
     //   btn_Download.bringSubview(toFront: playerView!)
      
            self.downloadButton.layer.cornerRadius = 6; // this value vary as per your desire
        self.downloadButton.clipsToBounds = true;
        self.downloadButton.backgroundColor = .brown
        
     self.downloadButton.setImage(#imageLiteral(resourceName: "download"), for: UIControlState.normal)
        //setBackgroundImage(UIImage(named: “testImage.png”), forState: UIControlState.Normal)
        
        
         playerView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.9, execute: {
        
            if HistoryVC.isMyPurchase == true {
         self.downloadButton.isHidden = false
        
    //  playerView?.addSubview(self.downloadButton)
            self.window.addSubview(self.downloadButton)
            self.show = true
            }
            
         })
       // btn_Download.frame = UIButton.init(frame: <#T##CGRect#>)
    self.present(playerController,animated:true,completion:nil)
        
        if detailType == .Detail
        {
            if HistoryVC.isMyPurchase == true {
                
            }
            else {
            let times = [NSValue(time:CMTimeMake(Int64(timeToStop),1))]
            
            _ = self.playerController.player?.addBoundaryTimeObserver(forTimes: times, queue: DispatchQueue.main, using:
                {
                    [weak self] in
                    self?.stopPlaying()
                    
                    self?.showAlert()
            })
           }
        }
        else
        {
          //  self.playerController.setValue(true, forKey: "requiresLinearPlayback")

        }
    self.playerController.player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
 
    
    }

}
extension AudioPlayerVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        show = false
        
        
        if detailType == .Detail
        {
        let url = productDetail?.arrAudios![indexPath.row].audioURL
            
            self.urlSong = url!
        StarAudio(Url: url!)
        }
        else
        {
        if let url = AddProuductData.sharedInstance?.arrAudios![indexPath.row].audioURL
        {
            StarAudio(Url: url)
        }
        }
    }
}

extension AudioPlayerVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMusicName.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell : SongListTableCell = tableView.dequeueReusableCell(withIdentifier: KsongListTableCell) as! SongListTableCell
        cell.LoadData(songName: arrMusicName[indexPath.row])
        cell.tag = indexPath.row
        cell.selectionStyle = .none
        return cell
    }
}

//extension AudioPlayerVC {
//    func showToastWhat(message : String) {
//
//        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
//        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        toastLabel.textColor = UIColor.white
//        toastLabel.textAlignment = .center;
//        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
//        toastLabel.text = message
//        toastLabel.alpha = 1.0
//        toastLabel.layer.cornerRadius = 10;
//        toastLabel.clipsToBounds  =  true
//        self.view.addSubview(toastLabel)
//        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
//            toastLabel.alpha = 0.0
//        }, completion: {(isCompleted) in
//            toastLabel.removeFromSuperview()
//        })
//    }
//
//}

