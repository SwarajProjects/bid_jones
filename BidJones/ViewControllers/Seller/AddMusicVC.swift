//
//  AddMusicVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import MobileCoreServices
import MediaPlayer
import GoogleMaps
import GooglePlaces
import GooglePlacePicker


class AddMusicVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,MusicCollectionCellDelegate,ImagesCollectionCellDelegate,VideosCollectionCellDelegate , LocationProtocol {
 
    
    
    
   
    
   
  
    
    @IBOutlet weak var btnOpenLink: UIButton!
    @IBOutlet var ViewDelete: UIView!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var btnProceed: CustomButton!
    @IBOutlet var longGesture: UILongPressGestureRecognizer!
    
    //MARK: - Label Outlets
    //MARK: - TextField Outlets
    @IBOutlet var txtFld_Location: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_KeySearchWord: CustomUITextFieldWithPadding!
    @IBOutlet var tblLoaction: UITableView!
    @IBOutlet var txtFld_MinBidAmount: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Duration: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Descrption: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_AlbumName: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_ArtistName: CustomUITextFieldWithPadding!
    @IBOutlet var txtFldBidTYpe: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Genre: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_ChangeName: UITextField!
  //  @IBOutlet var txtFld_Title: UITextField!
    @IBOutlet var txtFld_forOpeningPickers: UITextField!
    //MARK: - UICollectionView Outlets
    @IBOutlet var CollectionView_Images: UICollectionView!
    @IBOutlet var CollectionView_Video: UICollectionView!
    @IBOutlet var CollectionView_Audio: UICollectionView!
    //MARK: - UISCrollView Outlets
    @IBOutlet var scrollView_main: UIScrollView!
    //MARK: - UIView Outlets
    @IBOutlet var viewChangeName: UIView!
    //MARK: - UIPickerView Variable
    private var pickerView = UIPickerView()
    //MARK: - Int Variable
    private var count = 0
    private var totalCount = 0
    private var selectedMusicCell = 0
    private var strGenreID:String?
    private var strBidTypeID:String?
    private var maxNumImages = 0
    private var DeletedIndex = 0

    //MARK: - NSData Variables
    //MARK: - Bool Variable
    var is_escrow = Bool()
    var isGenrePicker = true
    //MARK: - String Variable
    //MARK: - array Variable
      var arrImages = [ImagesData]()
      var arrVideos = [VideoData]()
      var arrAudios  = [AudioData]()
      var arrImagesUrl = [String]()
      var arrVideosUrl  = [String]()
      var arrAudiosUrl  = [String]()
      var arrGenre =   [GenreData]()
      var arrBidType = [BidTypeData]()
      var arrRemoveMedia   = [String]()

      var arrPicker = [Any]()
      var awsType:AWSDataType?
      var deleteItemType:AWSDataType?


    //MARK: - UIImagePickerController
    private var imagePicker =  UIImagePickerController()
    //MARK: - Enum Variable

    //MARK: - dictionary Variable
    //Mark: - MPMusicPlayerController Variable
    
    //Mark: - NStimer Variable
    var timer = Timer()
    //MARK: - UIImagePickerController Variable
    //MARK: - AudioCollection_heightConstraints
    @IBOutlet var leftArrowAudioCenterConstraints: NSLayoutConstraint!
    @IBOutlet var AudioCollection_heightConstraints: NSLayoutConstraint!
    @IBOutlet var ImagesCollection_heightConstarints: NSLayoutConstraint!
    //MARK: - Enum Variable
    
    enum PickerType: Int {
        case GenrePicker = 1
        case BidTypePicker = 2
        init() {
            self = .GenrePicker
        }
    }
    var productDetail:ProductData?
    private var pickerType: PickerType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.hideKeyboardWhenTappedAround()
        ViewDelete.isHidden = true

        GetGener()
       // is_escrow = true;
     //   AddProuductData.sharedInstance?.Reinitilize()
      //  AudioCollection_heightConstraints.constant = 0
        self.automaticallyAdjustsScrollViewInsets = false
        pickerView.delegate = self
        viewChangeName.isHidden = true
        pickerView.dataSource = self
        txtFld_forOpeningPickers.inputView = pickerView
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFld_forOpeningPickers.inputAccessoryView = toolBar
        txtFld_MinBidAmount.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonAction), Title: KNext)
        txtFld_Duration.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonDurationAction), Title: KNext)
        AddProuductData.sharedInstance?.Reinitilize()
      LoadDataForEdit()
        setupLongPressGesture()
        //Set Attributed text for btn open link
        KCommonFunctions.setAttributedStringInBtnOpenLink(button: btnOpenLink)
        
        
//        txtFld_Location.text = "Mohali"
//
//        AddProuductData.sharedInstance?.lat = 30.7115
//        //Float(place.coordinate.latitude)
//        AddProuductData.sharedInstance?.lng = 76.7142

    }
    override func viewDidAppear(_ animated: Bool) {
          }
    override func viewWillAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Other Functions
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self as? UIGestureRecognizerDelegate
        self.CollectionView_Audio.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.CollectionView_Audio)
            if let indexPath = CollectionView_Audio.indexPathForItem(at: touchPoint){
                print(indexPath.row)
                if indexPath.row < arrAudios.count
                {
                ChangeMusicName(Index:indexPath.row)
                }
            }
        }
    }
    
    //Set data for web page view controller
    func setDataForWebPageViewController(){
        var dict = [String: Any]()
        dict["url"] = "https://app.bernstein.io/bidjones"
        dict["title"] = "Secure Trade"
        KCommonFunctions.PushToContrller(from: self, ToController: .TermsNew, Data: dict)
    }
    func LoadDataForEdit()
    {
        if let Data = productDetail
        {
            self.title = "EDIT MUSIC"
            btnProceed.setTitle("UPDATE", for: .normal)

            if let ItemID = Data.itemId
            {
                AddProuductData.sharedInstance?.itemID = ItemID
            }
            if let title =  Data.title
            {
                txtFld_AlbumName.text = title
                AddProuductData.sharedInstance?.title = title
            }
            if let description = Data.description
            {
                txtFld_Descrption.text = description
                AddProuductData.sharedInstance?.description = description
            }
            if let albumName = Data.title
            {
                txtFld_AlbumName.text = albumName
                AddProuductData.sharedInstance?.albumName = albumName
            }
            if let artistName = Data.artistName
            {
                txtFld_ArtistName.text = artistName
                AddProuductData.sharedInstance?.artistName = artistName
            }
            if let id = UserDefaults.standard.getUserID()
            {
                AddProuductData.sharedInstance?.id = id
            }
            if let GenreID = Data.genreId
            {
                strGenreID = "\(GenreID)"
                AddProuductData.sharedInstance?.genreId = Int(GenreID)
            }
            if let Genre = Data.serviceType
            {
                txtFld_Genre.text = Genre
                AddProuductData.sharedInstance?.genre = Genre
            }
            if let Genre = Data.genre, let itemType = Data.itemType, itemType == 3
            {
                txtFld_Genre.text = Genre
                AddProuductData.sharedInstance?.genre = Genre
            }
            if let bidTypeID = Data.minBidTypeId
            {
                strBidTypeID = "\(bidTypeID)"
                AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
            }
            if let duration = Data.duration
            {
                txtFld_Duration.text = duration
                AddProuductData.sharedInstance?.duration = duration
            }
            if let minBid = Data.minBidAmount
            {
                txtFld_MinBidAmount.text = "\(minBid)"
                //AddProuductData.sharedInstance?.minBidAmount = Float(minBid)
                AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
            }
            if let ArrVideos = productDetail?.arrVideos
            {
                arrVideos = ArrVideos
                CollectionView_Video.reloadData()
                AddProuductData.sharedInstance?.arrVideos = ArrVideos
            }
            if let keywords = Data.keySearchWord
            {
                txtFld_KeySearchWord.text = keywords
                AddProuductData.sharedInstance?.keySearchWord = keywords
            }
//            if arrImages.count>0
//            {
//                AddProuductData.sharedInstance?.arrImages = arrImages
//            }
                        if let ArrImages = Data.arrImages
                        {
                            arrImages = ArrImages
                            CollectionView_Images.reloadData()
                            AddProuductData.sharedInstance?.arrImages = ArrImages
                        }
//                        if let ArrVideos = Data.arrVideos
//                        {
//                            arrVideos = ArrVideos
//                            CollectionView_Video.reloadData()
//                            AddProuductData.sharedInstance?.arrVideos = ArrVideos
//                        }
            if let ArrAudios = Data.arrAudios
            {
                arrAudios = ArrAudios
                CollectionView_Video.reloadData()
                AddProuductData.sharedInstance?.arrAudios = arrAudios
            }
                        if let isEscrow = Data.is_escrow
                        {
                            AddProuductData.sharedInstance?.is_escrow = isEscrow
                            if isEscrow == true
                            {
                                is_escrow = true
                                if let image  = UIImage(named: Kselected)
                                {
                                    btnYes.setImage(image, for: .normal)
                                }
                                if let image  = UIImage(named: Kunselected)
                                {
                                    btnNo.setImage(image, for: .normal)
                                }
                            }
                            else
                            {
                                is_escrow = false
                                if let image  = UIImage(named: Kselected)
                                {
                                    btnNo.setImage(image, for: .normal)
                                }
                                if let image  = UIImage(named: Kunselected)
                                {
                                    btnYes.setImage(image, for: .normal)
                                }
                            }
                            // AddProuductData.sharedInstance?.location = location
                        }
//            if arrAudios.count>0
//            {
//                var arrMusicNames = [String]()
//
//                for item in arrAudios
//                {
//                    arrMusicNames.append(item.title)
//                }
//                print(arrMusicNames)
//                AddProuductData.sharedInstance?.musicNames = arrMusicNames
//                print(AddProuductData.sharedInstance?.musicNames ?? "Blnk")
//
//                AddProuductData.sharedInstance?.arrAudios = arrAudios
//            }
//            if arrVideos.count>0
//            {
//                AddProuductData.sharedInstance?.arrVideos = arrVideos
//            }
            if let location = Data.location
            {
                txtFld_Location.text = location
                AddProuductData.sharedInstance?.location = location
            }
            if let minBidType = Data.minBidType
            {
                txtFldBidTYpe.text = minBidType
                AddProuductData.sharedInstance?.minBidType = minBidType
            }
                        if let lat = Data.lat
                        {
                            AddProuductData.sharedInstance?.lat = lat
                        }
                        if let lng = Data.lng
                        {
                            AddProuductData.sharedInstance?.lng = lng
                        }
                        AddProuductData.sharedInstance?.IsEdited = true
                      AddProuductData.sharedInstance?.itemType = KMusicType
//            print(Data)
//            if let ItemID = Data.itemId
//            {
//                AddProuductData.sharedInstance?.itemID = ItemID
//            }
//            if let title =  Data.title
//            {
//                txtFld_Title.text = title
//                // AddProuductData.sharedInstance?.title = title
//            }
//
//            if let title =  Data.title
//            {
//                txtFld_Title.text = title
//                // AddProuductData.sharedInstance?.title = title
//            }
//            if let description = Data.description
//            {
//                txtFld_Descrption.text = description
//                //AddProuductData.sharedInstance?.description = description
//            }
//            //            if let id = UserDefaults.standard.getUserID()
//            //            {
//            //                AddProuductData.sharedInstance?.id = id
//            //            }
//            if let bidTYpe = Data.minBidType
//            {
//                txtFld_BidType.text = bidTYpe
//                //AddProuductData.sharedInstance?.minBidType = bidTYpe
//            }
//            if let bidTypeID = Data.minBidTypeId
//            {
//                AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
//            }
//            if let serviceType = Data.serviceType
//            {
//                txtFld_Genre.text = serviceType
//                //AddProuductData.sharedInstance?.serviceType = serviceType
//            }
//            if let GenreID = Data.genreId
//            {
//                AddProuductData.sharedInstance?.genreId = Int(GenreID)
//            }
//            if let minBid = Data.minBidAmount
//            {
//                txtFld_MinBidAmount.text = "\(minBid)"
//                //                print(minBid)
//                //                print(Float(minBid) as Any)
//                //                print(Double(minBid) as Any)
//                //                AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
//                //                print(Double(minBid))
//                //                print(AddProuductData.sharedInstance?.minBidAmount)
//            }
//            if let toc = Data.TC
//            {
//                txtFld_TCService.text = toc
//                AddProuductData.sharedInstance?.TC = toc
//            }
//            if let ArrImages = Data.arrImages
//            {
//                arrImages = ArrImages
//                CollectionView_Images.reloadData()
//                AddProuductData.sharedInstance?.arrImages = ArrImages
//            }
//            if let keywords = Data.keySearchWord
//            {
//                txtFld_KeySearchWord.text = keywords
//                //AddProuductData.sharedInstance?.keySearchWord = keywords
//            }
//            if let ArrVideos = Data.arrVideos
//            {
//                arrVideos = ArrVideos
//                CollectionView_Video.reloadData()
//                AddProuductData.sharedInstance?.arrVideos = ArrVideos
//            }
//            if let location = Data.location
//            {
//                txtFld_Location.text = location
//                // AddProuductData.sharedInstance?.location = location
//            }
//            //            if let minBidType = txtFld_BidType.text
//            //            {
//            //                txtFld_BidType.text = minBidType
//            //                AddProuductData.sharedInstance?.minBidType = minBidType
//            //            }
//
//            if let isEscrow = Data.is_escrow
//            {
//                AddProuductData.sharedInstance?.is_escrow = isEscrow
//                if isEscrow == true
//                {
//                    is_escrow = true
//                    if let image  = UIImage(named: Kselected)
//                    {
//                        btnYes.setImage(image, for: .normal)
//                    }
//                    if let image  = UIImage(named: Kunselected)
//                    {
//                        btnNo.setImage(image, for: .normal)
//                    }
//                }
//                else
//                {
//                    is_escrow = false
//                    if let image  = UIImage(named: Kselected)
//                    {
//                        btnNo.setImage(image, for: .normal)
//                    }
//                    if let image  = UIImage(named: Kunselected)
//                    {
//                        btnYes.setImage(image, for: .normal)
//                    }
//                }
//                // AddProuductData.sharedInstance?.location = location
//            }
//            //            if let itemType = Data.itemType
//            //            {
//            //                AddProuductData.sharedInstance?.itemType = itemType
//            //                // AddProuductData.sharedInstance?.location = location
//            //            }
//            //            AddProuductData.sharedInstance?.is_escrow = is_escrow
//            AddProuductData.sharedInstance?.itemType = KServicesType
//            if let lat = Data.lat
//            {
//                AddProuductData.sharedInstance?.lat = lat
//            }
//            if let lng = Data.lng
//            {
//                AddProuductData.sharedInstance?.lng = lng
//            }
//            AddProuductData.sharedInstance?.IsEdited = true
        }
        
    }
    func ChangeMusicName(Index: Int) {
        //print((sender as AnyObject).tag)
        //print(arrAudios.count)

        if(Index < arrAudios.count)
        {
        viewChangeName.isHidden = false
        txtFld_ChangeName.text = arrAudios[Index].title
        txtFld_ChangeName.becomeFirstResponder()
            selectedMusicCell = Index
        }
    }
    @objc func doneButtonDurationAction()
    {
        self.txtFld_MinBidAmount.becomeFirstResponder()
    }
    
    @objc func doneButtonAction()
    {
        self.txtFld_KeySearchWord.becomeFirstResponder()
    }
    
    func getDocumentsDirectory() -> URL? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func Validations() throws
    {
        guard let albumName  = txtFld_AlbumName.text, !albumName.isEmpty, !albumName.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyName
        }
        guard let artistName = txtFld_ArtistName.text,  !artistName.isEmpty, !artistName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyArtistName
        }
        guard let Genre  = txtFld_Genre.text, !Genre.isEmpty, !Genre.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyGenre
        }
        guard let descrption  = txtFld_Descrption.text, !descrption.isEmpty, !descrption.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyDescription
        }
        guard let duration  = txtFld_Duration.text, !duration.isEmpty, !duration.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyDuration
        }
        if !duration.MinimumRangeofTextFieldValue(minCharCount: 8, value: duration)
         {
            throw ValidationError.incorrectDuration
        }
        guard let bidAmount  = txtFld_MinBidAmount.text, !bidAmount.isEmpty, !bidAmount.trimmingCharacters(in: .whitespaces).isEmpty, !bidAmount.replacingOccurrences(of: ".", with: "").isEmpty else
        {
            throw ValidationError.emptyBidAmount
        }
        guard let bidType  = txtFldBidTYpe.text, !bidType.isEmpty, !bidType.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyBidType
        }
        guard let keysearchWord  = txtFld_KeySearchWord.text, !keysearchWord.isEmpty, !keysearchWord.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyKeySearchWord
        }
        guard let location  = txtFld_Location.text, !location.isEmpty, !location.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyLocation
        }
//        guard let keysearchWord  = txtFld_KeySearchWord.text, !keysearchWord.isEmpty, !keysearchWord.trimmingCharacters(in: .whitespaces).isEmpty else
//        {
//            throw ValidationError.emptyKeySearchWord
//        }
        if arrImages.count == 0
        {
            throw ValidationError.emptyImages
        }
        if arrAudios.count == 0
        {
            throw ValidationError.emptyAudios
        }
//        if arrVideos.count == 0
//        {
//            throw ValidationError.emptyVideos
//        }
        
       
    }
    func GetGener()  {
        let urlStr = KGenerApi + String(KMusicType)
        AddProduct.sharedManager.GetApi(url: urlStr,Target: self, completionResponse: { (Genre,BidType,maxImages) in
            self.arrGenre = Genre
             self.arrBidType = BidType
             self.maxNumImages = maxImages
            self.CollectionView_Images.reloadData()
            //print(Genre)
            //print(BidType)
            //print(maxImages)
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    
                    self.GetGener()
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        
        switch pickerType
        {
        case .GenrePicker?:
            let dic = arrPicker[row] as! GenreData
            let title = dic.name
            let id = dic.id
            let ID:String = String(id)
            strGenreID = ID
            txtFld_Genre.text = title
        case .BidTypePicker?:
            let dic = arrPicker[row] as! BidTypeData
            let title = dic.name
            let id = dic.id
            let ID:String = String(id)
            strBidTypeID = ID
            txtFldBidTYpe.text = title
        case .none:
            print("none")
        }
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    @objc func CancelPicker()
    {
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    
    func Submitstep1() {
                let imagescount = arrImages.count
                let videoCount = arrVideos.count
                let audioCount = arrAudios.count
                totalCount = imagescount + videoCount + audioCount
                //print(imagescount)
                //print(videoCount)
                //print(audioCount)
                //print(count)
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
             if(count < imagescount)
                {
                     MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
                    //print(arrImages)
                    AmazonServices.sharedInstance.UploadData(awsType: .Image, url: arrImages[count].imgUrl, completionHandler: { (error, urlStr) -> (Void) in
                        //print(error ?? "Blank")
                        //print(urlStr ?? "Blank")
                        if error == nil
                        {
                            self.arrImagesUrl.append(urlStr!)
                            self.count = self.count+1
                            DispatchQueue.main.async {
                                self.Submitstep1()
                            }
                        }
                        else
                        {
                            self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                                self.count = 0
                                self.arrImagesUrl.removeAll()
                                self.arrVideosUrl.removeAll()
                                self.arrAudiosUrl.removeAll()
                                //self.SubmitAction(AnyObject.self)
                            })
                        }
                    }, Target: self,networkError: {(error) in
                        self.showAlertMessage(titleStr: KMessage, messageStr: error)
                    })
                }
                else if (count < audioCount+imagescount)
                {
                     MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
                    //print(arrAudios)
                    //print(arrAudios[count-imagescount].audioURL)
        
                    AmazonServices.sharedInstance.UploadData(awsType: .Song, url: arrAudios[count-imagescount].audioURL, completionHandler: { (error, urlStr) -> (Void) in
                        //print(error ?? "Blank")
                        //print(urlStr ?? "Blank")
                        if error == nil
                        {
                            self.arrAudiosUrl.append(urlStr!)
                        
                        self.count = self.count+1
                        DispatchQueue.main.async {
                            self.Submitstep1()
                        }
                        }else
                        {
                            self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                                self.count = 0
                                self.arrImagesUrl.removeAll()
                                self.arrVideosUrl.removeAll()
                                self.arrAudiosUrl.removeAll()
                                //self.SubmitAction(AnyObject.self)
                            })
                        }
                    }, Target: self,networkError: {(error) in
                        self.showAlertMessage(titleStr: KMessage, messageStr: error)
                    })
                }
                else if(count < audioCount+imagescount+videoCount)
                {
                     MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
                    //print(arrVideos)
                    AmazonServices.sharedInstance.UploadData(awsType: .Video, url: arrVideos[count-imagescount-audioCount].videoUrl, completionHandler: { (error, urlStr) -> (Void) in
                        //print(error ?? "Blank")
                        //print(urlStr ?? "Blank")
                        if error == nil
                        {
                            self.arrVideosUrl.append(urlStr!)
                        
                        self.count = self.count+1
                        DispatchQueue.main.async {
                            self.Submitstep1()
                        }
                    }
                        else
                        {
                            self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                                self.count = 0
                                self.arrImagesUrl.removeAll()
                                self.arrVideosUrl.removeAll()
                                self.arrAudiosUrl.removeAll()
                                //self.SubmitAction(AnyObject.self)
                            })
                        }
                    }, Target: self,networkError: {(error) in
                            self.showAlertMessage(titleStr: KMessage, messageStr: error)
                    })
                }
                else
                {
                    //print(arrImagesUrl)
                    //print(arrAudiosUrl)
                    //print(arrVideosUrl)
                    Submitstep2()
        }
    }
    
    func Submitstep2()
    {
        if let title =  txtFld_AlbumName.text
        {
            AddProuductData.sharedInstance?.title = title
        }
        if let description = txtFld_Descrption.text
        {
            AddProuductData.sharedInstance?.description = description
        }
                            if let albumName = txtFld_AlbumName.text
                            {
                                AddProuductData.sharedInstance?.albumName = albumName
                            }
                            if let artistName = txtFld_ArtistName.text
                            {
                                AddProuductData.sharedInstance?.artistName = artistName
                            }
                            if let id = UserDefaults.standard.getUserID()
                            {
                                AddProuductData.sharedInstance?.id = id
                            }
                            if let GenreID = strGenreID
                            {
                                AddProuductData.sharedInstance?.genreId = Int(GenreID)
                            }
                           if let Genre = txtFld_Genre.text
                           {
                            AddProuductData.sharedInstance?.genre = Genre
                            }
                            if let bidTypeID = strBidTypeID
                            {
                                AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
                            }
                            if let duration = txtFld_Duration.text
                            {
                                AddProuductData.sharedInstance?.duration = duration
                            }
                            if let minBid = txtFld_MinBidAmount.text
                            {
                                //AddProuductData.sharedInstance?.minBidAmount = Float(minBid)
                                AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
                            }
                            if let keywords = txtFld_KeySearchWord.text, !keywords.isEmpty
                            {
                                AddProuductData.sharedInstance?.keySearchWord = keywords
                            }
                            if arrImages.count>0
                            {
                                AddProuductData.sharedInstance?.arrImages = arrImages
                            }
                            if arrAudios.count>0
                            {
                                var arrMusicNames = [String]()
                                
                                for item in arrAudios
                                {
                                    arrMusicNames.append(item.title)
                                }
                                print(arrMusicNames)
                                AddProuductData.sharedInstance?.musicNames = arrMusicNames
                                print(AddProuductData.sharedInstance?.musicNames ?? "Blnk")

                                AddProuductData.sharedInstance?.arrAudios = arrAudios
                            }
                            if arrVideos.count>0
                            {
                                AddProuductData.sharedInstance?.arrVideos = arrVideos
                            }
                             else
                            {
                                let blank = [VideoData]()
                                AddProuductData.sharedInstance?.arrVideos = blank
                            }
        if let location = txtFld_Location.text
        {
            AddProuductData.sharedInstance?.location = location
        }
        if let minBidType = txtFldBidTYpe.text
        {
            AddProuductData.sharedInstance?.minBidType = minBidType
        }
        AddProuductData.sharedInstance?.is_escrow = is_escrow
        AddProuductData.sharedInstance?.itemType = KMusicType
        AddProuductData.sharedInstance?.arrRemoveMedia = arrRemoveMedia
        KCommonFunctions.PushToContrller(from: self, ToController: .Preview, Data: nil)
                        }
    
    
    //MARK: - IBActions
    
    //Below is Inaction for picking music from media library
    @IBAction func LongPressDetection(_ sender: UILongPressGestureRecognizer) {
        print("Detect Long Press")
    }
    
    //For open the link
    @IBAction func onClickOpenLink(_ sender: UIButton) {
        setDataForWebPageViewController()
    }
    
    @IBAction func MapAction(_ sender: Any)
    {
//        //        mapView.isHidden = false
//        //        imgView_Pin.isHidden = false
//        //        btnSave.title = "SAVE"
//        //        btnSave.isEnabled = true
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePickerViewController(config: config)
//        placePicker.delegate = self
//        present(placePicker, animated: true, completion: nil)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
        vc.locationDelegate = self;
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    func Location(lat: String, lng: String, address: String) {
        
        print("protcol address")
        print(address)
        print(lat)
        print(lng)
        
        txtFld_Location.text = address
        AddProuductData.sharedInstance?.lat = Float(lat)
        //Float(place.coordinate.latitude)
        AddProuductData.sharedInstance?.lng = Float(lng)
        
    }
    
    
    @IBAction func YesDeleteAction(_ sender: Any) {
        
        switch deleteItemType {
        case .Song?:
            let index = DeletedIndex
            if index < arrAudios.count
            {
                self.arrRemoveMedia.append((arrAudios[index].audioURL.absoluteString))
                arrAudios.remove(at: index)
                CollectionView_Audio.reloadData()
            }
        case .Video?:
            //print((sender as AnyObject).tag)
            let index = DeletedIndex
            if index < arrVideos.count
            {
                self.arrRemoveMedia.append((arrVideos[index].videoUrl?.absoluteString)!)
                arrVideos.remove(at: index)
                CollectionView_Video.reloadData()
            }
        case .Image?:
            //print((sender as AnyObject).tag)
            let index = DeletedIndex
            if index < arrImages.count
            {
                self.arrRemoveMedia.append((arrImages[index].imgUrl?.absoluteString)!)
                arrImages.remove(at: index)
                CollectionView_Images.reloadData()
            }
        default:
            print("Fail")
        }
        ViewDelete.isHidden = true
    }
    @IBAction func NoDeleteAction(_ sender: Any) {
        ViewDelete.isHidden = true
    }
    
    
    @IBAction func YesAction(_ sender: Any) {
        is_escrow = true
        if let image  = UIImage(named: Kselected)
        {
            btnYes.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnNo.setImage(image, for: .normal)
        }
    }
    @IBAction func NoAction(_ sender: Any) {
        is_escrow = false
        if let image  = UIImage(named: Kselected)
        {
            btnNo.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnYes.setImage(image, for: .normal)
        }
    }

    
    @IBAction func ScrollCollectionAction(_ sender: Any)
    {
        //print((sender as AnyObject).tag)
        
        if (sender as AnyObject).tag == 0
        {
            let visibleItems: NSArray = self.CollectionView_Images.indexPathsForVisibleItems as NSArray
            print(visibleItems)
            let minIndex = visibleItems.map { ($0 as AnyObject).row }.min()
            let nextItem: IndexPath = IndexPath(item: minIndex! - 1, section: 0)
            print("nextItem : \(nextItem)")
           if(nextItem.row >= 0)
            {
            print("LeftScroll")
            self.CollectionView_Images.scrollToItem(at: nextItem, at: .right, animated: true)
            }
        }
        else if  (sender as AnyObject).tag == 1
        {
            let visibleItems: NSArray = self.CollectionView_Images.indexPathsForVisibleItems as NSArray
            print(visibleItems)
            let maxIndex = visibleItems.map { ($0 as AnyObject).row }.max()
            let nextItem: IndexPath = IndexPath(item: maxIndex! + 1, section: 0)
            print(nextItem)
            if(maxNumImages > nextItem.row)
            {
                print("RightScroll")
                self.CollectionView_Images.scrollToItem(at: nextItem, at: .left, animated: true)
            }
        }
        else if  (sender as AnyObject).tag == 2
        {
            let visibleItems: NSArray = self.CollectionView_Audio.indexPathsForVisibleItems as NSArray
            print(visibleItems)
            let minIndex = visibleItems.map { ($0 as AnyObject).row }.min()
            print(minIndex ?? "blank")
            let nextItem: IndexPath = IndexPath(item: minIndex! - 1, section: 0)
            print(nextItem)
            if(nextItem.row >= 0)
            {
                print("RightScroll")
                self.CollectionView_Audio.scrollToItem(at: nextItem, at: .right, animated: true)
            }
        }
        else if  (sender as AnyObject).tag == 3
        {
            let visibleItems: NSArray = self.CollectionView_Audio.indexPathsForVisibleItems as NSArray
            print(visibleItems)
            let maxIndex = visibleItems.map { ($0 as AnyObject).row }.max()
            print(maxIndex ?? "blank")
            let nextItem: IndexPath = IndexPath(item: maxIndex! + 1, section: 0)
            print(nextItem)
            if(KMaxAudio > nextItem.row)
            {
                print("RightScroll")
                self.CollectionView_Audio.scrollToItem(at: nextItem, at: .left, animated: true)
            }
        }
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func SubmitAction(_ sender: Any) {
        
        
        self.view.endEditing(true)
        do {
            try Validations()
           // MBProgressHUD.showAdded(to: self.view, animated: true).labelText = KLoading
          Submitstep2()
        } catch let error {
            switch  error {
            case ValidationError.emptyName:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenteralbumname)
            case ValidationError.emptyArtistName:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterartistname)
            case ValidationError.emptyTitle:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentersongtitle)
            case ValidationError.emptyGenre:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectGenre)
            case ValidationError.emptyDescription:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterdescrption)
            case ValidationError.emptyDuration:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterduration)
            case ValidationError.incorrectDuration:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentercorrectdurationime)
            case ValidationError.emptyKeySearchWord:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterkeysearchword)
            case ValidationError.emptyBidType:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterBidType)
            case ValidationError.emptyBidAmount:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterBidAmount)
            case ValidationError.emptyImages:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectatleastoneimage)
            case ValidationError.emptyAudios:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectatleastoneaudio)
            case ValidationError.emptyVideos:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectVideo)
            case ValidationError.emptyLocation:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterlocation)
            case ValidationError.emptyKeySearchWord:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterkeysearchword)

            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KUnknownerror)
            }
        }
    }

    @IBAction func BidTypeAction(_ sender: Any)
    {
        if(
            arrBidType.count>0)
        {
            isGenrePicker = false
            pickerType = PickerType(rawValue: 2)
            self.arrPicker = self.arrBidType
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFldBidTYpe.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrBidType, str: (txtFldBidTYpe.text)!, key: Kname,type:.BidTypeData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
    }
    @IBAction func GenerAction(_ sender: Any)
    {
    if(
            arrGenre.count>0)
        {
            isGenrePicker = true
            pickerType = PickerType(rawValue: 1)
            self.arrPicker = self.arrGenre
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_Genre.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrGenre, str: (txtFld_Genre.text)!, key: Kname,type:.GenreData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
    }
    
    
    
    
    @IBAction func CancelAction(_ sender: Any) {
        self.view.endEditing(true)
        txtFld_ChangeName.text = ""
        viewChangeName.isHidden = true
    }

    @IBAction func OKAction(_ sender: Any) {
        guard let songName  = txtFld_ChangeName.text, !songName.isEmpty, !songName.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            showAlertMessage(titleStr: Kmessage, messageStr: KPleaseentersongname)
            return
        }
        
        if(selectedMusicCell < arrAudios.count)
        {
            let item = arrAudios[selectedMusicCell]
            let title = txtFld_ChangeName.text
            let albumTitle = item.albumTitle
            let audioURL = item.audioURL
            let artist = item.artist
            let assetUrl = item.assetUrl
            let radio = item.isRadio

           
            
            if let Data = AudioData(Title: title,AudioURL: audioURL, AlbumTitle: albumTitle, Artist: artist,AssetUrl:assetUrl, IsRadio: radio)
            {
                //print(self.arrAudios)
               self.arrAudios[selectedMusicCell] = Data
                //print(self.arrAudios)

            }
           CollectionView_Audio.reloadData()
            
        }
        viewChangeName.isHidden = true
        self.view.endEditing(true)
        txtFld_ChangeName.text = ""

    }
    
    @IBAction func btnMediaPickerAction(_ sender: UIButton) {
        if (Int(arrAudios.count) < KMaxAudio)
        {
        
        let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.music)
        mediaPicker.delegate = self
        mediaPicker.allowsPickingMultipleItems = false
        mediaPicker.showsCloudItems = false
        let indexPath = IndexPath(row: 0, section: 0)
        self.CollectionView_Audio.scrollToItem(at: indexPath, at: .left, animated: true)
        self.present(mediaPicker, animated: true, completion: nil)
        }
        else
        {
            showAlertMessage(titleStr: KMessage, messageStr: "\(KYoucanuploadmaximum) \(KMaxAudio) \(KSongs)")
        }
    }
    
//    @IBAction func AddMoreAction(_ sender: Any) {
//        viewAddMore.isHidden = false
//    }
    
    @IBAction func AddVideoAction(_ sender: Any) {
        
        if (Int(arrVideos.count) < 1)
        {
            self.OpenGallaryCameraForVideo(pickerController: imagePicker)
        }
        else
        {
            showAlertMessage(titleStr: KMessage, messageStr: "\(KYoucanuploadmaximum) \(KMaxVideo) \(Kvideo)")
        }
    }
    
    @IBAction func AddImageAction(_ sender: Any) {
        if (Int(arrImages.count) < maxNumImages)
        {
            self.OpenGallaryCamera(pickerController: imagePicker)
        }
        else
        {
            showAlertMessage(titleStr: KMessage, messageStr: "\(KYoucanuploadmaximum) \(maxNumImages) \(Kimages)")
        }
    }
    
    //MARK: - CollectionCell Delegate
    func DeleteImage(_ sender: Any) {
        DeletedIndex = (sender as AnyObject).tag
        deleteItemType = .Image
        ShowAlert()
    }
    
    func DeleteVideo(_ sender: Any) {
        DeletedIndex = (sender as AnyObject).tag
        deleteItemType = .Video
        ShowAlert()

    }
    
    func DeleteMusic(_ sender: Any)
    {
        DeletedIndex = (sender as AnyObject).tag
        deleteItemType = .Song
        ShowAlert()
    }
    
    func ShowAlert()
    {
        ViewDelete.isHidden = false
    }
    //MARK: - TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            if(!isGenrePicker)
            {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 250
                }, completion: nil)
            }
            }
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 0
                }, completion: nil)
            }
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
        if (textField == txtFld_AlbumName)
        {
            return textField.RestrictMaxCharacter(maxCount: 50, range: range, string: string)
        }
        if textField == txtFld_Duration
        {
            if textField.RestrictMaxCharacter(maxCount: 8, range: range, string: string)
            {
            if ((textField.text?.count)! == 2 || (textField.text?.count)! == 5) && (string != "")
            {
                textField.text =  textField.text! + ":"
            }
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            }
            else
            {
                return false
            }
        }
        if textField == txtFld_KeySearchWord
        {
            return textField.RestrictMaxCharacter(maxCount: 30, range: range, string: string)
        }
       
        if textField == txtFld_Descrption
        {
         return textField.RestrictMaxCharacter(maxCount: 1000, range: range, string: string)
            
        }
        if textField == txtFld_MinBidAmount 
        {
            if textField.RestrictMaxCharacter(maxCount: 10, range: range, string: string)
            {
            if !((textField.text?.contains("."))! && string.contains("."))
            {
                var separator = txtFld_MinBidAmount.text?.components(separatedBy: ".")
                let count = Double((separator?.count)!)
                if count > 1
                {
                    let sepStr1 = "\(separator![1])"
                    if sepStr1.count == 2 && string != ""  {
                        return false
                    }
                    if let amount = Double(textField.text! + string)
                    {
                        if amount > 970873.77
                        {
                            return false
                        }
                    }
                }
                return true
            }
            }
             return false
        }
    return true
    }

    
    //MARK: - UImage & Video Picker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
    // Handle a movie capture
        if mediaType == kUTTypeMovie {
            guard let videoURL = info[UIImagePickerControllerMediaURL] as? URL else
            {return}
            if let thumbNailImage =  getThumbnailImage(forUrl: videoURL)
            {
                if let Data = VideoData(VideoUrl: videoURL, Image: thumbNailImage)
                {
                    ////print(videoURL)
                    arrVideos.removeAll()
               arrVideos.insert(Data, at: 0)
                }
                CollectionView_Video.reloadData()
            }

        }
        // Handle Image Capture
        else{
            
            //print(info)
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            // choose a name for your image
            let fileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            if let data = UIImageJPEGRepresentation(pickedImage, 1.0),
                !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    // writes the image data to disk
                    try data.write(to: fileURL)
                    if let Data = ImagesData(ImgUrl: fileURL, Image: pickedImage)
                    {
                        arrImages.insert(Data, at: 0)
                    }
                    //print(arrImages)
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.CollectionView_Images.scrollToItem(at: indexPath, at: .left, animated: true)
                    CollectionView_Images.reloadData()
                    //print("file saved")
                } catch {
                    //print("error saving file:", error)
                }
            }
            }
            

        }
         dismiss(animated:true, completion: nil)
        
    }
    
    @objc func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        var title = "Success"
        var message = "Video was saved"
        if let _ = error {
            title = "Error"
            message = "Video failed to save"
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
}
extension AddMusicVC : GMSMapViewDelegate,GMSPlacePickerViewControllerDelegate
{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("lat \(place.coordinate.latitude)")
        print("lng \(place.coordinate.longitude)")
        print("lng \(place.name)")
        print(place.addressComponents ?? "Blank")
        print("lng \(place.placeID)")
        if let address = place.formattedAddress
        {
            txtFld_Location.text = address
//            AddProuductData.sharedInstance?.lat = Float(place.coordinate.latitude)
//            AddProuductData.sharedInstance?.lng = Float(place.coordinate.longitude)
//            AddProuductData.sharedInstance?.lat = 30.7115
//            //Float(place.coordinate.latitude)
//            AddProuductData.sharedInstance?.lng =     76.7142
            
            
        }
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    
    //    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    //    {
    //        let lat = mapView.camera.target.latitude
    //        let lng = mapView.camera.target.longitude
    //      // lattitude = Float(lat)
    //        longitude = Float(lng)
    //    }
    //    func fetchCountryAndCity(location: CLLocation, completion: @escaping (String) -> ()) {
    //        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
    ////            print(placemarks?.first ?? "BLANK")
    ////            print(placemarks?.first?.country)
    ////            print(placemarks?.first?.subLocality)
    ////            print(placemarks?.first?.locality)
    ////            print(placemarks?.first?.location)
    ////            print(placemarks?.first?.region)
    ////            print(placemarks?.first?.administrativeArea)
    ////            print(placemarks?.first?.areasOfInterest)
    ////            print(placemarks?.first?.name)
    ////            print(placemarks?.first?.subThoroughfare)
    ////            print(placemarks?.first?.thoroughfare)
    //            if let error = error {
    //                print(error)
    //            }
    //            else if let city = placemarks?.first?.locality {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.subLocality
    //            {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.administrativeArea {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.name {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.thoroughfare {
    //                completion(city)
    //            }
    //            else if let city = placemarks?.first?.subThoroughfare {
    //                completion(city)
    //            }
    //        }
    //    }
    
}



    
extension AddMusicVC{
    
    //MARK: UICollectionViewDataSource
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        switch collectionView {
        case CollectionView_Images:
            return maxNumImages
        case CollectionView_Video:
            return 1
        case CollectionView_Audio:
            return arrAudios.count+17
        default:
           return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case CollectionView_Images:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KImagesCollectionCell, for: indexPath as IndexPath) as? ImagesCollectionCell
            {
                cell.delegate = self
                if(arrImages.count>indexPath.row)
                {
                    cell.LoadData(Data: arrImages, index: indexPath)
                }
                else
                {
                    cell.LoadBlankData(index: indexPath)
                }
                return cell
            }
        case CollectionView_Video:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KvideosCollectionCell, for: indexPath as IndexPath) as? VideosCollectionCell
            {
                cell.delegate = self
                if(arrVideos.count>indexPath.row)
                {
                    cell.LoadData(Data: arrVideos, index: indexPath)
                }
                else
                {
                    cell.LoadBlankData(index: indexPath)
                }
                return cell
            }
        case CollectionView_Audio:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KmusicCollectionCell, for: indexPath as IndexPath) as? MusicCollectionCell
            {
                cell.delegate = self
                if(arrAudios.count>indexPath.row)
                {
                    cell.LoadData(Data: arrAudios, index: indexPath)
                }
                else
                {
                    cell.LoadBlankData(index: indexPath)
                }
                return cell
            }
        default:
            print("nil")
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 0
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        if(collectionView == CollectionView_Audio)
        {
            //print(collectionView.bounds.height)
            if(arrAudios.count>0)
            {
            leftArrowAudioCenterConstraints.constant = -25
            AudioCollection_heightConstraints.constant = 50

            return CGSize(width: dim, height: dim+50)
            }
            AudioCollection_heightConstraints.constant = 0
            return CGSize(width: dim, height: dim)
        }
        ImagesCollection_heightConstarints.constant = dim
        return CGSize(width: dim, height: dim)
    }
    
//    func configureCell(cell: UICollectionViewCell, forItemAtIndexPath: NSIndexPath)
//        {
//        cell.backgroundColor = UIColor.black
//        }
    
    //MARK: UICollectionViewDelegate
     func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    }
    
     func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
    }
    
 
}

//MARK: Music

extension AddMusicVC : MPMediaPickerControllerDelegate
{
    
    // MPMediaPickerController Delegate methods
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        self.dismiss(animated: true, completion: nil)
        //print("you picked: \(mediaItemCollection)")
        guard let mediaItem = mediaItemCollection.items.first else {
            NSLog("No item selected.")
            return
        }
        let mediaType  = mediaItem.mediaType
        //print(mediaType)
        let title = mediaItem.title
        //print(title ?? "")
        let albumTitle = mediaItem.albumTitle
        //print(albumTitle ?? "")
        let artist = mediaItem.artist
        //print(artist ?? "")
        let assetURL = mediaItem.assetURL
        //print(assetURL ?? "")
        let persistentID = mediaItem.persistentID
        //print(persistentID)
        
        self.AlertMessageWithOkCancelAction(titleStr: KBIDJONES, messageStr: "Do you want into Radio", Target: self) { (str) in
            var Radio:Bool?
            if (str == "Yes")
            {
                Radio = true
            }
            else
            {
                Radio = false
            }
            
            self.exportiTunesSong(assetURL: assetURL!, completionHandler: { (filePath) in
                if (filePath != nil) {
                    //print("FilePath : \(String(describing: filePath))")
                    if let Data = AudioData(Title: title,AudioURL: filePath, AlbumTitle: albumTitle, Artist: artist,AssetUrl:assetURL,IsRadio:Radio)
                    {
                        self.arrAudios.insert(Data, at: 0)
                    }
                    
                    if(self.arrAudios.count>0)
                    {
                        self.AudioCollection_heightConstraints.constant = 50
                    }
                    else
                    {
                        self.AudioCollection_heightConstraints.constant = 0
                    }
                    DispatchQueue.main.async {
                        self.CollectionView_Audio.reloadData()
                    }
                }
            })
        }
       
        
       
    }
    
    // MARK:- Convert itunes song to avsset in temp location Methode.
    func exportiTunesSong(assetURL: URL, completionHandler: @escaping (_ fileURL: URL?) -> ()) {
        MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
        let songAsset = AVURLAsset(url: assetURL, options: nil)
        
        let exporter = AVAssetExportSession(asset: songAsset, presetName: AVAssetExportPresetAppleM4A)
        
        exporter?.outputFileType = AVFileType.m4a
        
        exporter?.metadata = songAsset.commonMetadata
        
//        let filename = AVMetadataItem.metadataItems(from: songAsset.commonMetadata, withKey: AVMetadataKey.commonKeyTitle, keySpace: AVMetadataKeySpace.common)
//
        
        let songName = "\(Double(Date.timeIntervalSinceReferenceDate * 1000))"

//        if filename.count > 0  {
//            songName = ((filename[0] as AVMetadataItem).value?.copy(with: nil) as? String)!
//        }
        
        //Export mediaItem to temp directory
        let exportURL = URL(fileURLWithPath: NSTemporaryDirectory())
            .appendingPathComponent(songName)
            .appendingPathExtension("m4a")
        
        exporter?.outputURL = exportURL
        //print("Export URL : \(exportURL)")
        
        // do the export
        // (completion handler block omitted)
        
        exporter?.exportAsynchronously(completionHandler: {
            let exportStatus = exporter!.status
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            }
            switch (exportStatus) {
            case .failed:
                let exportError = exporter?.error
                //print("AVAssetExportSessionStatusFailed: \(String(describing: exportError))")
                completionHandler(nil)
                break
            case .completed:
                //print("AVAssetExportSessionStatusCompleted")
                completionHandler(exportURL)
                break
            case .unknown:
                //print("AVAssetExportSessionStatusUnknown")
                break
            case .exporting:
                //print("AVAssetExportSessionStatusExporting")
                break
            case .cancelled:
                //print("AVAssetExportSessionStatusCancelled")
                completionHandler(nil)
                break
            case .waiting:
                //print("AVAssetExportSessionStatusWaiting")
                break
            }
        }
        
        )
    }

}

extension AddMusicVC: UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dic = arrPicker[row]
        var title = ""
        switch pickerType
        {
        case .GenrePicker?:
        title = (dic as! GenreData).name
        case .BidTypePicker?:
        title = (dic as! BidTypeData).name
        case .none:
            print("none")
        }
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }
}

