//
//  MerchPickUp.swift
//  BidJones
//
//  Created by Rakesh Kumar on 8/6/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class MerchPickUp: UIViewController,UIScrollViewDelegate {
    private lazy var arrList = [MerchPickupData]()
    var count = 0
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isLoading:Bool?
    @IBOutlet var tblMerch: UITableView!
    
    @IBOutlet var lblNoItem: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = false
        dataLoaded = false
        isScrolling = false
        if #available(iOS 11.0, *) {
            tblMerch.contentInsetAdjustmentBehavior = .automatic
        } else {
            // Fallback on earlier versions
        }
        lblNoItem.isHidden = true
        tblMerch.estimatedRowHeight = 1000
        tblMerch.rowHeight = UITableViewAutomaticDimension
        self.tblMerch.tableFooterView = UIView()

        
        self.automaticallyAdjustsScrollViewInsets = false
        
       // tblMerch.separatorColor = nil
        tblMerch.separatorColor = UIColor.clear
        GetItemsLIst()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - IBAction
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            print(arrList.count)
            print(arrList.count)
            // print(isScrolling)
            //  print(isLoading)
            //print(dataLoaded)
            
            
            if (arrList.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
            {
                print("222222222222")
                isScrolling = true
                print("reach bottom")
                isLoading = true
                self.count = self.count+KPaginationcount
                GetItemsLIst()
            }
//            else if isDeleted!
//            {
//                isDeleted = false
//                print("222222222222")
//                isScrolling = true
//                print("reach bottom")
//                isLoading = true
//                GetSearchData()
//            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    //MARK: - Other functions
    func GetItemsLIst()
    {
        if let userID = UserDefaults.standard.getUserID()
        {
            
            let receivedCount = 0
            //KsellerbidRecords
            
            let urlStr = "/seller/MergePickup"
                //+ "\(userID)" +  "/" + "\(KPaginationcount)" +  "/" + "\(count)/"
                //+ "received" + "/" + "\(receivedCount)"
            print(urlStr)
            dataLoaded = false
            //parm[Kcategory_id] = KServicesType
            
            let id = UserDefaults.standard
            
            let loginUserID = id.getUserID()
            
            
            let parm = ["seller_id" : loginUserID! , "category_id": 0 , "offset" : 0 , "limit" : 10] as [String : Any]
            
            
            Bids.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
                
                print("here is response : \(Response)")
                print(Response.count)
                if Response.count>0
                {
                    if(self.count == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrList.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrList.insert(item, at: self.arrList.count)
                            }
                        }
                        self.tblMerch.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrList.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrList.insert(item, at: self.arrList.count)
                            }
                        }
                        self.tblMerch.reloadData()
                        print(self.arrList)
                    }
                    print(self.count)
                    self.lblNoItem.isHidden = true
                    self.tblMerch.isHidden = false
                    
                }
                else
                {
                    if(self.count != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.count == 0 || self.arrList.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblMerch.isHidden = true
                    }
                }
                self.isLoading = false
                //            if self.isDeleted!
                //            {
                //                self.isDeleted = false
                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                //            }
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetItemsLIst()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.count == 0 || self.arrList.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblMerch.isHidden = true
                            
                        }
                    }
                    else
                    {
                        if (self.count == 0 || self.arrList.count == 0)
                        {
                            self.arrList.removeAll()
                            self.tblMerch.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblMerch.isHidden = true
                            
                        }
                        
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
            
            
//
//            Bids.sharedManager.GetRecieveBidsApi(url: urlStr,Target: self, completionResponse: { (Response) in
//
//                print("here is response : \(Response)")
//                print(Response.count)
//                if Response.count>0
//                {
//                    if(self.count == 0)
//                    {
//                        // self.arrServiceSell = Response
//                        for item in Response
//                        {
//                            let oldIds = self.arrList.map { $0.id } as? [Int]
//                            print(oldIds as Any)
//                            if !((oldIds?.contains(item.id!))!)
//                            {
//                                self.arrList.insert(item, at: self.arrList.count)
//                            }
//                        }
//                        self.tblMerch.reloadData()
//                    }
//                    else
//                    {
//                        for item in Response
//                        {
//                            let oldIds = self.arrList.map { $0.id } as? [Int]
//                            print(oldIds as Any)
//                            if !((oldIds?.contains(item.id!))!)
//                            {
//                                self.arrList.insert(item, at: self.arrList.count)
//                            }
//                        }
//                        self.tblMerch.reloadData()
//                        print(self.arrList)
//                    }
//                    print(self.count)
//                    self.lblNoItem.isHidden = true
//                    self.tblMerch.isHidden = false
//
//                }
//                else
//                {
//                    if(self.count != 0)
//                    {
//                        print("abcdefgh")
//                        self.dataLoaded = true
//                    }
//                    if (self.count == 0 || self.arrList.count == 0)
//                    {
//                        self.lblNoItem.isHidden = false
//                        self.tblMerch.isHidden = true
//                    }
//                }
//                self.isLoading = false
//                //            if self.isDeleted!
//                //            {
//                //                self.isDeleted = false
//                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
//                //            }
//            }
//                , completionnilResponse: { (Response) in
//                    //print(Response)
//                    let statusCode = Response[Kstatus] as! Int
//                    if statusCode == 500
//                    {
//                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                            //print(resonse)
//                            self.GetItemsLIst()
//                        })
//                    }
//                    else if statusCode == 201
//                    {
//                        self.dataLoaded = true
//                        if (self.count == 0 || self.arrList.count == 0)
//                        {
//                            self.lblNoItem.isHidden = false
//                            self.tblMerch.isHidden = true
//
//                        }
//                    }
//                    else
//                    {
//                        if (self.count == 0 || self.arrList.count == 0)
//                        {
//                            self.arrList.removeAll()
//                            self.tblMerch.reloadData()
//                            self.lblNoItem.isHidden = false
//                            self.tblMerch.isHidden = true
//
//                        }
//
//                    }
//                    self.isLoading = false
//
//            }, completionError: { (error) in
//                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
//                self.isLoading = false
//            },networkError: {(error) in
//                self.showAlertMessage(titleStr: KMessage, messageStr: error)
//                self.isLoading = false
//            })
        }
    }

}
extension MerchPickUp : UITableViewDelegate {
    
    
        
    
        
        
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
        
        self.tblMerch.deselectRow(at: indexPath, animated: true)
        
        
     //   self.showAlertMessage(titleStr: KMessage, messageStr: KpaymenthasBeenalreadyDone)
        let data = arrList[indexPath.row]
       // Index = indexPath.row
        // let itemId = data.id
        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .ChangePrice, Data: data)
    }
}

extension MerchPickUp : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        return arrList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ProductTableCell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell) as! ProductTableCell
        // cell.delegate = self
       // cell.LoadData(dic:arrList[indexPath.row], type: .Book)
        
        cell.LoadDataPickup(dic:arrList , indexPath : indexPath)
        cell.tag = indexPath.row
        cell.selectionStyle = .none
        
        cell.backgroundColor = UIColor.clear
        cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        
        return cell
    }
}



