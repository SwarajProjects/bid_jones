
    import UIKit
    import MapKit


protocol LocationProtocol {
    
    func Location(lat : String ,lng  : String , address : String)
}

    class SearchAddressVC: UIViewController {
        
        var locationDelegate:LocationProtocol?
        
        @IBOutlet var textfieldAddress: UISearchBar!
        //@IBOutlet weak var textfieldAddress: UITextField!
        @IBOutlet weak var tableviewSearch: UITableView!
        @IBOutlet weak var constraintSearchIconWidth: NSLayoutConstraint!
        @IBOutlet weak var searchView: UIView!
        @IBOutlet weak var mapview: MKMapView!
        var autocompleteResults :[GApiResponse.Autocomplete] = []
        var locationManager = CLLocationManager()
        var chnagePlace = true;
        var lattitudeStr = "";
        var longitudeStr = "";
      //  var Address = true;

        
        @IBAction func searchButtonPressed(_ sender: Any) {
            textfieldAddress.becomeFirstResponder()
        }
        
        
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            
            textfieldAddress.delegate = self;
            self.locationManager.delegate = self
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 10
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            mapview.showsUserLocation = false;
        
            
            
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
        }
        
        @IBAction func DoneAction(_ sender: Any) {
            if(textfieldAddress.text != "")
            {
                locationDelegate?.Location(lat: lattitudeStr, lng: longitudeStr, address: textfieldAddress.text ?? "")
                self.navigationController?.popViewController(animated: true)

                //self.navigationController?.dismiss(animated: true, completion: nil)
            }
           
        }
        @IBAction func BackAction(_ sender: Any)
        {
           self.navigationController?.popViewController(animated: true)
            //self.navigationController?.dismiss(animated: true, completion: nil)
        }
        func showResults(string:String){
            var input = GInput()
            input.keyword = string
            GoogleApi.shared.callApi(input: input) { (response) in
                if response.isValidFor(.autocomplete) {
                    DispatchQueue.main.async {
                        self.searchView.isHidden = false
                        self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                        self.tableviewSearch.reloadData()
                    }
                } else { print(response.error ?? "ERROR") }
            }
        }
        func hideResults(){
            searchView.isHidden = true
            autocompleteResults.removeAll()
            tableviewSearch.reloadData()
        }
    }
    
    
    extension SearchAddressVC: CLLocationManagerDelegate {
        
        
        //Location Manager delegates
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            let location = locations.last
            if let lat = location?.coordinate.latitude, let lng = location?.coordinate.longitude{
                let center  = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                self.mapview.setRegion(region, animated: true)
            }
            self.locationManager.stopUpdatingLocation()
            
        }
    }
    
    extension SearchAddressVC : UISearchBarDelegate
    {
        
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            //searchActive = true;
        }
        
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            // searchActive = false;
        }
        
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            //searchActive = false;
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            textfieldAddress.resignFirstResponder()
            showResults(string:searchBar.text ?? "")
            
            // searchActive = false;
        }
        
        func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
        {
            let Text = searchBar.text! as NSString
            let fullText = Text.replacingCharacters(in: range, with: text)
            if fullText.count > 2 {
                showResults(string:fullText)
            }else{
                hideResults()
            }
            return true
            
        }
        // called before
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        }
        
    }
    
    extension SearchAddressVC : UITextFieldDelegate {
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            hideResults() ; return true
        }
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let text = textField.text! as NSString
            let fullText = text.replacingCharacters(in: range, with: string)
            if fullText.count > 2 {
                showResults(string:fullText)
            }else{
                hideResults()
            }
            return true
        }
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            constraintSearchIconWidth.constant = 0.0 ; return true
        }
        func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            constraintSearchIconWidth.constant = 38.0 ; return true
        }
    }
    extension SearchAddressVC : MKMapViewDelegate {
        func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
            var input = GInput()
            let destination = GLocation.init(latitude: mapview.region.center.latitude, longitude: mapview.region.center.longitude)
            input.destinationCoordinate = destination
            if let lati = destination.latitude , let long = destination.longitude {
            self.lattitudeStr = "\(lati)"
            self.longitudeStr = "\(long)"
            GoogleApi.shared.callApi(.reverseGeo , input: input) { (response) in
                if let places = response.data as? [GApiResponse.ReverseGio], response.isValidFor(.reverseGeo) {
                    DispatchQueue.main.async {
                        if (self.chnagePlace)
                        {
                            self.textfieldAddress.text = places.first?.formattedAddress
                        }
                        else
                        {
                            self.chnagePlace = true;
                            
                        }
                    }
                } else { print(response.error ?? "ERROR") }
            }
          }
        }
    }
    extension SearchAddressVC : UITableViewDataSource,UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return autocompleteResults.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultCell")
            let label = cell?.viewWithTag(1) as! UILabel
            label.text = autocompleteResults[indexPath.row].formattedAddress
            return cell!
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            chnagePlace = false;
            textfieldAddress.text = autocompleteResults[indexPath.row].formattedAddress
            textfieldAddress.resignFirstResponder()
            var input = GInput()
            input.keyword = autocompleteResults[indexPath.row].placeId
            GoogleApi.shared.callApi(.placeInformation,input: input) { (response) in
                if let place =  response.data as? GApiResponse.PlaceInfo, response.isValidFor(.placeInformation) {
                    DispatchQueue.main.async {
                        self.searchView.isHidden = true
                        if let lat = place.latitude, let lng = place.longitude{
                            self.lattitudeStr = "\(lat)"
                            self.longitudeStr = "\(lng)"
                            let center  = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                            self.mapview.setRegion(region, animated: true)
                        }
                        self.tableviewSearch.reloadData()
                    }
                } else { print(response.error ?? "ERROR") }
            }
        }
    }



