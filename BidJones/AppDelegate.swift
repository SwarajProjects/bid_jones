
//  AppDelegate.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/8/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
//import Fabric
//import Crashlytics
import AWSCore
import GoogleMaps


import GooglePlaces
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import TwitterKit
import Stripe
import UserNotifications
//import Flurry_iOS_SDK
//import Firebase
import FirebaseFirestore
import Firebase
import Sentry


var notificationData : [String: Any]?
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate{
    
    var window: UIWindow?
    var isNotification = false
    var userId = Int()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        do {
            Client.shared = try Client(dsn: "https://8d9384404d004a9094c55a7e3fdad37a@sentry.io/5168584")
            try Client.shared?.startCrashHandler()
        } catch let error {
            print("\(error)")
        }
        
        
        //        let builder = FlurrySessionBuilder.init()
        //        .withAppVersion("1.0")
        //        .withLogLevel(FlurryLogLevelAll)
        //        .withCrashReporting(true)
        //        .withSessionContinueSeconds(10)
        //
        //        Flurry.startSession("F5MD2XCWJCDKWT24KGTV", with: builder)
        //              /// Flurry.logEvent("App started", timed: true)
        //startSession(“F5MD2XCWJCDKWT24KGTV”)
        
        //flu
        UIApplication.shared.applicationIconBadgeNumber = 0
        FirebaseApp.configure()
        _ = Firestore.firestore()
        //        let settings1 = db.settings
        //        settings1.areTimestampsInSnapshotsEnabled = true
        //        db.settings = settings1
        
        //let timestamp: Timestamp = documentSnapshot.get("created_at") as! Timestamp
        let date = FieldValue.serverTimestamp()
        print("yes my date : \(date)")
        //  Fabric.with([Crashlytics.self])
        sleep(2)
        GMSServices.provideAPIKey(KGoogleApiKey)
        GMSPlacesClient.provideAPIKey(KGoogleApiKey)
        GoogleApi.shared.initialiseWithKey(KGoogleApiKey)
        
        KCommonFunctions.CameraGallaryPrmission()
        // KCommonFunctions.LocationAccess()
        KCommonFunctions.AmazonAuthntication()
        Location.sharedInstance.InitilizeGPS()
        let isLoggedIn : Bool =  UserDefaults.standard.isLoggedIn()
        if let userID = UserDefaults.standard.getUserID() {
            userId = userID
        }
        
        if(isLoggedIn)
        {
            KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
        }
        
        if let ProductDetail = UserDefaults.standard.getRatingData()
        {
            print(ProductDetail)
            KCommonFunctions.SetRootViewController(rootVC:.RatingNavigation)
        }
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        registerForPushNotifications()
        
        /*if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        // iOS 7 support
        else
        {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }*/
        
        TWTRTwitter.sharedInstance().start(withConsumerKey:"r3IwGtlTKZkrXGpPwYSVArizy", consumerSecret:"DVkKvnEbhiWVPbCF2jAq5L0a7opkzTVkO6ajVfpC6new9CWb2Y")
        STPPaymentConfiguration.shared.publishableKey = "pk_test_8qFDtezJbEFFBjestSU6TaFO00HPPmu2Tx"
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    //MARK:- Push Configuration
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    //MARK:- Push Delegates
    //    func application(_ application: UIApplication,
    //                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //        let tokenParts = deviceToken.map { data -> String in
    //            return String(format: "%02.2hhx", data)
    //        }
    //        let token = tokenParts.joined()
    //        print("Device Token: \(token)")
    //        UserDefaults.standard.setDeviceTokken(value: token)
    //    }
    //
    //    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    //        let characterSet = CharacterSet(charactersIn: "<>")
    //        let deviceTokenString = deviceToken.description.trimmingCharacters(in: characterSet).replacingOccurrences(of: " ", with: "");
    //        print(deviceTokenString)
    //    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        UserDefaults.standard.setDeviceTokken(value: token)
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
        UserDefaults.standard.setDeviceTokken(value: "4354656768")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        print("Push notification received: \(userInfo)")
        var value1 = userInfo
        //as! NSDictionary
        let nsvalue = value1["aps"] as! [String : Any]
        
        print("you get :\(nsvalue)")
        
        let msg = nsvalue["alert"] as Any
        
        print(msg)
        
        if let chatnotify = value1["notifychatmessage"] as? Int {
            
            print("my chatnotify : \(chatnotify)")
            if chatnotify == 1 {
                print("my data : \(value1)")
                let myData = value1 as? [String : Any]
                notificationData = myData!
            }
        }
        
        if application.applicationState == .active {
            var anotherUserID : String?
            if let data = notificationData {
                if let data1 = data as? [String : Any]
                {
                    let newData = data1["data"] as? [String : Any]
                    let item = newData!["item"] as? [[String : Any]]
                    let anotheruser_id = item![0]["seller_id"] as? Int
                    anotherUserID = "\(anotheruser_id!)"
                }
            }
            
            if let againstUserID = anotherUserID_global , let anotherUser = anotherUserID  {
                print("you against:\(againstUserID)")
                
                if anotherUser == againstUserID {
                    print("These are current User for chatting :\(anotherUserID!) and \(anotherUserID_global!)")
                }
                else
                {
                    Notificationview.sharedInstance.createNotificationview(win: self.window!)
                    Notificationview.sharedInstance.showNotificationView(userInfo as NSDictionary)
                    notificationData = nil
                }
            }
            else{
                Notificationview.sharedInstance.createNotificationview(win: self.window!)
                Notificationview.sharedInstance.showNotificationView(userInfo as NSDictionary)
                notificationData = nil
            }
        }
        
        if isNotification == true {
            //    print("my stored data : \(notificationData!)")
            if let data = notificationData {
                
                let data1 = data
                //as? [String : Any]
                
                if let chatnotify = data1["notifychatmessage"] as? Int {
                    
                    print("my chatnotify : \(chatnotify)")
                    
                    if chatnotify == 1
                    {
                        isNotification = false
                        let vc = StoryBoards.Home.instantiateViewController(withIdentifier: "ChatRoomNavigation")
                        UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                }
            }
        }
        
        guard let popupStatus = value1["popup_status"] as? String else {
            return
        }
        
        if (popupStatus == "1") {
            goToMySearchAlertVC()
        } else if (popupStatus == "2" || popupStatus == "3" || popupStatus == "4" || popupStatus == "9" || popupStatus == "11" || popupStatus == "12" || popupStatus == "19") {
            goToBidsVC(isSentSelected: true)
        } else if (popupStatus == "5") {
            goToFollowingVC()
        } else if (popupStatus == "6") {
        } else if (popupStatus == "7" || popupStatus == "8" || popupStatus == "10" || popupStatus == "13" || popupStatus == "14" || popupStatus == "16") {
            goToBidsVC(isSentSelected: false)
        } else if (popupStatus == "15") {
            goToMyServicesVC()
        } else if (popupStatus == "17") {
            goToTalkVC()
        } else if (popupStatus == "18") {
            goToMusicListVC()
        }
    }
    
    func goToMySearchAlertVC() {
        let vc = StoryBoards.Home.instantiateViewController(withIdentifier: kmysearchAlertVC)
        let nav = self.window?.rootViewController as? UINavigationController
        nav?.pushViewController(vc, animated: true)
    }
    
    func goToBidsVC(isSentSelected: Bool) {
        let vc = StoryBoards.Home.instantiateViewController(withIdentifier: "bidsVC") as! BidsVC
        vc.isSentSelected = isSentSelected
        let nav = self.window?.rootViewController as? UINavigationController
        nav?.pushViewController(vc, animated: true)
    }
    
    func goToFollowingVC() {
        let vc = StoryBoards.Seller.instantiateViewController(withIdentifier: "FollowerVC") as! FollowerFollowingVC
        vc.followerID = userId
        vc.followtype = "follower"
        let nav = self.window?.rootViewController as? UINavigationController
        nav?.pushViewController(vc, animated: true)
    }
    
    func goToMyServicesVC() {
        let vc = StoryBoards.Seller.instantiateViewController(withIdentifier: KmyServiceSellListVC)
        let nav = self.window?.rootViewController as? UINavigationController
        nav?.pushViewController(vc, animated: true)
    }
    
    func goToTalkVC() {
        let vc = StoryBoards.Home.instantiateViewController(withIdentifier: kTalkVC)
        let nav = self.window?.rootViewController as? UINavigationController
        nav?.pushViewController(vc, animated: true)
    }
    
    func goToMusicListVC() {
        let vc = StoryBoards.Home.instantiateViewController(withIdentifier: "MusicListVC")
        let nav = self.window?.rootViewController as? UINavigationController
        nav?.pushViewController(vc, animated: true)
    }
    
    func goToRatingVC() {
        let vc = StoryBoards.Buyer.instantiateViewController(withIdentifier: KRatingListVC) as! RatingListVC
        vc.userId = userId
        let nav = self.window?.rootViewController as? UINavigationController
        nav?.pushViewController(vc, animated: true)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        print("here your notification data: \(userInfo)")
        if application.applicationState == .active
        {
            Notificationview.sharedInstance.createNotificationview(win: self.window!)
            Notificationview.sharedInstance.showNotificationView(userInfo as NSDictionary)
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("didReceive")
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("willPresent")
        completionHandler([.badge, .alert, .sound])
    }
    
    //MARK:- App Lifecycle
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        //  let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(app,open: url as URL ,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        let twitterDidHandle = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        
        if let scheme = url.scheme,
           scheme.localizedCaseInsensitiveCompare("myapp://") == .orderedSame,
           let view = url.host {
            
            var parameters: [String: String] = [:]
            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                parameters[$0.name] = $0.value
            }
            return true
            //redirect(to: view, with: parameters)
        }
        
        return facebookDidHandle || twitterDidHandle
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application,open: url as URL ,sourceApplication: sourceApplication,annotation: annotation)
        
    }
    
    //   func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    //   {
    //    let shouldOpen :Bool = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    //    return shouldOpen
    //
    //   }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //  isNotification = true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
