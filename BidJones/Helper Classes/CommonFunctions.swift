//
//  CommonFunctions.swift
//  ArchitectureDemo
//
//  Created by Rakesh Kumar on 2/15/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AWSS3
import AWSCore

@available(iOS 10.0, *)
class CommonFunctions
{
    static let sharedInstance = CommonFunctions()
    private init()
    {
    }
    
    //MARK: - Navigation functions
    
    func PushToContrller(from Target : UIViewController,  ToController:ViewControllers, Data:Any?)
     {
    
        print(ToController.viewcontroller ?? "")
         print(DetailVC.self)
        
        if let obj =  ToController.viewcontroller as? FreeDetailVC
        {
            if let item = Data as? ItemListData
            {
                obj.product = item
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? WebViewVC
        {
            if let obj1 = Target as? VerifyOTPVC
            {
                obj.delegate = obj1
            }
            if let obj2 = Target as? TradeVC
            {
                obj.delegate = obj2
            }
            if let obj3 = Target as? FreeVC
            {
                obj.delegate = obj3
            }
            if let obj4 = Target as? FreeDetailVC
            {
                obj.delegate = obj4
            }
            if let obj5 = Target as? MyBookSListVC
            {
                obj.delegate = obj5
            }
            if let obj6 = Target as? MySongListVC
            {
                obj.delegate = obj6
            }
            if let obj6 = Target as? MyServiceSellListVC
            {
                obj.delegate = obj6
            }
            if let obj6 = Target as? MyStuffListVC
            {
                obj.delegate = obj6
            }
            if let obj6 = Target as? MYGraphicLIstVC
            {
                obj.delegate = obj6
            }
            if let obj6 = Target as? MyVideoListVC
            {
                obj.delegate = obj6
            }
            if let obj6 = Target as? DetailVC
            {
                obj.delegate = obj6
            }
            
            if let ID = Data as? Int
            {
                obj.UserID = ID
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        
        
        
      if let obj =  ToController.viewcontroller as? TradeDetailVC
      {
        if let item = Data as? TradeRequestListData
        {
          obj.tradeDetail = item
        }
        Target.navigationController?.pushViewController(obj, animated: true)
        return
      }
      if let obj =  ToController.viewcontroller as? AddTradeProductVC
      {
        if let itemID = Data as? Int
        {
          obj.itemId = itemID
          
        }
        if let itemID = Data as? ProductData
        {
          obj.productDetail = itemID
          
        }
        Target.navigationController?.pushViewController(obj, animated: true)
        return
      }
        if let obj =  ToController.viewcontroller as? AddFreeProductVC
        {
            if let itemID = Data as? Int
            {
                obj.itemId = itemID
                
            }
            if let itemID = Data as? ProductData
            {
                obj.productDetail = itemID
                
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? ChangePriceVC
        {
            if let data = Data as? MerchPickupData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? SellerInfoVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? RatingListVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? PaymentRequestSellerVC
        {
            if let data = Data as? ItemListData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? PaymentRequestBuyerVC
        {
            if let data = Data as? ItemListData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? AddGraphicsVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? AddMusicVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? AddBookVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? AddSellServiceVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? AddListingVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? AddVideoVC
        {
            if let data = Data as? ProductData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        
        if let obj =  ToController.viewcontroller as? RatingVC
        {
            if let data = Data as? ItemListData
            {
                obj.productDetail = data
            }
          if let data = Data as? TradeRequestListData
          {
            obj.tradeProductDetail = data
          }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
//      if let obj =  ToController.viewcontroller as? RatingVC
//      {
//       
//        Target.navigationController?.pushViewController(obj, animated: true)
//        return
//      }
        if let obj =  ToController.viewcontroller as? PaymentVC
        {
            if let data = Data as? ItemListData
            {
                obj.productDetail = data
            }
            if let data = Data as? TradeRequestListData
            {
                obj.TradeProductDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? InVoiceVC
        {
            if let data = Data as? ItemListData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? CheckOutVC
        {
            if let data = Data as? ItemListData
            {
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
           if let obj =  ToController.viewcontroller as? DetailVC
           {
            
            
            
            if let obj1 = Target as? SearchListVC
            {
                obj.delegate = obj1 
            }
            if let obj2 = Target as? FreeVC
            {
                obj.removedelegate = obj2
            }
            if let obj2 = Target as? FreeSearchAlertsVC
            {
                obj.removedelegate = obj2
            }
            if let obj3 = Target as? FreeRequestsVC
            {
                obj.removedelegate = obj3
            }
            
            if let data = Data as? ItemListData{
              obj.product = data
               if let objBids = Target as? BidsVC{
               obj.isItemList = true
               }
            }
            if let data = Data as? TradeRequestListData
            {
              obj.tradeProduct = data
            }
            
            Target.navigationController?.pushViewController(obj, animated: true)
            return
           }
        if let obj =  ToController.viewcontroller as? SearchListVC
        {
            if let data = Data as? SearchData
            {
                print(data)
                obj.dataSearch = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? AudioPlayerVC
        {
            if let data = Data as? ProductData
            {
                print(data)
                obj.productDetail = data
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        if let obj =  ToController.viewcontroller as? PdfVC
        {
            if let url = Data as? URL
            {
                print(url)
                obj.pdfUrl = url
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
//        if let obj =  ToController.viewcontroller as? ConditionsVC
//        {
//            if let url = Data as? String
//            {
//                print(url)
//                obj.openUrl = url
//               // obj.nav_Title = title
//            }
//            Target.navigationController?.pushViewController(obj, animated: true)
//            return
//        }
        if let obj = ToController.viewcontroller as? WebPageViewController{
            if let data = Data{
                let dictData = data as? [String:Any]
                
                if let url = dictData?["url"] as? String{
                    obj.webUrl = url
                }
                
                if let title = dictData?["title"] as? String{
                    obj.title = title
                }
            }
            Target.navigationController?.pushViewController(obj, animated: true)
            return
        }
        


        Target.navigationController?.pushViewController(ToController.viewcontroller!, animated: true)
    }
    
    func SetRootViewController(rootVC : RootViewControllers)
    {
     KappDelegate.window?.rootViewController = rootVC.rootViewcontroller
    }
    
    func popTocontroller(from Target : UIViewController)
    {
        Target.navigationController?.popViewController(animated: true)
    }
    func PresentTocontroller(from Target : UIViewController,  ToController:ViewControllers)
    {
       Target.present(ToController.viewcontroller!, animated: true, completion: nil)
    }
    
    //MARK: - Amazon permission
    func AmazonAuthntication() {
        //        // for AWS configration
        //        let pooId = Bucket.bucketInfo.poolId
        //        print(pooId)
        //        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId:pooId)
        //        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
        //        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //  ACCESS_KEY = AKIAJJNDTANX7JZ5NFPA
        //  SECRET_KEY = L1jzpBfqzIUtdJt8ktRP6x5MZHM3Cx3v
        // let accessKey = "AKIAIQIOMV2DMEAU2XXA"
       //   let secretKey = "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"
        let accessKey = KAWSaccessKey
        let secretKey = KAWSsecretKey
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }

    //MARK: - Validation functions

    func checkTextSufficientComplexity( text : String) -> Bool{
        
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        print("\(numberresult)")
        
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        
        let specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")
        
        return capitalresult || numberresult || specialresult
        
    }
    
    //MARK: -    Check Location
    func LocationAccess() {
        
    if CLLocationManager.locationServicesEnabled() {
    switch CLLocationManager.authorizationStatus() {
    case .notDetermined, .restricted, .denied:
    print("No access")
    Location.sharedInstance.InitilizeGPS()

    case .authorizedAlways, .authorizedWhenInUse:
    print("Access")
    }
    } else {
    print("Location services are not enabled")
    }
        
    }
    
   
    //MARK: - Gallary permission Functions
    
    func CameraGallaryPrmission() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                //access granted
            } else {
            }
        }
        
        //Photos
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                } else {}
            })
        }
    }
    
    func SessionExpired(Target : UIViewController, completionResponse:  @escaping (String) -> Void) {
        let urlStr = KRefreshTokkenApi
        var parm = [String : Any]()
        if let refreshTokken = UserDefaults.standard.getRefreshTokken()
        {
            parm["refresh_token"] = refreshTokken
        }
        print("Refersh Tokken parm: \(parm)")
        RefreshTokkenService.sharedmanagerRfrshTkn.PostApi(url: urlStr, parameter: parm, Target: Target, completionResponse: { (Response) in
            print(Response)
            if let accessTokken  = Response[Kaccess_token]
            {
                UserDefaults.standard.setAccessTokken(value: accessTokken as! String)
                print(UserDefaults.standard.getAccessTokken() ?? "Blank")
               let timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { timer in
                    completionResponse("Success")
                }
            }
            if let refresh_token = Response[Krefresh_token]
            {
                UserDefaults.standard.setRefreshTokken(value: refresh_token as! String)
                print(UserDefaults.standard.getRefreshTokken() ?? "Blank")
            }
        }, completionnilResponse: { (Response) in
            print(Response)
            if let msg =  Response[Kmessage]
            {
            Target.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            Target.showAlertMessage(titleStr: KMessage, messageStr: KError)
        },networkError: {(error) in
            Target.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    
    //Set Attributed text for btn open link
    func setAttributedStringInBtnOpenLink(button: UIButton){
        let attributedStringProperties = [NSAttributedStringKey.foregroundColor : UIColor.blue,NSAttributedStringKey.underlineStyle : 1,NSAttributedString.Key.font: UIFont(name: "OpenSans", size: 14.0) ?? false] as [NSAttributedStringKey : Any]
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:"Would you like to secure your original work through blockchain technology?", attributes:attributedStringProperties)
        attributedString.append(buttonTitleStr)
        button.setAttributedTitle(attributedString, for: .normal)
        button.titleLabel!.lineBreakMode = NSLineBreakMode.byWordWrapping
    }
}


