//
//  Global.swift
//  E-Ticketing
//
//  Created by Harpreet Singh on 22/08/16.
//  Copyright © 2016 Beant Singh. All rights reserved.
//

import UIKit

/**
 :brief: Global is a singleton class which i have used to save all constants variables , methods etc.
 */

class Global: NSObject {
    
    static let sharedInstance = Global()
    var isFromSafari:Bool = false
    
    var camefromProfile : Bool = false //To check from where user navgated.
    
    var camefromNotification : Bool = false //To check from wether user received notification.
    
    var chronologicalOrder : Bool = true //To keep track of filter at friends event screen.
    
    var selectedProduct = "" // Used to save product id for using at different places.
    
    var array_Blocked_Friends: NSMutableArray = NSMutableArray()
    var array_Blocked_Friends_ids: NSMutableArray = NSMutableArray()
    var array_Friends: NSMutableArray = NSMutableArray()
    var array_Users: NSMutableArray = NSMutableArray()
    /**
     
     :brief: Structs are preferable if they are relatively small and copiable because copying is way safer than having multiple reference to the same instance as happens with classes. This is especially important when passing around a variable to many classes and/or in a multithreaded environment. If you can always send a copy of your variable to other places, you never have to worry about that other place changing the value of your variable underneath you.
     
     With Structs there is no need to worry about memory leaks or multiple threads racing to access/modify a single instance of a variable.
     
     */
    
    struct macros {
        
        
        
        //static let BASE_URL: String = "http://www.hotfuse.com/dev/mobile/df_follow/"
        //static let BASE_URL: String = "http://52.41.4.214/mobile/df_follow/"
        
        //http://templates2.seasiaconsulting.com:9041/Hotfuse
        
        
        static let BASE_URL1: String = "http://www.hotfuse.com/mobilenewmar/users/"
        static let BASE_URL_services: String = "http://www.hotfuse.com/mobilenewmar/services/"
        
        static let fetching_all_users: String = "fetching_all_users"
        
        static let fetching_events_listing: String = "fetching_events_listing"
        
        
        
        static let creating_events: String = "creating_events"
        
        static let chat_message_notification: String = "chat_message_notification"
        
//        static let BASE_URL1: String = "http://www.hotfuse.com/client/mobilenewmar/users/"
//        static let BASE_URL_services: String = "http://www.hotfuse.com/client/mobilenewmar/services/"
        
       
        
        //static let get_following_list: String = "get_following_list"
        static let expire_App_Api: String = "expire_app"
        static let add_post_Api: String = "addpost"
        static let user_data_Api: String = "user_data"
        static let change_password_Api: String = "changee_password"
        static let like_post_Api: String = "like"
        static let Unlike_post_Api: String = "unlike"
        
        static let send_comment_Api: String = "send_comment"
        static let getcomment_list_Api: String = "comment"
        
       //  http://templates2.seasiaconsulting.com:9041/Hotfuse/mobilenewmar/users/change_post_status
        
        static let delete_post_Api: String = "delete_post"
        static let changepost_status_Api: String = "change_post_status"
        
        static let get_following_list_Api: String = "get_following_list"
        static let get_followers_list_Api: String = "get_followers_list"
        static let follow_Unfollow_Api: String = "follow_unfollow"
        static let Search_user_Api: String = "users_search"
        static let Search_allTag_Api: String = "search_allTag"
        static let SearchTag_Api: String = "search_Tag"
        
        
        static let add_AudioPost_Api: String = "add_audio_data"//
        static let edit_AudioPost_Api: String = "update_audio_post"
        
        static let add_message_Api: String = "add_message"
        static let edit_message_Api: String = "edit_message"

        
        static let storeCalender_Api: String = "store_calendar"
        static let getDay_event_Api: String = "get_day_event"
        static let accept_rejectRequest_Api: String = "approvefollowrequest"

       static let get_other_followerslsist_Api: String = "get_other_followerslsist"
       static let get_other_followingsss_Api: String = "get_other_followingsss"
       static let searchdirect_Api: String = "searchdirect"
     
         static let LikeUnlike_comment_Api: String = "like_comment"
         static let Poll_commentAll_Api: String = "poll_comment_all"
         static let delete_poll_Api: String = "delete_poll"
         static let poll_vote_Api: String = "poll_vote"
         static let block_message_Api: String = "block_message"
        static let read_message_Api: String = "read_message"
         static let upvote_downvote_Api: String = "upvote_downvote"
        static let like_reply_comment_Api: String = "like_reply_comment"
        static let unlike_reply_comment_Api: String = "unlike_reply_comment"
        static let user_data: String = "user_data"
        
        static let display_single_event: String = "display_single_event"
        static let display_all_event: String = "display_allevent"
        static let edit_single_event: String = "edit_single_event"
        static let delete_event: String = "deleted_event"
        static let all_post: String = "all_post"
        static let ExploreWithPagination: String = "new_explore_withpagination"
        
        static let reply_comment: String = "reply_comment"
        static let user_post_data: String = "user_post_data"
        static let user_post_data_pagination: String = "user_post_data_pagination"
         //http://www.hotfuse.com/mobilenewmar/users/user_post_data_pagination
        
        static let update_user_data: String = "update_user_data"
        
        static let display_reply_comment: String = "display_reply_comment"
        static let change_premium: String = "change_premium"
        static let clear_newpostdata: String = "clear_newpostdata"
        static let twitter_login: String = "tw_login"
        static let facebook_login: String = "fb_login"

        //http://templates2.seasiaconsulting.com:9041/Hotfuse/mobilenewmar/services/unlike_reply_comment
      
        static let kNotificationSettings : String = "change_notification_status"
        static let kgetMakeProfilePrivate : String = "change_profile_status"
        
            
        static let kgetFollowersList : String = "getlistfollowrequest"
        static let kgetUserProfileData : String = "user_data"
        
         static let kdeleteEvent : String = "delete_event"
        
        static let kmobileOTP : String = "oldsendotp"
        
       //http://www.hotfuse.com/mobilenewmar/users/oldsendotp
        
        
        static let kfullname : String = "name"
        static let kfirstname : String = "firstname"
        static let klastname : String = "surname"
        static let kemail : String = "email"
        static let knotif_settings : String = "notif_settings"
        
        static let kurl : String = "picture_url"
        static let kuserid : String = "userid"
        static let kfriends : String = "friends"
        static let kaccount : String = "account"
        static let kbirthday : String = "birthday"
        static let kday : String = "day"
        static let kdate : String = "date"
        static let kmonth : String = "month"
        static let kyear : String = "year"
        static let kaddress : String = "address"
        static let kid1 : String = "id1"
        static let kfcmtoken : String = "fcmtoken"
        static let kwishlist : String = "wishlist"
        static let kusers : String = "users"
        static let k_shipping : String = "_shipping"
        static let kcity : String = "city"
        static let kcountry : String = "country"
        static let kextra : String = "extra"
        static let knpa : String = "npa"
        static let kphone : String = "phone"
        static let kstate : String = "state"
        static let kinternetC : String = "No internet connection"
        static let kskipit : String = "skipit"
        static let kgotit : String = "gotit"
        
        static let kconfig : String = "configs"
        static let kProduct_default : String = "default"
        static let kProduct_description : String = "description"
        static let kImage_Product : String = "image"
        static let kProduct_title : String = "title"
        
        static let kProduct_price : String = "price"
        static let kProduct_currency : String = "currency"
        static let kProduct_value : String = "value"
        static let kProduct_Already_Buy : String = "Product_Already_Buy"
        
        
        static let kKeyValue : String = "kKeyValue"
        static let kTo_Gift : String = "togift"
        static let kFriends : String = "friends"
        static let kBlocked_friends : String = "blocked_friends"
        
        static let kCheck : String = "check"
        
        //Branches
        static let kevents : String = "events"
        static let kevent_id : String = "event_id"
        
        
        //purchase history
        static let k_Product : String = "Product"
        static let k_User_Info : String = "userinfo"
        static let k_recipient_id : String = "recipient_id"
        static let k_buyer_id : String = "buyer_id"
        
        // Slide menu identifiers
        static let k_p_history : String = "nav_p_history"
        static let k_sowispace : String = "tab"
        static let k_profile : String = "nav_profile"
        static let k_nav_event : String = "nav_event"
        static let k_mysowi : String = "nav_mysowi"
        static let k_togift : String = "nav_togift"
        static let k_sowifriends : String = "nav_sowifriends"
        static let k_upcoming : String = "nav_upcoming"
        static let k_nav_notifications : String = "nav_notifications"
        
        
        //Profile Screen.
        static let k_linkedwithFacebook : String = "linkedwithFacebook"
        static let k_linkedwithGooglePlus : String = "linkedwithGooglePlus"
        
        //To gift
        static let k_SoldFrom : String = "soldfrom"
        
        //Notifications.
        
        static let k_fcm_token_key : String = "fcm_token"
        static let k_notif_key : String = "notif_key"
        static let k_product_key : String = "product"
        static let k_sender_name_key : String = "sender_name"
        static let k_receiver_name_key : String = "receiver_name"
        static let k_arrayFriends_key : String = "arrayFriends"
        static let k_arrayFriendsid_key : String = "arrayFriendsid"
        static let k_purchase_accepted : String = "purchase_accepted"
        static let k_purchase_delivered : String = "purchase_delivered"
        static let k_product_recommended : String = "product_recommended"
        
        
        
        
        // Event Management
        static let k_my_events : String = "my_events"
        static let k_friends_events : String = "friends_events"
        
        static let k_is_editable : String = "is_editable"
        static let k_is_notif : String = "is_notif"
        static let k_is_visible : String = "is_visible"
        static let k_is_recursive : String = "is_recursive"
        
        
        //      http://templates2.seasiainfotech.com:9040/Eticketing/Paymentjob/addpayjob
        static let kAppDelegate = UIApplication.shared.delegate
        static let AppWindow:UIWindow = Global.macros.kAppDelegate!.window!!
        static let Storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        static let autoLogin: String = "autoLogin"
        
        static let navigationControllerIdentifier: String = "navigationController"
        static let userInfoVCIdentifier: String = "UserInfoVC"
        static let loginVCIdentifier: String = "LoginVC"
        static let homeVCIdentifier: String = "HomeVC"
        static let setupVCIdentifier: String = "SetupVC"
        static let serviceTicketFormVCIdentifier: String = "ServiceTicketFormVC"
        static let warrantyAndPaymentVCIdentifier: String = "WarAndPayVC"
        static let optionalFeatureVCIdentifier: String = "OptionalFeatureVC"
        static let creditCardVCIdentifier: String = "CreditCardVC"
        static let setupVCToUserInfoVCIdentifier: String = "SetupVCToUserInfoVC"
        static let activeServiceTicketsApi:String  = "Ticket/getusertickets"
        
        //        static let userID: String = "userId"
        static let userDetail: String = "UserDetail"
        static let userServices: String = "UserServices"
        static let companyData: String = "companyData"
        
        static var currentlyloggedInUserID: NSString = ""
        
        
        // Macros
        static var WorkDescriptionUpdated : String = "WorkDescriptionUpdated"
        static var PartsAndMaterialsUpdated : String = "PartsAndMaterialUpdated"
        static var TimeAndClockUpdated : String = "TimeAndClockUpdated"
    }
}


/**
 This is global extension to show toasts rather than Alerts.
 */

extension Global {
    
    func showToast(_ toastMessage:NSString) {
        
        //        JLToast.makeText(toastMessage as String).show()
        //        JLToast.makeText("You can set duration. `JLToastDelay.ShortDelay` means 2 seconds.\n" +
        //            "`JLToastDelay.LongDelay` means 3.5 seconds.", duration: JLToastDelay.LongDelay).show()
        
        //JLToast.makeText(toastMessage as String, delay: 0, duration: 2).show()
    }
}

/**
 Adding extention to use Hex- code colors.
 */


/**
 Adding extention for applying different
 */

extension  UIDevice {
    
    var iPhone: Bool {
        return UIDevice().userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4
        case iPhone5
        case iPhone6
        case iPhone6Plus
        case Unknown
    }
    var screenType: ScreenType? {
        guard iPhone else { return nil }
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        default:
            return nil
        }
    }
}
