//  Constant.swift
//  BidJones
//  Created by Rakesh Kumar on 3/8/18.
//  Copyright © 2018 Seasia. All rights reserved.



import Foundation
import UIKit

let KappDelegate                                     =        UIApplication.shared.delegate as! AppDelegate
let KAlbumName                                       =        "BidJones"
//let kBaseURL                                       =        "http://10.8.11.87:8080/bidjones-admin/api/v1"
//let kBaseURL                                       =        "http://phpdemo.seasiainfotech.com:9040/bidjones/api/v1"
//let kBaseURL                                       =        "https://stgsp1.appsndevs.com/bidjones/api/v1"
let kBaseURL                                         =        "https://bidjones.com/bidjones/api/v1"


let KGoogleApiKey                                    =         "AIzaSyCl182iFS3VtZMU5wIJgJv2TGoePXQe5vY"
    //=  "AIzaSyDfCcAJOo_cqeIYwKggFlv152B5wVr6iso"
//"AIzaSyBfPao82CRhQb_6nLT43x1iRYG0WYN8W7E"

//Cabbies
//"AIzaSyB8EaaTHpOVOB30o0kpbNzEoK4g5wymCA4"
//mexi
//"AIzaSyCsv-KCeAcZlpLudK9Ke9-0BNEnHKRYZuY"
//bidjone
//"AIzaSyBfPao82CRhQb_6nLT43x1iRYG0WYN8W7E"
let KMaxAudio                                        =     17
let KMaxVideo                                        =     1
let KPaginationcount                                 =     10
//MARK: - ProductType
let KBookType                                        =    1
let KGraphicsType                                    =    2
let KMusicType                                       =    3
let KServicesType                                    =    4
let KStuffType                                       =    5
let KvideosType                                      =    6
let KTradeItemType                                   =    7
let KFreeItemType                                   =    8



//MARK: - BidType
let KOpenBid                                          =    1
let KMinimumBid                                       =    2
let KFixedBid                                         =    3
let KmaxDefultImages                                  =    3

//MARK: - Api's
let KLoginApi                                           =        "/user/loginmobile"
let Ktradeacceptrejectrequest                           =        "/trade/acceptrejectrequest"
let KuserchangePassword                                 =        "/user/changePassword"
let KRegisterApi                                        =        "/user/registermobile"
let KAddItemApi                                         =        "/seller/newitem"
let KAddTradeItemApi                                    =        "/trade/newitem"
let Kfreenewitem                                        =        "/free/newitem"

let Ksellerupdateitem                                   =        "/seller/updateitem"
let Ktradeupdateitem                                   =        "/trade/updateitem"
let Kfreeupdateitem                                    =        "/free/updateitem"
let KForgotApi                                          =        "/password/email"
let KnewsFeedApi                                        =        "newsfeed"
let Kuserhelp                                           =        "/user/help"

let KCountryApi                                         =        "/countries"
let KCityApi                                            =        "/cities/"
let KStateApi                                           =        "/states/"
let KverifyUser                                         =        "/verifyUser"
let KGenerApi                                           =        "/seller/listsubcategories/"
let Ksellerdeletemedia                                  =        "/seller/deletemedia"

let KRefreshTokkenApi                                   =        "/user/refreshtoken"
let KsellertrashitemApi                                 =        "/seller/trashitem"
let Ktradetrashitem                                     =        "/trade/trashitem"

let KUploadMedia                                        =        "/seller/uploadmedia/"
//let KItemDetail                                         =        "/seller/item/"
let KItemDetail                                         =        "/itemdetail/"

let KsellerlistitemsApi                                 =        "/seller/listitems"
let Ktrademytradeitems                                  =        "/trade/mytradeitems"
let Kfreemyfreeitems                                 =        "/free/myfreeitems"

let Ktradereceivedrequests                              =        "/trade/receivedrequests"
let Ktradetradelisting                                  =        "/trade/tradelisting"
let Ktradesendrequests                                  =        "/trade/sendrequests"
let Kfreeitemlisting                                    =        "/free/itemlisting"
let Kfreesendrequests                                  =        "/free/sendrequests"
let Kfreerecieverequests                                  =        "/free/receivedrequests"


let KsellermakePayment                                  =        "/seller/makePayment"
let KsellermakePartPayment                              =        "/seller/makePartPayment"
let Ktradeupdatepayment                              =        "/trade/updatepayment"

let KmakeMeSeller                                       =        "/makeMeSeller/"
let KpayForService                                      =        "/seller/payForService"
let KbuyerpaymentRequest                                =        "/buyer/paymentRequest"
let Ksellerratings                                      =        "/sellerratings"
let KsellerpaymentRequest                               =        "/seller/paymentRequest"
let KsellergetCompairRates                              =        "/seller/getCompairRates"
let KsellerpostBid                                      =        "/seller/postBid"
let Kfreebooking                                     =        "/free/booking"

let KsellerbidRecords                                   =        "/seller/bidRecords/"
// MARK: - parameter

let KrateSeller                                     =        "/rateSeller"
let KitemHistory                                    =        "/itemHistory"
let Ksellerallitems                                  =        "/seller/allitems"

let Ksearchitems                                    =        "/searchitems"
let KsearchTradeitems                               =   "/trade/searchitems"
let Kfreesearchitems                                =   "/free/searchitems"
let Kbuyersavesearchpreference                     =        "/buyer/savesearchpreference"
let Kfreebuyersavesearchpreference                 =        "/free/buyer/savesearchpreference"

let KsellerMergePickup                              =        "/seller/MergePickup"
let KsellerupdateBid                                =        "/seller/updateBid"
let Ksellerinfo                                     =        "/seller/info"
let kurlFollowMe                                    =        "/seller/followMe"
let Kresult                                         =        "result"


let Kname                                           =        "name"
let Kbidtypes                                       =        "bidtypes"
let Kmax_images                                     =        "max_images"
let Kid                                             =        "id"
let Kmessage                                        =        "message"
let KshowMessage                                    =        "show_message"
let Ksortname                                       =        "sortname"
let Kdevice_type                                    =        "device_type"
let Kdevice_token                                   =        "device_token"
let Krequest_id                                     =        "request_id"
let Kpassword                                       =        "password"
let Kemail                                          =        "email"
let Kfirst_name                                     =        "first_name"
let Klast_name                                      =        "last_name"
let Kusername                                       =        "username"
let Kis_account                                     =        "account_already"
let Kimage                                          =        "image"
let Kaddress                                        =        "address"
let Kcountry_id                                     =        "country_id"
let Kstate_id                                       =        "state_id"
let Kcity_id                                        =        "city_id"
let Kzip_code                                       =        "zip_code"
let Kis_seller                                      =        "is_seller"
let Kstripe_Id                                      =        "stripe_account_id"
let Kseller_id                                      =        "seller_id"
let Ksender_id                                      =        "sender_id"
let Kotp                                            =        "otp"
let Kbuyer_id                                       =        "buyer_id"
let Kuser_id                                        =        "user_id"
let Kreceiver_id                                    =        "receiver_id"

let Kdescription                                     =        "description"
let Kcategory_id                                     =        "category_id"
let Kalbum_name                                      =        "album_name"
let Kcategory_meta_id                                =        "category_meta_id"
let Kbid_type_id                                     =        "bid_type_id"
let Kduration                                        =        "duration"
let Kmin_bid                                         =        "min_bid"
let Kkeywords                                        =        "keywords"
let Kimages                                          =        "images"
let kpreview_images                                   =        "preview_images"
let KYoucanuploadmaximum                             =        "You can upload maximum"
let Kpdf                                             =        "pdf"
let Kmusic                                           =        "music"
let Kmusic_name                                      =        "music_name"
let Kvideos                                          =        "videos"
let kvideosDiscript                                  =        "videoDescript"
let Kvideo                                           =        "video"
let KSongs                                           =        "Songs"

let Kvideo_thumb                                     =        "video_thumb"
let Kurl                                             =        "url"
let Kauthor                                          =        "author"
let Ksinger                                          =        "singer"
let Ktoc                                             =        "toc"
let Ktotal_pages                                     =        "total_pages"
let Kescrow_service                                  =        "escrow_service"
let Klat                                             =        "lat"
let Klng                                             =        "lng"
let KRATINGSELLER                                    =        "RATE SELLER"
//MARK: - Amazon Detail
//for BIdjoens
//let KAWSaccessKey                                  =        "AKIAJJNDTANX7JZ5NFPA"
//let KAWSsecretKey                                  =        "L1jzpBfqzIUtdJt8ktRP6x5MZHM3Cx3v"
//for Hotfuse
let KAWSaccessKey                                   =        "AKIAIQIOMV2DMEAU2XXA"
let KAWSsecretKey                                   =      "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"

//MARK:- Image Names
let Kselected                                       =        "selected"
let Kunselected                                     =        "unselected"
let Kback_arrow                                     =        "back_arrow"

// MARK: - String
let Kstatus                                         =        "status"
let Kredirect                                       =          "redirect_to_stripe"
let KMessage                                        =        "BidJones"
let KBIDJONES                                       =        "BIDJONES"
let KNext                                           =         "Next"
let KErroroccuredwhileuploadingPleasetryagain       = "Error occured while uploading. Please try again."
let KsuccessDownloadImage                           =   "Successfully downloaded Image and saved into Gallery"

let KError                                            =        "Server error"
let KPleaseentercardnumber                            =        "Please enter card number"
let KPleaseenteramount                                =        "Please enter amount"
let KPleaseenterexpiredate                            =        "Please enter expiration date"
let KPleaseentercvv                                   =        "Please enter cvv"
let KPleaseentervalidcardnumber                       =        "Please enter valid card number"
let KPleaseentervalidexpirationdate                   =        "Please enter valid expiration date"
let Pleaseentervalidcvv                               =        "Please enter valid cvv"
let PleaseentervalidMonth                             =        "Please enter valid expiration month"
let PleaseentervalidYear                              =        "Please enter valid expiration year"
let PleaseentervalidMonthandYear                      =        "Please enter valid expiration month and year"





let KPleasefillFirstname                            =        "Please enter first name"
let KPleaseentersearchkeyword                       =        "Please enter search keyword"
let KPleaseselectcategory                           =        "Please select category"

let KPleaseentersongtitle                           =        "Please enter title"
let KPleaseentertitle                               =        "Please enter title"

let KPleaseenterBidAmount                           =        "Please enter Bid Amount"
let KPleaseenterTradeAmount                            =        "Please enter Trade Amount"
let KPleaseenterLocation                           =        "Please enter pickup location"

let KPleaseenternumberofPages                       =        "Please enter number of Pages"
let KPleaseselectbooktype                           =        "Please select book type"

let KPleaseenteralbumname                           =        "Please enter album name"
let KPleaseenterartistname                           =        "Please enter Artist name"

let KPleaseenterduration                            =        "Please enter duration"
let KPleaseentercorrectdurationime                            =        "Please enter correct duration time"
let KPleaseenterkeysearchword                       =        "Please enter key search word"
let KPleaseenterTCservice                           =        "Please enter T&C service"
let KPleaseselectGenre                              =        "Please select genre"
let KPleaseselectservicetype                        =        "Please select service type"
let KPleaseenterservicetype                        =        "Please enter service type"


let KPleaseselectatleastoneimage                 =        "Please select at least one image"
let KPleaseselectatleastoneaudio                 =        "Please select at least one audio"
let KPleaseselectVideo                           =        "Please select Video"
let KPleaseselectPdf                             =        "Please select Pdf"
let KPleaseentersongname                         =        "Please enter song name"
let KPleaseenterlocation                         =        "Please enter location"

let KPleaseenterBidType                             =        "Please enter Bid Type"
let KPleasefillLastname                             =        "Please enter last name"
let KPleaseenterdescrption                          =        "Please enter description"
let KPleasefillStatename                            =        "Please select state"
let KPleaseselectTerms                            =        "Please accept Terms and Conditions"
let KPleasefillCountryname                          =        "Please select country"
let KPleasefillCityname                             =        "Please select city"
let KPleasefillEmailAddress                         =        "Please enter email address"
let KPleaseenterOTP                                 =        "Please enter OTP"
let KEmail                                          =        "Email"
let KPassword                                       =        "Password"
let KComingSoon                                     =        "Coming soon"
let KDoyoureallywanttologout                        =        "Do you really want to logout?"
let KEmailaddressalreadyexist                       =        "Email address already exist"
let KUserregisteredsuccessfully                     =        "User registered successfully"
let KOTPSentToMail                                  =           "We sent a link to your registered email to connect your stripe account with BidJones to receive payments."
let KEmailaddressdoesnotexist                       =        "Email address does not exist"
let KPasswordhasbeensenttoyouremailaddresssuccessfully  =    "Password has been sent to your email address successfully"
let KInvalidemailaddressorpassword                  =         "Invalid email address or password"


let KPleasefillUsername                             =        "Please enter username"
let KPleasefillpassword                             =        "Please enter password"
let KPleaseenteroldpassword                         =        "Please enter old password"
let Koldpassworddoesnotmatch                        =        "Old password does not match"
let KPleaseenternewpassword                         =        "Please enter new password"
let KPleaseenterconfirmpassword                     =        "Please enter confirm password"

let KEnvalidemailaddress                            =        "Please enter valid email address"
let KPleaseentervalidbookurl                        =        "Please enter valid book url"

let KPleasefillPostalCode                           =        "Please enter postal code"
let KUnknownerror                                   =        "Unknown error"
let KNoInternetConnection                           =        "No internet connection"
let KLoading                                        =         "Loading..."
let KUploading                                      =         "Uploading..."
let KDownloading                                    =         "Downloading..."
let KDone                                           =         "Done"
let KCancel                                         =         "Cancel"
let KUsernameshouldbeof                             =         "Please enter minimum 2 characters of username"
let KFirstnameshouldbeof                            =         "Please enter minimum 2 characters of first name"
let KLastNameShouldbeof                             =         "Please enter minimum 2 characters of last name"
let KTitleShouldbeof                                =         "Please enter minimum 6 characters of title"
let KAddressShouldbeof                              =         "Please enter minimum 6 characters of address"
let KPasswordshouldbeof                             =         "Please enter minimum 8 characters of password"
let KPostalCodeshouldbeof                           =         "Please enter minimum 4 characters of postal code"
let KPleaseenteratleastonecaptialletter             =         "Please enter at least one numeric , one special character and one capital letter"
let KPleaseAddImg                                   =         "Please add profile image."
let KPasswordVerification                           =       "Please enter password for verification."
let KMismatchPassword                               =         "Please add same password for verification."
let KPleasefilladdreass                             =         "Please fill address."
let KDeviceType                                     =         "1"
let KDummyDeviceTokken                              =         "xvfdsxfvdshfudshfkjdskfhkjdshfhdsfkdsjfdsfdsfdsfsfdsf"
let KOpenSettingForPhotos                           =         "BidJones does not have access to your photos. To enable access, tap Settings and turn on Photos."
let KOpenSettingForCamera                           =         "BidJones does not have access to your camera. To enable access, tap Settings and turn on Camera."
let KOK                                             =         "OK"
let KLOGIN                                          =         "LOGIN"
let KpaymenthasBeenalreadyDone                      =         "You are unable to edit the payment as first payment has already been done."

let KChooseImage                                    =         "Choose Image"
let KChooseVideo                                    =         "Choose Video"
let KCamera                                         =         "Camera"
let KGallery                                        =         "Gallery"
let KYoudonthavecamera                              =         "You don't have camera"
let KSettings                                       =         "Settings"
let KMyItems                                        =         "My Items"
let KMyProducts                                     =         "My Products"

let KZero                                           =         "0"
let KBase64Prefix                                   =         "data:image/png;base64,"
let Kadd                                            =         "add"

let Ktitle                                          =         "title"
//let Kstatus                                         =         "status"

let KAlerts                                         =         "Alerts"
let KAlertsTrade                                         =         "My Search Alerts"
let KTradeAlerts                                    =         "Trade Alerts"

let KMyRequests                                     =         "My Requests"

let KHelp                                           =         "Help"
let KAboutUs                                        =         "About Us"
let KLogout                                         =         "Logout"
let Kicon                                           =         "icon"
let Ksetting_menu                                   =         "setting_menu"
let Kalert_menu                                     =         "alert_menu"
let Khelp_menu                                      =         "help_menu"
let Kabt_us_menu                                    =         "abt_us_menu"
let Klogout                                         =         "logout"

let KSellServices                                   =         "My Services"
let KMyStuff                                        =         "My Stuff"
let KMyMusic                                        =         "My Music"
let KMyBooks                                        =         "My Books"
let KMyVideos                                       =         "My Videos"
let KMyGraphics                                     =         "My Graphics"
let KTradeItems                                     =         "My Trade Items"
let KFreeItems                                     =          "My Free Items"
let Ksell_ic                                        =         "sell_ic"
let Kstuff_ic                                       =         "stuff_ic"
let Kmusic_ic                                       =         "music_ic"
let Kbook_ic                                        =         "book_ic"
let Kvideo_ic                                       =         "video_ic"
let Kgraphics_ic                                    =         "graphics_ic"
let KSETTINGS                                       =         "SETTINGS"

let KMyProfile                                      =         "My Profile"
let Kmy_profile_ic                                  =         "my_profile_ic"
let KStuffSales                                     =         "Stuff Sales"
let KServicesSales                                  =         "Services Sales"
let Kservices_ic                                    =         "services_ic"
let KMusicSales                                     =         "Music Sales"
let KBookSales                                      =         "Book Sales"
let KVideoSales                                     =         "Video Sales"
let Kcreated_at                                     =         "created_at"
let Kpayment_platform_id                            =         "payment_platform_id"
let Kpayment_platform_type_id                       =         "Kpayment_platform_type_id"
let Kaccess_token                                   =         "access_token"
let Krefresh_token                                  =         "refresh_token"
let Ktoken_type                                     =         "token_type"
let KProductTableCell                               =         "ProductTableCell"



//MARK: - UItableViewCell Idetifier

let KcellMenu                                       =         "cellMenu"
let Kcell                                           =         "cell"
let KproductTableCell                               =         "productTableCell"
let KsongListTableCell                              =         "songListTableCell"
let KcompareCell                                    =         "compareCell"

//MARK: - UICollectionViewCell Idetifier

let KImagesCollectionCell                           =         "imagesCollectionCell"
let KvideosCollectionCell                           =         "videosCollectionCell"
let KvideosDiscriptCollectionCell                  =         "videosDiscriptCollectionCell"
let KmusicCollectionCell                            =         "musicCollectionCell"
let KpreviewCollectionCell                          =         "previewCollectionCell"
let KpreviewTableCell                               =         "previewTableCell"

//MARK: - storyBoard Identifiers
let KTermsNewVC                                     =          "TermsNewVC"
let KWebViewVC                                      =          "WebViewVC"

let KTermsVC                                        =         "TermsVC"
let KRegisterVC                                     =         "RegisterVC"
let KLoginVC                                        =         "LoginVC"
let KForgotVC                                       =         "ForgotVC"
let KverifyOTPVC                                    =         "verifyOTPVC"
let KaddMusicVC                                     =         "addMusicVC"
let KaddGraphicsVC                                  =         "addGraphicsVC"
let KaddBookVC                                      =         "addBookVC"
let KaddListingVC                                   =         "addListingVC"
let KaddSellServiceVC                               =         "addSellServiceVC"
let KmyBookSListVC                                  =         "myBookSListVC"
let KmySongListVC                                   =         "mySongListVC"
let KmyStuffListVC                                  =         "myStuffListVC"
let KmYGraphicLIstVC                                =         "mYGraphicLIstVC"
let KmYVideoListVC                                  =         "myVideoListVC"
let KaddVideoVC                                     =         "addVideoVC"
let KmyServiceSellListVC                            =         "myServiceSellListVC"
let KpreviewVC                                      =         "previewVC"
let KsuccessVC                                      =         "successVC"
let KdetailVC                                       =         "detailVC"
let KsearchVC                                       =         "searchVC"
let kfreeVC                                         =         "FreeVC"
let KsearchListVC                                   =         "searchListVC"
let KaudioPlayerVC                                  =         "audioPlayerVC"
let KpdfVC                                          =         "pdfVC"
let KcompareVC                                      =         "compareVC"
let KcheckOutVC                                     =         "checkOutVC"
let KinVoiceVC                                      =         "inVoiceVC"
let KpaymentVC                                      =         "paymentVC"
//let KpaymentVC                                    =         "paymentVC"
let KpaymentRequestSellerVC                         =         "paymentRequestSellerVC"
let KpaymentRequestBuyerVC                          =         "paymentRequestBuyerVC"
let KRatingListVC                                   =         "ratingListVC"
let KsellerInfoVC                                   =         "sellerInfoVC"
let KmerchPickUp                                    =         "merchPickUp"
let KchangePriceVC                                  =         "changePriceVC"
let KalertsVC                                       =         "alertsVC"
let KratingVC                                       =         "ratingVC"
let kserviceVC                                      =         "ServiceVC"
let kmyProfileVC                                    =         "MyProfileVC"
let kmySaleVC                                       =          "MySaleVC"
let kstuffSalesVC                                   =          "StuffSalesVC"
let kserviceSalesVC                                 =          "ServiceSalesVC"
let kmusicSalesVC                                   =          "MusicSalesVC"
let kbookSalesVC                                    =          "BookSalesVC"
let kvideoSalesVC                                   =          "VideoSalesVC"
let kmysearchAlertVC                                =          "MySearchAlertVC"
let kTalkVC                                         =          "TalkVC"
let kTradeVC                                        =          "TradeVC"
let kTradeDetailVC                                  =          "TradeDetailVC"
//let kFreeDetailVC                                  =          "FreeDetailVC"


let kTradeListVC                                    =          "TradeListVC"
let kAddTradeProductVC                              =          "AddTradeProductVC"
let kAddFreeProductVC                               =           "AddFreeProductVC"



//MARK: - RootView With Navigations Identifier
let KLoginNavigation                                =         "LoginNavigation"
let KHomeNavigation                                 =         "HomeNavigation"
let KRatingNavigation                               =         "RatingNavigation"
let kChatRoomNavigation                             =         "ChatRoomNavigation"

//MARK: - storyBoard


let KHome                                           =         "Home"
let KAuth                                           =         "Auth"
let KMain                                           =         "Main"



//Mark: Singleton Class objects

let KCommonFunctions                                =  CommonFunctions.sharedInstance


let KACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"


