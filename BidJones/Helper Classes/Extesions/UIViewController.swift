//
//  UIViewController.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/22/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import MobileCoreServices


//MARK: - UIViewController Extnesion
extension UIViewController
{
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
    }
     func sessionLogout() {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: KMessage, message: KDoyoureallywanttologout, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: KLogout, style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                NSLog("OK Pressed")
                btnShowMenu.tag = 0
                self.loggingOut()
                //gagan
                UserDefaults.standard.setStripeExist(value: false)
                
                UserDefaults.standard.setLoggedIn(value: false)
                UserDefaults.standard.setRefreshTokken(value: nil)
                UserDefaults.standard.setAccessTokken(value: nil)
                
                //userd// Bool
                KCommonFunctions.SetRootViewController(rootVC:.LoginNavigation)
            }
            let cancelAction = UIAlertAction(title: KCancel, style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    func loggingOut(){
        let id = UserDefaults.standard
              let loginUserID = id.getUserID()
              let isDevicetype = 1 as! NSNumber
              //NSNumber(1)
              
              let url = "/user/logout"
              let parm = ["is_mobile" : isDevicetype] as! [String : Any]
              
              Logout.sharedManager.PostApi(url: url, parameter: parm, Target: self , completionResponse: {
                  (response) in
                  
                  print("my Logout result : \(response)")
                  
                  
              }, completionnilResponse: { (Response) in
                  //print(Response)
                  let statusCode = Response[Kstatus] as! Int
                  if statusCode == 500
                  {
                      KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                          //print(resonse)
                          //   self.GetRecieveBidList()
                          // self.getAlertData()
                          self.loggingOut()
                      })
                  }
                  else if statusCode == 201
                  {
                      
                  }
                  
                  
                  
                  
                  
              }, completionError: { (error) in
                  self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                  
              }, networkError: {(error) in
                  self.showAlertMessage(titleStr: KMessage, messageStr: error)
                  
              })
              
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    //MARK: - GallaryCamera Actionc
    
    func OpenGallaryCamera(pickerController : UIImagePickerController) {
        let alert = UIAlertController(title: KChooseImage, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: KCamera, style: .default, handler: { _ in
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                //  Open Camera
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                    pickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    pickerController.sourceType = UIImagePickerControllerSourceType.camera
                    pickerController.allowsEditing = true
                    pickerController.mediaTypes = [kUTTypeImage as String]
                    
                    //  pickerController.mediaTypes = [kUTTypeMovie as String]
                    //   UIImagePickerController.availableMediaTypes(for:.camera)!;
                    self.present(pickerController, animated: true, completion: nil)
                }
                else {
                    self.showAlertMessage(titleStr: Kmessage, messageStr: KYoudonthavecamera)
                }
                //////////
            } else
            {
                // Open setting alert for camera
                let alert = UIAlertController(title: KOpenSettingForCamera , message: "", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: KCancel, style: .default))
                alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                })
                self.present(alert, animated: true)
                ////////
            }
        }))
        alert.addAction(UIAlertAction(title: KGallery, style: .default, handler: { _ in
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                // Open Gallary
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                    pickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
                    pickerController.mediaTypes = [kUTTypeImage as String]
                    pickerController.allowsEditing = true
                    self.present(pickerController, animated: true, completion: nil)
                    /////////
                }
            case .denied, .restricted:
                // Open setting Alert for galllary
                // Open setting alert for camera
                let alert = UIAlertController(title: KOpenSettingForPhotos , message: "", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: KCancel, style: .default))
                alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                })
                self.present(alert, animated: true)
            //////////
            case .notDetermined:
                break
            default:
                break
            }
        }))
        alert.addAction(UIAlertAction.init(title: KCancel, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func OpenGallaryCameraForVideo(pickerController : UIImagePickerController)
    {
        let alert = UIAlertController(title: KChooseImage, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: KCamera, style: .default, handler: { _ in
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                //  Open Camera
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                    pickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    pickerController.sourceType = UIImagePickerControllerSourceType.camera
                   // pickerController.allowsEditing = true
                    pickerController.videoMaximumDuration = 30
                    pickerController.videoQuality = UIImagePickerControllerQualityType.typeMedium
                    pickerController.allowsEditing = false
                    pickerController.mediaTypes = [kUTTypeMovie as String]
                    //   UIImagePickerController.availableMediaTypes(for:.camera)!;
                    self.present(pickerController, animated: true, completion: nil)
                }
                else {
                    self.showAlertMessage(titleStr: Kmessage, messageStr: KYoudonthavecamera)
                }
                //////////
            } else
            {
                // Open setting alert for camera
                let alert = UIAlertController(title: KOpenSettingForCamera , message: "", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: KCancel, style: .default))
                alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                })
                self.present(alert, animated: true)
                ////////
            }
        }))
        alert.addAction(UIAlertAction(title: KGallery, style: .default, handler: { _ in
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                // Open Gallary
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                    pickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
                    pickerController.allowsEditing = true
                    pickerController.videoMaximumDuration = 30
                    pickerController.videoQuality = UIImagePickerControllerQualityType.typeMedium
                    //pickerController.allowsEditing = true
                    pickerController.mediaTypes = [kUTTypeMovie as String]
                    //   UIImagePickerController.availableMediaTypes(for:.camera)!;
                    self.present(pickerController, animated: true, completion: nil)
                }
            case .denied, .restricted:
                // Open setting Alert for galllary
                // Open setting alert for camera
                let alert = UIAlertController(title: KOpenSettingForPhotos , message: "", preferredStyle: UIAlertControllerStyle.alert)

                alert.addAction(UIAlertAction(title: KCancel, style: .default))
                alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                })
                self.present(alert, animated: true)
            //////////
            case .notDetermined:
                break
            default:
                break
            }
        }))
        alert.addAction(UIAlertAction.init(title: KCancel, style: .cancel, handler: nil))

        
        self.present(alert, animated: true, completion: nil)
    }
    
    func OpenVideoCamera(pickerController : UIImagePickerController)
    {
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                //  Open Camera
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                    pickerController.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                    pickerController.sourceType = UIImagePickerControllerSourceType.camera
                    pickerController.allowsEditing = true
                    pickerController.videoMaximumDuration = 30
                    pickerController.videoQuality = UIImagePickerControllerQualityType.typeMedium
                     pickerController.allowsEditing = false
                    pickerController.mediaTypes = [kUTTypeMovie as String]
                    //   UIImagePickerController.availableMediaTypes(for:.camera)!;
                    self.present(pickerController, animated: true, completion: nil)
                }
                else {
                    self.showAlertMessage(titleStr: Kmessage, messageStr: KYoudonthavecamera)
                }
                //////////
            } else
            {
                // Open setting alert for camera
                let alert = UIAlertController(title: KOpenSettingForCamera , message: "", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: KCancel, style: .default))
                alert.addAction(UIAlertAction(title: KSettings, style: .cancel) { (alert) -> Void in
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                })
                self.present(alert, animated: true)
                ////////
            }
    }
    
   
    //MARK: - Alert and Toast
    
    func AlertMessageWithOkAction(titleStr:String, messageStr:String,Target : UIViewController, completionResponse:@escaping () -> Void) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            completionResponse()
        }
        // Add the actions
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func AlertMessageWithOkCancelAction(titleStr:String, messageStr:String,Target : UIViewController, completionResponse:@escaping (String) -> Void) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            completionResponse("Yes")
        }
        let CancelAction = UIAlertAction(title: KCancel, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            completionResponse("No")
        }
        // Add the actions
        alert.addAction(okAction)
        alert.addAction(CancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessage(titleStr:String, messageStr:String) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        // Add the actions
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    func showToast(message : String)
    {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-100, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 8.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        KappDelegate.window?.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //MARK: - Screen width
    
    func ViewHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
    //MARK: - Screen Height
    
    func ViewWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    //MARK: - Internet Check
    func checkInternetConnection() -> Bool {
        // To check internet connection.
        var isInternetActive: Bool!
        var internetConnectionReach: Reachability!
        
        internetConnectionReach = Reachability.reachabilityForInternetConnection()
        
        var netStatus: Reachability.NetworkStatus!
        
        netStatus = internetConnectionReach.currentReachabilityStatus
        
        if(netStatus == Reachability.NetworkStatus.notReachable) {
            isInternetActive = false;
            return isInternetActive
        }
        else {
            isInternetActive = true
            return isInternetActive
        }
    }
    
    //MARK: - Navigatin after Alert
    
    func AlertWithNavigatonPurpose(message: String, navigationType:NavigationType, ViewController:ViewControllers,rootViewController:RootViewControllers,Data:Any?)
    {
        let alertController = UIAlertController(title: KMessage, message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            switch navigationType {
            case .push:
                KCommonFunctions.PushToContrller(from: self, ToController: ViewController, Data: Data)
            case .present:
           KCommonFunctions.PresentTocontroller(from: self, ToController: ViewController
                 )
            case .pop:
            KCommonFunctions.popTocontroller(from: self)
            case .root:
            KCommonFunctions.SetRootViewController(rootVC:rootViewController)
            }
        }
         self.dismiss(animated: true, completion: nil)
        //        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
        //            UIAlertAction in
        //            NSLog("Cancel Pressed")
        //        }
        
        // Add the actions
        alertController.addAction(okAction)
        //alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
        
    //MARK:-  Get Index of object from Array of dictionry with key name
    
    func GetIndexFromArrayForString(arr:[Any], str:String, key:String, type:ArrayType) -> Int?
    {
       // let k = arr as Array
       // print(k)
        switch type {
            
            
            
        case .AddressData:
            let index = arr.index {
                if let dic = $0 as? AddressData{
                    if let value = dic.name as? String, value == str{
                        print(value)
                        return true
                    }
                }
                return false
            }
            
            
            
            
            if let value = index
            {
                print(value)
                return value
            }
        case .GenreData:
            let index = arr.index {
                if let dic = $0 as? GenreData{
                    if let value = dic.name as? String, value == str{
                        return true
                    }
                }
                return false
            }
            if let value = index
            {
                print(value)
                return value
            }
        case .BidTypeData:
            let index = arr.index {
                if let dic = $0 as? BidTypeData{
                    if let value = dic.name as? String, value == str{
                        return true
                    }
                }
                return false
            }
            if let value = index
            {
                print(value)
                return value
            }
        
        case .CategoryData:
        let index = arr.index {
            if let dic = $0 as? CategoryData{
                if let value = dic.name as? String, value == str{
                    return true
                }
            }
            return false
        }
        if let value = index
        {
            print(value)
            return value
        }
        }
            return nil
    }
    
    func GetIndexFromArrayForInt(arr:[Any], num:Int, key:String, type:ArrayType) -> Int?
    {
        switch type {
        case .AddressData:
            let index = arr.index {
                if let dic = $0 as? AddressData{
                    if let value = dic.id as? Int, value == num{
                        return true
                    }
                }
                return false
            }
            if let value = index
            {
                print(value)
                return value
            }
            else
            {
                return nil
            }
        case .GenreData:
            let index = arr.index {
                if let dic = $0 as? GenreData{
                    if let value = dic.id as? Int, value == num{
                        return true
                    }
                }
                return false
            }
            if let value = index
            {
                print(value)
                return value
            }
            else
            {
                return nil
            }
        case .BidTypeData:
            let index = arr.index {
                if let dic = $0 as? BidTypeData{
                    if let value = dic.id as? Int, value == num{
                        return true
                    }
                }
                return false
            }
            if let value = index
            {
                print(value)
                return value
            }
            else
            {
                return nil
            }
        case .CategoryData:
            let index = arr.index {
                if let dic = $0 as? CategoryData{
                    if let value = dic.id as? Int, value == num{
                        return true
                    }
                }
                return false
            }
            if let value = index
            {
                print(value)
                return value
            }
            else
            {
                return nil
            }
        }
    }
    
    //MARK: - ThumbNail From Video Url
    
    func getThumbnailImage(forUrl url: URL) -> UIImage?
    {
        guard let videoTrack = AVAsset(url: url).tracks(withMediaType: AVMediaType.video).first else {
            return nil
        }
        
        let transformedVideoSize = videoTrack.naturalSize.applying(videoTrack.preferredTransform)
        let videoIsPortrait = abs(transformedVideoSize.width) < abs(transformedVideoSize.height)
        print("Mode : \(videoIsPortrait)")
//        let asset: AVAsset = AVAsset(url: url)
//        let imageGenerator = AVAssetImageGenerator(asset: asset)
//
//        do {
//            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
//            if videoIsPortrait
//            {
//            return UIImage(cgImage: thumbnailImage).rotate(radians: .pi/2)
//            }
//            else
//            {
//              return UIImage(cgImage: thumbnailImage)
//            }
//        } catch let error {
//            print(error)
//        }
        let asset:AVAsset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform = true

        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
//                        if videoIsPortrait
//                        {
//                        return UIImage(cgImage: imageRef).rotate(radians: .pi/2)
//                        }
//                        else
//                        {
                          return UIImage(cgImage: imageRef)
                       // }
           // return nil
        }
        catch
        {
           return nil
        }
    }
}
