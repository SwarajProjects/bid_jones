//
//  Enums.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/20/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


enum ValidationError: Error {
    case emptyUsername
    case emptyPassword
    case emptyOldPassword
    case wrongOldPassword
    case emptyNewPassword
    case emptyConfirmPassword
    case emptyNewPasswordDoesntMatch
    case invalidEmail
    case emptyFirstName
    case emptyLastName
    case emptyAddress
    case emptyCountry
    case emptyState
    case emptyCity
    case emptyPostalCode
    case emptyImg
    case emptyEmailAddress
    case emptyMessage
    case emptyOTPAddress
    case firstNameMinChar
    case lastNameMinChar
    case userNameMinChar
    case postalCodeMinChar
    case passwordMinChar
    case passwordSufficientComplexity
    case addressMinChar
    case emptyTitle
    case emptyArtistName
    case minTitleLength
    case emptyDescription
    case invalidBookUrl
    case emptyBidAmount
    case emptyBidType
    case emptyLocation
    case emptyNumOfpage
    case emptyGenre
    case emptyName
    case emptyDuration
    case incorrectDuration
    case emptyKeySearchWord
    case emptyTCService
    case emptyPdf
    case emptyImages
    case emptyAudios
    case emptyVideos
    case emptyCardNum
    case emptyCvv
    case emptyExpireDate
    case incorrectCardNum
    case incorrectCvv
    case incorrectExpireDate
    case incorrectExpireMonthAndYear
    case incorrectExpireMonth
    case incorrectExpireYear
    case emptyAmount
    case MismatchPassword
    case enterVerifyPassword
}

enum NetworkingError: Error {
case nullData
}


enum ArrayType
{
    case AddressData
    case GenreData
    case BidTypeData
    case CategoryData



}




