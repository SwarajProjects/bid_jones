//
//  AamzonServices.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/10/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import UIKit
import AWSS3

//Bucket name :   hotfuse
//Access key ID  :- AKIAIQIOMV2DMEAU2XXA
//Secret access key :- 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
//Region: us-east-1

enum AWSDataType
{
    case Image
    case Video
    case Song
    case Pdf
    case Graphic
}


struct Bucket {
    struct bucketInfo
    {
        static let name = "hotfuse1"
        //static let poolId = "us-east-1:046ccf92-0458-4e8f-835a-708ffcb88039"
    }
    static let transferUtility = AWSS3TransferUtility.default()
}


class AmazonServices
{
    let uploadExpression = AWSS3TransferUtilityUploadExpression()
    let downloadExpression = AWSS3TransferUtilityDownloadExpression()
    static let sharedInstance = AmazonServices()
    private init()
    {
    }
     typealias RequestHandler = (_ error:Error?, _ url:String?)->(Void)

    
    func UploadDataPdf(awsType:AWSDataType,url:NSURL?,completionHandler:@escaping RequestHandler,Target : UIViewController,networkError: @escaping (String) -> Void)  {
        ////print(url ?? "")
        ////print(awsType)
        print(url ?? "")
        if let URL = url
        {
            //        let amazonS3Manager = AmazonS3RequestManager(bucket: bucketName,
            //                                                     region: .USStandard,
            //                                                     accessKey: "AKIAIQIOMV2DMEAU2XXA",
            //                                                     secret: "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")
            //
            //
            //                if let path = Bundle.main.path(forResource: "IMG_4715", ofType: "MOV") {
            //                    ////print(path)
            //                    let fileUrl = NSURL(fileURLWithPath: path)
            //       // let fileURL: NSURL = NSURL(fileURLWithPath: "pathToMyObject")
            //
            //                   let uploadRequest = amazonS3Manager.upload(from: fileUrl as URL, to: "FirstVideo.MOV")
            //        }
            if Target.checkInternetConnection()
            {
                
                let uploadRequest = AWSS3TransferManagerUploadRequest()!
                let S3BucketName = Bucket.bucketInfo.name
                let randomvalue = "\(Double(Date.timeIntervalSinceReferenceDate * 1000))"
                var remoteName = "Bidjones/Rakesh/\(randomvalue)"
                uploadRequest.body = URL as URL
                uploadRequest.bucket = S3BucketName
                
                switch awsType {
                case .Image:
                    remoteName = remoteName + ".jpg"
                    uploadRequest.contentType = "Image/jpg"
                case .Song:
                    remoteName = remoteName + ".mp3"
                    uploadRequest.contentType = "Audio/mp3"
                case .Video:
                    remoteName = remoteName + ".mp4"
                    uploadRequest.contentType = "Video/mp4"
                case .Pdf:
                    remoteName = remoteName + ".pdf"
                    uploadRequest.contentType = "Doc/pdf"
                case .Graphic:
                    remoteName = remoteName + ".jpg"
                    uploadRequest.contentType = "Image/jpg"
                default:
                    print("nil")
                }
                //print(remoteName)
                uploadRequest.key = remoteName
                uploadRequest.acl = .publicRead
                uploadRequest.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
                    DispatchQueue.main.async(execute: {
                        //  self.amountUploaded = totalBytesSent // To show the updating data status in label.
                        // self.fileSize = totalBytesExpectedToSend
                        print("\(totalBytesSent)/\(totalBytesExpectedToSend)")
                    })
                }
                //  AWSS3UploadPartOutput
                
                let transferManager = AWSS3TransferManager.default()
                uploadExpression.progressBlock = {(task, progress) in DispatchQueue.main.async(execute: {
                    // Do something e.g. Update a progress bar.
                    //print(progress)
                })
                }
                transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
                    DispatchQueue.main.async {
                        //self?.uploadButton.isHidden = false
                        // self?.activityIndicator.stopAnimating()
                    }
                    
                    if let error = task.error {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionHandler(error,nil)
                        
                        print("Upload failed with error: (\(error.localizedDescription))")
                    }
                    
                    if task.result != nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                        if let absoluteString = publicURL?.absoluteString {
                            print("Uploaded to:\(absoluteString)")
                            completionHandler(nil,"\(absoluteString)")
                        }
                    }
                    
                    return nil
                }
            }
            else
            {
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                networkError(KNoInternetConnection)
            }
        }
    }
    func UploadData(awsType:AWSDataType,url:URL?,completionHandler:@escaping RequestHandler,Target : UIViewController,networkError: @escaping (String) -> Void)  {
        ////print(url ?? "")
        ////print(awsType)
        print(url ?? "")
        if let URL = url
        {
        //        let amazonS3Manager = AmazonS3RequestManager(bucket: bucketName,
        //                                                     region: .USStandard,
        //                                                     accessKey: "AKIAIQIOMV2DMEAU2XXA",
        //                                                     secret: "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")
        //
        //
        //                if let path = Bundle.main.path(forResource: "IMG_4715", ofType: "MOV") {
        //                    ////print(path)
        //                    let fileUrl = NSURL(fileURLWithPath: path)
        //       // let fileURL: NSURL = NSURL(fileURLWithPath: "pathToMyObject")
        //
        //                   let uploadRequest = amazonS3Manager.upload(from: fileUrl as URL, to: "FirstVideo.MOV")
        //        }
            if Target.checkInternetConnection()
            {
           
            let uploadRequest = AWSS3TransferManagerUploadRequest()!
            let S3BucketName = Bucket.bucketInfo.name
            let randomvalue = "\(Double(Date.timeIntervalSinceReferenceDate * 1000))"
            var remoteName = "Bidjones/Rakesh/\(randomvalue)"
            uploadRequest.body = URL as URL
            uploadRequest.bucket = S3BucketName

            switch awsType {
            case .Image:
                remoteName = remoteName + ".jpg"
                uploadRequest.contentType = "Image/jpg"
            case .Song:
                remoteName = remoteName + ".mp3"
                uploadRequest.contentType = "Audio/mp3"
            case .Video:
                remoteName = remoteName + ".mp4"
                uploadRequest.contentType = "Video/mp4"
            case .Pdf:
                remoteName = remoteName + ".pdf"
                uploadRequest.contentType = "Doc/pdf"
            case .Graphic:
                remoteName = remoteName + ".jpg"
                uploadRequest.contentType = "Image/jpg"
            default:
                print("nil")
            }
         //print(remoteName)
            uploadRequest.key = remoteName
            uploadRequest.acl = .publicRead
            uploadRequest.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
                DispatchQueue.main.async(execute: {
                    //  self.amountUploaded = totalBytesSent // To show the updating data status in label.
                    // self.fileSize = totalBytesExpectedToSend
                    print("\(totalBytesSent)/\(totalBytesExpectedToSend)")
                })
            }
            //  AWSS3UploadPartOutput
            
            let transferManager = AWSS3TransferManager.default()
            uploadExpression.progressBlock = {(task, progress) in DispatchQueue.main.async(execute: {
                // Do something e.g. Update a progress bar.
                //print(progress)
            })
            }
            transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
                DispatchQueue.main.async {
                    //self?.uploadButton.isHidden = false
                    // self?.activityIndicator.stopAnimating()
                }

                if let error = task.error {
                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    completionHandler(error,nil)
                    print("Upload failed with error: (\(error.localizedDescription))")
                }
                
                if task.result != nil {
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                    if let absoluteString = publicURL?.absoluteString {
                        print("Uploaded to:\(absoluteString)")
                        completionHandler(nil,"\(absoluteString)")
                    }
                }
                return nil
            }
        }
            else
            {
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                networkError(KNoInternetConnection)
            }
    }
    
        //////////////////////////////////
        //         let image = UIImage.init(named:"imgToUpload")
        //         let data:Data = UIImagePNGRepresentation(image!)!
        //        uploadExpression.progressBlock = {(task, progress) in DispatchQueue.main.async(execute: {
        //            // Do something e.g. Update a progress bar.
        //            //print(progress)
        //        })
        //        }
        //       var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        //        completionHandler = { (task, error) -> Void in
        //            DispatchQueue.main.async(execute: {
        //                //print(error ?? "")
        //                AWSDDLog.sharedInstance.logLevel = .verbose
        //                //print(task)
        //            })
        //        }
        //        Bucket.transferUtility.uploadData(data,
        //                                   bucket: bucketName,
        //                                   key: "images/sixthImg.jpg",
        //                                   contentType: "image/jpg",
        //                                   expression: uploadExpression,
        //                                   completionHandler: completionHandler).continueWith { (task) -> AnyObject! in
        //                                    if let error = task.error {
        //                                        //print("Error: \(error.localizedDescription)")
        //                                    }
        //                                    if let _ = task.result {
        //                                    }
        //                                    return nil;
        //        }
    }
    func Uploadfile()
    {
//       // //print(bucketName)
//        if let path = Bundle.main.path(forResource: "firstVideo", ofType: "3gp") {
//            //print(path)
//            let fileUrl = NSURL(fileURLWithPath: path)
//            uploadExpression.progressBlock = {(task, progress) in DispatchQueue.main.async(execute: {
//                //print(progress)
//            })
//            }
//            var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
//            completionHandler = { (task, error) -> Void in
//                DispatchQueue.main.async(execute: {
//                    //print(error ?? "")
//                    //print(task)
//                })
//            }
//            Bucket.transferUtility.uploadFile(fileUrl as URL, bucket: Bucket.bucketInfo.name, key: "firstVideo.3gp", contentType: "video/3gp", expression: uploadExpression, completionHandler: completionHandler
//                ).continueWith {
//                    (task) -> AnyObject! in if let error = task.error {
//                        //print("Error: \(error.localizedDescription)")
//                    }
//                    if let _ = task.result
//                    {
//                    }
//                    return nil;
//            }
//        }
    }
    
    //MARK:- Download
    func downloadData()
    {
//        downloadExpression.progressBlock = {(task, progress) in DispatchQueue.main.async(execute: {
//            // Do something e.g. Update a progress bar.
//            //print(progress)
//        })
//        }
//        var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
//        completionHandler = { (task, url, data, error) -> Void in
//            DispatchQueue.main.async(execute: {
//                //print(data?.count ?? "")
//                let image : UIImage = UIImage(data: data!)!
//                //print(image)
//                //print(url ?? "")
//            })
//        }
//        Bucket.transferUtility.downloadData(fromBucket: Bucket.bucketInfo.name, key: "secondImg.jpg", expression: downloadExpression, completionHandler: completionHandler).continueWith { (task) -> AnyObject! in
//            if let error = task.error {
//                //print("Error: \(error.localizedDescription)")
//            }
//            if let data = task.result {
//                //print(data)
//            }
//            return nil;
//        }
//    }
}
}

