//
//  MusicListData.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/27/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct MusicListData {

    var imageSong : String?
    var songTilte : String?
    var audioSong : String?
    var id : Int?
    
    
    
    init?(dict:[String:Any]) {

        print(dict)
        if let id = dict["id"] as? Int {
            
            self.id = id
            
            
        }
        

        if let songTitle1 = dict["mediaName"] as? String {
            
            print("your song media name : \(songTitle1)")
            // if songTitle1 != nil {
            self.songTilte = songTitle1
            //  }
        }
        
        
        if let metaData = dict["meta"] as? [String : Any] {
            print("This is id : \(metaData)")

            if  let image1 = metaData["images"] as? [String] {
                self.imageSong = "\(image1[0])"
            }
            
           
            
            if let audioSong1 = metaData["music"] as? [String] {
                
                self.audioSong = audioSong1[0]
                
            }

        }

    }
    
}
