//
//  VideoData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/9/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct VideoData {
    static var VideosArr = [VideoData]()
    var videoUrl:URL?
    var image:UIImage?
    var fileName:String?

    
    init?(VideoUrl:URL?,Image:UIImage?)
    {
        guard let VideoUrl = VideoUrl else
        {
            return nil
        }
        videoUrl = VideoUrl
        image = Image
    }
}
