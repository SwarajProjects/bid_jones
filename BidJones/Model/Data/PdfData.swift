//
//  ImagesData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/4/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct PdfData {
    var pdfUrl:URL
    var name:String?
    var fileName:String?

    init?(PdfUrl:URL?,Name:String?)
    {
        guard let url = PdfUrl else
        {
            return nil
        }
        pdfUrl = url as URL
        name = Name
    }
}

