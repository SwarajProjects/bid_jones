//
//  MerchPickupData.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct MerchPickupData {
    
   // var image : String?
    
    var titleName : String?
    var id : Int?
    var amount : String?
    var buyerID : Int?
    var bid_id  : Int?
    var image : String?
    
    
    
    init(dict: [String : Any ]) {
        
        
        if let id = dict["id"] as? Int {
            
            self.id = id
        }
        
        
        
        if let amount = dict["amount"] as? String {
            
            print("is my amount : \(amount)")
            self.amount = amount
            
        }
        
        
        if   let item1 = dict["item"] as? [String : Any] {
            
            
            
            if let image1 = item1["imgName"] as? String {
                
                self.image = image1
                
                
            }
            
            
        if let titleName = item1["title"] as? String {
            
            self.titleName = titleName
            
        }
        
        if let id = item1["id"] as? Int {
            
            self.bid_id = id
        }
            
            if let buyerId = item1["buyer_id"] as? Int {
                
                
                self.buyerID = buyerId
                
            }
        
        

}

}
}
