//
//  SellerInfoData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 8/10/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

class SellerInfoData {
    
var buttonText:String?
var currentlySelling:String?
var followers:Int?
var following:Int?
var sellerImage:String?
var sellerName:String?
var totalSales:Int?


 init?(dict:[String:Any]) {
    
    if let ButtonText = dict["buttonText"] as? String
    {
        buttonText = ButtonText
    }
    if let CurrentlySelling = dict["currentlySelling"] as? String
    {
        currentlySelling = CurrentlySelling
    }
    if let Followers = dict["followers"] as? Int
    {
        followers = Followers
    }
    if let Following = dict["following"] as? Int
    {
        following = Following
    }
    if let SellerImage = dict["sellerImage"] as? String
    {
        sellerImage = SellerImage
    }
    if let SellerName = dict["sellerName"] as? String
    {
        sellerName = SellerName
    }
    if let TotalSales = dict["totSales"] as? Int
    {
        totalSales = TotalSales
    }
}
}
