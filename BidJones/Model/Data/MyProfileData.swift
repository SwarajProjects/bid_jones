//
//  MyProfileData.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/26/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


class MyProfileData {
    
  
    var firstName     : String?
    var lastName      : String?
    var username      : String?
    var address       : String?
    var country       : String?
    var state         : String?
    var city          : String?
    var postalCode    : String?
    var emailAddress  : String?
    var emailPassword : String?
    var profileImage  : String?
    var id            : Int?
    var countryID     : Int?
    var stateID       : Int?
    var cityID        : Int?
    var stripe_account_id : String?
  
    
    init?(dict:[String:Any]) {

        
        if let id = dict["id"] as? Int {
            print("This is id : \(id)")
            self.id = id
            
        }

       
        if let firstname = dict["first_name"] as? String
        {
            
            self.firstName = firstname
        }
        if let lastname = dict["last_name"] as? String
        {
            
            self.lastName  = lastname
        }
        
        
        if let username = dict["username"] as? String
        {
            
            self.username  = username
        }
        
        if let address = dict["address"] as? String
        {
            
            self.address  = address
        }
       
        if let country = dict["country_name"] as? String
        {
            
            self.country  = country
        }
        
        if let state  = dict["state_name"] as? String
        {
            
            self.state  = state
        }
        
        if let city = dict["city_name"] as? String {
            
            self.city = city
            
            
        }
        
        if let postalCode = dict["zip_code"] as? String {
            
            self.postalCode = postalCode
            
            
        }
        

        if let emailAddress = dict["email"] as? String {
            
            self.emailAddress = emailAddress
            
            
        }
        
        
        if let profileImage = dict["image"] as? String {
            
            self.profileImage = profileImage
            
            
        }
        
        if let countryID = dict["country_id"] as? Int {
            
            self.countryID = countryID
            
            
        }
        print(dict["stripe_account_id"])
        if let stripe_account_id = dict["stripe_account_id"] as? Any
        {
            print("stripe_account_id",stripe_account_id)
            self.stripe_account_id = "\(stripe_account_id)"
        }
        
        if let stateID = dict["state_id"] as? Int {
            print("your state id is : \(stateID)")
            self.stateID = stateID
            
        }

        if let cityID = dict["city_id"] as? Int {
          //  if cityID != nil {
            self.cityID = cityID
         //   }
            
        }

}


}
