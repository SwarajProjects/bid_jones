//
//  CategoryData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/21/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct CategoryData
{
    var id:Int
    var name:String
    init(ID:Int, Name: String)
    {
        id = ID
        name = Name
    }
}
