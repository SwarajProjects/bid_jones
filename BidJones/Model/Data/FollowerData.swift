//
//  FollowerData.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/15/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct FollowerData {

    var image : String?
    
    var name : String?
    
    
    
    

    init(dict: [String : Any ]) {
        
        if let name = dict["name"] as? String {
            
            self.name = name
            
        }
 
        if let image = dict["image"] as? String {
            
            self.image = image
            
        }
        
        
        
    }
    
    
    
}
