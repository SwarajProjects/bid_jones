//
//  SearchData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/21/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

class SearchData
{
    var keyword:String?
    var radius:String?
    var unit:String?
    var category:String?
    var categoryID:Int?
    var subCategoryID:Int?
    var is_escrow:Bool?
    init(KeyWord: String?, Radius: String?, Unit: String?, Category: String?, CategoryID: Int?, SubCategoryID: Int?,IS_Escrow:Bool?)
     {
        keyword = KeyWord
        radius = Radius
        unit = Unit
        category = Category
        categoryID = CategoryID
        subCategoryID = SubCategoryID
        is_escrow = IS_Escrow
     }
}

