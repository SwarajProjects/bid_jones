//
//  TalkData.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct TalkData {
    
    var index : Int?
    var userId : Int?
    var username : String?
    var userImage : String?
    var itemName : String?
    var imgName : String?
    var itemID : Int?
    
    
    
    init(dict : [String : Any]){
        
        
        if let index = dict["index"] as? Int {
            self.index = index
            
        }
        
        
        if let userId = dict["userId"] as? Int {
            self.userId = userId
   
        }
        
        if let username = dict["username"] as? String {
            self.username = username
          }
        
        if let imageName = dict["userimage"] as? String {
            
            self.userImage = imageName
            
        }
        
        if let itemName = dict["itemname"] as? String {
            
            
            self.itemName = itemName
            
        }
        
        if let imgName = dict["imgName"] as? String {
            
            self.imgName = imgName
            
        }
        
        if let itemID = dict["itemid"] as? Int {
            
           print(itemID)
            self.itemID = itemID
            
        }
        
        
        
    }
    
    
    
}
