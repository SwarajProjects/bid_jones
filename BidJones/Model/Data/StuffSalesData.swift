//
//  StuffSalesData.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct StuffSalesData {


    var titleName : String?
    var description : String?
    var imageSale : String?
    
    
    init( dict : [String : Any ]) {

        
        if let titleName = dict["title"] as? String {
            
            
            self.titleName = titleName
            
            
        }
        
        if let description = dict["description"] as? String {
            
            self.description = description
            
        }
        
        if let meta = dict["meta"] as? [String: Any] {
           
            if let image = meta["images"] as? [String] {
                print("image6 :\(image) ")
                self.imageSale = "\(image[0])"
            }
            
        }
        
        
    }
    
    

}


