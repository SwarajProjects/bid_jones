 //
//  ItemListData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct TradeRequestListData
{
  var id:Int?
  var status:Int?
  var sender_id:Int?
  var trade_item_id:Int?
  var item_id:Int?
  var reciver_id:Int?
 // var sender_id:Int?
  var amount:Double?
  var createdDate:String?
  var updated_at:String?
  var index:Int?
  var myProduct:Int?
  var isHistory:Int?
  var tradeAmount:Double?
  var paymentComplete:Bool?
  var tradeAmountPaid:Double?
  var freeSent:Int?
  var freeRecieve:Int?
  var FreeHistory:Int?
  var FreeMyItemHistory:Int?

 


  var sender_detail:TradeSenderDetailData?
  var reciever_detail:TradeSenderDetailData?
  var item_detail:ProductData?
  var trade_item_detail:ProductData?
   var free_item_detail:ProductData?

  
  
  
  init?(dict:[String:Any]) {
    print(dict)
    guard let Status = dict[Kstatus], let Sender_id = dict[Ksender_id], let Id = dict[Kid]  else
    {
      return nil
    }
    status = Status as? Int
    sender_id = Sender_id as? Int
    id = Id as? Int
    
    if let TradeItemID = dict["trade_item_id"] as? Int
    {
      trade_item_id = TradeItemID
    }
    if let ItemID = dict["item_id"] as? Int
    {
      item_id = ItemID
    }
    if let Receiver_id = dict["receiver_id"] as? Int
    {
      reciver_id = Receiver_id
    }
   // print(dict["amount"]!)


    if let TradePaidAmount = dict["amount"] as? String
    {
      tradeAmountPaid =   Double(TradePaidAmount)
      if(tradeAmountPaid! > 0.0)
      {
        paymentComplete = true
      }
    }
    if let Created_at = dict["created_at"] as? String
    {
        createdDate = Created_at

        createdDate = "\(Created_at)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let Date = dateFormatter.date(from: self.createdDate!)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: Date!)
        createdDate = strDate
      
    }
//    if let Updated_at = dict["updated_at"] as? String
//    {
//      updated_at = Updated_at
//    }
    if let Updated_at = dict["updated_at"] as? String
    {
     // Updated_at = Updated_at
      
      updated_at = "\(Updated_at)"
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
      let Date = dateFormatter.date(from: self.updated_at!)
      dateFormatter.dateFormat = "dd-MM-yyyy"
      let strDate = dateFormatter.string(from: Date!)
      updated_at = strDate
      
    }
    if let item = dict["item_detail"] as? [String:Any]
    {
      if let Item =  ProductData(json: item)
    {
      item_detail = Item
    }
    }
    if let Amount = dict["amount"] as? Double
    {
      amount = Amount
    }
    
    if let item = dict["trade_item_detail"] as? [String:Any]
    {
      if let Item =  ProductData(json: item)
      {
        trade_item_detail = Item
      }
    }
    if let item = dict["free_item_detail"] as? [String:Any]
    {
        if let Item =  ProductData(json: item)
        {
            free_item_detail = Item
        }
    }
    if let item = dict["sender_detail"] as? [String:Any]
    {
      if let Item =  TradeSenderDetailData(dict: item)
      {
        sender_detail = Item
      }
    }
    if let item = dict["reciever_detail"] as? [String:Any]
    {
      if let Item =  TradeSenderDetailData(dict: item)
      {
        reciever_detail = Item
      }
    }
  }
}

struct TradeSenderDetailData
{
  var first_name:String?
  var id:Int?
  var image:String?
  var last_name:String?
  init?(dict:[String:Any]) {
    print(dict)
    guard let FirstName = dict[Kfirst_name], let LastName = dict[Klast_name], let Id = dict[Kid], let Image = dict["image"] else
    {
      return nil
    }
    first_name = FirstName as? String
    id = Id as? Int
    last_name = LastName as? String
    image = Image as? String

}
}

