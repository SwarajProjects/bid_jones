//
//  Search.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/21/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import Alamofire

class Trade
{
  static let sharedManager = Trade()
  private init()
  {
  }
  func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([ItemListData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
  {
    if Target.checkInternetConnection()
    {
      MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
      let urlComplete = kBaseURL+url
      print(urlComplete)
      print(parameter)
      var accessTokken = ""
      if let str = UserDefaults.standard.getAccessTokken()
      {
        accessTokken = str
      }
      ////print("Kaccess_token : \(accessTokken)")
      let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
      
      Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
        .responseJSON { response in
          print(response.timeline)
          print(response.result)
          print(response.error ?? "Blank Error")
          print(response.debugDescription)
          print(response.description)
          print(response.data ?? "Blank Data")
          print(response.request ?? "Blank Request")
          print(response.value ?? "Blank value")
          if response.result.isSuccess
          {
            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            guard let data = response.value as? [String:Any] else {
              completionError(response.error)
              return
            }
            let statusCode = data[Kstatus] as! Int
            if statusCode == 200
            {
              print("respones of yes no : \(data)")
              guard let array = data[Kresult] as? [[String :Any]]else{
                MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                completionnilResponse(data)
                return
              }
              var arrItemListData = [ItemListData]()
              for item in array{
                guard let Item =  ItemListData(dict: item)else{continue}
                //arrItemListData.insert(Item, at: 0)
                arrItemListData.append(Item)
                
                //print(arrItemListData)
              }
              MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
              completionResponse(arrItemListData)
            }else if statusCode == 500{
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                Target.sessionLogout()
            }
            else
            {
              completionnilResponse(data)
            }
          }
          else
          {
            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            completionError(response.error)
            return
          }
      }
      
    }
    else
    {
      MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
      networkError(KNoInternetConnection)
    }
  }
  func PostApiForTradeRequestList(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([TradeRequestListData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
  {
    if Target.checkInternetConnection()
    {
      MBProgressHUD.hide(for: KappDelegate.window, animated: true)
      MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
      let urlComplete = kBaseURL+url
      print(urlComplete)
      print(parameter)
      var accessTokken = ""
      if let str = UserDefaults.standard.getAccessTokken()
      {
        accessTokken = str
      }
      ////print("Kaccess_token : \(accessTokken)")
      let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
      
      let configuration = URLSessionConfiguration.default
      configuration.timeoutIntervalForRequest = 1200
      let alamoManager = Alamofire.SessionManager(configuration: configuration)
      
      alamoManager.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
        .responseJSON { response in
          print(response.timeline)
          print(response.result)
          print(response.error ?? "Blank Error")
          print(response.debugDescription)
          print(response.description)
          print(response.data ?? "Blank Data")
          print(response.request ?? "Blank Request")
          print(response.value ?? "Blank value")
          if response.result.isSuccess
          {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            guard let data = response.value as? [String:Any] else {
              completionError(response.error)
              return
            }
            let statusCode = data[Kstatus] as! Int
            if statusCode == 200
            {
              guard let array = data[Kresult] as? [[String :Any]]else{
                return
              }
              var arrItemListData = [TradeRequestListData]()
              for item in array{
                guard let Item =  TradeRequestListData(dict: item)else{continue}
                //arrItemListData.insert(Item, at: 0)
                arrItemListData.append(Item)
                //print(arrItemListData)
              }
              completionResponse(arrItemListData)
            }
            else
            {
              completionnilResponse(data)
            }
          }
          else
          {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            completionError(response.error)
            return
          }
          alamoManager.session.invalidateAndCancel()
      }
      
    }
    else
    {
      networkError(KNoInternetConnection)
    }
  }
  
  func GetApi(url : String,Target : UIViewController, completionResponse:  @escaping (_ genre:[GenreData],_ BidType:[BidTypeData
    ],_ maxImageSelection:Int) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
  {
    if Target.checkInternetConnection()
    {
      MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
      let urlComplete = kBaseURL+url
      print("URL : \(urlComplete)")
      
      var accessTokken = ""
      if let str = UserDefaults.standard.getAccessTokken()
      {
        accessTokken = str
      }
      print("Kaccess_token : \(accessTokken)")
      let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
      Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
        .responseJSON { response in
          print(response.timeline)
          print(response.result)
          print(response.error ?? "Blank Error")
          print(response.debugDescription)
          print(response.description)
          print(response.data ?? "Blank Data")
          print(response.request ?? "Blank Request")
          print(response.value ?? "Blank value")
          if response.result.isSuccess
          {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            guard let data = response.value as? [String:Any] else {
              completionError(response.error)
              return
            }
            let statusCode = data[Kstatus] as! Int
            if statusCode == 200
            {
              var genreList = [GenreData]()
              var bidTypeList = [BidTypeData]()
              var maxImages = KmaxDefultImages
              
              if let genreArray = data[Kresult] as? [[String :Any]]{
                for item in genreArray{
                  guard let country =  GenreData(dict: item)else{continue}
                  genreList.append(country)
                }
              }
              if let bidTypeArray = data[Kbidtypes] as? [[String :Any]]{
                for item in bidTypeArray{
                  guard let country =  BidTypeData(dict: item)else{continue}
                  bidTypeList.append(country)
                }
              }
              if let MaxImages = data[Kmax_images] as? Int{
                maxImages = MaxImages
              }
              completionResponse(genreList,bidTypeList, maxImages)
            }else if statusCode == 500{
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                Target.sessionLogout()
            }
            else
            {
              completionnilResponse(data)
            }
          }
          else
          {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            completionError(response.error)
            return
          }
      }
    }
    else
    {
      networkError(KNoInternetConnection)
    }
  }
  func PostApiForAR(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([String:Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
  {
    if Target.checkInternetConnection()
    {
      MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
      let urlComplete = kBaseURL+url
      print(urlComplete)
      print(parameter)
      var accessTokken = ""
      if let str = UserDefaults.standard.getAccessTokken()
      {
        accessTokken = str
      }
      ////print("Kaccess_token : \(accessTokken)")
      let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
      
      Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
        .responseJSON { response in
          print(response.timeline)
          print(response.result)
          print(response.error ?? "Blank Error")
          print(response.debugDescription)
          print(response.description)
          print(response.data ?? "Blank Data")
          print(response.request ?? "Blank Request")
          print(response.value ?? "Blank value")
          if response.result.isSuccess
          {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            guard let data = response.value else{return}
            let responseData  = data as! [String : Any]
            let statusCode = responseData["status"] as! Int
            //print(statusCode)
            if statusCode == 200
            {
              completionResponse(responseData)
            }else if statusCode == 500{
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                Target.sessionLogout()
            }
            else
            {
              completionnilResponse(responseData)
            }
          }
          else
          {
            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            completionError(response.error)
            return
          }
      }
      
    }
    else
    {
      MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
      networkError(KNoInternetConnection)
    }
  }
}

