//
//  MySale.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/1/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import Alamofire

class MySale {
    
    static let sharedManager = MySale()
    
    private init()
    {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([MySaleData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            //print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    // print(response.description)
                    //   print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(status)")
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        
                        guard let data1 = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        
                        if statusCode == 200
                        {
                            DispatchQueue.main.async {
                              
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            }
                            print("here is your :\(response)")
                            
                            
                            if let array = data1[Kresult] as? [[String :Any]] {
                                
                                var arrItemListData = [MySaleData]()
                                for item in array {
                                    //   guard let Item =  MusicListData(dict: item) else{continue}
                                    let Item = MySaleData(dict: item)
                                    //arrItemListData.insert(Item, at: 0)
                                    arrItemListData.append(Item)
                                    //print(arrItemListData)
                                }
                                
                                //  MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                                completionResponse(arrItemListData)
                                
                                
                            }
                            else {
                                var arrItemListData = [MySaleData]()
                                
                                completionResponse(arrItemListData)
                            }
                           
                            
                            
                            
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionnilResponse(responseData)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
            
        }
        else
        {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    
    
    
    func GetApi(url : String,Target : UIViewController, completionResponse:  @escaping ([MySaleGenreData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            //  MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print("URL : \(urlComplete)")
            
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        print("here is status code : \(statusCode)")
                        if statusCode == 200
                        {
                            print("you get response here : \(response)")
                            // let userID = data[]
                            
//                            guard let array = data[Kresult] as? [String: Any] else {
//
//                                return
//                            }
                            let array = data[Kresult] as? [String: Any]
                            var dataModel = [MySaleGenreData]()
                            print("This is data here:\(array)")

                            let getData = response.value as! [String : Any]
                            
                            print("your data1 : \(getData)")
                            let array1 = getData["result"] as! [[String : Any]]
                            
                            print("your array1 : \(array1)")
                            
                            for item in array1 {
                            
                            
                                let model = MySaleGenreData(dict: item as! [String : Any])

                                                            dataModel.append(model)
                            
                            
                            
                                                        }
                        
                            print("this is my data : \(dataModel)")
                        
                            completionResponse(dataModel)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            networkError(KNoInternetConnection)
        }
    }
    
}
