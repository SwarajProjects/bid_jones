//
//  Service.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/25/18.
//  Copyright © 2018 Seasia. All rights reserved.
//



import Foundation
import Alamofire


class Service
{
    static let sharedManager = Service()
    private init()
    {
}

    
    func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([ServiceModel]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            //print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(status)")
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        
                        guard let data1 = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        
                        if statusCode == 200
                        {
                            
                            print("here is your :\(response)")
                            
                            
                            guard let array = data1[Kresult] as? [[String :Any]]else{
                                return
                            }
                            var arrItemListData = [ServiceModel]()
                            for item in array{
                                guard let Item =  ServiceModel(dict: item)else{continue}
                                //arrItemListData.insert(Item, at: 0)
                                arrItemListData.append(Item)
                                //print(arrItemListData)
                            }
                             MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionResponse(arrItemListData)
                            
                           
                            
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionnilResponse(responseData)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
            
        }
        else
        {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    
    
    


    func GetApi(url : String,Target : UIViewController, completionResponse:  @escaping (_ genre:[GenreData],_ BidType:[BidTypeData
        ],_ maxImageSelection:Int) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print("URL : \(urlComplete)")
            
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        print("here is status code : \(statusCode)")
                        if statusCode == 200
                        {
                            var genreList = [GenreData]()
                            var bidTypeList = [BidTypeData]()
                            var maxImages = KmaxDefultImages
                            
                            print("here is your output : \(data)")

                            if let genreArray = data[Kresult] as? [[String :Any]]{
                                for item in genreArray{
                                    guard let country =  GenreData(dict: item)else{continue}
                                    genreList.append(country)
                                }
                            }
                            if let bidTypeArray = data[Kbidtypes] as? [[String :Any]]{
                                for item in bidTypeArray{
                                    guard let country =  BidTypeData(dict: item)else{continue}
                                    bidTypeList.append(country)
                                }
                            }
                            if let MaxImages = data[Kmax_images] as? Int{
                                maxImages = MaxImages
                            }
                            completionResponse(genreList,bidTypeList, maxImages)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            networkError(KNoInternetConnection)
        }
}
}
