//
//  CountryResponse.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/30/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct CountryResponse {
    var countries:[CountryData]
    init(json:Any) throws {
        guard let array = json as? [[String:Any]] else {
            throw NetworkingError.nullData
        }
        var countries = [CountryData]()
        for item in array{
            
            guard let country =  CountryData(dict: item)else{continue}
            countries.append(country)
        }
       self.countries = countries
    }
    
}
