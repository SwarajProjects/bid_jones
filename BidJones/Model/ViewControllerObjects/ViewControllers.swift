//
//  ViewControllers.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/26/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


enum NavigationType
{
    case root
    case push
    case present
    case pop
}


enum ViewControllers
{
    case TermsNew
    case Terms
    case Login 
    case Register
    case Forgot
    case Home
    case VerifyOTP
    case AddMusic
    case AddBook
    case AddStuff
    case AddSellService
    case AddGaraphics
    case AddVideos
    case MYBookSList
    case MySongList
    case MyStuffList
    case MYServiceSellList
    case MyGraphicsList
    case MyVideoList
    case Preview
    case Success
    case Detail
    //case FreeDetail
    case Search
    case Free
    case SearchList
    case AudioPlayer
    case Pdf
    case compare
    case CheckOut
    case Invoice
    case Payment
    case Rating
    case PaymentRequestSeller
    case PaymentRequestBuyer
    case RatingList
    case SellerInfo
    case MerchPickUp
    case ChangePrice
    case Alerts
    case ServiceVC
    case MyProfile
    case MySale
    case StuffSales
    case ServiceSales
    case MusicSales
    case BookSales
    case VideoSales
    case MySearchAlert
    case Talk
    case Trade
    case TradeList
    case AddTradeProduct
    case TradeDetail
    case AddFreeProduct
    case WebView
    case MyTradeItemsVC
    case MyFreeItemsVC


    case none
    
        var viewcontroller: UIViewController?{
        switch self {
        case .MyFreeItemsVC:
            return StoryBoards.Free.instantiateViewController(withIdentifier: "MyFreeItemsVC")
        case .MyTradeItemsVC:
            return StoryBoards.Trade.instantiateViewController(withIdentifier: "MyTradeItemsVC")
            
        case .TermsNew:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KTermsNewVC)
        case .WebView:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KWebViewVC)
        case .Terms:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KTermsVC)
        case .Login:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KLoginVC)
        case .Register:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KRegisterVC)
        case .Forgot:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KForgotVC)
        case .Home:
            return StoryBoards.Home.instantiateViewController(withIdentifier: KHome)
        case .VerifyOTP:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KverifyOTPVC)
        case .AddMusic:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KaddMusicVC)
        case .AddBook:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KaddBookVC)
        case .AddStuff:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KaddListingVC)
        case .AddSellService:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KaddSellServiceVC)
        case .AddGaraphics:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KaddGraphicsVC)
        case .AddVideos:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KaddVideoVC)
        case .MYBookSList:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KmyBookSListVC)
        case .MySongList:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KmySongListVC)
        case .MyStuffList:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KmyStuffListVC)
        case .MYServiceSellList:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KmyServiceSellListVC)
        case .MyGraphicsList:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KmYGraphicLIstVC)
        case .MyVideoList:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KmYVideoListVC)
        case .Preview:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KpreviewVC)
        case .Success:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KsuccessVC)
        case .Detail:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KdetailVC)
        case .Search:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KsearchVC)
        case .Free:
            return StoryBoards.Free.instantiateViewController(withIdentifier: kfreeVC)
        case .SearchList:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KsearchListVC)
        case .AudioPlayer:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KaudioPlayerVC)
        case .Pdf:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KpdfVC)
        case .compare:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KcompareVC)
        case .CheckOut:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KcheckOutVC)
        case .Invoice:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KinVoiceVC)
        case .Payment:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KpaymentVC)
        case .Rating:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KratingVC)
        case .PaymentRequestSeller:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KpaymentRequestSellerVC)
        case .PaymentRequestBuyer:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KpaymentRequestBuyerVC)
        case .RatingList:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KRatingListVC)
        case .SellerInfo:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KsellerInfoVC)
        case .MerchPickUp:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KmerchPickUp)
        case .ChangePrice:
            return StoryBoards.Seller.instantiateViewController(withIdentifier: KchangePriceVC)
        case .Alerts:
            return StoryBoards.Home.instantiateViewController(withIdentifier: KalertsVC)
            
        case .ServiceVC:
            return StoryBoards.Home.instantiateViewController(withIdentifier: kserviceVC)
            
        case .MyProfile:
            return StoryBoards.Home.instantiateViewController(withIdentifier: kmyProfileVC)
            
        case .MySale:
            return StoryBoards.Home.instantiateViewController(withIdentifier: kmySaleVC)
            
        case .StuffSales:
            return StoryBoards.Home.instantiateViewController(withIdentifier:  kstuffSalesVC)
            
        case .ServiceSales :
            return StoryBoards.Home.instantiateViewController(withIdentifier: kserviceSalesVC)
            
            
        case .MusicSales :
            return StoryBoards.Home.instantiateViewController(withIdentifier: kmusicSalesVC)
            
            
        case .BookSales :
            return StoryBoards.Home.instantiateViewController(withIdentifier: kbookSalesVC)
            
        case .VideoSales :
            return StoryBoards.Home.instantiateViewController(withIdentifier: kvideoSalesVC)
            
        case .MySearchAlert :
            return StoryBoards.Home.instantiateViewController(withIdentifier: kmysearchAlertVC)
            
        case .Talk  :
            return StoryBoards.Home.instantiateViewController(withIdentifier: kTalkVC)
        case .Trade  :
          return StoryBoards.Trade.instantiateViewController(withIdentifier: kTradeVC)
        case .TradeList  :
          return StoryBoards.Trade.instantiateViewController(withIdentifier: kTradeVC)
        case .AddTradeProduct  :
          return StoryBoards.Trade.instantiateViewController(withIdentifier: kAddTradeProductVC)
        case .AddFreeProduct  :
            return StoryBoards.Free.instantiateViewController(withIdentifier:kAddFreeProductVC )
        case .TradeDetail  :
          return StoryBoards.Trade.instantiateViewController(withIdentifier: kTradeDetailVC)
//        case .FreeDetail  :
//            return StoryBoards.Free.instantiateViewController(withIdentifier: kFreeDetailVC)
            
              case .none:
        return nil
        }
            
    }
}
enum RootViewControllers
{
    case LoginNavigation
    case HomeNavigation
    case RatingNavigation
    case ChatRoomNavigation

    case none

    var rootViewcontroller: UIViewController?{
        switch self {
        case .LoginNavigation:
            return StoryBoards.Auth.instantiateViewController(withIdentifier: KLoginNavigation)
        case .HomeNavigation:
            return StoryBoards.Home.instantiateViewController(withIdentifier: KHomeNavigation)
        case .RatingNavigation:
            return StoryBoards.Buyer.instantiateViewController(withIdentifier: KRatingNavigation)
        
        case .ChatRoomNavigation:
            return StoryBoards.Home.instantiateViewController(withIdentifier: kChatRoomNavigation)
            
        case .none:
            return nil
        }
    }
}

struct StoryBoards {
    
    static var Main : UIStoryboard
    {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    static var Auth : UIStoryboard
    {
        return UIStoryboard(name: "Auth", bundle: nil)
    }
    static var Home : UIStoryboard
    {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    static var Seller : UIStoryboard
    {
        return UIStoryboard(name: "Seller", bundle: nil)
    }
    static var Buyer : UIStoryboard
    {
        return UIStoryboard(name: "Buyer", bundle: nil)
    }
   static var Trade : UIStoryboard
  {
    return UIStoryboard(name: "Trade", bundle: nil)
  }
    static var Free : UIStoryboard
    {
        return UIStoryboard(name: "Free", bundle: nil)
    
    }
}
